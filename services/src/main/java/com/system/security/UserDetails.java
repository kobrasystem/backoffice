package com.system.security;

import com.system.base.Setup;
import java.util.Collection;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author imamsolikhin
 */
@Data
public class UserDetails extends User {

  private Setup setup;

  public UserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
    super(username, password, authorities);
  }

}
