package com.system.exception;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author imamsolikhin
 */
public class LogsFilter extends AbstractMatcherFilter {

  @Override
  public FilterReply decide(Object event) {
    if (!isStarted()) {
      return FilterReply.NEUTRAL;
    }

    LoggingEvent loggingEvent = (LoggingEvent) event;
    List<Level> eventsToKeep = Arrays.asList(Level.ERROR, Level.INFO, Level.DEBUG);
    if (eventsToKeep.contains(loggingEvent.getLevel())
            && !loggingEvent.getThreadName().equals("restartedMain")
            && (!loggingEvent.getThreadName().endsWith("housekeeper") && !loggingEvent.getThreadName().endsWith("connection adder"))) {
      return FilterReply.NEUTRAL;
    } else {
      return FilterReply.DENY;
    }
  }

}
