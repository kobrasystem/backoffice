package com.system.base;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author imamsolikhin
 */
@Getter @Setter
public class Register {

  private String fullname;
  private String username;
  private String password;
  private String repassword;
}
