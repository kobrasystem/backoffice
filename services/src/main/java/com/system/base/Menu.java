package com.system.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author imamsolikhin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Menu {

  @JsonInclude(Include.NON_NULL)
  private String code;

  @JsonInclude(Include.NON_NULL)
  private String type;

  @JsonInclude(Include.NON_NULL)
  private String label;

  @JsonInclude(Include.NON_NULL)
  private String icon;

  @JsonInclude(Include.NON_NULL)
  private String to;

  @Getter(onMethod = @__(
          @JsonIgnore))
  private String link = "";

  @JsonInclude(Include.NON_EMPTY)
  private List<Menu> items = new ArrayList();
}
