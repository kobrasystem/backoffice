package com.system.base;


import lombok.Data;

@Data
public class Setup {

  private String code = "";
  private String branchCode = "";
  private String branchName = "";
  private String branchPhone = "";
  private String branchFax = "";
  private String branchAddress = "";
  private String divisionCode = "";
  private String divisionName = "";
  private String companyName = "";
  private String companyAcronym = "";
  private String logoPath = "";
  private String webTitle = "";
  private String currencyCode = "";
  private String currencyName = "";
  private String roleCode = "";
  private String roleName = "";
  private String fullName = "";
  private String username = "";
  private byte activeStatus = 0;
  private byte superUser = 0;
  private String error = "";
  private String message = "";
}
