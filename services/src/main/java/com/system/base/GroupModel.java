package com.system.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Data;

/**
 *
 * @author imamsolikhin
 */
@Data
@MappedSuperclass
public abstract class GroupModel extends BaseModel{

  @Column(name = "branchCode")
  private String branchCode = "";

  @Column(name = "divisionCode")
  private String divisionCode = "";
}
