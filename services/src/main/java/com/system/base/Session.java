/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.base;

import com.system.security.UserDetails;
import com.system.exception.CustomException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 *
 * @author imamsolikhin
 */
@Component
@Service
public class Session {

  @PersistenceContext
  public EntityManager em;

  public String FAILED = "FAILED";
  public String SUCCESS = "SUCCESS";

  public String INSERT = "INSERT";
  public String UPDATE = "UPDATE";
  public String DELETE = "DELETE";

  public Query query;
  public org.hibernate.Session session;
  public Transaction transaction;

  public static UserDetails userdetail() {
    try {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      if (authentication == null) {
        return null;
      }
      if (authentication.getDetails() == null) {
        return null;
      }
      UserDetails detail = (UserDetails) authentication.getDetails();
      return detail;
    } catch (Exception e) {
      throw new CustomException("Token is not valid", HttpStatus.FORBIDDEN);
    }
  }

  public static Setup setup() {
    try {
      if (userdetail() == null) {
        return null;
      }
      return userdetail().getSetup();
    } catch (Exception e) {
      throw new CustomException("Token is not valid", HttpStatus.FORBIDDEN);
    }
  }

  public static String username() {
    try {
      if (userdetail() == null) {
        return null;
      }
      return userdetail().getSetup().getUsername();
    } catch (Exception e) {
      throw new CustomException("Token is not valid", HttpStatus.FORBIDDEN);
    }
  }

  public static String branchCode() {
    try {
      if (userdetail() == null) {
        return null;
      }
      return userdetail().getSetup().getBranchCode();
    } catch (Exception e) {
      throw new CustomException("Token is not valid", HttpStatus.FORBIDDEN);
    }
  }

  public static String divisionCode() {
    try {
      if (userdetail() == null) {
        return null;
      }
      return userdetail().getSetup().getDivisionCode();
    } catch (Exception e) {
      throw new CustomException("Token is not valid", HttpStatus.FORBIDDEN);
    }
  }

  public static boolean superUserCode() {
    try {
      if (userdetail() == null) {
        return false;
      }
      if (userdetail().getSetup().getSuperUser() == 1) {
        return true;
      }
      return false;
    } catch (Exception e) {
      throw new CustomException("Token is not valid", HttpStatus.FORBIDDEN);
    }
  }

}
