package com.system.base;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author imamsolikhin
 */
@Getter @Setter
public class Login {

  private String branch;
  private String username;
  private String password;
}
