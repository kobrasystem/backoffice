/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.generator;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.QRCode;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.imageio.ImageIO;

/**
 *
 * @author imamsolikhin
 */
public class QrCodeGenerator {

  public BufferedImage generateQrCode(final String qrCodeText, final int width, final int height) throws Exception {
    QRCodeWriter qrCodeWriter = new QRCodeWriter();
    BitMatrix bitMatrix = qrCodeWriter.encode(qrCodeText, BarcodeFormat.QR_CODE, width, height);
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    MatrixToImageWriter.writeToStream(bitMatrix, "PNG", byteArrayOutputStream);

    final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());

    return ImageIO.read(byteArrayInputStream);
  }

  private final String DIR = "/images";
  private final String ext = ".png";
  public static String LOGO = "http://103.163.139.199:9000/images/bpom.png";
  public static String TEXT = "TEST";
  private String CONTENT = "some content here";
  private int WIDTH = 300;
  private int HEIGHT = 300;

  public byte[] generatePotraitByte(final String qrCodeText, final int width, final int height, String text) throws IOException {
    ByteArrayInputStream baos = generateQr(qrCodeText, width, height, text);
    return baos.readAllBytes();
  }

  public BufferedImage generatePotrait(final String qrCodeText, final int width, final int height, String text) throws IOException {
    ByteArrayInputStream baos = generateQr(qrCodeText, width, height, text);
    return ImageIO.read(baos);
  }

  public ByteArrayInputStream generateQr(final String qrCodeText, final int width, final int height, String text) {
    WIDTH = width;
    HEIGHT = height;
    CONTENT = qrCodeText;
    // Create new configuration that specifies the error correction
    Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
    hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

    QRCodeWriter writer = new QRCodeWriter();
    BitMatrix bitMatrix = null;
    ByteArrayOutputStream os = new ByteArrayOutputStream();

    try {
      // init directory
//      cleanDirectory(DIR);
//      initDirectory(DIR);
      // Create a qr code with the url as content and a size of WxH px
      bitMatrix = writer.encode(CONTENT, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);

      // Load QR image
      BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, getMatrixConfig());

      // Load logo image
//      BufferedImage overly = getOverly(LOGO);
//      BufferedImage bpon = getOverly("http://192.168.248.59/test/temp1/images/bpom.png");
      // Calculate the delta height and width between QR code and logo
//      int deltaHeight = qrImage.getHeight() - overly.getHeight();
//      int deltaWidth = qrImage.getWidth() - overly.getWidth();
      // Initialize combined image
      BufferedImage combined = new BufferedImage(qrImage.getWidth() - 30, qrImage.getHeight() - 12, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = (Graphics2D) combined.getGraphics();
      g.setBackground(Color.WHITE);
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, qrImage.getWidth(), qrImage.getHeight());

      // Write QR code to new image at position 0/0
      g.drawImage(qrImage, -15, -17, null);
      g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

      // Write logo into combine image at position (deltaWidth / 2) and
      // (deltaHeight / 2). Background: Left/Right and Top/Bottom must be
      // the same space for the logo to be centered
//      center logo
//      g.drawImage(overly, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2.2), null);
//      g.drawImage(overly, qrImage.getWidth() - (qrImage.getWidth() * 17 / 100) - 10, qrImage.getHeight() - (qrImage.getHeight() * 15 / 100) - 10, null);
      if (!text.equals(null) || text != null) {
        g.setFont(new Font("Tahoma", Font.CENTER_BASELINE, 36));
        Color textColor = Color.BLACK;
        g.setColor(textColor);
        FontMetrics fm = g.getFontMetrics();
        g.drawString(text, (qrImage.getWidth() / 2) - (fm.stringWidth(text) / 2) - 10, qrImage.getHeight() - (qrImage.getHeight() * 5 / 100) - 1);
      }

      // Write combined image as PNG to OutputStream
      ImageIO.write(combined, "png", os);
      // Store Image
//      Files.copy(new ByteArrayInputStream(os.toByteArray()), Paths.get(DIR + generateRandoTitle(new Random(), 9) + ext), StandardCopyOption.REPLACE_EXISTING);

      final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(os.toByteArray());

      return byteArrayInputStream;
    } catch (WriterException e) {
      e.printStackTrace();
      //LOG.error("WriterException occured", e);
    } catch (IOException e) {
      e.printStackTrace();
      //LOG.error("IOException occured", e);
    }
    return null;
  }

  public BufferedImage generateLandscape(final String qrCodeText, final int width, final int height, String[] text) {
    WIDTH = width;
    WIDTH = height;
    CONTENT = qrCodeText;
    // Create new configuration that specifies the error correction
    Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
    hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

    QRCodeWriter writer = new QRCodeWriter();
    BitMatrix bitMatrix = null;
    ByteArrayOutputStream os = new ByteArrayOutputStream();

    try {
      // init directory
//      cleanDirectory(DIR);
//      initDirectory(DIR);
      // Create a qr code with the url as content and a size of WxH px
      bitMatrix = writer.encode(CONTENT, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);

      // Load QR image
      BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, getMatrixConfig());

      // Load logo image
      BufferedImage overly = getOverly(LOGO);
//      BufferedImage bpon = getOverly("http://192.168.248.59/test/temp1/images/bpom.png");

      // Calculate the delta height and width between QR code and logo
//      int deltaHeight = qrImage.getHeight() - overly.getHeight();
//      int deltaWidth = qrImage.getWidth() - overly.getWidth();
      // Initialize combined image
      BufferedImage combined = new BufferedImage(qrImage.getWidth() * 2, qrImage.getHeight() / 2, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = (Graphics2D) combined.getGraphics();
      g.setBackground(Color.WHITE);
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, qrImage.getWidth() * 3, qrImage.getHeight());

      // Write QR code to new image at position 0/0
      g.drawImage(qrImage, -25, -75, null);
      g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

      // Write logo into combine image at position (deltaWidth / 2) and
      // (deltaHeight / 2). Background: Left/Right and Top/Bottom must be
      // the same space for the logo to be centered
//      g.drawImage(overly, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2.2), null);
      g.drawImage(overly, 125, 130, null);

      if (!TEXT.equals(null) || TEXT != null) {
        g.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 14));
        g.setColor(Color.BLACK);
        FontMetrics fm = g.getFontMetrics();
        g.drawString("Check Keaslian Produk", 260 - (fm.stringWidth(TEXT) / 2), 20);
      }

      if (text.length > 0) {
        g.setFont(new Font("Tahoma", Font.LAYOUT_RIGHT_TO_LEFT, 12));
        Color textColor = Color.BLACK;
        g.setColor(textColor);
        FontMetrics fm = g.getFontMetrics();
        int startingYposition = 40;
        for (String displayText : text) {
          System.out.println(displayText);
          System.out.println(startingYposition);
//          g.drawString(TEXT, (combined.getWidth()/2) - (fm.stringWidth(TEXT)/2), startingYposition);
          g.drawString(displayText, 160, startingYposition);
          startingYposition += 15;
        }
      }

      if (!TEXT.equals(null) || TEXT != null) {
        g.setFont(new Font("Tahoma", Font.CENTER_BASELINE, 12));
        Color textColor = Color.BLACK;
        g.setColor(textColor);
        FontMetrics fm = g.getFontMetrics();
        g.drawString(TEXT, 285 - (fm.stringWidth(TEXT) / 2), 140);
      }

      // Write combined image as PNG to OutputStream
      ImageIO.write(combined, "png", os);
      // Store Image
//      Files.copy(new ByteArrayInputStream(os.toByteArray()), Paths.get(DIR + generateRandoTitle(new Random(), 9) + ext), StandardCopyOption.REPLACE_EXISTING);

      final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(os.toByteArray());

      return ImageIO.read(byteArrayInputStream);
    } catch (WriterException e) {
      e.printStackTrace();
      //LOG.error("WriterException occured", e);
    } catch (IOException e) {
      e.printStackTrace();
      //LOG.error("IOException occured", e);
    }
    return null;
  }

  private BufferedImage getOverly(String LOGO) throws IOException {
    URL url = new URL(LOGO);
    return ImageIO.read(url);
  }

  private void initDirectory(String DIR) throws IOException {
    Files.createDirectories(Paths.get(DIR));
  }

  private void cleanDirectory(String DIR) {
    try {
      Files.walk(Paths.get(DIR), FileVisitOption.FOLLOW_LINKS)
              .sorted(Comparator.reverseOrder())
              .map(path -> path.toFile())
              .forEach(File::delete);
    } catch (IOException e) {
      // Directory does not exist, Do nothing
    }
  }

  private MatrixToImageConfig getMatrixConfig() {
    // ARGB Colors
    // Check Colors ENUM
    return new MatrixToImageConfig(QrCodeGenerator.Colors.BLACK.getArgb(), QrCodeGenerator.Colors.WHITE.getArgb());
  }

  private String generateRandoTitle(Random random, int length) {
    return random.ints(48, 122)
            .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
            .mapToObj(i -> (char) i)
            .limit(length)
            .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
            .toString();
  }

  public enum Colors {

    BLUE(0xFF40BAD0),
    RED(0xFFE91C43),
    PURPLE(0xFF8A4F9E),
    ORANGE(0xFFF4B13D),
    WHITE(0xFFFFFFFF),
    BLACK(0xFF000000);

    private final int argb;

    Colors(final int argb) {
      this.argb = argb;
    }

    public int getArgb() {
      return argb;
    }
  }
}
