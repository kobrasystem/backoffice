/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.model;

import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Imam Solikhin
 */
@Data
@Entity
@Table(name = "pur_purchase_order_detail")
public class PurchaseOrderDetail extends BaseModel {

  @Id
  @Column(name = "id")
  @SheetColumn("id")
  private String id = "";
  
  @Column(name = "code")
  @SheetColumn("Code")
  private String code = "";

  @Column(name = "headerCode")
  @SheetColumn("HeaderCode")
  private String headerCode = "";

  @Column(name = "purchaseRequestDetailCode")
  @SheetColumn("PurchaseRequestDetailCode")
  private String purchaseRequestDetailCode = "";

  @Column(name = "itemCode")
  @SheetColumn("ItemCode")
  private String itemCode = "";

  @Column(name = "itemAlias")
  @SheetColumn("ItemAlias")
  private String itemAlias = "";

  @Column(name = "remark")
  @SheetColumn("Remark")
  private String remark = "";

  @Column(name = "quantity")
  @SheetColumn("Quantity")
  private BigDecimal quantity = new BigDecimal(0.00);

  @Column(name = "bonusQuantity")
  @SheetColumn("BonusQuantity")
  private BigDecimal bonusQuantity = new BigDecimal(0.00);

  @Column(name = "price")
  @SheetColumn("Price")
  private BigDecimal price = new BigDecimal(0.00);

  @Column(name = "discount1Percent")
  @SheetColumn("Discount1Percent")
  private BigDecimal discount1Percent = new BigDecimal(0.00);

  @Column(name = "discount1Amount")
  @SheetColumn("Discount1Amount")
  private BigDecimal discount1Amount = new BigDecimal(0.00);

  @Column(name = "discount2Percent")
  @SheetColumn("Discount2Percent")
  private BigDecimal discount2Percent = new BigDecimal(0.00);

  @Column(name = "discount2Amount")
  @SheetColumn("Discount2Amount")
  private BigDecimal discount2Amount = new BigDecimal(0.00);

  @Column(name = "discount3Percent")
  @SheetColumn("Discount3Percent")
  private BigDecimal discount3Percent = new BigDecimal(0.00);

  @Column(name = "discount3Amount")
  @SheetColumn("Discount3Amount")
  private BigDecimal discount3Amount = new BigDecimal(0.00);

  @Column(name = "nettPrice")
  @SheetColumn("NettPrice")
  private BigDecimal nettPrice = new BigDecimal(0.00);

  @Column(name = "totalAmount")
  @SheetColumn("TotalAmount")
  private BigDecimal totalAmount = new BigDecimal(0.00);

  @Column(name = "discountHeaderPercent")
  @SheetColumn("DiscountHeaderPercent")
  private BigDecimal discountHeaderPercent = new BigDecimal(0.00);

  @Column(name = "discountHeaderAmount")
  @SheetColumn("DiscountHeaderAmount")
  private BigDecimal discountHeaderAmount = new BigDecimal(0.00);
}
