/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.model;

import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author lukassh
 */
@Data
@Entity
@Table(name = "pur_purchase_request_detail")
public class PurchaseRequestDetail extends BaseModel{
    
  @Id
  @Column(name = "id")
  @SheetColumn("id")
  private String id = "";
  
  @Column(name = "code")
  @SheetColumn("Code")
  private String code = "";

  @Column(name = "headerCode")
  @SheetColumn("HeaderCode")
  private String headerCode = "";

  @Column(name = "itemCode")
  @SheetColumn("ItemCode")
  private String itemCode = "";

  @Column(name = "itemAlias")
  @SheetColumn("ItemAlias")
  private String itemAlias = "";

  @Column(name = "remark")
  @SheetColumn("Remark")
  private String remark = "";

  @Column(name = "quantity")
  @SheetColumn("Quantity")
  private BigDecimal quantity = new BigDecimal(0.00);
    
}
