/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class PurchaseRequestDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM pur_purchase_request WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " SELECT COUNT(model.code) FROM ( "
              + "    SELECT "
              + "      prq.code, "
              + "      prq.branchCode, "
              + "      br.Name branchName, "
              + "      prq.transactionDate, "
              + "      DATE_FORMAT(prq.transactionDate, '%d-%m-%Y') purchaseRequestDate, "
              + "      IFNULL(prq.requestBy,'') requestBy, "
              + "      prq.approvalBy, "
              + "      prq.approvalStatus, "
              + "      DATE_FORMAT(prq.approvalDate, '%d-%m-%Y') approvalDate, "
              + "      prq.closingBy, "
              + "      prq.closingStatus, "
              + "      DATE_FORMAT(prq.closingDate, '%d-%m-%Y') closingDate, "
              + "      IFNULL(prq.refNo,'') refNo, "
              + "      IFNULL(prq.remark,'') remark, "
              + "      prq.divisionCode,"
              + "      idiv.Name AS divisionName"
//              + "      IFNULL(GROUP_CONCAT( DISTINCT jnpo.purchaseOrderCode ORDER BY jnpo.purchaseOrderCode ASC),'-') AS purchaseOrderCode "
              + "   FROM pur_purchase_request prq "
              + "      INNER JOIN mst_branch br ON prq.BranchCode = br.Code "
              + "      INNER JOIN mst_division idiv ON prq.DivisionCode = idiv.code "
              + "      LEFT JOIN pur_purchase_order_jn_purchase_request jnpo ON prq.code = jnpo.PurchaseRequestCode "
              + "   WHERE prq.Code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "      AND DATE(prq.TransactionDate) BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') "
              + "      AND prq.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "      AND prq.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "      AND IFNULL(prq.refNo,'') LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
              + "      AND IFNULL(prq.remark,'') LIKE '%" + AppUtils.toString(obj, "remark") + "%' "
              + "   GROUP BY prq.code "
              + " ) model ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " SELECT model.* FROM ( "
              + "    SELECT "
              + "      prq.code, "
              + "      prq.branchCode, "
              + "      br.Name branchName, "
              + "      prq.transactionDate, "
              + "      DATE_FORMAT(prq.transactionDate, '%d-%m-%Y') purchaseRequestDate, "
              + "      IFNULL(prq.requestBy,'') requestBy, "
              + "      prq.approvalBy, "
              + "      prq.approvalStatus, "
              + "      DATE_FORMAT(prq.approvalDate, '%d-%m-%Y') approvalDate, "
              + "      prq.closingBy, "
              + "      prq.closingStatus, "
              + "      DATE_FORMAT(prq.closingDate, '%d-%m-%Y') closingDate, "
              + "      IFNULL(prq.refNo,'') refNo, "
              + "      IFNULL(prq.remark,'') remark, "
              + "      prq.divisionCode,"
              + "      idiv.Name AS divisionName"
//              + "      IFNULL(GROUP_CONCAT( DISTINCT jnpo.purchaseOrderCode ORDER BY jnpo.purchaseOrderCode ASC),'-') AS purchaseOrderCode "
              + "   FROM pur_purchase_request prq "
              + "      INNER JOIN mst_branch br ON prq.BranchCode = br.Code "
              + "      INNER JOIN mst_division idiv ON prq.DivisionCode = idiv.code "
              + "      LEFT JOIN pur_purchase_order_jn_purchase_request jnpo ON prq.code = jnpo.PurchaseRequestCode "
              + "   WHERE prq.Code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "      AND DATE(prq.TransactionDate) BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') "
              + "      AND prq.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "      AND prq.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "      AND IFNULL(prq.refNo,'') LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
              + "      AND IFNULL(prq.remark,'') LIKE '%" + AppUtils.toString(obj, "remark") + "%' "
              + "   GROUP BY prq.code "
              + " ) model "
              + " GROUP BY model.Code ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public long countDetail(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  COUNT(prDetailQry.code) FROM( "
              + " SELECT "
              + "  pur_purchase_request_detail.code, "
              + "  pur_purchase_request_detail.headerCode "
              + "  FROM pur_purchase_request_detail  "
              + "  INNER JOIN pur_purchase_request ON pur_purchase_request_detail.HeaderCode=pur_purchase_request.Code  "
              + "  INNER JOIN mst_item ON pur_purchase_request_detail.ItemCode=mst_item.Code  "
              + "  LEFT JOIN ( "
              + " 	 SELECT  "
              + " 	 pur_purchase_order_detail.ItemCode, "
              + " 	 pur_purchase_order_detail.Price, "
              + " 	 SUM(pur_purchase_order_detail.Quantity) AS quantity, "
              + " 	 pur_purchase_order_detail.PurchaseRequestDetailCode "
              + " 	 FROM pur_purchase_order_detail "
              + " 	 GROUP BY pur_purchase_order_detail.PurchaseRequestDetailCode "
              + "  )qryPo ON qryPo.PurchaseRequestDetailCode=pur_purchase_request_detail.code "
              + "  WHERE "
              + "   pur_purchase_request_detail.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' "
              + "  )prDetailQry ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT * FROM( "
              + "  SELECT "
              + "  pur_purchase_request_detail.code, "
              + "  pur_purchase_request_detail.headerCode, "
              + "  pur_purchase_request_detail.itemCode,  "
              + "  pur_purchase_request_detail.itemAlias,  "
              + "  mst_item.Name AS itemName,  "
              + "  pur_purchase_request_detail.remark,  "
              + "  pur_purchase_request_detail.quantity,  "
              + "  IFNULL(qryPo.Quantity,0) AS poQuantity,  "
              + "  IFNULL(pur_purchase_request_detail.Quantity,0) - IFNULL(qryPo.Quantity,0) AS outStandingPoQuantity,  "
              + "  qryPo.Price AS poPrice  "
//              + "  mst_item.UnitOfMeasureCode AS unitOfMeasureCode,                                                    "
//              + "  IFNULL(onHandStok.ActualStock,0) AS onHandStock,   "
//              + "  mst_item.MinStock AS minStock,  "
//              + "  mst_item.MaxStock AS maxStock  "
              + "  FROM pur_purchase_request_detail  "
              + "  INNER JOIN pur_purchase_request ON pur_purchase_request_detail.HeaderCode=pur_purchase_request.Code  "
              + "  INNER JOIN mst_item ON pur_purchase_request_detail.ItemCode=mst_item.Code  "
              + "  LEFT JOIN ( "
              + " 	 SELECT  "
              + " 	 pur_purchase_order_detail.ItemCode, "
              + " 	 pur_purchase_order_detail.Price, "
              + " 	 SUM(pur_purchase_order_detail.Quantity) AS quantity, "
              + " 	 pur_purchase_order_detail.PurchaseRequestDetailCode "
              + " 	 FROM pur_purchase_order_detail "
              + " 	 GROUP BY pur_purchase_order_detail.PurchaseRequestDetailCode "
              + "  )qryPo ON qryPo.PurchaseRequestDetailCode=pur_purchase_request_detail.code "
              + "  WHERE "
              + "   pur_purchase_request_detail.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' "
              + " )prDetailQry "
              + " ORDER BY prDetailQry." + sort[0] + " " + sort[1];
      long countData = this.countDetail(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT * FROM( "
              + "  SELECT "
              + "    pur_purchase_request.code, "
              + "    pur_purchase_request.code, "
              + "    DATE_FORMAT(pur_purchase_request.transactionDate, '%d-%m-%Y') transactionDate, "
              + "    pur_purchase_request.branchCode, "
              + "    branch.Name AS branchName, "
              + "    pur_purchase_request.divisionCode, "
              + "    IFNULL(divs.name,'') AS divisionName, "
//              + "    IFNULL(divs.departmentCode,'') AS departmentCode, "
//              + "    IFNULL(dep.name,'') AS departmentName, "
              + "    pur_purchase_request.requestBy, "
              + "    pur_purchase_request.refNo, "
              + "    pur_purchase_request.remark, "
              + "    pur_purchase_request.approvalReasonCode, "
              + "    pur_purchase_request.approvalStatus, "
              + "    pur_purchase_request.approvalDate, "
              + "    pur_purchase_request.approvalBy, "
              + "    pur_purchase_request.approvalRemark, "
              + "    pur_purchase_request.closingStatus, "
              + "    pur_purchase_request.closingDate, "
              + "    pur_purchase_request.closingBy, "
              + "    pur_purchase_request.closingRemark "
              + "  FROM pur_purchase_request "
              + "  INNER JOIN mst_branch branch ON pur_purchase_request.branchCode = branch.Code "
              + "  INNER JOIN mst_division divs ON pur_purchase_request.DivisionCode = divs.code "
              + " WHERE "
              + "  pur_purchase_request.code = '" + AppUtils.toString(obj, "code") + "' "
              + "  ) qry ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "  SELECT * FROM( "
              + "    SELECT "
              + "      pur_purchase_request.code, "
              + "      pur_purchase_request.branchCode, "
              + "      br.Name branchName, "
              + "      pur_purchase_request.transactionDate, "
              + "      DATE_FORMAT(pur_purchase_request.transactionDate, '%d-%m-%Y') purchaseRequestDate, "
              + "      IFNULL(pur_purchase_request.requestBy,'') requestBy, "
              + "      pur_purchase_request.approvalBy, "
              + "      pur_purchase_request.approvalStatus, "
              + "      DATE_FORMAT(pur_purchase_request.approvalDate, '%d-%m-%Y') approvalDate, "
              + "      pur_purchase_request.closingBy, "
              + "      pur_purchase_request.closingStatus, "
              + "      DATE_FORMAT(pur_purchase_request.closingDate, '%d-%m-%Y') closingDate, "
              + "      IFNULL(pur_purchase_request.refNo,'') refNo, "
              + "      IFNULL(pur_purchase_request.remark,'') remark, "
              + "      pur_purchase_request.divisionCode,"
              + "      idiv.Name AS divisionName,"
              + "      IFNULL(GROUP_CONCAT( DISTINCT jnpo.purchaseOrderCode ORDER BY jnpo.purchaseOrderCode ASC),'-') AS purchaseOrderCode "
              + "   FROM pur_purchase_request pur_purchase_request "
              + "      INNER JOIN mst_branch br ON pur_purchase_request.BranchCode = br.Code "
              + "      INNER JOIN mst_division idiv ON pur_purchase_request.DivisionCode = idiv.code "
              + "      LEFT JOIN pur_purchase_order_jn_purchase_request jnpo ON pur_purchase_request.code = jnpo.PurchaseRequestCode "
              + "   WHERE pur_purchase_request.Code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "      AND DATE(pur_purchase_request.TransactionDate) BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') "
              + "      AND pur_purchase_request.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "      AND pur_purchase_request.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "      AND IFNULL(pur_purchase_request.refNo,'') LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
              + "      AND IFNULL(pur_purchase_request.remark,'') LIKE '%" + AppUtils.toString(obj, "remark") + "%' ";
      qry += " ORDER BY qry." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
