/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.model;

import lombok.Data;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.system.base.BaseModel;
import org.springframework.format.annotation.DateTimeFormat;
import io.github.millij.poi.ss.model.annotations.SheetColumn;


/**
 *
 * @author Rayis
 */
@Data
@Entity
@Table(name = "pur_purchase_return")
public class PurchaseReturn extends BaseModel {

    @Id
    @Column(name = "id")
    @SheetColumn("id")
    private String id = "";
    
    @Column(name = "code")
    @SheetColumn("Code")
    private String code = "";

    @Column(name = "branchCode")
    @SheetColumn("BranchCode")
    private String branchCode = "";

    @Column(name = "transactionDate")
    @SheetColumn("TransactionDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date transactionDate = new Date();

    @Column(name = "warehouseCode")
    @SheetColumn("WarehouseCode")
    private String warehouseCode = "";

    @Column(name = "vendorInvoiceCode")
    @SheetColumn("VendorInvoiceCode")
    private String vendorInvoiceCode = "";
    
    @Column(name = "paymentTermCode")
    @SheetColumn("PaymentTermCode")
    private String paymentTermCode = "";

    @Column(name = "dueDate")
    @SheetColumn("DueDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dueDate = new Date();

    @Column(name = "exchangeRate")
    @SheetColumn("ExchangeRate")
    private BigDecimal exchangeRate = new BigDecimal(0.00);

    @Column(name = "vendorInvoiceNo")
    @SheetColumn("VendorInvoiceNo")
    private String vendorInvoiceNo = "";

    @Column(name = "taxInvoiceNo")
    @SheetColumn("TaxInvoiceNo")
    private String taxInvoiceNo = "";

    @Column(name = "vendorCode")
    @SheetColumn("VendorCode")
    private String vendorCode = "";

    @Column(name = "currencyCode")
    @SheetColumn("CurrencyCode")
    private String currencyCode = "";

    @Column(name = "totalTransactionAmount")
    @SheetColumn("TotalTransactionAmount")
    private BigDecimal totalTransactionAmount = new BigDecimal(0.00);

    @Column(name = "discountPercent")
    @SheetColumn("DiscountPercent")
    private BigDecimal discountPercent = new BigDecimal(0.00);

    @Column(name = "discountAmount")
    @SheetColumn("DiscountAmount")
    private BigDecimal discountAmount = new BigDecimal(0.00);

    @Column(name = "discountChartOfAccountCode")
    @SheetColumn("DiscountChartOfAccountCode")
    private String discountChartOfAccountCode = "";

    @Column(name = "discountDescription")
    @SheetColumn("DiscountDescription")
    private String discountDescription = "";

    @Column(name = "taxBaseSubTotalAmount")
    @SheetColumn("TaxBaseSubTotalAmount")
    private BigDecimal taxBaseSubTotalAmount = new BigDecimal(0.00);

    @Column(name = "vatPercent")
    @SheetColumn("VATPercent")
    private BigDecimal vatPercent = new BigDecimal(0.00);

    @Column(name = "vatAmount")
    @SheetColumn("VATAmount")
    private BigDecimal vatAmount = new BigDecimal(0.00);

    @Column(name = "otherFeeAmount")
    @SheetColumn("OtherFeeAmount")
    private BigDecimal otherFeeAmount = new BigDecimal(0.00);

    @Column(name = "otherFeeChartOfAccountCode")
    @SheetColumn("OtherFeeChartOfAccountCode")
    private String otherFeeChartOfAccountCode = "";

    @Column(name = "otherFeeDescription")
    @SheetColumn("OtherFeeDescription")
    private String otherFeeDescription = "";

    @Column(name = "grandTotalAmount")
    @SheetColumn("GrandTotalAmount")
    private BigDecimal grandTotalAmount = new BigDecimal(0.00);

    @Column(name = "paidAmount")
    @SheetColumn("PaidAmount")
    private BigDecimal paidAmount = new BigDecimal(0.00);

    @Column(name = "settlementDate")
    @SheetColumn("SettlementDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date settlementDate = new Date();

    @Column(name = "settlementDocumentNo")
    @SheetColumn("SettlementDocumentNo")
    private String settlementDocumentNo = "";

    @Column(name = "refNo")
    @SheetColumn("RefNo")
    private String refNo = "";

    @Column(name = "remark")
    @SheetColumn("Remark")
    private String remark = "";

    private transient String rackCode ="";
    
    @Transient
    private transient List<PurchaseReturnDetail> listPurchaseReturnDetail;

}
