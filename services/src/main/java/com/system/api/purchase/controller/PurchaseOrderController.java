/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.controller;

import com.system.api.purchase.model.PurchaseOrder;
import com.system.api.purchase.model.PurchaseOrderDetail;
import com.system.api.purchase.model.PurchaseRequest;
import com.system.api.purchase.model.PurchaseRequestDetail;
import com.system.api.purchase.service.PurchaseOrderService;
import com.system.base.ReportBase;
import com.system.base.ResponsBody;
import static com.system.base.Session.username;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.CustomJasperReport;
import com.system.utils.DateUtils;
import com.system.utils.IOExcel;
import com.system.utils.JasperReportExportFormat;
import com.system.utils.JasperReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author lukassh
 */
@RestController
@Api(tags = "Purchase Order")
@RequestMapping("${jwt.path-purchase}/purchase-order")
public class PurchaseOrderController extends ReportBase {

  @Autowired
  private AppUtils appUtils;
  @Autowired
  private JasperReportService jasperReportService;

  @Autowired
  private PurchaseOrderService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"transactionStartDate\":\"\",\"transactionEndDate\":\"\",\"vendorCode\":\"\",\"vendorName\":\"\",\"currencyCode\":\"\",\"currencyName\":\"\",\"refNo\":\"\",\"remark\":\"\",\"approvalStatus\":\"\",\"closingStatus\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,DESC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrder.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/lookup-for-invoice/search")
  public HttpEntity<Object> listLookupForInvoice(
          @RequestParam(defaultValue = "{\"code\":\"\",\"vendorCode\":\"\",\"vendorName\":\"\",\"refNo\":\"\",\"remark\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingLookupForInvoice(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrder.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/prq/search")
  public HttpEntity<Object> listPqrDetail(@RequestParam(defaultValue = "{\"headerCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingPrqDetail(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/detail/search")
  public HttpEntity<Object> listDetail(@RequestParam(defaultValue = "{\"headerCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingDetail(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/vdp/search")
  public HttpEntity<Object> listVdpDetail(@RequestParam(defaultValue = "{\"headerCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingVdpDetail(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/item-prq/search")
  public HttpEntity<Object> listItemPrq(@RequestParam String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingItemPrq(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseRequestDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, PurchaseOrder.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "look-up-prq/search")
  public HttpEntity<Object> listPurchaseRequest(
          @RequestParam(defaultValue = "{\"code\":\"\",\"transactionStartDate\":\"2022-01-01\",\"transactionEndDate\":\"2025-01-01\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.findPrq(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseRequest.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "look-up-for-grn/search")
  public HttpEntity<Object> listPoForGRN(
          @RequestParam(defaultValue = "{\"code\":\"\",\"refNo\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.findPoForGRN(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrder.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody PurchaseOrder model) {
    try {
      long data = service.checkData(model.getCode(), PurchaseOrder.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody PurchaseOrder model) {
    try {
      long data = service.checkData(model.getCode(), PurchaseOrder.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/approve/update")
  public HttpEntity<Object> approve(@RequestBody PurchaseOrder model) {
    try {

      long data = service.checkData(model.getCode(), PurchaseOrder.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      model.setApprovalBy(username());
      model.setApprovalDate(DateUtils.newDate());
      model.setApprovalStatus(model.getApprovalStatus());
      model.setApprovalRemark(model.getApprovalRemark());
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Approved data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/closing/update")
  public HttpEntity<Object> closing(@RequestBody PurchaseOrder model) {
    try {

      long data = service.checkData(model.getCode(), PurchaseOrder.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      model.setClosingBy(username());
      model.setClosingDate(DateUtils.newDate());
      model.setClosingStatus(model.getClosingStatus());
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Approved data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, PurchaseOrder.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/history/search")
  public HttpEntity<Object> listHistory(@RequestParam(defaultValue = "{\"code\":\"\",\"transactionStartDate\":\"\",\"transactionEndDate\":\"\",\"vendorCode\":\"\",\"vendorName\":\"\",\"itemCode\":\"\",\"itemName\":\"\",\"refNo\":\"\",\"remark\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingHistory(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @GetMapping(value = "/purchase/purchase-order/pdf")
  public ResponseEntity<byte[]> printPOPdf(@RequestParam(defaultValue = "token") String token) {
    try {
      JSONObject data = new JSONObject(appUtils.getDecode(token));

      CustomJasperReport report = new CustomJasperReport();
      report.setOutputFilename("Purchase Order");
//            report.setReportName("purchase-order-half-folio");
      report.setReportName("purchase-order-local");
      report.getParameters().put("prmCode", AppUtils.toString(data, "code"));

      report.setReportDir(appUtils.getResouceAbsolutePath("classpath:reports/purchase"));
      report.setConnection(appUtils.getConnection());
      report.setReportFormat(JasperReportExportFormat.PDF_FORMAT);

      report.getParameters().put("prmUserName", AppUtils.toString(data, "userName"));
      report.getParameters().put("prmBranchName", AppUtils.toString(data, "branchName"));
      report.getParameters().put("prmBranchTelp", AppUtils.toString(data, "branchTelp"));
      report.getParameters().put("prmBranchFax", AppUtils.toString(data, "branchFax"));
      report.getParameters().put("prmBranchAddress", AppUtils.toString(data, "branchAddress"));
      report = jasperReportService.generaterJasperReportFinalization(report);
      return respondReportOutput(report, false);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/exports")
  public void exports(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "", required = false) String bankCode,
          @RequestParam(defaultValue = "", required = false) String bankName,
          @RequestParam(defaultValue = "", required = false) String title,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort,
          HttpServletResponse response) throws ParseException {

    JSONObject obj = new JSONObject("{'code':'" + code
            + "','name':'" + name
            + "','activeStatus':'" + activeStatus
            + "'}");
    List<Map<String, Object>> list = service.list(obj, page, size, sort, PurchaseOrder.class);

    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, title);
    IOExcel.expCellValues("Master Purchase Order", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (int i = 0; i < list.size(); i++) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      row++;
    }
    try {
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition", "attachment;filename=purchase-" + title + ".xlsx");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

}
