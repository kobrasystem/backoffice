/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.service;

import com.system.api.purchase.dao.PurchaseReturnDao;
import com.system.api.purchase.model.PurchaseReturn;
import com.system.api.purchase.model.PurchaseReturnDetail;
import com.system.api.purchase.repo.PurchaseReturnDetailRepo;
import com.system.api.purchase.repo.PurchaseReturnRepo;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import com.system.utils.AutoNumber;
import com.system.utils.DateUtils;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rayis
 */
@Service
public class PurchaseReturnService {

  @Autowired
  private PurchaseReturnRepo repo;

  @Autowired
  private PurchaseReturnDetailRepo detailRepo;

  @Autowired
  private PurchaseReturnDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PurchaseReturn data(String code) {
    try {
      return repo.findByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object dataVIN(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.dataVIN(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingVIN(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingVIN(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingDetailInv(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDetailInv(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  private String createCode(PurchaseReturn model) {
    try {
      String prefix = model.getBranchCode() + "/PRT/" + DateUtils.formatingDate(model.getTransactionDate(), true, true, false) + "/";
      List<PurchaseReturn> list = repo.findMaxCode(prefix);

      String oldID = "";
      if (list != null) {
        if (list.size() > 0) {
          if (list.get(0) != null) {
            oldID = list.get(0).getCode();
          }
        }
      }
      return AutoNumber.generate(prefix, oldID, AutoNumber.TRANSACTION_LENGTH_5);
    } catch (HibernateException e) {
      throw e;
    }
  }

  public PurchaseReturn save(PurchaseReturn model) {
    try {
      model.setCode(createCode(model));
      for (int i = 0; i <= model.getListPurchaseReturnDetail().size(); i++) {
        model.getListPurchaseReturnDetail().get(i).setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        model.getListPurchaseReturnDetail().get(i).setHeaderCode(model.getCode());
      }
      detailRepo.saveAll(model.getListPurchaseReturnDetail());
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PurchaseReturn update(PurchaseReturn model) {
    try {
      PurchaseReturn data = repo.findByCodes(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " is exist!", HttpStatus.CONFLICT);
      }
      List<PurchaseReturnDetail> listPrtDetail = detailRepo.findAllByCode(model.getCode());
      if (listPrtDetail == null) {
        throw new CustomException("Item Detail data doesn't exist", HttpStatus.NOT_FOUND);
      }
      detailRepo.deleteByHeaderCode(model.getCode());
      for (int i = 0; i <= model.getListPurchaseReturnDetail().size(); i++) {
        model.getListPurchaseReturnDetail().get(i).setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        model.getListPurchaseReturnDetail().get(i).setHeaderCode(model.getCode());
      }
      detailRepo.saveAll(model.getListPurchaseReturnDetail());
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      detailRepo.deleteByHeaderCode(code);
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

}
