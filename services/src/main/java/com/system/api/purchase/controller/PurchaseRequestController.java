/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.controller;

import com.system.api.purchase.model.PurchaseRequest;
import com.system.api.purchase.model.PurchaseRequestDetail;
import com.system.api.purchase.service.PurchaseRequestService;
import com.system.base.ReportBase;
import com.system.base.ResponsBody;
import static com.system.base.Session.username;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.CustomJasperReport;
import com.system.utils.DateUtils;
import com.system.utils.JasperReportExportFormat;
import com.system.utils.JasperReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lukassh
 */
@RestController
@Api(tags = "Purchase Request")
@RequestMapping("${jwt.path-purchase}/purchase-request")
public class PurchaseRequestController extends ReportBase {

  @Autowired
  private AppUtils appUtils;
  @Autowired
  private JasperReportService jasperReportService;

  @Autowired
  private PurchaseRequestService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"type\":\"\"\"approvalStatus\":\"\",\"closingStatus\":\"\",\"transactionStartDate\":\"2022-01-01\",\"transactionEndDate\":\"2025-01-01\",\"refNo\":\"\",\"remark\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseRequest.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/detail/search")
  public HttpEntity<Object> listDetail(@RequestParam(defaultValue = "{\"headerCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingDetail(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseRequestDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, PurchaseRequest.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody PurchaseRequest model) {
    try {
      long data = service.checkData(model.getCode(), PurchaseRequest.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody PurchaseRequest model) {
    try {
      long data = service.checkData(model.getCode(), PurchaseRequest.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/approve/update")
  public HttpEntity<Object> approve(@RequestBody PurchaseRequest model) {
    try {

      long data = service.checkData(model.getCode(), PurchaseRequest.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      model.setApprovalBy(username());
      model.setApprovalDate(DateUtils.newDate());
      model.setApprovalStatus(model.getApprovalStatus());
      model.setApprovalRemark(model.getApprovalRemark());

      service.update(model);
      return new HttpEntity<>(new ResponsBody("Approved data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/closing/update")
  public HttpEntity<Object> closing(@RequestBody PurchaseRequest model) {
    try {

      long data = service.checkData(model.getCode(), PurchaseRequest.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      model.setClosingBy(username());
      model.setClosingDate(DateUtils.newDate());
      if (model.getClosingStatus().isEmpty()) {
        model.setClosingStatus("CLOSED");
      } else {
        model.setClosingStatus(model.getClosingStatus());
      }
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Approved data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, PurchaseRequest.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @GetMapping(value = "/pdf")
  public ResponseEntity<byte[]> printPRPdf(@RequestParam(defaultValue = "token") String token) {
    try {
      JSONObject data = new JSONObject(appUtils.getDecode(token));

      CustomJasperReport report = appUtils.template(data);
      report.setOutputFilename(AppUtils.toString(data, "code"));
      report.setReportName("purchase-request-local");
      report.getParameters().put("prmCode", AppUtils.toString(data, "code"));

      report.setReportDir(appUtils.getResouceAbsolutePath("classpath:reports/purchase"));
      report.setReportFormat(JasperReportExportFormat.PDF_FORMAT);

      report = jasperReportService.generaterJasperReportFinalization(report);
      return respondReportOutput(report, false);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
}
