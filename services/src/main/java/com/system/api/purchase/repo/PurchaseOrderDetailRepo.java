package com.system.api.purchase.repo;

import com.system.api.purchase.model.PurchaseOrderDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface PurchaseOrderDetailRepo extends JpaRepository<PurchaseOrderDetail, String> {

  @Query("SELECT m FROM PurchaseOrderDetail m WHERE m.headerCode = :headerCode ")
  List<PurchaseOrderDetail> findDataListByCode(@Param("headerCode") String code);
  
  @Query("SELECT m FROM PurchaseOrderDetail m WHERE m.headerCode = :code ")
  List<PurchaseOrderDetail> findDetailByCode(@Param("code") String code);

  @Query("SELECT m FROM PurchaseOrderDetail m WHERE m.code = :code ")
  PurchaseOrderDetail findByCode(@Param("code") String code);
    
  @Transactional
  void deleteByHeaderCode(String code);
  
  @Transactional
  void deleteByCode(String code);

}
