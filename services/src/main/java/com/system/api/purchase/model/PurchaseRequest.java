/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author lukassh
 */
@Data
@Entity
@Table(name = "pur_purchase_request")
public class PurchaseRequest extends BaseModel {
    
    @Id
    @Column(name = "id")
    @SheetColumn("id")
    private String id = "";
    
    @Column(name = "code")
    @SheetColumn("Code")
    private String code = "";

    @Column(name = "transactionDate")
    @SheetColumn("TransactionDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date transactionDate = new Date();

    @Column(name = "branchCode")
    @SheetColumn("BranchCode")
    private String branchCode = "";

    @Column(name = "divisionCode")
    @SheetColumn("DivisionCode")
    private String divisionCode = "";

    @Column(name = "requestBy")
    @SheetColumn("RequestBy")
    private String requestBy = "";

    @Column(name = "refNo")
    @SheetColumn("RefNo")
    private String refNo = "";

    @Column(name = "remark")
    @SheetColumn("Remark")
    private String remark = "";

    @Column(name = "closingStatus")
    @SheetColumn("ClosingStatus")
    private String closingStatus = "OPEN";

    @Column(name = "closingDate")
    @SheetColumn("ClosingDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date closingDate = new Date();

    @Column(name = "closingBy")
    @SheetColumn("ClosingBy")
    private String closingBy = "";

    @Column(name = "closingRemark")
    @SheetColumn("ClosingRemark")
    private String closingRemark = "";

    @Column(name = "approvalStatus")
    @SheetColumn("ApprovalStatus")
    private String approvalStatus = "PENDING";

    @Column(name = "approvalDate")
    @SheetColumn("ApprovalDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date approvalDate = new Date();

    @Column(name = "approvalBy")
    @SheetColumn("ApprovalBy")
    private String approvalBy = "";

    @Column(name = "approvalRemark")
    @SheetColumn("ApprovalRemark")
    private String approvalRemark = "";
    
    @Column(name = "approvalReasonCode")
    @SheetColumn("ApprovalReasonCode")
    private String approvalReasonCode = "";
    
  @Transient
  private transient List<PurchaseRequestDetail> listPurchaseRequestDetail;
    
}
