/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.model;

import lombok.Data;
import java.math.BigDecimal;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

/**
 *
 * @author Rayis
 */

@Data
@Entity
@Table(name = "pur_purchase_return_item_detail")
public class PurchaseReturnDetail extends BaseModel{
    
  @Id
  @Column(name = "id")
  @SheetColumn("id")
  private String id = "";
  
  @Column(name = "code")
  @SheetColumn("Code")
  private String code = "";

  @Column(name = "headerCode")
  @SheetColumn("HeaderCode")
  private String headerCode = "";
  
  @Column(name = "vendorInvoiceItemDetailCode")
  @SheetColumn("VendorInvoiceItemDetailCode")
  private String vendorInvoiceItemDetailCode = "";

  @Column(name = "itemCode")
  @SheetColumn("ItemCode")
  private String itemCode = "";
  
  @Column(name = "quantity")
  @SheetColumn("Quantity")
  private BigDecimal quantity = new BigDecimal(0.00);
  
  @Column(name = "price")
  @SheetColumn("Price")
  private BigDecimal price = new BigDecimal(0.00);
  
  @Column(name = "totalAmount")
  @SheetColumn("TotalAmount")
  private BigDecimal totalAmount = new BigDecimal(0.00);

  @Column(name = "rackCode")
  @SheetColumn("RackCode")
  private String rackCode = "";
  
  @Column(name = "remark")
  @SheetColumn("Remark")
  private String remark = "";

}
