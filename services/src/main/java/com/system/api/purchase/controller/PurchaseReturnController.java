/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.controller;

import java.util.Map;
import java.util.List;
import java.io.IOException;
import org.json.JSONObject;
import com.poiji.bind.Poiji;
import java.text.ParseException;
import io.swagger.annotations.Api;
import com.poiji.exception.PoijiExcelType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Authorization;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.system.api.purchase.model.PurchaseReturn;
import com.system.api.purchase.model.PurchaseReturnDetail;
import com.system.api.purchase.service.PurchaseReturnService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.IOExcel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Rayis
 */
@RestController
@Api(tags = "Purchase Return")
@RequestMapping("${jwt.path-purchase}/purchase-return")
public class PurchaseReturnController extends BaseProgress {

  @Autowired
  private PurchaseReturnService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"transactionStartDate\":\"2022-01-01\",\"transactionEndDate\":\"2025-01-01\",\"vendorCode\":\"\",\"vendorName\":\"\",\"refNo\":\"\",\"remark\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseReturn.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/detail/search")
  public HttpEntity<Object> listDetail(@RequestParam(defaultValue = "{\"headerCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingDetail(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseReturnDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, PurchaseReturn.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "vin/data")
  public HttpEntity<Object> dataVIN(@RequestParam String code) {
    try {
      Object model = service.dataVIN(code, PurchaseReturn.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/vin/search")
  public HttpEntity<Object> listVIN(@RequestParam(defaultValue = "{\"code\":\"\",\"transactionStartDate\":\"2022-01-01\",\"transactionEndDate\":\"2025-01-01\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingVIN(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseReturnDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/item-vin/search")
  public HttpEntity<Object> listDetailInv(@RequestParam(defaultValue = "{\"headerCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}", required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingDetailInv(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), PurchaseReturnDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody PurchaseReturn model) {
    try {
      long data = service.checkData(model.getCode(), PurchaseReturn.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody PurchaseReturn model) {
    try {
      long data = service.checkData(model.getCode(), PurchaseReturn.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, PurchaseReturn.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/imports")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<PurchaseReturn> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, PurchaseReturn.class);
      for (PurchaseReturn m : model) {
        service.save(m);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/exports")
  public void exports(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "", required = false) String bankCode,
          @RequestParam(defaultValue = "", required = false) String bankName,
          @RequestParam(defaultValue = "", required = false) String title,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort,
          HttpServletResponse response) throws ParseException {

    JSONObject obj = new JSONObject("{'code':'" + code
            + "','name':'" + name
            + "','activeStatus':'" + activeStatus
            + "'}");
    List<Map<String, Object>> list = service.list(obj, page, size, sort, PurchaseReturn.class);

    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, title);
    IOExcel.expCellValues("Purchase Return", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (int i = 0; i < list.size(); i++) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      row++;
    }
    try {
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition", "attachment;filename=purchase-" + title + ".xlsx");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }

}
