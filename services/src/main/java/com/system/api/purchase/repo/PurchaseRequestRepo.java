/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.repo;

import com.system.api.purchase.model.PurchaseRequest;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lukassh
 */
public interface PurchaseRequestRepo extends JpaRepository<PurchaseRequest, String> {

  @Query("SELECT m FROM PurchaseRequest m WHERE m.code = :code ")
  PurchaseRequest findByCode(@Param("code") String code);
    
    
  @Query("SELECT max(m) FROM PurchaseRequest m WHERE m.code LIKE :prefixCode% ")
  List<PurchaseRequest> findMaxCode(@Param("prefixCode") String prefixCode);

  @Transactional
  void deleteByCode(String code);
    
}
