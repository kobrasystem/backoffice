/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * AND open the template in the editor.
 */
package com.system.api.purchase.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class PurchaseReturnDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM pur_purchase_return WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(prt.code) FROM( SELECT";
            String list = "SELECT prt.* FROM( SELECT ";
            String sorte = " ORDER BY prt." + sort[0] + " " + sort[1];
            String select = " "
                    + "    prt.code, "
                    + "    DATE_FORMAT(prt.transactionDate, '%d-%m-%Y') purchaseReturnDate, "
                    + "    prt.transactionDate, "
                    + "    IFNULL(prt.VendorInvoiceCode,'') AS vendorInvoiceCode, "
                    + "    prt.vendorCode, "
                    + "    IFNULL(ven.name,'') AS vendorName, "
                    + "    IFNULL(prt.refNo,'') AS refNo, "
                    + "    IFNULL(prt.remark,'') AS remark, "
                    + "    prt.totalTransactionAmount, "
                    + "    prt.discountPercent, "
                    + "    prt.discountAmount, "
                    + "    prt.discountChartOfAccountCode, "
                    + "    coa.Name discountChartOfAccountName, "
                    + "    prt.discountDescription, "
                    + "    prt.taxBaseSubTotalAmount, "
                    + "    prt.vatPercent, "
                    + "    prt.vatAmount, "
                    + "    prt.otherFeeAmount, "
                    + "    prt.otherFeeChartOfAccountCode, "
                    + "    othCoa.Name otherFeeChartOfAccountName, "
                    + "    prt.otherFeeDescription, "
                    + "    prt.grandTotalAmount ";
            String qry = " "
                    + "  FROM pur_purchase_return prt "
                    + "  INNER JOIN mst_vendor ven ON prt.VendorCode = ven.Code "
                    + "  LEFT JOIN mst_chart_of_account coa ON coa.Code = prt.discountChartOfAccountCode "
                    + "  LEFT JOIN mst_chart_of_account othCoa ON othCoa.Code = prt.otherFeeChartOfAccountCode "
                    + ") prt "
                    + " WHERE "
                    + " prt.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
                    + " AND prt.vendorCode LIKE '%" + AppUtils.toString(obj, "vendorCode") + "%' "
                    + " AND prt.vendorName LIKE '%" + AppUtils.toString(obj, "vendorName") + "%' "
                    + " AND prt.refNo LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
                    + " AND prt.remark LIKE '%" + AppUtils.toString(obj, "remark") + "%' "
                    + " AND DATE(prt.transactionDate) BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') ";
            long countData = countResult(count + select + qry + sorte);
            return new PaginatedResults(listPagingResult(list + select + qry + sorte, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(prtd.code) FROM( SELECT";
            String list = "SELECT prtd.* FROM( SELECT ";
            String sorte = " ORDER BY prtd." + sort[0] + " " + sort[1];
            String select = " "
                    + "  prtd.code, "
                    + "  prtd.headerCode, "
                    + "  prtd.itemCode,  "
                    + "  item.Name AS itemName,  "
                    + "  item.Name AS itemAlias,  "
                    + "  prtd.quantity,  "
                    + "  item.unitOfMeasureCode, "
                    + "  prtd.price,  "
                    + "  prtd.totalAmount,  "
                    + "  prtd.remark,  "
                    + "  prtd.rackCode,  "
                    + "  rack.Name rackName  ";
            String qry = " "
                    + "  FROM pur_purchase_return_item_detail prtd "
                    + "  INNER JOIN mst_item item ON prtd.ItemCode=item.Code  "
                    + "  INNER JOIN mst_rack rack ON prtd.RackCode=rack.Code  "
                    + ") prtd "
                    + " WHERE prtd.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' ";
            long countData = countResult(count + select + qry + sorte);
            return new PaginatedResults(listPagingResult(list + select + qry + sorte, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT * FROM( "
                    + " SELECT "
                    + " prt.code, "
                    + " DATE_FORMAT(prt.transactionDate, '%d-%m-%Y') transactionDate, "
                    + " prt.branchCode, "
                    + " branch.Name AS branchName, "
                    + " prt.paymentTermCode,"
                    + " term.Name paymentTermName,"
                    + " term.Days paymentTermDays,"
                    + " DATE_FORMAT(prt.dueDate, '%d-%m-%Y') dueDate,"
                    + " prt.warehouseCode, "
                    + " IFNULL(war.name,'') AS warehouseName, "
                    + " war.DOCK_IN_Code AS rackCode, "
                    + " IFNULL(dockIn.Name,'') as rackName, "
                    + " prt.vendorInvoiceCode ,"
                    + " vin.vendorCode, "
                    + " IFNULL(ven.name,'') AS vendorName,"
                    + " vin.currencyCode, "
                    + " cur.Name currencyName,"
                    + " vin.exchangeRate, "
                    + " prt.refNo, "
                    + " prt.remark,"
                    + " prt.totalTransactionAmount, "
                    + " prt.discountPercent, "
                    + " prt.discountAmount, "
                    + " prt.discountChartOfAccountCode, "
                    + " coa.Name discountChartOfAccountName, "
                    + " prt.discountDescription, "
                    + " prt.taxBaseSubTotalAmount, "
                    + " prt.vatPercent, "
                    + " prt.vatAmount, "
                    + " prt.grandTotalAmount "
                    + " FROM pur_purchase_return prt "
                    + " LEFT JOIN mst_chart_of_account coa ON prt.discountChartOfAccountCode = coa.Code"
                    + " INNER JOIN fin_vendor_invoice vin ON prt.VendorInvoiceCode = vin.Code"
                    + " INNER JOIN mst_payment_term term ON prt.PaymentTermCode = term.Code "
                    + " INNER JOIN mst_warehouse war ON prt.warehouseCode = war.Code "
                    + " INNER JOIN mst_branch branch ON prt.branchCode = branch.Code "
                    + " LEFT JOIN mst_rack dockIn ON dockIn.Code = war.DOCK_IN_Code "
                    + " INNER JOIN mst_currency cur ON vin.CurrencyCode = cur.Code "
                    + " INNER JOIN mst_vendor ven ON vin.VendorCode = ven.Code  "
                    + "  ) qry "
                    + " WHERE "
                    + "  qry.code = '" + AppUtils.toString(obj, "code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Object dataVIN(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT * FROM( "
                    + " SELECT "
                    + "	  vin.code, "
                    + "	  vin.branchCode, "
                    + "	  branch.Name branchName, "
                    + "	  DATE_FORMAT(vin.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "	  vin.transactionDate searchDate, "
                    + "	  vin.purchaseOrderCode, "
                    + "	  vin.currencyCode, "
                    + "	  cur.Name currencyName, "
                    + "	  vin.vendorCode, "
                    + "	  ven.Name vendorName, "
                    + "	  vin.exchangeRate, "
                    + "	  vin.paymentTermCode, "
                    + "	  term.Name AS paymentTermName, "
                    + "	  term.Days AS paymentTermDays, "
                    + "	  vin.dueDate, "
                    + "	  vin.vendorInvoiceNo, "
                    + "	  vin.vendorInvoiceDate, "
                    + "	  vin.vendorTaxInvoiceNo, "
                    + "	  vin.vendorTaxInvoiceDate, "
                    + "	  vin.refno, "
                    + "	  vin.remark, "
                    + "	  vin.totalTransactionAmount, "
                    + "	  vin.discountPercent, "
                    + "	  vin.discountAmount, "
                    + "	  vin.discountChartOfAccountCode, "
                    + "	  discountCoa.Name discountChartOfAccountName, "
                    + "	  vin.discountDescription, "
                    + "	  vin.downPaymentAmount, "
                    + "	  vin.vatPercent, "
                    + "	  vin.vatAmount, "
                    + "	  vin.taxBaseSubTotalAmount, "
                    + "	  vin.otherFeeAmount, "
                    + "	  vin.otherFeeChartOfAccountCode, "
                    + "	  discountOther.Name otherFeeChartOfAccountName, "
                    + "	  vin.otherFeeDescription, "
                    + "	  vin.grandTotalAmount "
                    + "	FROM fin_vendor_invoice vin "
                    + "	INNER JOIN mst_vendor ven ON ven.Code = vin.VendorCode "
                    + "	INNER JOIN mst_currency cur ON cur.Code = vin.CurrencyCode "
                    + "	INNER JOIN mst_branch branch ON branch.Code = vin.BranchCode "
                    + "	INNER JOIN mst_payment_term term ON term.Code = vin.PaymentTermCode "
                    + "	INNER JOIN pur_purchase_order po ON po.Code = vin.PurchaseOrderCode "
                    + "	LEFT JOIN mst_chart_of_account discountCoa ON vin.DiscountChartOfAccountCode=discountCoa.Code "
                    + "	LEFT JOIN mst_chart_of_account discountOther ON vin.OtherFeeChartOfAccountCode=discountOther.Code "
                    + "  ) qry "
                    + " WHERE "
                    + "  qry.code = '" + AppUtils.toString(obj, "code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingVIN(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(vin.code) FROM( SELECT";
            String list = "SELECT vin.* FROM( SELECT ";
            String sorte = " ORDER BY vin." + sort[0] + " " + sort[1];
            String select = " "
                    + "	  vin.code, "
                    + "	  vin.branchCode, "
                    + "	  branch.Name branchName, "
                    + "	  DATE_FORMAT(vin.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "	  vin.transactionDate searchDate, "
                    + "	  vin.purchaseOrderCode, "
                    + "	  vin.currencyCode, "
                    + "	  cur.Name currencyName, "
                    + "	  vin.vendorCode, "
                    + "	  ven.Name vendorName, "
                    + "	  vin.exchangeRate, "
                    + "	  vin.paymentTermCode, "
                    + "	  term.Name AS paymentTermName, "
                    + "	  term.Days AS paymentTermDays, "
                    + "	  vin.dueDate, "
                    + "	  vin.vendorInvoiceNo, "
                    + "	  vin.vendorInvoiceDate, "
                    + "	  vin.vendorTaxInvoiceNo, "
                    + "	  vin.vendorTaxInvoiceDate, "
                    + "	  vin.refno, "
                    + "	  vin.remark, "
                    + "	  vin.totalTransactionAmount, "
                    + "	  vin.discountPercent, "
                    + "	  vin.discountAmount, "
                    + "	  vin.discountChartOfAccountCode, "
                    + "	  discountCoa.Name discountChartOfAccountName, "
                    + "	  vin.discountDescription, "
                    + "	  vin.downPaymentAmount, "
                    + "	  vin.vatPercent, "
                    + "	  vin.vatAmount, "
                    + "	  vin.taxBaseSubTotalAmount, "
                    + "	  vin.otherFeeAmount, "
                    + "	  vin.otherFeeChartOfAccountCode, "
                    + "	  discountOther.Name otherFeeChartOfAccountName, "
                    + "	  vin.otherFeeDescription, "
                    + "	  vin.grandTotalAmount ";
            String qry = " "
                    + "	FROM fin_vendor_invoice vin "
                    + "	INNER JOIN mst_vendor ven ON ven.Code = vin.VendorCode "
                    + "	INNER JOIN mst_currency cur ON cur.Code = vin.CurrencyCode "
                    + "	INNER JOIN mst_branch branch ON branch.Code = vin.BranchCode "
                    + "	INNER JOIN mst_payment_term term ON term.Code = vin.PaymentTermCode "
                    + "	INNER JOIN pur_purchase_order po ON po.Code = vin.PurchaseOrderCode "
                    + "	LEFT JOIN mst_chart_of_account discountCoa ON vin.DiscountChartOfAccountCode=discountCoa.Code "
                    + "	LEFT JOIN mst_chart_of_account discountOther ON vin.OtherFeeChartOfAccountCode=discountOther.Code "
                    + ") vin "
                    + " WHERE "
                    + "  vin.Code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
                    + "  AND vin.searchDate BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND DATE('" + AppUtils.toString(obj, "transactionEndDate") + "')  ";
            long countData = countResult(count + select + qry + sorte);
            return new PaginatedResults(listPagingResult(list + select + qry + sorte, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingDetailInv(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(vinid.code) FROM( SELECT";
            String list = "SELECT vinid.* FROM( SELECT ";
            String sorte = " GROUP BY vinid.GoodsReceivedNoteDetailCode ORDER BY vinid." + sort[0] + " " + sort[1];
            String select = " "
                    + "vinid.code, "
                    + "vinid.headerCode,  "
                    + "vinid.itemCode,  "
                    + "mst_item.Name AS itemName, "
                    + "mst_item.Name AS itemAlias, "
                    + "mst_item.inventoryType, "
                    + "mst_item.unitOfMeasureCode, "
                    + "mst_item.itemBrandCode, "
                    + "mst_item_brand.Name AS itemBrandName, "
                    + "vinid.remark , "
                    + "vinid.goodsReceivedNoteDetailCode , "
                    + "vinid.quantity, "
                    + "IFNULL(purchase_return.Quantity,0) AS prtQuantity, "
                    + "vinid.Quantity - ifnull(purchase_return.Quantity,0) AS balancedQuantity, "
                    + "vinid.price, "
                    + "vinid.discount1Percent, "
                    + "vinid.discount1Amount, "
                    + "vinid.discount2Percent, "
                    + "vinid.discount2Amount, "
                    + "vinid.discount3Percent, "
                    + "vinid.discount3Amount, "
                    + "vinid.nettPrice, "
                    + "IFNULL(vinid.TotalAmount,0) AS totalAmount, "
                    + "mst_item.COGSIDR AS cogsIdr ";
            String qry = " "
                    + "  FROM fin_vendor_invoice_item_detail vinid "
                    + "  LEFT JOIN("
                    + "		SELECT"
                    + "			pur_purchase_return.Code,"
                    + "			pur_purchase_return.VendorInvoiceCode,"
                    + "			pur_purchase_return_item_detail.VendorInvoiceItemDetailCode,"
                    + "			pur_purchase_return_item_detail.ItemCode,"
                    + "			SUM(pur_purchase_return_item_detail.Quantity)AS Quantity"
                    + "		FROM pur_purchase_return"
                    + "		INNER JOIN pur_purchase_return_item_detail ON pur_purchase_return.Code=pur_purchase_return_item_detail.HeaderCode"
                    + "		where pur_purchase_return.VendorInvoiceCode = '" + obj.get("headerCode") + "' "
                    + "		GROUP BY pur_purchase_return_item_detail.VendorInvoiceItemDetailCode"
                    + "	)as purchase_return on vinid.Headercode=purchase_return.VendorInvoiceCode"
                    + "	 and vinid.ItemCode=purchase_return.ItemCode and vinid.Code= purchase_return.VendorInvoiceItemDetailCode "
                    + "	INNER JOIN mst_item ON mst_item.Code = vinid.ItemCode "
                    + "	LEFT JOIN mst_item_brand ON mst_item_brand.Code = mst_item.ItemBrandCode "
                    + "	LEFT JOIN ivt_goods_received_note_item_detail grnD ON grnD.Code = vinid.GoodsReceivedNoteDetailCode "
                    + ") vinid "
                    + "WHERE vinid.HeaderCode = '" + obj.get("headerCode") + "' ";
            long countData = countResult(count + select + qry + sorte);
            return new PaginatedResults(listPagingResult(list + select + qry + sorte, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT * FROM( "
                    + "  SELECT "
                    + "    pur_purchase_return.code, "
                    + "    pur_purchase_return.transactionDate, "
                    + "    pur_purchase_return.branchCode, "
                    + "    pur_purchase_return.divisionCode, "
                    + "    IFNULL(mst_division.name,'') AS divisionName, "
                    + "    pur_purchase_return.requestBy, "
                    + "    pur_purchase_return.refNo, "
                    + "    pur_purchase_return.remark, "
                    + "    pur_purchase_return.approvalReasonCode, "
                    + "    pur_purchase_return.approvalStatus, "
                    + "    pur_purchase_return.approvalDate, "
                    + "    pur_purchase_return.approvalBy, "
                    + "    pur_purchase_return.approvalRemark, "
                    + "    pur_purchase_return.closingStatus, "
                    + "    pur_purchase_return.closingDate, "
                    + "    pur_purchase_return.closingBy, "
                    + "    pur_purchase_return.closingRemark "
                    + "  FROM pur_purchase_return "
                    + "  INNER JOIN mst_division ON pur_purchase_return.ItemDivisionCode = mst_division.Code "
                    + "  LEFT JOIN mst_approval_reason ON pur_purchase_return.approvalReasonCode = mst_approval_reason.Code "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
                    + " AND qry.transactionDate BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') "
                    + " AND qry.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
                    + " AND qry.divisionName LIKE '%" + AppUtils.toString(obj, "divisionName") + "%' "
                    + " AND qry.refNo LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
                    + " AND qry.remark LIKE '%" + AppUtils.toString(obj, "remark") + "%' ";

            qry += AppUtils.toString(obj, "approvalStatus") == "" ? "" : " AND qry.closingStatus = '" + AppUtils.toString(obj, "approvalStatus") + "'";
            qry += AppUtils.toString(obj, "closingStatus") == "" ? "" : " AND qry.closingStatus = '" + AppUtils.toString(obj, "closingStatus") + "'";
            qry += " ORDER BY qry." + sort[0] + " " + sort[1];
//            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
