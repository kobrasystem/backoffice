/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.repo;

import com.system.api.purchase.model.PurchaseRequestDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lukassh
 */
public interface PurchaseRequestDetailRepo extends JpaRepository<PurchaseRequestDetail, String> {

    @Query("SELECT m FROM PurchaseRequestDetail m WHERE m.headerCode = :headerCode ")
    List<PurchaseRequestDetail> findDataListByCode(@Param("headerCode") String code);

    @Query("SELECT m FROM PurchaseRequestDetail m WHERE m.headerCode = :code ")
    List<PurchaseRequestDetail> findDetailByCode(@Param("code") String code);

    @Query("SELECT m FROM PurchaseRequestDetail m WHERE m.code = :code ")
    PurchaseRequestDetail findByCode(@Param("code") String code);

    @Transactional
    void deleteByHeaderCode(String code);

    @Transactional
    void deleteByCode(String code);

}
