/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Imam Solikhin
 */
@Data
@Entity
@Table(name = "pur_purchase_order")
public class PurchaseOrder extends GroupModel {

    @Id
    @Column(name = "id")
    @SheetColumn("id")
    private String id = "";
    
    @Column(name = "code")
    @SheetColumn("Code")
    private String code = "";

    @Column(name = "transactionDate")
    @SheetColumn("TransactionDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date transactionDate = new Date();

    @Column(name = "purchaseRequestCode")
    @SheetColumn("purchaseRequestCode")
    private String purchaseRequestCode = "";

    @Column(name = "branchCode")
    @SheetColumn("BranchCode")
    private String branchCode = "";

    @Column(name = "deliveryDateStart")
    @SheetColumn("DeliveryDateStart")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date deliveryDateStart = new Date();

    @Column(name = "deliveryDateEnd")
    @SheetColumn("DeliveryDateEnd")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date deliveryDateEnd = new Date();

    @Column(name = "paymentTermCode")
    @SheetColumn("PaymentTermCode")
    private String paymentTermCode = "";

    @Column(name = "currencyCode")
    @SheetColumn("CurrencyCode")
    private String currencyCode = "";

    @Column(name = "vendorCode")
    @SheetColumn("VendorCode")
    private String vendorCode = "";

    @Column(name = "vendorPromoCode")
    @SheetColumn("VendorPromoCode")
    private String vendorPromoCode = "";

    @Column(name = "discountType")
    @SheetColumn("DiscountType")
    private String discountType = "";

    @Column(name = "billToCode")
    @SheetColumn("BillToCode")
    private String billToCode = "";

    @Column(name = "shipToCode")
    @SheetColumn("ShipToCode")
    private String shipToCode = "";

    @Column(name = "expeditionCode")
    @SheetColumn("ExpeditionCode")
    private String expeditionCode = "";

    @Column(name = "refNo")
    @SheetColumn("RefNo")
    private String refNo = "";

    @Column(name = "remark")
    @SheetColumn("Remark")
    private String remark = "";

    @Column(name = "totalTransactionAmount")
    @SheetColumn("TotalTransactionAmount")
    private BigDecimal totalTransactionAmount = new BigDecimal(0.00);

    @Column(name = "discountPercent")
    @SheetColumn("DiscountPercent")
    private BigDecimal discountPercent = new BigDecimal(0.00);

    @Column(name = "discountAmount")
    @SheetColumn("DiscountAmount")
    private BigDecimal discountAmount = new BigDecimal(0.00);

    @Column(name = "discountChartOfAccountCode")
    @SheetColumn("DiscountChartOfAccountCode")
    private String discountChartOfAccountCode = "";

    @Column(name = "discountDescription")
    @SheetColumn("DiscountDescription")
    private String discountDescription = "";

    @Column(name = "taxBaseSubTotalAmount")
    @SheetColumn("TaxBaseSubTotalAmount")
    private BigDecimal taxBaseSubTotalAmount = new BigDecimal(0.00);

    @Column(name = "pphPercent")
    @SheetColumn("PPhPercent")
    private BigDecimal pphPercent = new BigDecimal(0.00);

    @Column(name = "pphAmount")
    @SheetColumn("PPhAmount")
    private BigDecimal pphAmount = new BigDecimal(0.00);

    @Column(name = "vatPercent")
    @SheetColumn("VATPercent")
    private BigDecimal vatPercent = new BigDecimal(0.00);

    @Column(name = "vatAmount")
    @SheetColumn("VATAmount")
    private BigDecimal vatAmount = new BigDecimal(0.00);

    @Column(name = "pphChartOfAccountCode")
    @SheetColumn("PPhChartOfAccountCode")
    private String pphChartOfAccountCode = "";

    @Column(name = "pphDescription")
    @SheetColumn("PPhDescription")
    private String pphDescription = "";

    @Column(name = "otherFeeAmount")
    @SheetColumn("OtherFeeAmount")
    private BigDecimal otherFeeAmount = new BigDecimal(0.00);

    @Column(name = "otherFeeChartOfAccountCode")
    @SheetColumn("OtherFeeChartOfAccountCode")
    private String otherFeeChartOfAccountCode = "";

    @Column(name = "otherFeeDescription")
    @SheetColumn("OtherFeeDescription")
    private String otherFeeDescription = "";

    @Column(name = "grandTotalAmount")
    @SheetColumn("GrandTotalAmount")
    private BigDecimal grandTotalAmount = new BigDecimal(0.00);

    @Column(name = "approvalStatus")
    @SheetColumn("ApprovalStatus")
    private String approvalStatus = "PENDING";

    @Column(name = "approvalBy")
    @SheetColumn("ApprovalBy")
    private String approvalBy = "";

    @Column(name = "approvalDate")
    @SheetColumn("ApprovalDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date approvalDate = new Date();

    @Column(name = "approvalRemark")
    @SheetColumn("ApprovalRemark")
    private String approvalRemark = "";

    @Column(name = "approvalReasonCode")
    @SheetColumn("ApprovalReasonCode")
    private String approvalReasonCode = "";

    @Column(name = "closingStatus")
    @SheetColumn("ClosingStatus")
    private String closingStatus = "OPEN";

    @Column(name = "closingDate")
    @SheetColumn("ClosingDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date closingDate = new Date();

    @Column(name = "closingBy")
    @SheetColumn("ClosingBy")
    private String closingBy = "";

    @Column(name = "closingRemark")
    @SheetColumn("ClosingRemark")
    private String closingRemark = "";

    private transient List<PurchaseOrderDetail> listPurchaseOrderDetail;
    }
