/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.repo;

import java.util.List;
import com.system.api.purchase.model.PurchaseReturn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rayis
 */
public interface PurchaseReturnRepo extends JpaRepository<PurchaseReturn, String> {

  @Query("SELECT m FROM PurchaseReturn m WHERE m.code = :code ")
  PurchaseReturn findByCode(@Param("code") String code);
    
  @Query("SELECT max(m) FROM PurchaseReturn m WHERE m.code LIKE :prefixCode% ")
  List<PurchaseReturn> findMaxCode(@Param("prefixCode") String prefixCode);
  
  @Query(value = ""
            + " SELECT "
                    + " prt.code, "
                    + "	prt.branchCode, "
                    + "	prt.transactionDate, "
                    + "	prt.warehouseCode, "
                    + "	mst_warehouse.DOCK_IN_Code AS rackCode, "
                    + "	prt.vendorInvoiceCode, "
                    + "	prt.paymentTermCode, "
                    + "	prt.dueDate, "
                    + "	prt.exchangeRate, "
                    + "	prt.vendorInvoiceNo, "
                    + "	prt.taxInvoiceNo, "
                    + "	prt.vendorCode, "
                    + "	prt.currencyCode, "
                    + "	prt.totalTransactionAmount, "
                    + "	prt.taxBaseSubTotalAmount, "
                    + "	prt.discountPercent, "
                    + "	prt.discountAmount, "
                    + "	prt.vatPercent, "
                    + "	prt.vatAmount, "
                    + "	prt.discountChartOfAccountCode, "
                    + "	prt.discountDescription, "
                    + "	prt.otherFeeChartOfAccountCode, "
                    + "	prt.otherFeeDescription, "
                    + "	prt.otherFeeAmount, "
                    + "	prt.paidAmount, "
                    + "	prt.settlementDate, "
                    + "	prt.settlementDocumentNo, "
                    + "	prt.grandTotalAmount, "
                    + "	prt.refNo, "
                    + "	prt.remark, "
                    + "	prt.createdBy, "
                    + "	prt.createdDate, "
                    + "	prt.updatedBy, "
                    + "	prt.updatedDate "
                    + " FROM pur_purchase_return prt "
                    + " INNER JOIN mst_warehouse ON mst_warehouse.Code = prt.WarehouseCode "
                    + " WHERE prt.code = :code ", nativeQuery = true)
    PurchaseReturn findByCodes(@Param("code") String code);

  @Transactional
  void deleteByCode(String code);
    
}
