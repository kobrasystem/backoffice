/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.repo;

import java.util.List;
import org.springframework.data.repository.query.Param;
import com.system.api.purchase.model.PurchaseReturnDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rayis
 */
public interface PurchaseReturnDetailRepo extends JpaRepository<PurchaseReturnDetail, String> {

  List<PurchaseReturnDetail> findAllByCode(@Param("code") String code);

  PurchaseReturnDetail findByCode(@Param("code") String code);

  @Transactional
  void deleteByHeaderCode(String code);

  @Transactional
  void deleteByCode(String code);

}
