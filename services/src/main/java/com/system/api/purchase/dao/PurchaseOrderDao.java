/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class PurchaseOrderDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM pur_purchase_order WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT COUNT(qry.code) FROM ( "
                    + " SELECT "
                    + "   po.code, "
                    + "   DATE_FORMAT(po.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "   po.transactionDate searchDate, "
                    + "   po.vendorCode, "
                    + "   vend.Name vendorName, "
                    + "   po.currencyCode, "
                    + "   curr.Name currencyName, "
                    + "   po.refNo, "
                    + "   po.remark, "
                    + "   po.approvalStatus, "
                    + "   po.closingStatus "
                    + "  FROM pur_purchase_order po "
                    + "  INNER JOIN mst_branch br ON br.Code = po.BranchCode "
                    + "  INNER JOIN mst_vendor vend ON vend.Code = po.VendorCode "
                    + "  INNER JOIN mst_currency curr ON curr.Code = po.CurrencyCode "
                    + "  LEFT JOIN mst_chart_of_account coa ON coa.Code = po.discountChartOfAccountCode "
                    + "  LEFT JOIN mst_chart_of_account pphCoa ON pphCoa.Code = po.pphChartOfAccountCode "
                    + "  LEFT JOIN mst_chart_of_account othCoa ON othCoa.Code = po.otherFeeChartOfAccountCode "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
                    + " AND DATE(qry.searchDate) BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND  DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') "
                    + " AND IFNULL(qry.vendorCode,'') LIKE '%" + AppUtils.toString(obj, "vendorCode") + "%' "
                    + " AND IFNULL(qry.vendorName,'') LIKE '%" + AppUtils.toString(obj, "vendorName") + "%' "
                    + " AND IFNULL(qry.currencyCode,'') LIKE '%" + AppUtils.toString(obj, "currencyCode") + "%' "
                    + " AND IFNULL(qry.currencyName,'') LIKE '%" + AppUtils.toString(obj, "currencyName") + "%' "
                    + " AND IFNULL(qry.refNo,'') LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
                    + " AND IFNULL(qry.remark,'') LIKE '%" + AppUtils.toString(obj, "remark") + "%' ";

            qry += AppUtils.toString(obj, "approvalStatus").toLowerCase().equals("all") || AppUtils.toString(obj, "approvalStatus").equals("") ? "" : " AND qry.approvalStatus = '" + AppUtils.toString(obj, "approvalStatus") + "'";
            qry += AppUtils.toString(obj, "closingStatus").toLowerCase().equals("all") || AppUtils.toString(obj, "closingStatus").equals("") ? "" : " AND qry.closingStatus = '" + AppUtils.toString(obj, "closingStatus") + "'";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {

            String approvalStatus = AppUtils.toString(obj, "approvalStatus").toLowerCase().equals("all") || AppUtils.toString(obj, "approvalStatus").equals("") ? "" : " AND qry.approvalStatus = '" + AppUtils.toString(obj, "approvalStatus") + "'";
            String closingStatus = AppUtils.toString(obj, "closingStatus").toLowerCase().equals("all") || AppUtils.toString(obj, "closingStatus").equals("") ? "" : " AND qry.closingStatus = '" + AppUtils.toString(obj, "closingStatus") + "'";
            String qry = " "
                    + " SELECT * FROM ( "
                    + "  SELECT "
                    + "    po.code, "
                    + "    DATE_FORMAT(po.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    po.transactionDate searchDate, "
                    + "    po.branchCode, "
                    + "    br.Name branchName, "
                    + "    DATE_FORMAT(po.deliveryDateStart, '%d-%m-%Y') deliveryDateStart, "
                    + "    DATE_FORMAT(po.deliveryDateEnd, '%d-%m-%Y') deliveryDateEnd, "
                    + "    po.paymentTermCode, "
                    + "    po.currencyCode, "
                    + "    curr.Name currencyName, "
                    + "    po.vendorCode, "
                    + "    vend.Name vendorName, "
                    + "    vend.DefaultContactPersonCode vendorContact, "
                    + "    vend.Address vendorAddress, "
                    + "    vend.Phone1 vendorPhone1, "
                    + "    vend.Phone2 vendorPhone2, "
                    + "    po.vendorPromoCode, "
                    + "    po.discountType, "
                    + "    po.billToCode, "
                    + "    po.shipToCode, "
                    + "    po.expeditionCode, "
                    + "    po.refNo, "
                    + "    po.remark, "
                    + "    po.approvalReasonCode, "
                    + "    po.approvalStatus, "
                    + "    IFNULL(DATE_FORMAT(po.approvalDate, '%d-%m-%Y'),'') approvalDate, "
                    + "    po.approvalBy, "
                    + "    po.approvalRemark, "
                    + "    po.closingStatus, "
                    + "    IFNULL(DATE_FORMAT(po.closingDate, '%d-%m-%Y'),'') closingDate, "
                    + "    po.closingBy, "
                    + "    po.closingRemark, "
                    + "    po.totalTransactionAmount, "
                    + "    po.discountPercent, "
                    + "    po.discountAmount, "
                    + "    po.discountChartOfAccountCode, "
                    + "    coa.Name discountChartOfAccountName, "
                    + "    po.discountDescription, "
                    + "    po.taxBaseSubTotalAmount, "
                    + "    po.pphPercent, "
                    + "    po.pphAmount, "
                    + "    po.vatPercent, "
                    + "    po.vatAmount, "
                    + "    po.pphChartOfAccountCode, "
                    + "    pphCoa.Name pphChartOfAccountName, "
                    + "    po.pphDescription, "
                    + "    po.otherFeeAmount, "
                    + "    po.otherFeeChartOfAccountCode, "
                    + "    othCoa.Name otherFeeChartOfAccountName, "
                    + "    po.otherFeeDescription, "
                    + "    po.grandTotalAmount, "
                    + "    IFNULL(grn.goodsReceivedNoteCode,'-') AS goodsReceivedNoteCode "
                    + "  FROM pur_purchase_order po "
                    + "  INNER JOIN mst_branch br ON br.Code = po.BranchCode "
                    + "  INNER JOIN mst_vendor vend ON vend.Code = po.VendorCode "
                    + "  INNER JOIN mst_currency curr ON curr.Code = po.CurrencyCode "
                    + "  LEFT JOIN mst_chart_of_account coa ON coa.Code = po.discountChartOfAccountCode "
                    + "  LEFT JOIN mst_chart_of_account pphCoa ON pphCoa.Code = po.pphChartOfAccountCode "
                    + "  LEFT JOIN mst_chart_of_account othCoa ON othCoa.Code = po.otherFeeChartOfAccountCode "
                    + "  LEFT JOIN "
                    + "  ( SELECT GROUP_CONCAT(DISTINCT grn.Code ORDER BY  grn.Code ASC) AS goodsReceivedNoteCode, grn.PurchaseOrderCode "
                    + "  FROM ivt_goods_received_note grn ORDER BY grn.Code ASC "
                    + "  )grn ON grn.PurchaseOrderCode = po.Code "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
                    + " AND DATE(qry.searchDate) BETWEEN DATE('" + AppUtils.toString(obj, "transactionStartDate") + "') AND  DATE('" + AppUtils.toString(obj, "transactionEndDate") + "') "
                    + " AND IFNULL(qry.vendorCode,'') LIKE '%" + AppUtils.toString(obj, "vendorCode") + "%' "
                    + " AND IFNULL(qry.vendorName,'') LIKE '%" + AppUtils.toString(obj, "vendorName") + "%' "
                    + " AND IFNULL(qry.currencyCode,'') LIKE '%" + AppUtils.toString(obj, "currencyCode") + "%' "
                    + " AND IFNULL(qry.currencyName,'') LIKE '%" + AppUtils.toString(obj, "currencyName") + "%' "
                    + " AND IFNULL(qry.refNo,'') LIKE '%" + AppUtils.toString(obj, "refNo") + "%' "
                    + " AND IFNULL(qry.remark,'') LIKE '%" + AppUtils.toString(obj, "remark") + "%' "
                    + approvalStatus
                    + closingStatus
                    + " ORDER BY qry." + sort[0] + " " + sort[1];

            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingLookupForInvoice(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(*) ";
            String select = "SELECT * ";
            String sortby = " GROUP BY qry.code ORDER BY qry.code ASC ";
            String qry = " "
                    + "FROM(    "
                    + "	SELECT "
                    + "	    pur_purchase_order.code, "
                    + "	    DATE_FORMAT(pur_purchase_order.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "	    pur_purchase_order.branchCode, "
                    + "	    mst_branch.Name branchName, "
                    + "	    DATE_FORMAT(pur_purchase_order.deliveryDateStart, '%d-%m-%Y') deliveryDateStart, "
                    + "	    DATE_FORMAT(pur_purchase_order.deliveryDateEnd, '%d-%m-%Y') deliveryDateEnd, "
                    + "	    pur_purchase_order.paymentTermCode, "
                    + "     mst_payment_term.Name paymentTermName, "
                    + "	    mst_payment_term.Days paymentTermDays, "
                    + "	    pur_purchase_order.currencyCode, "
                    + "	    mst_currency.Name currencyName, "
                    + "	    pur_purchase_order.vendorCode, "
                    + "	    mst_vendor.Name vendorName, "
                    + "	    mst_vendor.DefaultContactPersonCode vendorContactPerson, "
                    + "	    mst_vendor.Address vendorAddress, "
                    + "	    mst_vendor.Phone1 vendorPhone1, "
                    + "	    mst_vendor.Phone2 vendorPhone2, "
                    + "	    pur_purchase_order.discountType, "
                    + "	    IFNULL(pur_purchase_order.refNo,'') refNo, "
                    + "	    IFNULL(pur_purchase_order.remark,'') remark, "
                    + "	    pur_purchase_order.totalTransactionAmount, "
                    + "	    pur_purchase_order.discountPercent, "
                    + "	    pur_purchase_order.discountAmount, "
                    + "	    pur_purchase_order.discountChartOfAccountCode, "
                    + "	    discountChartOfAccount.Name discountChartOfAccountName, "
                    + "	    pur_purchase_order.discountDescription, "
                    + "	    pur_purchase_order.taxBaseSubTotalAmount, "
                    + "	    pur_purchase_order.vatPercent, "
                    + "	    pur_purchase_order.vatAmount, "
                    + "	    pur_purchase_order.otherFeeAmount, "
                    + "	    pur_purchase_order.otherFeeChartOfAccountCode, "
                    + "	    otherFeeChartOfAccount.Name otherFeeChartOfAccountName, "
                    + "	    pur_purchase_order.otherFeeDescription, "
                    + "	    IFNULL(goods_received_note.countGrn,0) countGrn, "
                    + "	    IFNULL(vendor_invoice_jn_goods_received_note.countInvoiceGrn,0) countInvoiceGrn "
                    + "	FROM pur_purchase_order "
                    + " LEFT JOIN( "
                    + "     SELECT "
                    + "		ivt_goods_received_note.PurchaseOrderCode, "
                    + "		GROUP_CONCAT(IFNULL(ivt_goods_received_note.Code,'')) AS grnCode, "
                    + "		COUNT(ivt_goods_received_note.Code)AS countGrn "
                    + "     FROM ivt_goods_received_note "
                    + "     GROUP BY ivt_goods_received_note.PurchaseOrderCode "
                    + " )AS goods_received_note ON pur_purchase_order.Code=goods_received_note.PurchaseOrderCode "
                    + " LEFT JOIN( "
                    + "     SELECT "
                    + "		fin_vendor_invoice.PurchaseOrderCode, "
                    + "		COUNT(fin_vendor_invoice_jn_goods_received_note.GoodsReceivedNoteCode)AS CountInvoiceGrn "
                    + "     FROM fin_vendor_invoice "
                    + "     INNER JOIN fin_vendor_invoice_jn_goods_received_note ON fin_vendor_invoice.Code=fin_vendor_invoice_jn_goods_received_note.Headercode "
                    + "     GROUP BY fin_vendor_invoice.PurchaseOrderCode "
                    + " )AS vendor_invoice_jn_goods_received_note ON pur_purchase_order.Code=vendor_invoice_jn_goods_received_note.PurchaseOrderCode"
                    + " LEFT JOIN fin_vendor_invoice ON pur_purchase_order.Code=fin_vendor_invoice.PurchaseOrderCode "
                    + "	INNER JOIN mst_branch ON mst_branch.Code = pur_purchase_order.BranchCode    "
                    + "	INNER JOIN mst_vendor ON mst_vendor.Code = pur_purchase_order.VendorCode    "
                    + "	INNER JOIN mst_currency ON mst_currency.Code = pur_purchase_order.CurrencyCode "
                    + " INNER JOIN mst_payment_term ON pur_purchase_order.PaymentTermCode=mst_payment_term.Code "
                    + "	LEFT JOIN mst_chart_of_account discountChartOfAccount ON discountChartOfAccount.Code = pur_purchase_order.discountChartOfAccountCode    "
                    + "	LEFT JOIN mst_chart_of_account otherFeeChartOfAccount ON otherFeeChartOfAccount.Code = pur_purchase_order.otherFeeChartOfAccountCode "
                    + ") qry   "
                    + "WHERE qry.code LIKE '%" + AppUtils.toString(obj, "code") + "%'  "
                    + "	AND qry.vendorCode LIKE '%" + AppUtils.toString(obj, "vendorCode") + "%'   "
                    + "	AND qry.vendorName LIKE '%" + AppUtils.toString(obj, "vendorName") + "%' "
                    + "	AND qry.refNo LIKE '%" + AppUtils.toString(obj, "refNo") + "%'   "
                    + "	AND qry.remark LIKE '%" + AppUtils.toString(obj, "remark") + "%' "
                    + " AND IFNULL(qry.countGrn,0) > 0 "
                    + " AND IFNULL(qry.countGrn,0) - IFNULL(qry.countInvoiceGrn,0) >0 "
                    ;
            long countData = countResult(count + qry);
            return new PaginatedResults(listPagingResult(select + qry + sortby, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countPrqDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT COUNT(qry.code) FROM ( "
                    + " SELECT "
                    + "   popr.code, "
                    + "   popr.purchaseOrderCode headerCode "
                    + "  FROM pur_purchase_order_jn_purchase_request popr "
                    + "  INNER JOIN pur_purchase_request pr on pr.Code = popr.PurchaseRequestCode "
                    + "  INNER JOIN mst_division divs on divs.Code = pr.DivisionCode "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' ";
            long countPrqDetail = countResult(qry);
            return countPrqDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingPrqDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM ( "
                    + "  SELECT "
                    + "   popr.code, "
                    + "   popr.purchaseOrderCode headerCode, "
                    + "   popr.purchaseRequestCode, "
                    + "   popr.branchCode, "
                    + "   DATE_FORMAT(pr.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "   pr.divisionCode, "
                    + "   divs.Name divisionName, "
                    + "   pr.requestBy, "
                    + "   pr.remark, "
                    + "   pr.refNo "
                    + "  FROM pur_purchase_order_jn_purchase_request popr "
                    + "  INNER JOIN pur_purchase_request pr on pr.Code = popr.PurchaseRequestCode "
                    + "  INNER JOIN mst_division divs on divs.Code = pr.DivisionCode "
                    + "  ) qry "
                    + " WHERE "
                    + "  qry.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' "
                    + " ORDER BY qry." + sort[0] + " " + sort[1];
            long countPrqDetail = this.countPrqDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countPrqDetail, totalPage(countPrqDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT COUNT(qry.code) FROM ( "
                    + " SELECT "
                    + "   pd.code, "
                    + "   pd.headerCode "
                    + "  FROM pur_purchase_order_detail pd "
                    + "  INNER JOIN mst_item item on item.Code = pd.ItemCode "
                    + "  INNER JOIN pur_purchase_request_detail prqd ON prqd.Code = pd.purchaseRequestDetailCode "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' ";
            long countDataDetail = countResult(qry);
            return countDataDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM ( "
                    + "  SELECT "
                    + "   pd.code, "
                    + "   pd.headerCode, "
                    + "   pd.purchaseRequestDetailCode, "
                    + "   prqd.headerCode purchaseRequestCode, "
                    + "   pd.itemCode, "
                    + "   item.Name itemName, "
                    + "   pd.itemAlias, "
                    + "   pd.remark, "
                    + "   pd.quantity, "
                    + "   item.unitOfMeasureCode, "
                    + "   pd.price, "
                    + "   pd.discount1Percent, "
                    + "   pd.discount1Amount, "
                    + "   pd.discount2Percent, "
                    + "   pd.discount2Amount, "
                    + "   pd.discount3Percent, "
                    + "   pd.discount3Amount, "
                    + "   pd.nettPrice, "
                    + "   pd.totalAmount, "
                    + "   pd.discountHeaderPercent, "
                    + "   pd.discountHeaderAmount "
                    + "  FROM pur_purchase_order_detail pd "
                    + "  INNER JOIN mst_item item on item.Code = pd.ItemCode "
                    + "  INNER JOIN pur_purchase_request_detail prqd ON prqd.Code = pd.purchaseRequestDetailCode "
                    + "  ) qry "
                    + " WHERE "
                    + "  qry.headerCode = '" + AppUtils.toString(obj, "headerCode") + "' "
                    + " ORDER BY qry." + sort[0] + " " + sort[1];
            long countDataDetail = this.countDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countDataDetail, totalPage(countDataDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingVdpDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(vdp.code) FROM( SELECT";
            String list = "SELECT vdp.* FROM( SELECT ";
            String select = " "
                    + "IFNULL(vdp.Code,'') code, "
                    + "IFNULL(vdp.Code,'') vendorDownPaymentCode, "
                    + "IFNULL(DATE_FORMAT(vdp.TransactionDate, '%d-%m-%Y'),'') transactionDate, "
                    + "IFNULL(vdp.VendorCode,'') vendorCode, "
                    + "IFNULL(vdp.CurrencyCode,'') currencyCode, "
                    + "IFNULL(vdp.ExchangeRate,0) exchangeRate, "
                    + "IFNULL(vdp.TotalTransactionAmount,0) vdpAmount, "
                    + "IFNULL(used.UsedAmount,0) usedAmount, "
                    + "(IFNULL(used.TotalTransactionAmount,0) - IFNULL(used.UsedAmount,0)) balanceAmount, "
                    + "IFNULL(paid.GrandTotalAmount,0) grandTotalAmount, "
                    + "IFNULL(paid.PaidAmount,0) paidAmount ";
            String qry = " "
                    + "FROM fin_vendor_down_payment vdp "
                    + "INNER JOIN fin_vendor_down_payment_used used ON used.Code = vdp.Code "
                    + "INNER JOIN fin_vendor_down_payment_paid paid ON paid.Code = vdp.Code "
                    + ")vdp "
                    + "WHERE vdp.VendorCode = '" + obj.get("headerCode") + "' "
                    + "AND vdp.GrandTotalAmount - vdp.PaidAmount > 0 "
                    + "ORDER BY vdp." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countItemPrq(JSONObject obj, Class<?> module) {
        try {

            String headerCode = "";
            JSONArray jsonArray = obj.getJSONArray("detailCode");
            String strDataList = jsonArray.toString();
            strDataList = strDataList.replaceAll(" ", "");
            strDataList = strDataList.replaceAll("[\\[\\]]", "");

            headerCode = "  prDetailQry.headerCode IN( " + strDataList + " )";
            String qry = " "
                    + " SELECT  COUNT(prDetailQry.code) FROM( "
                    + " SELECT "
                    + "  prtd.code, "
                    + "  prtd.code purchaseRequestDetailCode, "
                    + "  prtd.headerCode "
                    + "  FROM pur_purchase_request_detail prtd  "
                    + "  INNER JOIN pur_purchase_request ON prtd.`HeaderCode`=pur_purchase_request.Code  "
                    + "  INNER JOIN mst_item ON prtd.ItemCode=mst_item.Code  "
                    + "  LEFT JOIN( "
                    + " 	SELECT "
                    + " 		mst_item_jn_current_stock.ItemCode, "
                    + " 		SUM(mst_item_jn_current_stock.ActualStock) AS actualStock "
                    + " 	FROM "
                    + " 		mst_item_jn_current_stock "
                    + " 	WHERE "
                    + " 		mst_item_jn_current_stock.RackCode LIKE '%-DI%' "
                    + " 	GROUP BY mst_item_jn_current_stock.ItemCode "
                    + "  ) AS onHandStok ON onHandStok.ItemCode = prtd.ItemCode  "
                    + "  LEFT JOIN ( "
                    + " 	 SELECT  "
                    + " 	 pur_purchase_order_detail.`ItemCode`, "
                    + " 	 pur_purchase_order_detail.`Price`, "
                    + " 	 SUM(pur_purchase_order_detail.`Quantity`) AS quantity, "
                    + " 	 pur_purchase_order_detail.`PurchaseRequestDetailCode` "
                    + " 	 FROM pur_purchase_order_detail "
                    + " 	 GROUP BY pur_purchase_order_detail.`PurchaseRequestDetailCode` "
                    + "  )qryPo ON qryPo.PurchaseRequestDetailCode=prtd.code "
                    + "  )prDetailQry "
                    + "  WHERE " + headerCode;
            long countItemPrq = countResult(qry);
            return countItemPrq;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingItemPrq(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String headerCode = "";
            JSONArray jsonArray = obj.getJSONArray("detailCode");
            String strDataList = jsonArray.toString();
            strDataList = strDataList.replaceAll(" ", "");
            strDataList = strDataList.replaceAll("[\\[\\]]", "");

            headerCode = "  prDetailQry.headerCode IN( " + strDataList + " )";
            String qry = " "
                    + "  SELECT * FROM( "
                    + "  SELECT "
                    + "  prtd.code, "
                    + "  prtd.headerCode, "
                    + "  prtd.code purchaseRequestDetailCode, "
                    + "  prtd.headerCode purchaseRequestCode, "
                    + "  prtd.itemCode,  "
                    + "  mst_item.Name AS itemName,  "
                    + "  prtd.itemAlias,  "
                    + "  prtd.remark,  "
                    + "  prtd.quantity,  "
                    + "  IFNULL(qryPo.Quantity,0) AS poQuantity,  "
                    + "  IFNULL(prtd.Quantity,0) - IFNULL(qryPo.`Quantity`,0) AS outStandingPoQuantity,  "
                    + "  qryPo.Price AS poPrice,  "
                    + "  mst_item.UnitOfMeasureCode AS unitOfMeasureCode,                                                    "
                    + "  IFNULL(onHandStok.ActualStock,0) AS onHandStock,   "
                    + "  mst_item.MinStock AS minStock,  "
                    + "  mst_item.MaxStock AS maxStock,  "
                    + "  0 price, "
                    + "  0 discount1Percent, "
                    + "  0 discount1Amount, "
                    + "  0 discount2Percent, "
                    + "  0 discount2Amount, "
                    + "  0 discount3Percent, "
                    + "  0 discount3Amount, "
                    + "  0 nettPrice, "
                    + "  0 totalAmount "
                    + "  FROM pur_purchase_request_detail prtd "
                    + "  INNER JOIN pur_purchase_request ON prtd.`HeaderCode`=pur_purchase_request.Code  "
                    + "  INNER JOIN mst_item ON prtd.ItemCode=mst_item.Code  "
                    + "  LEFT JOIN( "
                    + " 	SELECT "
                    + " 		mst_item_jn_current_stock.ItemCode, "
                    + " 		SUM(mst_item_jn_current_stock.ActualStock) AS actualStock "
                    + " 	FROM "
                    + " 		mst_item_jn_current_stock "
                    + " 	GROUP BY mst_item_jn_current_stock.ItemCode "
                    + "  ) AS onHandStok ON onHandStok.ItemCode = prtd.ItemCode  "
                    + "  LEFT JOIN ( "
                    + " 	 SELECT  "
                    + " 	 pur_purchase_order_detail.`ItemCode`, "
                    + " 	 pur_purchase_order_detail.`Price`, "
                    + " 	 SUM(pur_purchase_order_detail.`Quantity`) AS quantity, "
                    + " 	 pur_purchase_order_detail.`PurchaseRequestDetailCode` "
                    + " 	 FROM pur_purchase_order_detail "
                    + " 	 GROUP BY pur_purchase_order_detail.`PurchaseRequestDetailCode` "
                    + "  )qryPo ON qryPo.PurchaseRequestDetailCode=prtd.code "
                    + " )prDetailQry "
                    + " WHERE "
                    + headerCode
                    + " ORDER BY prDetailQry." + sort[0] + " " + sort[1];
            long countItemPrq = this.countItemPrq(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countItemPrq, totalPage(countItemPrq, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countPrq(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "SELECT COUNT(*) FROM "
                    + "(SELECT "
                    + "COUNT(*) "
                    + "FROM pur_purchase_request "
                    + "INNER JOIN mst_branch ON pur_purchase_request.BranchCode=mst_branch.Code "
                    + "INNER JOIN mst_division ON pur_purchase_request.DivisionCode=mst_division.Code "
                    + "INNER JOIN pur_purchase_request_detail ON pur_purchase_request.Code=pur_purchase_request_detail.HeaderCode "
                    + "LEFT JOIN( "
                    + "SELECT IFNULL(GROUP_CONCAT( DISTINCT pur_purchase_order.code ORDER BY pur_purchase_order.code ASC),'-') AS purchaseOrderNo , "
                    + "poJNprq.PurchaseRequestCode, "
                    + "pur_purchase_order_detail.ItemCode, "
                    + "SUM(pur_purchase_order_detail.Quantity)AS requestQuantity "
                    + "FROM pur_purchase_order "
                    + "INNER JOIN pur_purchase_order_detail ON pur_purchase_order_detail.HeaderCode = pur_purchase_order.Code "
                    + "INNER JOIN pur_purchase_order_jn_purchase_request  poJNprq ON poJNprq.PurchaseOrderCode = pur_purchase_order.Code "
                    + "GROUP BY poJNprq.PurchaseRequestCode,pur_purchase_order_detail.ItemCode "
                    + ")AS data_po ON pur_purchase_request.code=data_po.PurchaseRequestCode AND pur_purchase_request_detail.`ItemCode`=data_po.ItemCode "
                    + "WHERE pur_purchase_request.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND DATE(pur_purchase_request.TransactionDate) BETWEEN '" + obj.get("transactionStartDate") + "' AND '" + obj.get("transactionEndDate") + "' "
                    + "AND pur_purchase_request.ClosingStatus ='OPEN' "
                    + "AND pur_purchase_request.ApprovalStatus ='APPROVED' "
                    + "AND (pur_purchase_request_detail.`Quantity`-IFNULL(data_po.requestQuantity,0)) > 0 "
                    //                    + "AND pur_purchase_request.BranchCode IN (select scr_user_branch.BranchCode from scr_user_branch where scr_user_branch.UserCode = '"+userCode+"') "
                    + "GROUP BY pur_purchase_request.Code)AS purchase_request ";
            long countPrq = countResult(qry);
            return countPrq;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults findPrq(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "SELECT "
                    + "pur_purchase_request.code, "
                    + "pur_purchase_request.branchCode, "
                    + "mst_branch.Name branchName, "
                    + "IFNULL(DATE_FORMAT(pur_purchase_request.transactionDate, '%d-%m-%Y'),'') AS transactionDate, "
                    + "pur_purchase_request.requestBy, "
                    + "pur_purchase_request.divisionCode, "
                    + "mst_division.Name AS divisionName, "
                    + "pur_purchase_request.refNo, "
                    + "pur_purchase_request.remark, "
                    + "pur_purchase_request.closingStatus, "
                    + "pur_purchase_request.closingBy, "
                    + "IFNULL(DATE_FORMAT(pur_purchase_request.ClosingDate, '%d-%m-%Y'),'') AS closingDate, "
                    + "data_po.purchaseOrderNo "
                    + "FROM pur_purchase_request "
                    + "INNER JOIN pur_purchase_request_detail ON pur_purchase_request.Code=pur_purchase_request_detail.HeaderCode "
                    + "INNER JOIN mst_branch ON pur_purchase_request.BranchCode=mst_branch.Code "
                    + "INNER JOIN mst_division ON pur_purchase_request.DivisionCode=mst_division.Code "
                    + "LEFT JOIN( "
                    + "SELECT IFNULL(GROUP_CONCAT( DISTINCT pur_purchase_order.code ORDER BY pur_purchase_order.code ASC),'-') AS purchaseOrderNo , "
                    + "poJNprq.PurchaseRequestCode, "
                    + "pur_purchase_order_detail.ItemCode, "
                    + "SUM(pur_purchase_order_detail.Quantity)AS requestQuantity "
                    + "FROM pur_purchase_order "
                    + "INNER JOIN pur_purchase_order_detail ON pur_purchase_order_detail.HeaderCode = pur_purchase_order.Code "
                    + "INNER JOIN pur_purchase_order_jn_purchase_request  poJNprq ON poJNprq.PurchaseOrderCode = pur_purchase_order.Code "
                    + "GROUP BY poJNprq.PurchaseRequestCode,pur_purchase_order_detail.ItemCode "
                    + ")AS data_po ON pur_purchase_request.code=data_po.PurchaseRequestCode AND pur_purchase_request_detail.`ItemCode`=data_po.ItemCode "
                    + "WHERE pur_purchase_request.Code LIKE '%" + obj.get("code") + "%' "
                    + "AND DATE(pur_purchase_request.TransactionDate) BETWEEN '" + obj.get("transactionStartDate") + "' AND '" + obj.get("transactionEndDate") + "' "
                    + "AND pur_purchase_request.ClosingStatus ='OPEN' "
                    + "AND pur_purchase_request.ApprovalStatus ='APPROVED' "
                    + "AND (pur_purchase_request_detail.`Quantity`-IFNULL(data_po.requestQuantity,0)) > 0 "
                    //                    + "AND pur_purchase_request.BranchCode IN (select scr_user_branch.BranchCode from scr_user_branch where scr_user_branch.UserCode = '"+userCode+"') "
                    + "GROUP BY pur_purchase_request.Code "
                    + " ORDER BY pur_purchase_request." + sort[0] + " " + sort[1];
            long countPrq = this.countPrq(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countPrq, totalPage(countPrq, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults findPoForGRN(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(po.code) FROM( SELECT";
            String list = "SELECT po.* FROM( SELECT ";
            String select = " "
                    + "    po.code, "
                    + "    DATE_FORMAT(po.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    po.transactionDate searchDate, "
                    + "    po.branchCode, "
                    + "    br.Name branchName, "
                    + "    po.currencyCode, "
                    + "    curr.Name currencyName, "
                    + "    po.vendorCode, "
                    + "    vend.Name vendorName, "
                    + "    vend.DefaultContactPersonCode vendorContact, "
                    + "    vend.Address vendorAddress, "
                    + "    vend.Phone1 vendorPhone1, "
                    + "    vend.Phone2 vendorPhone2, "
                    + "    po.refNo, "
                    + "    po.remark, "
                    + "    po.approvalStatus, "
                    + "    po.closingStatus, "
                    + "    IFNULL(sumQuantityPO.totalSum,0) AS totalSumQuantityPO, "
                    + "    IFNULL(sumQuantityGrn.totalSum,0) AS totalSumQuantityGrn ";
            String qry = " "
                    + "FROM pur_purchase_order po "
                    + "  INNER JOIN mst_branch br ON br.Code = po.BranchCode "
                    + "  INNER JOIN mst_vendor vend ON vend.Code = po.VendorCode "
                    + "  INNER JOIN mst_currency curr ON curr.Code = po.CurrencyCode "
                    + "  LEFT JOIN ( "
                    + "		    SELECT "
                    + "		    pod.HeaderCode, "
                    + "		    SUM(pod.Quantity) AS totalSum "
                    + "	      FROM pur_purchase_order_detail pod"
                    + "	      GROUP BY pod.HeaderCode ) AS sumQuantityPO ON sumQuantityPO.HeaderCode = po.Code "
                    + "	 LEFT JOIN( "
                    + "		    SELECT "
                    + "		    grn.Code, "
                    + "		    grn.PurchaseOrderCode, "
                    + "		    SUM(grnD.Quantity) AS totalSum "
                    + "	      FROM ivt_goods_received_note grn "
                    + "		    INNER JOIN ivt_goods_received_note_item_detail grnD ON grnD.HeaderCode = grn.Code "
                    + "	      GROUP BY grn.PurchaseOrderCode ) AS sumQuantityGrn ON sumQuantityGrn.PurchaseOrderCode = po.Code "
                    + ")po "
                    + "WHERE po.Code LIKE '%" + obj.get("code") + "%' "
                    + "  AND IFNULL(po.refNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "  AND po.totalSumQuantityPO > po.totalSumQuantityGrn "
                    + "  AND po.approvalStatus = 'APPROVED' "
                    + "  AND po.closingStatus = 'OPEN' "
                    //                    + "  AND DATE(po.searchDate) BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "')  "
                    + "ORDER BY po." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countHistory(JSONObject obj, Class<?> module) {
        try {
            String qry
                    = "SELECT COUNT(document.code) "
                    + " FROM( "
                    + "     SELECT "
                    + "         fin_vendor_invoice.code, "
                    + "         'VIN' AS documentType, "
                    + "         fin_vendor_invoice.branchCode, "
                    + "         mst_branch.Name as branchName, "
                    + "         DATE_FORMAT(fin_vendor_invoice.TransactionDate, '%d/%m/%Y') AS documentDate, "
                    + "         fin_vendor_invoice.transactionDate, "
                    + "         fin_vendor_invoice.PurchaseOrderCode AS poNo, "
                    + "         DATE_FORMAT(pur_purchase_order.TransactionDate, '%d/%m/%Y') AS poDate, "
                    + "         ivt_goods_received_note.Code AS grnNo, "
                    + "         DATE_FORMAT(ivt_goods_received_note.TransactionDate, '%d/%m/%Y') AS grnDate, "
                    + "         fin_vendor_invoice.currencyCode, "
                    + "         fin_vendor_invoice.exchangeRate, "
                    + "         fin_vendor_invoice.paymentTermCode, "
                    + "         fin_vendor_invoice.dueDate, "
                    + "         fin_vendor_invoice.vendorCode, "
                    + "         mst_vendor.Name AS vendorName, "
                    + "         fin_vendor_invoice.VendorTaxInvoiceNo AS taxInvoiceNo, "
                    + "         fin_vendor_invoice.VendorTaxInvoiceDate AS taxInvoiceDate, "
                    + "         fin_vendor_invoice.vendorInvoiceNo, "
                    + "         fin_vendor_invoice.vendorInvoiceDate, "
                    + "         fin_vendor_invoice.refNo, "
                    + "         fin_vendor_invoice.remark, "
                    + "         fin_vendor_invoice.totalTransactionAmount, "
                    + "         ROUND(fin_vendor_invoice.TotalTransactionAmount * fin_vendor_invoice.ExchangeRate,4)AS totalTransactionAmountLocal, "
                    + "         fin_vendor_invoice.discountPercent, "
                    + "         fin_vendor_invoice.discountAmount, "
                    + "         ROUND(fin_vendor_invoice.DiscountAmount * fin_vendor_invoice.ExchangeRate,4)AS discountAmountLocal, "
                    + "         fin_vendor_invoice.vatPercent, "
                    + "         fin_vendor_invoice.vatAmount, "
                    + "         ROUND(fin_vendor_invoice.VATAmount * fin_vendor_invoice.ExchangeRate,4)AS vatAmountLocal, "
                    + "         fin_vendor_invoice_item_detail.nettPrice, "
                    + "         fin_vendor_invoice.grandTotalAmount, "
                    + "         ROUND(fin_vendor_invoice.GrandTotalAmount * fin_vendor_invoice.ExchangeRate,4) AS grandTotalAmountLocal, "
                    + "         fin_vendor_invoice.paidAmount, "
                    + "         ROUND(fin_vendor_invoice.PaidAmount * fin_vendor_invoice.ExchangeRate,4)AS paidAmountLocal, "
                    + "         ivt_goods_received_note_item_detail.itemCode, "
                    + "         mst_item.Name as itemName, "
                    + "         mst_item_sub_category.Code AS itemSubCategoryCode, "
                    + "         mst_item_sub_category.Name AS itemSubCategoryName, "
                    + "         mst_item_brand.Code AS itemBrandCode, "
                    + "         mst_item_brand.Name AS itemBrandName, "
                    + "         mst_item.UnitOfMeasureCode as unitOfMeasureCode, "
                    + "         fin_vendor_invoice_item_detail.quantity, "
                    + "         fin_vendor_invoice_item_detail.price, "
                    + "         fin_vendor_invoice_item_detail.discount1Amount, "
                    + "         fin_vendor_invoice_item_detail.discount2Amount, "
                    + "         fin_vendor_invoice_item_detail.discount3Amount, "
                    + "         IFNULL(fin_vendor_invoice_item_detail.DiscountHeaderDetailAmount,0) AS discountHeaderDetailAmount,  "
                    + "         IFNULL(fin_vendor_invoice_item_detail.TaxBaseHeaderDetailAmount,0) AS taxBaseHeaderDetailAmount, "
                    + "         IFNULL(fin_vendor_invoice_item_detail.VatHeaderDetailAmount,0) AS vatHeaderDetailAmount, "
                    + "         IFNULL(fin_vendor_invoice_item_detail.TaxBaseHeaderDetailAmount,0) + "
                    + "         IFNULL(fin_vendor_invoice_item_detail.VatHeaderDetailAmount,0) AS grandTotalHeaderDetailAmount "
                    + "     FROM fin_vendor_invoice "
                    + "     INNER JOIN fin_vendor_invoice_item_detail ON fin_vendor_invoice_item_detail.HeaderCode = fin_vendor_invoice.Code "
                    + "     INNER JOIN ivt_goods_received_note_item_detail ON fin_vendor_invoice_item_detail.GoodsReceivedNoteDetailCode = ivt_goods_received_note_item_detail.Code "
                    + "     INNER JOIN ivt_goods_received_note ON ivt_goods_received_note.Code = ivt_goods_received_note_item_detail.HeaderCode "
                    + "     INNER JOIN mst_branch ON fin_vendor_invoice.BranchCode=mst_branch.Code "
                    + "     INNER JOIN pur_purchase_order ON pur_purchase_order.Code = fin_vendor_invoice.PurchaseOrderCode "
                    + "     INNER JOIN mst_vendor ON fin_vendor_invoice.VendorCode=mst_vendor.Code "
                    + "     INNER JOIN mst_item ON fin_vendor_invoice_item_detail.ItemCode=mst_item.Code "
                    + "     LEFT JOIN mst_item_brand ON mst_item_brand.Code=mst_item.ItemBrandCode "
                    + "     LEFT JOIN mst_item_sub_category ON mst_item_sub_category.Code=mst_item.ItemSubCategoryCode "
                    + "     WHERE fin_vendor_invoice.Code LIKE '%" + obj.get("code") + "%'  "
                    + "         AND fin_vendor_invoice.VendorCode LIKE '%" + obj.get("vendorCode") + "%' "
                    + "         AND mst_vendor.Name LIKE '%" + obj.get("vendorName") + "%' "
                    + "         AND IFNULL(fin_vendor_invoice.RefNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "         AND IFNULL(fin_vendor_invoice.Remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "         AND mst_item.Code LIKE '%" + obj.get("itemCode") + "%' "
                    + "         AND mst_item.Name LIKE '%" + obj.get("itemName") + "%' "
                    + "         AND DATE(fin_vendor_invoice.transactionDate) BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + " )AS document ";
            long countHistory = countResult(qry);
            return countHistory;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingHistory(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry
                    = "SELECT document.* "
                    + " FROM( "
                    + "     SELECT "
                    + "         fin_vendor_invoice.code, "
                    + "         'VIN' AS documentType, "
                    + "         fin_vendor_invoice.branchCode, "
                    + "         mst_branch.Name as branchName, "
                    + "         DATE_FORMAT(fin_vendor_invoice.TransactionDate, '%d/%m/%Y') AS documentDate, "
                    + "         fin_vendor_invoice.transactionDate, "
                    + "         fin_vendor_invoice.PurchaseOrderCode AS poNo, "
                    + "         DATE_FORMAT(pur_purchase_order.TransactionDate, '%d/%m/%Y') AS poDate, "
                    + "         ivt_goods_received_note.Code AS grnNo, "
                    + "         DATE_FORMAT(ivt_goods_received_note.TransactionDate, '%d/%m/%Y') AS grnDate, "
                    + "         fin_vendor_invoice.currencyCode, "
                    + "         fin_vendor_invoice.exchangeRate, "
                    + "         fin_vendor_invoice.paymentTermCode, "
                    + "         fin_vendor_invoice.dueDate, "
                    + "         fin_vendor_invoice.vendorCode, "
                    + "         mst_vendor.Name AS vendorName, "
                    + "         fin_vendor_invoice.VendorTaxInvoiceNo AS taxInvoiceNo, "
                    + "         fin_vendor_invoice.VendorTaxInvoiceDate AS taxInvoiceDate, "
                    + "         fin_vendor_invoice.vendorInvoiceNo, "
                    + "         fin_vendor_invoice.vendorInvoiceDate, "
                    + "         fin_vendor_invoice.refNo, "
                    + "         fin_vendor_invoice.remark, "
                    + "         fin_vendor_invoice.totalTransactionAmount, "
                    + "         ROUND(fin_vendor_invoice.TotalTransactionAmount * fin_vendor_invoice.ExchangeRate,4)AS totalTransactionAmountLocal, "
                    + "         fin_vendor_invoice.discountPercent, "
                    + "         fin_vendor_invoice.discountAmount, "
                    + "         ROUND(fin_vendor_invoice.DiscountAmount * fin_vendor_invoice.ExchangeRate,4)AS discountAmountLocal, "
                    + "         fin_vendor_invoice.vatPercent, "
                    + "         fin_vendor_invoice.vatAmount, "
                    + "         ROUND(fin_vendor_invoice.VATAmount * fin_vendor_invoice.ExchangeRate,4)AS vatAmountLocal, "
                    + "         fin_vendor_invoice_item_detail.nettPrice, "
                    + "         fin_vendor_invoice.grandTotalAmount, "
                    + "         ROUND(fin_vendor_invoice.GrandTotalAmount * fin_vendor_invoice.ExchangeRate,4) AS grandTotalAmountLocal, "
                    + "         fin_vendor_invoice.paidAmount, "
                    + "         ROUND(fin_vendor_invoice.PaidAmount * fin_vendor_invoice.ExchangeRate,4)AS paidAmountLocal, "
                    + "         ivt_goods_received_note_item_detail.itemCode, "
                    + "         mst_item.Name as itemName, "
                    + "         IFNULL(ivt_goods_received_note_item_detail.itemAlias,'') itemAlias, "
                    + "         mst_item_sub_category.Code AS itemSubCategoryCode, "
                    + "         mst_item_sub_category.Name AS itemSubCategoryName, "
                    + "         mst_item_brand.Code AS itemBrandCode, "
                    + "         mst_item_brand.Name AS itemBrandName, "
                    + "         mst_item.UnitOfMeasureCode as unitOfMeasureCode, "
                    + "         fin_vendor_invoice_item_detail.quantity, "
                    + "         fin_vendor_invoice_item_detail.price, "
                    + "         fin_vendor_invoice_item_detail.discount1Amount, "
                    + "         fin_vendor_invoice_item_detail.discount2Amount, "
                    + "         fin_vendor_invoice_item_detail.discount3Amount, "
                    + "         IFNULL(fin_vendor_invoice_item_detail.DiscountHeaderDetailAmount,0) AS discountHeaderDetailAmount,  "
                    + "         IFNULL(fin_vendor_invoice_item_detail.TaxBaseHeaderDetailAmount,0) AS taxBaseHeaderDetailAmount, "
                    + "         IFNULL(fin_vendor_invoice_item_detail.VatHeaderDetailAmount,0) AS vatHeaderDetailAmount, "
                    + "         IFNULL(fin_vendor_invoice_item_detail.TaxBaseHeaderDetailAmount,0) + "
                    + "         IFNULL(fin_vendor_invoice_item_detail.VatHeaderDetailAmount,0) AS grandTotalHeaderDetailAmount "
                    + "     FROM fin_vendor_invoice "
                    + "     INNER JOIN fin_vendor_invoice_item_detail ON fin_vendor_invoice_item_detail.HeaderCode = fin_vendor_invoice.Code "
                    + "     INNER JOIN ivt_goods_received_note_item_detail ON fin_vendor_invoice_item_detail.GoodsReceivedNoteDetailCode = ivt_goods_received_note_item_detail.Code "
                    + "     INNER JOIN ivt_goods_received_note ON ivt_goods_received_note.Code = ivt_goods_received_note_item_detail.HeaderCode "
                    + "     INNER JOIN mst_branch ON fin_vendor_invoice.BranchCode=mst_branch.Code "
                    + "     INNER JOIN pur_purchase_order ON pur_purchase_order.Code = fin_vendor_invoice.PurchaseOrderCode "
                    + "     INNER JOIN mst_vendor ON fin_vendor_invoice.VendorCode=mst_vendor.Code "
                    + "     INNER JOIN mst_item ON fin_vendor_invoice_item_detail.ItemCode=mst_item.Code "
                    + "     LEFT JOIN mst_item_brand ON mst_item_brand.Code=mst_item.ItemBrandCode "
                    + "     LEFT JOIN mst_item_sub_category ON mst_item_sub_category.Code=mst_item.ItemSubCategoryCode "
                    + "     WHERE fin_vendor_invoice.Code LIKE '%" + obj.get("code") + "%'  "
                    + "         AND fin_vendor_invoice.VendorCode LIKE '%" + obj.get("vendorCode") + "%' "
                    + "         AND mst_vendor.Name LIKE '%" + obj.get("vendorName") + "%' "
                    + "         AND IFNULL(fin_vendor_invoice.RefNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "         AND IFNULL(fin_vendor_invoice.Remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "         AND mst_item.Code LIKE '%" + obj.get("itemCode") + "%' "
                    + "         AND mst_item.Name LIKE '%" + obj.get("itemName") + "%' "
                    + "         AND DATE(fin_vendor_invoice.transactionDate) BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + " )AS document "
                    + " ORDER BY document.Code " + sort[1];
            long countHistory = this.countHistory(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countHistory, totalPage(countHistory, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    po.code, "
                    + "    DATE_FORMAT(po.transactionDate, '%d-%m-%Y') purchaseOrderDate, "
                    + "    DATE_FORMAT(po.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    DATE_FORMAT(po.deliveryDateStart, '%d-%m-%Y') deliveryDateStart, "
                    + "    DATE_FORMAT(po.deliveryDateEnd, '%d-%m-%Y') deliveryDateEnd, "
                    + "    po.branchCode, "
                    + "    branch.Name branchName, "
                    + "    po.paymentTermCode, "
                    + "    term.Name paymentTermName, "
                    + "    po.currencyCode, "
                    + "    cur.Name currencyName, "
                    + "    po.vendorCode, "
                    + "    ven.Name vendorName, "
                    + "    ven.DefaultContactPersonCode vendorContact, "
                    + "    ven.address vendorAddress, "
                    + "    ven.phone1 vendorPhone1, "
                    + "    ven.phone2 vendorPhone2, "
                    + "    po.vendorPromoCode, "
                    + "    po.discountType, "
                    + "    po.billToCode, "
                    + "    billTo.Name billToName, "
                    + "    billTo.Address billToAddress, "
                    + "    billTo.ContactPerson billToContactPerson, "
                    + "    billTo.Phone1 billToPhone, "
                    + "    po.shipToCode, "
                    + "    shipTo.Name shipToName, "
                    + "    shipTo.Address shipToAddress, "
                    + "    shipTo.ContactPerson shipToContactPerson, "
                    + "    shipTo.Phone1 shipToPhone, "
                    + "    po.expeditionCode, "
                    + "    po.refNo, "
                    + "    po.remark, "
                    + "    po.totalTransactionAmount, "
                    + "    po.discountPercent, "
                    + "    po.discountAmount, "
                    + "    po.discountChartOfAccountCode, "
                    + "    po.discountDescription, "
                    + "    po.taxBaseSubTotalAmount, "
                    + "    po.pphPercent, "
                    + "    po.pphAmount, "
                    + "    po.vatPercent, "
                    + "    po.vatAmount, "
                    + "    po.pphChartOfAccountCode, "
                    + "    po.pphDescription, "
                    + "    po.otherFeeAmount, "
                    + "    po.otherFeeChartOfAccountCode, "
                    + "    po.otherFeeDescription, "
                    + "    po.grandTotalAmount, "
                    + "    po.approvalStatus, "
                    + "    po.approvalBy, "
                    + "    po.approvalDate, "
                    + "    po.approvalRemark, "
                    + "    po.approvalReasonCode, "
                    + "    po.closingStatus, "
                    + "    po.closingDate, "
                    + "    po.closingBy, "
                    + "    po.closingRemark "
                    + "  FROM pur_purchase_order po "
                    + "  INNER JOIN mst_purchase_destination billTo ON billTo.Code = po.BillToCode "
                    + "  INNER JOIN mst_purchase_destination shipTo ON shipTo.Code = po.ShipToCode "
                    + "  INNER JOIN mst_payment_term term ON term.Code = po.PaymentTermCode "
                    + "  INNER JOIN mst_branch branch ON branch.Code = po.BranchCode "
                    + "  INNER JOIN mst_currency cur ON cur.Code = po.CurrencyCode "
                    + "  INNER JOIN mst_vendor ven ON ven.Code = po.VendorCode "
                    + " WHERE "
                    + "  po.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    pur_purchase_order.code, "
                    + "    pur_purchase_order.transactionDate, "
                    + "    pur_purchase_order.branchCode, "
                    + "    pur_purchase_order.deliveryDateStart, "
                    + "    pur_purchase_order.deliveryDateEnd, "
                    + "    pur_purchase_order.purchaseRequestCode, "
                    + "    pur_purchase_order.paymentTermCode, "
                    + "    pur_purchase_order.currencyCode, "
                    + "    pur_purchase_order.vendorCode, "
                    + "    pur_purchase_order.vendorPromoCode, "
                    + "    pur_purchase_order.discountType, "
                    + "    pur_purchase_order.billToCode, "
                    + "    pur_purchase_order.shipToCode, "
                    + "    pur_purchase_order.expeditionCode, "
                    + "    pur_purchase_order.refNo, "
                    + "    pur_purchase_order.remark, "
                    + "    pur_purchase_order.totalTransactionAmount, "
                    + "    pur_purchase_order.discountPercent, "
                    + "    pur_purchase_order.discountAmount, "
                    + "    pur_purchase_order.discountChartOfAccountCode, "
                    + "    pur_purchase_order.discountDescription, "
                    + "    pur_purchase_order.taxBaseSubTotalAmount, "
                    + "    pur_purchase_order.pphPercent, "
                    + "    pur_purchase_order.pphAmount, "
                    + "    pur_purchase_order.vatPercent, "
                    + "    pur_purchase_order.vatAmount, "
                    + "    pur_purchase_order.pphChartOfAccountCode, "
                    + "    pur_purchase_order.pphDescription, "
                    + "    pur_purchase_order.otherFeeAmount, "
                    + "    pur_purchase_order.otherFeeChartOfAccountCode, "
                    + "    pur_purchase_order.otherFeeDescription, "
                    + "    pur_purchase_order.grandTotalAmount, "
                    + "    pur_purchase_order.approvalStatus, "
                    + "    pur_purchase_order.approvalBy, "
                    + "    pur_purchase_order.approvalDate, "
                    + "    pur_purchase_order.approvalRemark, "
                    + "    pur_purchase_order.approvalReasonCode, "
                    + "    pur_purchase_order.closingStatus, "
                    + "    pur_purchase_order.closingDate, "
                    + "    pur_purchase_order.closingBy, "
                    + "    pur_purchase_order.closingRemark "
                    + "  FROM pur_purchase_order "
                    + " WHERE "
                    + "  pur_purchase_order.code LIKE '%" + obj.get("code") + "%' "
                    + " ORDER BY pur_purchase_order." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> listDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "   pur_purchase_order_detail.code, "
                    + "   pur_purchase_order_detail.headerCode, "
                    + "   pur_purchase_order_detail.purchaseRequestDetailCode, "
                    + "   pur_purchase_order_detail.itemCode, "
                    + "   pur_purchase_order_detail.itemAlias, "
                    + "   pur_purchase_order_detail.remark, "
                    + "   pur_purchase_order_detail.quantity, "
                    + "   pur_purchase_order_detail.bonusQuantity, "
                    + "   pur_purchase_order_detail.price, "
                    + "   pur_purchase_order_detail.discount1Percent, "
                    + "   pur_purchase_order_detail.discount1Amount, "
                    + "   pur_purchase_order_detail.discount2Percent, "
                    + "   pur_purchase_order_detail.discount2Amount, "
                    + "   pur_purchase_order_detail.discount3Percent, "
                    + "   pur_purchase_order_detail.discount3Amount, "
                    + "   pur_purchase_order_detail.nettPrice, "
                    + "   pur_purchase_order_detail.totalAmount, "
                    + "   pur_purchase_order_detail.discountHeaderPercent, "
                    + "   pur_purchase_order_detail.discountHeaderAmount "
                    + "  FROM pur_purchase_order_detail "
                    + " WHERE "
                    + "  pur_purchase_order_detail.headerCode LIKE '%" + obj.get("code") + "%' "
                    + " ORDER BY pur_purchase_order_detail." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
