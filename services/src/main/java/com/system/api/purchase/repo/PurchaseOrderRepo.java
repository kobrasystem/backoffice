package com.system.api.purchase.repo;

import com.system.api.purchase.model.PurchaseOrder;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface PurchaseOrderRepo extends JpaRepository<PurchaseOrder, String> {

  @Query("SELECT m FROM PurchaseOrder m WHERE m.code = :code ")
  PurchaseOrder findByCode(@Param("code") String code);
    
    
  @Query("SELECT max(m) FROM PurchaseOrder m WHERE m.code LIKE :prefixCode% ")
  List<PurchaseOrder> findMaxCode(@Param("prefixCode") String prefixCode);
  
  @Transactional
  void deleteByCode(String code);

}
