/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.purchase.service;

import com.system.api.purchase.dao.PurchaseRequestDao;
import com.system.api.purchase.model.PurchaseRequest;
import com.system.api.purchase.repo.PurchaseRequestDetailRepo;
import com.system.api.purchase.repo.PurchaseRequestRepo;
import com.system.base.PaginatedResults;
import com.system.base.Session;
import com.system.exception.CustomException;
import com.system.utils.AutoNumber;
import com.system.utils.DateUtils;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author lukassh
 */
@Service
public class PurchaseRequestService extends Session {

  @Autowired
  private PurchaseRequestRepo repo;

  @Autowired
  private PurchaseRequestDetailRepo detailRepo;

  @Autowired
  private PurchaseRequestDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PurchaseRequest data(String code) {
    try {
      return repo.findByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PurchaseRequest save(PurchaseRequest model) {
    try {

      String where = " branchCode='" + model.getBranchCode() + "' AND divisionCode='" + model.getDivisionCode() + "' ";
      String maxCode = AutoNumber.getMaxCode(em, "code", PurchaseRequest.class, where);
      String date = DateUtils.formatingDate(model.getTransactionDate(), true, true, false);
      String prefix = model.getBranchCode() + "/PRQ/" + date + "/";
      String code = AutoNumber.generateCode(prefix, maxCode, 5);
//      String code = AutoNumber.generateCode(model.getDivisionCode().replace(model.getBranchCode(), "") + date, maxCode, 5);

      model.setId(code);
      model.setCode(code);
      for (int i = 0; i < model.getListPurchaseRequestDetail().size(); i++) {
        model.getListPurchaseRequestDetail().get(i).setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        model.getListPurchaseRequestDetail().get(i).setHeaderCode(model.getCode());
      }
      detailRepo.saveAll(model.getListPurchaseRequestDetail());
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PurchaseRequest update(PurchaseRequest model) {
    try {
      detailRepo.deleteByHeaderCode(model.getCode());
      for (int i = 0; i < model.getListPurchaseRequestDetail().size(); i++) {
        model.getListPurchaseRequestDetail().get(i).setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        model.getListPurchaseRequestDetail().get(i).setHeaderCode(model.getCode());
      }
      detailRepo.saveAll(model.getListPurchaseRequestDetail());
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      detailRepo.deleteByHeaderCode(code);
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

}
