/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author Imam Solikhin
 */

@Data
@Entity
@Table(name = "sal_sales_order")
public class SalesOrder extends GroupModel{

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code = "";

  @SheetColumn("transactionDate")
  @Column(name = "transactionDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();

  @SheetColumn("currencyCode")
  @Column(name = "currencyCode")
  private String currencyCode = "IDR";

  @SheetColumn("salesPersonCode")
  @Column(name = "salesPersonCode")
  private String salesPersonCode = "";

  @SheetColumn("customerCode")
  @Column(name = "customerCode")
  private String customerCode = "";

  @SheetColumn("customerAddress")
  @Column(name = "customerAddress")
  private String customerAddress = "";

  @SheetColumn("refNo")
  @Column(name = "refNo")
  private String refNo = "";

  @SheetColumn("remark")
  @Column(name = "remark")
  private String remark = "";

  @SheetColumn("transactionType")
  @Column(name = "transactionType")
  private String transactionType = "COD";
  
  @SheetColumn("bankCode")
  @Column(name = "bankCode")
  private String bankCode = "COD";
  
  @SheetColumn("courierCode")
  @Column(name = "courierCode")
  private String courierCode = "";
  
  @SheetColumn("courierAmount")
  @Column(name = "courierAmount")
  private String courierAmount = "";
  
  @SheetColumn("totalTransactionAmount")
  @Column(name = "totalTransactionAmount")
  private BigDecimal totalTransactionAmount = new BigDecimal(0.00);

  @SheetColumn("DiscountAmount")
  @Column(name = "DiscountAmount")
  private BigDecimal discountAmount = new BigDecimal(0.00);

  @SheetColumn("subTotalAmount")
  @Column(name = "subTotalAmount")
  private BigDecimal subTotalAmount = new BigDecimal(0.00);

  @SheetColumn("vatPercent")
  @Column(name = "vatPercent")
  private BigDecimal vatPercent = new BigDecimal(0.00);

  @SheetColumn("vatAmount")
  @Column(name = "vatAmount")
  private BigDecimal vatAmount = new BigDecimal(0.00);

  @SheetColumn("grandTotalAmount")
  @Column(name = "grandTotalAmount")
  private BigDecimal grandTotalAmount = new BigDecimal(0.00);

  @SheetColumn("approvalStatus")
  @Column(name = "approvalStatus")
  private String approvalStatus = "PENDING";

  @SheetColumn("approvalBy")
  @Column(name = "approvalBy")
  private String approvalBy = "";

  @SheetColumn("approvalDate")
  @Column(name = "approvalDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date approvalDate = new Date();

  @SheetColumn("approvalRemark")
  @Column(name = "approvalRemark")
  private String approvalRemark = "";
  
  @SheetColumn("closingStatus")
  @Column(name = "closingStatus")
  private String closingStatus = "OPEN";

  @SheetColumn("closingDate")
  @Column(name = "closingDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date closingDate = new Date();

  @SheetColumn("closingBy")
  @Column(name = "closingBy")
  private String closingBy = "";
  
  private transient List<SalesOrderDetail> listSalesOrderDetail;
}
