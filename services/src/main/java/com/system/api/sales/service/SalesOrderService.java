package com.system.api.sales.service;

import com.system.api.purchase.model.PurchaseOrder;
import com.system.api.sales.dao.SalesOrderDao;
import com.system.api.sales.model.SalesOrder;
import com.system.api.sales.model.SalesOrderDetail;
import com.system.api.sales.repo.SalesOrderDetailRepo;
import com.system.api.sales.repo.SalesOrderRepo;
import com.system.base.PaginatedResults;
import com.system.base.Session;
import com.system.exception.CustomException;
import com.system.utils.AutoNumber;
import com.system.utils.DateUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class SalesOrderService extends Session {

  @Autowired
  private SalesOrderRepo repo;

  @Autowired
  private SalesOrderDetailRepo dRepo;

  @Autowired
  private SalesOrderDao soDao;

  public Object dataTest(JSONObject obj) {
    try {
      List<Map<String, Object>> list = soDao.test(obj, 10000, 0);
      list.stream().map(map -> map.entrySet())
              .forEach(System.out::println);
      return null;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public long checkData(String code, Class<?> module) {
    try {
      return soDao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return soDao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object dataForDeliveryNote(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return soDao.dataForDeliveryNote(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingSoApproval(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.pagingSoApproval(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults detail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.pagingDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults dataStock(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.dataStock(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults termDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.termDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults outstandingARDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.pagingARDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.lookUp(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults lookUpForDeliveryNote(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.lookUpForDeliveryNote(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public SalesOrder save(SalesOrder model) {
    try {

      String where = " branchCode='" + model.getBranchCode() + "' AND divisionCode='" + model.getDivisionCode() + "' ";
      String maxCode = AutoNumber.getMaxCode(em, "code", PurchaseOrder.class, where);
      String date = DateUtils.formatingDate(model.getTransactionDate(), true, true, false);
      String prefix = model.getBranchCode() + "/SOD/" + date + "/";
      String code = AutoNumber.generateCode(prefix, maxCode, 5);
//      String code = AutoNumber.generateCode(model.getDivisionCode().replace(model.getBranchCode(), "") + date, maxCode, 5);
      model.setCode(code);
      int i = 1;

      for (SalesOrderDetail salesOrderDetail : model.getListSalesOrderDetail()) {
        salesOrderDetail.setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        salesOrderDetail.setHeaderCode(model.getCode());
        dRepo.save(salesOrderDetail);
        i++;
      }

      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public SalesOrder update(SalesOrder model) {
    try {

      dRepo.deleteByHeaderCode(model.getCode());

      //Insert New Detail
      int i = 1;
      for (SalesOrderDetail salesOrderDetail : model.getListSalesOrderDetail()) {
        salesOrderDetail.setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        salesOrderDetail.setHeaderCode(model.getCode());
        dRepo.save(salesOrderDetail);
        i++;
      }

      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      dRepo.deleteByHeaderCode(code);
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public SalesOrder unApprove(String code) {
    try {
      SalesOrder data = repo.findByCode(code);
      if (data == null) {
        throw new CustomException("Data " + code + " can't update!", HttpStatus.CONFLICT);
      }

      data.setApprovalStatus("PENDING");
      data.setApprovalBy(username());
      data.setApprovalDate(new Date());

      return repo.save(data);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingHistory(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.pagingHistory(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults detailHistory(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return soDao.pagingHistoryDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  // approval  
  public SalesOrder approve(SalesOrder model) {
    try {

      SalesOrder data = repo.findByCode(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
      }

      data.setApprovalStatus(model.getApprovalStatus());
      data.setApprovalBy(username());
      data.setApprovalDate(new Date());

      return repo.save(data);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  // closing  
  public SalesOrder closing(SalesOrder model) {
    try {

      SalesOrder data = repo.findByCode(model.getCode());
      if (data == null) {
        throw new CustomException("Data " + model.getCode() + " can't update!", HttpStatus.CONFLICT);
      }
      data.setClosingStatus(model.getClosingStatus());
      data.setClosingBy(username());
      data.setClosingDate(new Date());
      return repo.save(data);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
