package com.system.api.sales.repo;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import com.system.api.sales.model.CustomerInvoiceSalesOrderItemDetail;

/**
 *
 * @author Rayis
 */
public interface CustomerInvoiceSalesOrderDetailRepo extends JpaRepository<CustomerInvoiceSalesOrderItemDetail, String> {

  CustomerInvoiceSalesOrderItemDetail findByCode(@Param("code") String code);

  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByHeaderCode(String code);
}
