package com.system.api.sales.model;

import lombok.Data;
import java.util.Date;
import javax.persistence.Id;
import java.math.BigDecimal;  
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import com.system.base.GroupModel;
import com.system.utils.DateUtils;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.util.List;

/**
 *
 * @author Rayis
 */
@Data
@Entity
@Table(name = "fin_customer_invoice_sales_order")
public class CustomerInvoiceSalesOrder extends GroupModel {

    @Id
    @SheetColumn("Code")
    @Column(name = "Code")
    private String code = "";

    @SheetColumn("TransactionDate")
    @Column(name = "TransactionDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date transactionDate = DateUtils.newDate(1900, 1, 1);

    @SheetColumn("SalesOrderCode")
    @Column(name = "SalesOrderCode")
    private String salesOrderCode = "";

    @SheetColumn("SalesPersonCode")
    @Column(name = "SalesPersonCode")
    private String salesPersonCode = "";

    @SheetColumn("CustomerCode")
    @Column(name = "CustomerCode")
    private String customerCode = "";

    @SheetColumn("CurrencyCode")
    @Column(name = "CurrencyCode")
    private String currencyCode = "";

    @SheetColumn("CurrencyCode")
    @Column(name = "ExchangeRate")
    private BigDecimal exchangeRate = new BigDecimal("0.00");

    @SheetColumn("BillToCode")
    @Column(name = "BillToCode")
    private String billToCode = "";

    @SheetColumn("PaymentTermCode")
    @Column(name = "PaymentTermCode")
    private String paymentTermCode = "";

    @SheetColumn("DueDate")
    @Column(name = "DueDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dueDate = DateUtils.newDate(1900, 1, 1);

    @SheetColumn("ProjectCode")
    @Column(name = "ProjectCode")
    private String projectCode = "";

    @SheetColumn("RefNo")
    @Column(name = "RefNo")
    private String refNo = "";

    @SheetColumn("Remark")
    @Column(name = "Remark")
    private String remark = "";

    @SheetColumn("TotalTransactionAmount")
    @Column(name = "TotalTransactionAmount")
    private BigDecimal totalTransactionAmount = new BigDecimal("0.00");

    @SheetColumn("DiscountCode")
    @Column(name = "DiscountCode")
    private String discountCode = "";

    @SheetColumn("DiscountAmount")
    @Column(name = "DiscountAmount")
    private BigDecimal discountAmount = new BigDecimal("0.00");

    @SheetColumn("DownPaymentAmount")
    @Column(name = "DownPaymentAmount")
    private BigDecimal downPaymentAmount = new BigDecimal("0.00");

    @SheetColumn("TaxBaseAmount")
    @Column(name = "TaxBaseAmount")
    private BigDecimal taxBaseAmount = new BigDecimal("0.00");
    
    @SheetColumn("RoundedAmount")
    @Column(name = "RoundedAmount")
    private BigDecimal roundedAmount = new BigDecimal("0.00");

    @SheetColumn("VatPercent")
    @Column(name = "VatPercent")
    private BigDecimal vatPercent = new BigDecimal("0.00");

    @SheetColumn("VatAmount")
    @Column(name = "VatAmount")
    private BigDecimal vatAmount = new BigDecimal("0.00");

    @SheetColumn("GrandTotalAmount")
    @Column(name = "GrandTotalAmount")
    private BigDecimal grandTotalAmount = new BigDecimal("0.00");

    @SheetColumn("PaidAmount")
    @Column(name = "PaidAmount")
    private BigDecimal paidAmount = new BigDecimal("0.00");

    @SheetColumn("SettlementDate")
    @Column(name = "SettlementDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date settlementDate = DateUtils.newDate(1900, 1, 1);

    @SheetColumn("SettlementDocumentNo")
    @Column(name = "SettlementDocumentNo")
    private String settlementDocumentNo = "";

    @SheetColumn("PrintStatus")
    @Column(name = "PrintStatus")
    private boolean printStatus;

    @SheetColumn("PrintCounter")
    @Column(name = "PrintCounter")
    private BigDecimal printCounter = new BigDecimal("0.00");

    private transient List<CustomerInvoiceSalesOrderDeliveryNote> listCustomerInvoiceSalesOrderDeliveryNote;

    private transient List<CustomerInvoiceSalesOrderItemDetail> listCustomerInvoiceSalesOrderItemDetail;

}
