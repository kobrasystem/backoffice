package com.system.api.sales.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.poiji.exception.PoijiExcelType;
import com.poiji.bind.Poiji;
import com.system.api.sales.model.CustomerInvoiceSalesOrder;
import com.system.api.sales.model.SalesOrder;
import com.system.api.sales.model.SalesOrderDetail;
import com.system.api.sales.service.SalesOrderService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.IOExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Authorization;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "Sales Order")
@RequestMapping("/sales-order")
public class SalesOrderController extends BaseProgress {

  @Autowired
  private SalesOrderService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(@RequestParam(defaultValue = "{\"code\":\"\",\"transactionStartDate\":\"\",\"transactionEndDate\":\"\",\"vendorCode\":\"\",\"vendorName\":\"\",\"currencyCode\":\"\",\"currencyName\":\"\",\"refNo\":\"\",\"remark\":\"\",\"approvalStatus\":\"\",\"closingStatus\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,DESC\"}",
          required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), SalesOrder.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/detail/search")
  public HttpEntity<Object> listDetail(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {

      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return new HttpEntity<>(service.detail(obj, page, size, sort, SalesOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, SalesOrder.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/for-delivery-note/data")
  public HttpEntity<Object> dataForDeliveryNote(@RequestParam String code) {
    try {
      Object model = service.dataForDeliveryNote(code, SalesOrder.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/term/search")
  public HttpEntity<Object> listTermDetail(
          @RequestParam(defaultValue = "", required = false) String headerCode,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {

      JSONObject obj = new JSONObject("{'headerCode':'" + headerCode + "'}");
      return new HttpEntity<>(service.termDetail(obj, page, size, sort, SalesOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/approve/update")
  public HttpEntity<Object> approve(@ApiParam("Body of post") @RequestBody String model) {
    try {

      ObjectMapper mapper = new ObjectMapper();
      SalesOrder models = mapper.reader().forType(SalesOrder.class).readValue(model);
      long data = service.checkData(models.getCode(), SalesOrder.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.approve(models);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/close/update")
  public HttpEntity<Object> closing(@ApiParam("Body of post") @RequestBody String model) {
    try {

      ObjectMapper mapper = new ObjectMapper();
      SalesOrder models = mapper.reader().forType(SalesOrder.class).readValue(model);
      long data = service.checkData(models.getCode(), SalesOrder.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.closing(models);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody SalesOrder model) {
    try {
      long data = service.checkData(model.getCode(), SalesOrder.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody SalesOrder model) {
    try {

      long data = service.checkData(model.getCode(), SalesOrder.class);
      if (data < 1) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.update(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, SalesOrder.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "un-approve/delete")
  public HttpEntity<Object> unApprove(@RequestParam String code) {
    try {
      Object data = service.data(code, SalesOrder.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.unApprove(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/history/search")
  public HttpEntity<Object> listHistory(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String customerCode,
          @RequestParam(defaultValue = "", required = false) String customerName,
          @RequestParam(defaultValue = "", required = false) String transactionStartDate,
          @RequestParam(defaultValue = "", required = false) String transactionEndDate,
          @RequestParam(defaultValue = "", required = false) String refNo,
          @RequestParam(defaultValue = "", required = false) String remark,
          @RequestParam(defaultValue = "", required = false) String itemCode,
          @RequestParam(defaultValue = "", required = false) String itemName,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {

      JSONObject obj = new JSONObject("{'code':'" + code
              + "','customerCode':'" + customerCode
              + "','customerName':'" + customerName
              + "','transactionStartDate':'" + transactionStartDate
              + "','transactionEndDate':'" + transactionEndDate
              + "','refNo':'" + refNo
              + "','remark':'" + remark
              + "','itemCode':'" + itemCode
              + "','itemName':'" + itemName
              + "'}");
      return new HttpEntity<>(service.pagingHistory(obj, page, size, sort, CustomerInvoiceSalesOrder.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/history-detail/search")
  public HttpEntity<Object> listHistoryDetail(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String type,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {

      JSONObject obj = new JSONObject("{'code':'" + code + "','type':'" + type + "'}");
      return new HttpEntity<>(service.detailHistory(obj, page, size, sort, SalesOrderDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/imports")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<SalesOrder> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, SalesOrder.class);
      for (SalesOrder m : model) {
        service.save(m);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/exports")
  public void exports(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "", required = false) String bankCode,
          @RequestParam(defaultValue = "", required = false) String bankName,
          @RequestParam(defaultValue = "", required = false) String title,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort,
          HttpServletResponse response) throws ParseException {

    JSONObject obj = new JSONObject("{'code':'" + code
            + "','name':'" + name
            + "','bankCode':'" + bankCode
            + "','bankName':'" + bankName
            + "','activeStatus':'" + activeStatus
            + "'}");
    List<Map<String, Object>> list = service.list(obj, page, size, sort, SalesOrder.class);

    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, title);
    IOExcel.expCellValues("Sales Order", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (int i = 0; i < list.size(); i++) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      row++;
    }
    try {
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition", "attachment;filename=sales-" + title + ".xlsx");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }
}
