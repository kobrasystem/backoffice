package com.system.api.sales.service;

import java.util.Map;
import java.util.List;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.system.api.purchase.model.PurchaseOrder;
import com.system.api.sales.dao.CustomerInvoiceSalesOrderDao;
import com.system.api.sales.model.CustomerInvoiceSalesOrder;
import com.system.api.sales.model.CustomerInvoiceSalesOrderItemDetail;
import com.system.api.sales.repo.CustomerInvoiceSalesOrderDetailRepo;
import com.system.api.sales.repo.CustomerInvoiceSalesOrderRepo;
import com.system.base.PaginatedResults;
import com.system.base.Session;
import com.system.exception.CustomException;
import com.system.utils.AutoNumber;
import com.system.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Rayis
 */
@Service
public class CustomerInvoiceSalesOrderService extends Session {

  @Autowired
  private CustomerInvoiceSalesOrderDetailRepo dRepo;

  @Autowired
  private CustomerInvoiceSalesOrderRepo repo;

  @Autowired
  private CustomerInvoiceSalesOrderDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults findOverDue(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.findOverDue(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults dlnDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDlnDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults cdpDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingCdpDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults detail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDetail(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults detailForReturn(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDetaildetailForReturn(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object dataLookUpSo(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.dataLookUpSo(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.lookUp(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults searchTerm(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.searchTerm(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public long countDataTerm(String code, Class<?> module) {
    try {
      return dao.countDataTerm(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object dataTerm(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.dataTerm(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public CustomerInvoiceSalesOrder save(CustomerInvoiceSalesOrder model) {
    try {
      String where = " branchCode='" + model.getBranchCode() + "' AND divisionCode='" + model.getDivisionCode() + "' ";
      String maxCode = AutoNumber.getMaxCode(em, "code", PurchaseOrder.class, where);
      String date = DateUtils.formatingDate(model.getTransactionDate(), true, true, false);
      String prefix = model.getBranchCode() + "/INV/" + date + "/";
      String code = AutoNumber.generateCode(prefix, maxCode, 5);
//      String code = AutoNumber.generateCode(model.getDivisionCode().replace(model.getBranchCode(), "") + date, maxCode, 5);
      model.setCode(code);

      int i = 1;
      for (CustomerInvoiceSalesOrderItemDetail invDetail : model.getListCustomerInvoiceSalesOrderItemDetail()) {
        invDetail.setCode(model.getCode() + "-" + StringUtils.leftPad(Integer.toString(i), 3, "0"));
        invDetail.setHeaderCode(model.getCode());
        dRepo.save(invDetail);
        i++;
      }

      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {

      Object documentNo = dao.findFivesGang(code);

      if (documentNo == null) {
        dRepo.deleteByHeaderCode(code);
        repo.deleteByCode(code);
      } else {
        throw new CustomException("Data Customer Invoice Sales Order telah jadi 5 sekawan !!!", HttpStatus.UNAUTHORIZED);
      }

    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void invoiceRecalculating(JSONObject obj) {
    try {
      dao.invoiceRecalculating(obj);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
