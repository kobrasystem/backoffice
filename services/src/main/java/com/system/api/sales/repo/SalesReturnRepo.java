/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.repo;

import com.system.api.sales.model.SalesReturn;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rayis
 */
public interface SalesReturnRepo extends JpaRepository<SalesReturn, String> {

  SalesReturn findByCode(@Param("code") String code);

  @Transactional
  void deleteByCode(String code);

}
