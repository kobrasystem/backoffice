
package com.system.api.sales.model;
 
import lombok.Data;
import javax.persistence.Id;
import java.math.BigDecimal;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

/**
 *
 * @author Rayis
 */

@Data
@Entity
@Table(name = "fin_customer_invoice_sales_order_item_detail")
public class CustomerInvoiceSalesOrderItemDetail extends BaseModel {
    
    @Id
    @SheetColumn("Code")
    @Column (name="Code")
    private String code = "";
    
    @SheetColumn("HeaderCode")
    @Column (name="HeaderCode")
    private String headerCode = "";
    
    @SheetColumn("DeliveryNoteSalesOrderDetailCode")
    @Column (name="DeliveryNoteSalesOrderDetailCode")
    private String deliveryNoteSalesOrderDetailCode = "";
        
    @SheetColumn("ItemCode")
    @Column (name="ItemCode")
    private String itemCode = "";
        
    @SheetColumn("ItemAlias")
    @Column (name="ItemAlias")
    private String itemAlias = "";
        
    @SheetColumn("Quantity")
    @Column (name="Quantity")
    private BigDecimal quantity = new BigDecimal("0.00");
    
    @SheetColumn("Price")
    @Column (name="Price")
    private BigDecimal price = new BigDecimal("0.00");
        
    @SheetColumn("DiscountCode")
    @Column (name="DiscountCode")
    private String discountCode = "";
        
    @SheetColumn("DiscountAmount")
    @Column (name="DiscountAmount")
    private BigDecimal discountAmount = new BigDecimal("0.00");
    
    @SheetColumn("NettPrice")    
    @Column (name="NettPrice")
    private BigDecimal nettPrice = new BigDecimal("0.00");
    
    @SheetColumn("TotalAmount")
    @Column (name="TotalAmount")
    private BigDecimal totalAmount = new BigDecimal("0.00");
        
    @SheetColumn("DiscountHeaderDetailPercentFull")
    @Column (name="DiscountHeaderDetailPercentFull")
    private BigDecimal discountHeaderDetailPercentFull = new BigDecimal("0.00");
    
    @SheetColumn("DiscountHeaderDetailAmountFull")
    @Column (name="DiscountHeaderDetailAmountFull")
    private BigDecimal discountHeaderDetailAmountFull = new BigDecimal("0.00");
    
    @SheetColumn("DiscountHeaderDetailPercent")
    @Column (name="DiscountHeaderDetailPercent")
    private BigDecimal discountHeaderDetailPercent = new BigDecimal("0.00");
    
    @SheetColumn("DiscountHeaderDetailAmount")
    @Column (name="DiscountHeaderDetailAmount")
    private BigDecimal discountHeaderDetailAmount = new BigDecimal("0.00");
    
    @SheetColumn("DownPaymentHeaderDetailAmount")
    @Column (name="DownPaymentHeaderDetailAmount")
    private BigDecimal downPaymentHeaderDetailAmount = new BigDecimal("0.00");
    
    @SheetColumn("TaxBaseHeaderDetailAmount")
    @Column (name="TaxBaseHeaderDetailAmount")
    private BigDecimal taxBaseHeaderDetailAmount = new BigDecimal("0.00");
    
    @SheetColumn("VatHeaderDetailAmount")
    @Column (name="VatHeaderDetailAmount")
    private BigDecimal VatHeaderDetailAmount = new BigDecimal("0.00");

}
