/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.model;

import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Imam Solikhin
 */
@Data
@Entity
@Table(name = "sal_sales_order_detail")
public class SalesOrderDetail extends BaseModel {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code = "";

  @Column(name = "headerCode")
  @SheetColumn("headerCode")
  private String headerCode = "";

  @Column(name = "itemCode")
  @SheetColumn("itemCode")
  private String itemCode = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @Column(name = "quantity")
  @SheetColumn("quantity")
  private BigDecimal quantity = new BigDecimal(0.00);

  @Column(name = "price")
  @SheetColumn("price")
  private BigDecimal price = new BigDecimal(0.00);

  @Column(name = "discount")
  @SheetColumn("discount")
  private BigDecimal discount = new BigDecimal(0.00);

  @Column(name = "TotalAmount")
  @SheetColumn("TotalAmount")
  private BigDecimal totalAmount = new BigDecimal(0.00);

}
