/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.repo;

import org.springframework.data.repository.query.Param;
import com.system.api.sales.model.SalesReturnDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rayis
 */
public interface SalesReturnDetailRepo extends JpaRepository<SalesReturnDetail, String> {

    SalesReturnDetail findByCode(@Param("code") String code);

    @Transactional
    void deleteByHeaderCode(String code);

    @Transactional
    void deleteByCode(String code);

}
