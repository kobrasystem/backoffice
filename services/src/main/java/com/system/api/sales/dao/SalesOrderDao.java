/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class SalesOrderDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM sal_sales_order WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingJobOrder(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String closingStatus = AppUtils.toString(obj, "closingStatus").toLowerCase().equals("all") || AppUtils.toString(obj, "closingStatus").equals("") ? "" : " AND so.closingStatus = '" + AppUtils.toString(obj, "closingStatus") + "'";
            String approvalStatus = AppUtils.toString(obj, "approvalStatus").toLowerCase().equals("all") || AppUtils.toString(obj, "approvalStatus").equals("") ? "" : " AND so.approvalStatus = '" + AppUtils.toString(obj, "approvalStatus") + "'";
            String count = "SELECT COUNT(so.code) FROM( SELECT";
            String list = "SELECT so.* FROM( SELECT ";
            String select = " "
                    + "    so.code, "
                    + "    so.branchCode, "
                    + "    so.transactionDate searchDate, "
                    + "    DATE_FORMAT(so.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    DATE_FORMAT(so.requestDeliveryDate, '%d-%m-%Y') requestDeliveryDate, "
                    + "    DATE_FORMAT(so.expiredDate, '%d-%m-%Y') expiredDate, "
                    + "    so.currencyCode, "
                    + "    so.unlockBy, "
                    + "    IFNULL(so.discountCode,'') AS discountCode, "
                    + "    IFNULL(discount_type.Name,'') AS discountName, "
                    + "    so.customerPurchaseOrderNo, "
                    + "    so.proformaInvoiceNo, "
                    + "    so.salesPersonCode, "
                    + "    sape.Name AS salesPersonName, "
                    + "    so.customerCode, "
                    + "    customer.Name AS customerName, "
                    + "    so.billToCode, "
                    + "    so.priceTypeCode, "
                    + "    so.shipToCode, "
                    + "    so.refNo, "
                    + "    so.remark, "
                    + "    so.totalTransactionAmount, "
                    + "    so.discountAmount, "
                    + "    so.taxBaseAmount, "
                    + "    so.vatPercent, "
                    + "    so.vatAmount, "
                    + "    so.grandTotalAmount, "
                    + "    so.approvalStatus, "
                    + "    so.approvalReasonCode, "
                    + "    so.approvalBy, "
                    + "    so.approvalDate, "
                    + "    so.closingStatus, "
                    + "    so.closingDate, "
                    + "    so.closingBy ";
            String qry = " "
                    + "FROM sal_sales_order so "
                    + " INNER JOIN mst_customer customer ON customer.code = so.customerCode "
                    + " INNER JOIN mst_sales_person sape ON sape.code = so.salesPersonCode "
                    + " LEFT JOIN mst_discount_type discount_type ON discount_type.code = so.discountCode "
                    + ")so "
                    + "WHERE so.Code LIKE '%" + obj.get("code") + "%' "
                    + "  so.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND so.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + "  AND so.customerName LIKE '%" + obj.get("customerName") + "%' "
                    + "  AND IFNULL(so.remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "  AND IFNULL(so.refNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "  AND so.searchDate BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + closingStatus
                    + approvalStatus
                    + "ORDER BY so." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "   COUNT(QR1.code) "
                    + "   FROM ("
                    + " SELECT ( sal_sales_order.code)"
                    + " FROM sal_sales_order "
                    + " INNER JOIN mst_customer ON mst_customer.code = sal_sales_order.customerCode "
                    + " INNER JOIN mst_sales_person ON mst_sales_person.code = sal_sales_order.salesPersonCode "
                    + " LEFT JOIN mst_discount_type discount_type ON discount_type.code = sal_sales_order.discountCode "
                    + " WHERE "
                    + "  sal_sales_order.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND sal_sales_order.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + "  AND mst_customer.Name LIKE '%" + obj.get("customerName") + "%' "
                    + "  AND  IFNULL(sal_sales_order.remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "  AND  IFNULL(sal_sales_order.refNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "  AND(  "
                    + "    CASE  "
                    + "     WHEN 'PENDING'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         sal_sales_order.ApprovalStatus='PENDING'  "
                    + "     WHEN 'APPROVED'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         sal_sales_order.ApprovalStatus='APPROVED'  "
                    + "     WHEN 'REJECTED'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         sal_sales_order.ApprovalStatus='REJECTED'  "
                    + "     ELSE 1=1  "
                    + "    END  "
                    + "   ) "
                    + "  AND(  "
                    + "    CASE  "
                    + "     WHEN 'OPEN'='" + obj.get("closingStatus") + "' THEN  "
                    + "         sal_sales_order.closingStatus='OPEN'  "
                    + "     WHEN 'CLOSED'='" + obj.get("closingStatus") + "' THEN  "
                    + "         sal_sales_order.closingStatus='CLOSED'  "
                    + "     ELSE 1=1  "
                    + "    END  "
                    + "   ) "
                    + "  AND sal_sales_order.transactionDate BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + " ) AS QR1";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " SELECT QR1.* FROM ("
                    + "  SELECT "
                    + "    sal_sales_order.code, "
                    + "    sal_sales_order.branchCode, "
                    + "    DATE_FORMAT(sal_sales_order.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    DATE_FORMAT(sal_sales_order.requestDeliveryDate, '%d-%m-%Y') requestDeliveryDate, "
                    + "    DATE_FORMAT(sal_sales_order.expiredDate, '%d-%m-%Y') expiredDate, "
                    + "    sal_sales_order.currencyCode, "
                    + "    sal_sales_order.unlockBy, "
                    + "    IFNULL(sal_sales_order.discountCode,'') AS discountCode, "
                    + "    IFNULL(discount_type.Name,'') AS discountName, "
                    + "    sal_sales_order.customerPurchaseOrderNo, "
                    + "    sal_sales_order.proformaInvoiceNo, "
                    + "    sal_sales_order.salesPersonCode, "
                    + "    sape.Name AS salesPersonName, "
                    + "    sal_sales_order.customerCode, "
                    + "    customer.Name AS customerName, "
                    + "    sal_sales_order.billToCode, "
                    + "    sal_sales_order.priceTypeCode, "
                    + "    sal_sales_order.shipToCode, "
                    + "    sal_sales_order.refNo, "
                    + "    sal_sales_order.remark, "
                    + "    sal_sales_order.totalTransactionAmount, "
                    + "    sal_sales_order.discountAmount, "
                    + "    IFNULL(sal_sales_order.roundedAmount,0) roundedAmount, "
                    + "    sal_sales_order.taxBaseAmount, "
                    + "    sal_sales_order.vatPercent, "
                    + "    sal_sales_order.vatAmount, "
                    + "    sal_sales_order.grandTotalAmount, "
                    + "    sal_sales_order.approvalStatus, "
                    + "    sal_sales_order.approvalReasonCode, "
                    + "    sal_sales_order.approvalBy, "
                    + "    sal_sales_order.approvalDate, "
                    + "    sal_sales_order.closingStatus, "
                    + "    sal_sales_order.closingDate, "
                    + "    sal_sales_order.closingBy "
                    + " FROM sal_sales_order"
                    + " INNER JOIN mst_customer customer ON customer.code = sal_sales_order.customerCode "
                    + " INNER JOIN mst_sales_person sape ON sape.code = sal_sales_order.salesPersonCode "
                    + " LEFT JOIN mst_discount_type discount_type ON discount_type.code = sal_sales_order.discountCode "
                    + " WHERE "
                    + "  sal_sales_order.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND sal_sales_order.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + "  AND customer.Name LIKE '%" + obj.get("customerName") + "%' "
                    + "  AND  IFNULL(sal_sales_order.remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "  AND  IFNULL(sal_sales_order.refNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "  AND(  "
                    + "    CASE  "
                    + "     WHEN 'PENDING'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         sal_sales_order.ApprovalStatus='PENDING'  "
                    + "     WHEN 'APPROVED'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         sal_sales_order.ApprovalStatus='APPROVED'  "
                    + "     WHEN 'REJECTED'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         sal_sales_order.ApprovalStatus='REJECTED'  "
                    + "     ELSE 1=1  "
                    + "    END  "
                    + "   ) "
                    + "  AND(  "
                    + "    CASE  "
                    + "     WHEN 'OPEN'='" + obj.get("closingStatus") + "' THEN  "
                    + "         sal_sales_order.closingStatus='OPEN'  "
                    + "     WHEN 'CLOSED'='" + obj.get("closingStatus") + "' THEN  "
                    + "         sal_sales_order.closingStatus='CLOSED'  "
                    + "     ELSE 1=1  "
                    + "    END  "
                    + "   ) "
                    + "  AND sal_sales_order.transactionDate BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + "  ) AS QR1 "
                    + " ORDER BY QR1." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public PaginatedResults pagingSoApproval(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(so.code) FROM( SELECT";
            String list = "SELECT so.* FROM( SELECT ";
            String select = " "
                    + "    so.code, "
                    + "    so.branchCode, "
                    + "    so.transactionDate, "
                    + "    DATE_FORMAT(so.transactionDate, '%d-%m-%Y') soTransactionDate, "
                    + "    DATE_FORMAT(so.requestDeliveryDate, '%d-%m-%Y') requestDeliveryDate, "
                    + "    DATE_FORMAT(so.expiredDate, '%d-%m-%Y') expiredDate, "
                    + "    so.currencyCode, "
                    + "    so.unlockBy, "
                    + "    IFNULL(so.discountCode,'') AS discountCode, "
                    + "    IFNULL(dt.Name,'') AS discountName, "
                    + "    so.customerPurchaseOrderNo, "
                    + "    so.proformaInvoiceNo, "
                    + "    so.salesPersonCode, "
                    + "    sape.Name AS salesPersonName, "
                    + "    so.customerCode, "
                    + "    customer.Name AS customerName, "
                    + "    so.billToCode, "
                    + "    so.priceTypeCode, "
                    + "    so.shipToCode, "
                    + "    so.refNo, "
                    + "    so.remark, "
                    + "    so.totalTransactionAmount, "
                    + "    so.discountAmount, "
                    + "    IFNULL(so.roundedAmount,0) roundedAmount, "
                    + "    so.taxBaseAmount, "
                    + "    so.vatPercent, "
                    + "    so.vatAmount, "
                    + "    so.grandTotalAmount, "
                    + "    so.approvalStatus, "
                    + "    so.approvalReasonCode, "
                    + "    so.approvalBy, "
                    + "    so.approvalDate, "
                    + "    so.closingStatus, "
                    + "    so.closingDate, "
                    + "    so.closingBy, "
                    + "    IFNULL(pick.code,'') pickingListSalesOrderCode ";
            String qry = " "
                    + " FROM sal_sales_order so "
                    + " INNER JOIN mst_customer customer ON customer.code = so.customerCode "
                    + " INNER JOIN mst_sales_person sape ON sape.code = so.salesPersonCode "
                    + " LEFT JOIN mst_discount_type dt ON dt.code = so.discountCode "
                    + " LEFT JOIN ivt_picking_list_sales_order pick ON pick.SalesOrderCode = so.Code "
                    + ")so "
                    + " WHERE "
                    + "  so.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND so.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + "  AND so.customerName LIKE '%" + obj.get("customerName") + "%' "
                    + "  AND  IFNULL(so.remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "  AND  IFNULL(so.refNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "  AND(  "
                    + "    CASE  "
                    + "     WHEN 'PENDING'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         so.ApprovalStatus='PENDING'  "
                    + "     WHEN 'APPROVED'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         so.ApprovalStatus='APPROVED'  "
                    + "     WHEN 'REJECTED'='" + obj.get("approvalStatus") + "' THEN  "
                    + "         so.ApprovalStatus='REJECTED'  "
                    + "     ELSE 1=1  "
                    + "    END  "
                    + "   ) "
                    + "  AND(  "
                    + "    CASE  "
                    + "     WHEN 'OPEN'='" + obj.get("closingStatus") + "' THEN  "
                    + "         so.closingStatus='OPEN'  "
                    + "     WHEN 'CLOSED'='" + obj.get("closingStatus") + "' THEN  "
                    + "         so.closingStatus='CLOSED'  "
                    + "     ELSE 1=1  "
                    + "    END  "
                    + "   ) "
                    + "  AND so.transactionDate BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + "ORDER BY so." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "        sal_sales_order.code, "
                    + "        sal_sales_order.branchCode, "
                    + "        branch.Name AS branchName, "
                    + "        DATE_FORMAT(sal_sales_order.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "        DATE_FORMAT(sal_sales_order.requestDeliveryDate, '%d-%m-%Y') requestDeliveryDate, "
                    + "        DATE_FORMAT(sal_sales_order.expiredDate, '%d-%m-%Y') expiredDate, "
                    + "        sal_sales_order.currencyCode, "
                    + "        currency.Name AS currencyName, "
                    + "        sal_sales_order.customerPurchaseOrderNo, "
                    + "        sal_sales_order.proformaInvoiceNo, "
                    + "        IFNULL(sal_sales_order.salesOrderLinkCode,'') AS salesOrderLinkCode, "
                    + "        IFNULL(so_link.TransactionDate,'01-01-1900') AS salesOrderLinkDate,  "
                    + "        sal_sales_order.salesPersonCode, "
                    + "        sape.Name AS salesPersonName, "
                    + "        sal_sales_order.subsidyStatus, "
                    + "        sal_sales_order.customerCode, "
                    + "        customer.Name AS customerName, "
                    + "        sal_sales_order.priceTypeCode, "
                    + "        price_type.Name AS priceTypeName, "
                    + "        sal_sales_order.billToCode, "
                    + "        billTo.Name AS billToName, "
                    + "        billTo.Address AS billToAddress, "
                    + "        billTo.ContactPerson AS billToContactPerson, "
                    + "        billTo.NPWPStatus AS npwpStatus, "
                    + "        sal_sales_order.shipToCode, "
                    + "        shipTo.Name AS shipToName, "
                    + "        shipTo.Address AS shipToAddress, "
                    + "        shipTo.CityCode AS shipToCityCode, "
                    + "        city.Name AS shipToCityName, "
                    + "        shipTo.ContactPerson AS shipToContactPerson, "
                    + "        IFNULL(sal_sales_order.projectCode,'') AS projectCode, "
                    + "        IFNULL(project.Name,'') AS projectName, "
                    + "        IFNULL(parent.Code,'') AS projectParentCode, "
                    + "        IFNULL(parent.Name,'') AS projectParentName, "
                    + "        sal_sales_order.unlockBy, "
                    + "        sal_sales_order.discountCode, "
                    + "        discount_type.Name AS discountName, "
                    + "        sal_sales_order.refNo, "
                    + "        sal_sales_order.remark, "
                    + "        sal_sales_order.totalTransactionAmount, "
                    + "        sal_sales_order.discountAmount, "
                    + "        IFNULL(sal_sales_order.roundedAmount,0) roundedAmount, "
                    + "        sal_sales_order.taxBaseAmount, "
                    + "        sal_sales_order.vatPercent, "
                    + "        sal_sales_order.vatAmount, "
                    + "        sal_sales_order.grandTotalAmount, "
                    + "        sal_sales_order.approvalStatus, "
                    + "        sal_sales_order.approvalReasonCode, "
                    + "        sal_sales_order.approvalBy, "
                    + "        sal_sales_order.approvalDate, "
                    + "        sal_sales_order.closingStatus, "
                    + "        sal_sales_order.closingDate, "
                    + "        sal_sales_order.closingBy  "
                    + "  FROM sal_sales_order "
                    + "        INNER JOIN mst_branch branch ON branch.Code = sal_sales_order.BranchCode "
                    + "        INNER JOIN mst_currency currency ON currency.Code = sal_sales_order.currencyCode "
                    + "        LEFT JOIN sal_sales_order so_link ON so_link.Code = sal_sales_order.SalesOrderLinkCode "
                    + "        INNER JOIN mst_sales_person sape ON sape.Code = sal_sales_order.SalesPersonCode "
                    + "        INNER JOIN mst_customer customer ON customer.Code = sal_sales_order.CustomerCode  "
                    + "        INNER JOIN mst_price_type price_type ON price_type.Code = sal_sales_order.PriceTypeCode "
                    + "        INNER JOIN mst_customer_jn_address billTo ON billTo.Code = sal_sales_order.BillToCode "
                    + "        INNER JOIN mst_customer_jn_address shipTo ON shipTo.Code = sal_sales_order.ShipToCode "
                    + "        INNER JOIN mst_city city ON city.Code = shipTo.CityCode "
                    + "        LEFT JOIN mst_project project ON project.Code = sal_sales_order.ProjectCode "
                    + "        LEFT JOIN mst_project_parent parent ON parent.Code = project.ProjectParentCode "
                    + "        LEFT JOIN mst_discount_type discount_type ON discount_type.Code = sal_sales_order.discountCode "
                    + " WHERE "
                    + "  sal_sales_order.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataForDeliveryNote(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "        sal_sales_order.code, "
                    + "        sal_sales_order.branchCode, "
                    + "        branch.Name AS branchName, "
                    + "        sal_sales_order.customerCode, "
                    + "        customer.Name AS customerName, "
                    + "        sal_sales_order.salesPersonCode, "
                    + "        sape.Name AS salesPersonName, "
                    + "        sal_sales_order.shipToCode, "
                    + "        shipTo.Name AS shipToName, "
                    + "        shipTo.Phone1 AS shipToPhone1, "
                    + "        shipTo.Address AS shipToAddress, "
                    + "        shipTo.CityCode AS shipToCityCode, "
                    + "        city.Name AS shipToCityName, "
                    + "        shipTo.ContactPerson AS shipToContactPerson "
                    + "  FROM sal_sales_order "
                    + "        INNER JOIN mst_branch branch ON branch.Code = sal_sales_order.BranchCode "
                    + "        INNER JOIN mst_sales_person sape ON sape.Code = sal_sales_order.SalesPersonCode "
                    + "        INNER JOIN mst_customer customer ON customer.Code = sal_sales_order.CustomerCode  "
                    + "        INNER JOIN mst_customer_jn_address shipTo ON shipTo.Code = sal_sales_order.ShipToCode "
                    + "        INNER JOIN mst_city city ON city.Code = shipTo.CityCode "
                    + " WHERE "
                    + "  sal_sales_order.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object dataStock(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT "
                    + " mst_item_jn_current_stock.code, "
                    + " mst_item_jn_current_stock.warehouseCode, "
                    + " mst_warehouse.Name AS warehouseName, "
                    + " mst_item_jn_current_stock.itemCode, "
                    + " mst_item.UnitOfMeasureCode AS unitOfMeasureCode, "
                    + " mst_item_jn_current_stock.rackCode, "
                    + " mst_rack.Name AS rackName, "
                    + " mst_item_jn_current_stock.actualStock "
                    + " FROM "
                    + " mst_item_jn_current_stock "
                    + " INNER JOIN mst_warehouse ON mst_warehouse.Code = mst_item_jn_current_stock.WarehouseCode "
                    + " INNER JOIN mst_item ON mst_item.Code = mst_item_jn_current_stock.ItemCode "
                    + " INNER JOIN mst_rack ON mst_item_jn_current_stock.RackCode =mst_rack.Code "
                    + " WHERE "
                    + " mst_item_jn_current_stock.itemCode = '" + obj.get("code") + "' "
                    + " GROUP BY mst_item_jn_current_stock.warehouseCode, mst_item_jn_current_stock.itemCode, mst_item_jn_current_stock.rackCode";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    sal_sales_order.code, "
                    + "    sal_sales_order.branchCode, "
                    + "    DATE_FORMAT(sal_sales_order.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    DATE_FORMAT(sal_sales_order.requestDeliveryDate, '%d-%m-%Y') requestDeliveryDate, "
                    + "    DATE_FORMAT(sal_sales_order.expiredDate, '%d-%m-%Y') expiredDate, "
                    + "    sal_sales_order.currencyCode, "
                    + "    sal_sales_order.unlockBy, "
                    + "    sal_sales_order.discountCode, "
                    + "    sal_sales_order.customerPurchaseOrderNo, "
                    + "    sal_sales_order.proformaInvoiceNo, "
                    + "    sal_sales_order.salesPersonCode, "
                    + "    sal_sales_order.customerCode, "
                    + "    sal_sales_order.billToCode, "
                    + "    sal_sales_order.priceTypeCode, "
                    + "    sal_sales_order.shipToCode, "
                    + "    sal_sales_order.refNo, "
                    + "    sal_sales_order.remark, "
                    + "    sal_sales_order.totalTransactionAmount, "
                    + "    sal_sales_order.discountAmount, "
                    + "    sal_sales_order.taxBaseAmount, "
                    + "    sal_sales_order.vatPercent, "
                    + "    sal_sales_order.vatAmount, "
                    + "    sal_sales_order.grandTotalAmount, "
                    + "    sal_sales_order.approvalStatus, "
                    + "    sal_sales_order.approvalReasonCode, "
                    + "    sal_sales_order.approvalBy, "
                    + "    sal_sales_order.approvalDate, "
                    + "    sal_sales_order.closingStatus, "
                    + "    sal_sales_order.closingDate, "
                    + "    sal_sales_order.closingBy "
                    + "  FROM sal_sales_order "
                    + " WHERE "
                    + "  sal_sales_order.code LIKE '%" + obj.get("code") + "%' "
                    + " ORDER BY sal_sales_order." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults lookUpForDeliveryNote(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(so.code) FROM( SELECT";
            String list = "SELECT so.* FROM( SELECT ";
            String select = " "
                    + "	sal_sales_order.code,  "
                    + "	DATE_FORMAT(sal_sales_order.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "	sal_sales_order.transactionDate searchDate, "
                    + "	sal_sales_order.branchCode,  "
                    + "	mst_branch.Name branchName, "
                    + "	sal_sales_order.approvalStatus, "
                    + "	sal_sales_order.customerCode, "
                    + "	mst_customer.Name customerName, "
                    + "	IFNULL(sal_sales_order.projectCode,'') projectCode, "
                    + "	IFNULL(prj.Name,'') projectName, "
                    + "	IFNULL(qry.jumlahPickingList,0) jumlahPickingList, "
                    + "	sal_sales_order.salesPersonCode,  "
                    + "	mst_sales_person.Name AS salesPersonName,  "
                    + "	sal_sales_order.shipToCode,  "
                    + "	mst_customer_jn_address.Name shipToName, "
                    + "	mst_customer_jn_address.Address shipToAddress, "
                    + "	mst_customer_jn_address.CityCode shipToCityCode, "
                    + "	mst_city.Name shipToCityName, "
                    + "	mst_customer_jn_address.ContactPerson shipToContactPerson, "
                    + "	mst_customer_jn_address.phone1 shipToPhone1, "
                    + "	mst_customer_jn_address.phone2 shipToPhone2, "
                    + "	mst_customer_jn_address.fax shipToFax ";
            String qry = " "
                    + "FROM sal_sales_order "
                    + "INNER JOIN mst_branch ON sal_sales_order.BranchCode=mst_branch.code "
                    + "INNER JOIN mst_customer ON mst_customer.code = sal_sales_order.customerCode  "
                    + "INNER JOIN mst_sales_person ON mst_sales_person.code = sal_sales_order.salesPersonCode  "
                    + "INNER JOIN mst_customer_jn_address ON sal_sales_order.BillToCode=mst_customer_jn_address.Code "
                    + "INNER JOIN mst_city ON mst_customer_jn_address.CityCode=mst_city.Code "
                    + "LEFT JOIN mst_project prj ON prj.code = sal_sales_order.projectCode "
                    + "LEFT JOIN mst_discount_type dt ON dt.code = sal_sales_order.discountCode "
                    + "LEFT JOIN( "
                    + "	SELECT "
                    + "		ivt_picking_list_sales_order.SalesOrderCode, "
                    + "		COUNT(ivt_picking_list_sales_order.SalesOrderCode) jumlahPickingList  "
                    + "	FROM ivt_picking_list_sales_order "
//                    + " LEFT JOIN( "
//                    + "	SELECT "
//                    + "		dln.SalesOrderCode, "
//                    + "		dln_jn_pick.PickingListSalesOrderCode  "
//                    + "	FROM ivt_delivery_note_sales_order dln "
//                    + "	INNER JOIN ivt_delivery_note_sales_order_jn_picking_list dln_jn_pick ON dln_jn_pick.HeaderCode = dln.Code "
//                    + ")dln ON ivt_picking_list_sales_order.Code=dln.PickingListSalesOrderCode "
                    + "	WHERE ivt_picking_list_sales_order.ConfirmationStatus = 'CONFIRMED' "
//                    + "	AND dln.PickingListSalesOrderCode IS NULL "
                    + "	GROUP BY ivt_picking_list_sales_order.SalesOrderCode "
                    + ")qry ON sal_sales_order.Code=qry.SalesOrderCode "
                    + " ) so "
                    + "WHERE so.approvalStatus='APPROVED' "
                    + " AND so.jumlahPickingList>0 "
                    + " AND so.code LIKE '%" + obj.get("code") + "%' "
                    + " AND so.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + " AND so.customerName LIKE '%" + obj.get("customerName") + "%' "
                    + " AND IFNULL(so.projectCode,'') LIKE '%" + obj.get("projectCode") + "%' "
                    + " AND IFNULL(so.projectName,'') LIKE '%" + obj.get("projectName") + "%' "
                    + " AND DATE(so.searchDate) BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + " ORDER BY so." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> listDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "   sal_sales_order_detail.code, "
                    + "   sal_sales_order_detail.headerCode, "
                    + "   sal_sales_order_detail.itemCode, "
                    + "   sal_sales_order_detail.itemAlias, "
                    + "   sal_sales_order_detail.remark, "
                    + "   sal_sales_order_detail.quantity, "
                    + "   sal_sales_order_detail.price, "
                    + "   sal_sales_order_detail.discountPercentage1, "
                    + "   sal_sales_order_detail.discountAmount1, "
                    + "   sal_sales_order_detail.discountPercentage2, "
                    + "   sal_sales_order_detail.discountAmount2, "
                    + "   sal_sales_order_detail.discountPercentage3, "
                    + "   sal_sales_order_detail.discountAmount3, "
                    + "   sal_sales_order_detail.discountHeaderPercent, "
                    + "   sal_sales_order_detail.discountHeaderAmount, "
                    + "   sal_sales_order_detail.nettPrice, "
                    + "   sal_sales_order_detail.totalAmount, "
                    + "   sal_sales_order_detail.discountHeaderDetailPercentFull, "
                    + "   sal_sales_order_detail.discountHeaderDetailAmountFull, "
                    + "   sal_sales_order_detail.discountHeaderDetailPercent, "
                    + "   sal_sales_order_detail.discountHeaderDetailAmount, "
                    + "   sal_sales_order_detail.taxBaseHeaderDetailAmount, "
                    + "   sal_sales_order_detail.vatHeaderDetailAmount, "
                    + "   sal_sales_order_detail.pph22HeaderDetailAmount, "
                    + "   sal_sales_order_detail.otherFeeHeaderDetailAmount "
                    + "  FROM sal_sales_order_detail "
                    + " WHERE "
                    + "  sal_sales_order_detail.headerCode LIKE '%" + obj.get("code") + "%' "
                    + " ORDER BY sal_sales_order_detail." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "   COUNT(sal_sales_order_detail.code) "
                    + " FROM sal_sales_order_detail "
                    + " WHERE "
                    + "  sal_sales_order_detail.headerCode = '" + obj.get("code") + "' ";
            long countDataDetail = countResult(qry);
            return countDataDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "   sal_sales_order_detail.code, "
                    + "   sal_sales_order_detail.headerCode, "
                    + "   sal_sales_order_detail.itemCode, "
                    + "   mst_item.name AS itemName, "
                    + "   sal_sales_order_detail.catalogNo, "
                    + "   sal_sales_order_detail.itemAlias, "
                    + "   sal_sales_order_detail.remark, "
                    + "   sal_sales_order_detail.quantity, "
                    + "   mst_item.unitOfMeasureCode, "
                    + "   sal_sales_order_detail.price, "
                    + "   CASE  "
                    + "     WHEN sal_sales_order.subsidyStatus= 1 THEN  "
                    + "         sal_sales_order_detail.price * 1.11   "
                    + "     ELSE 0 "
                    + "   END  AS subsidyPrice,"
                    + "   sal_sales_order_detail.discountCode, "
                    + "   discount_type.Name AS discountName, "
                    + "   sal_sales_order_detail.discountAmount, "
                    + "   sal_sales_order_detail.nettPrice, "
                    + "   sal_sales_order_detail.totalAmount, "
                    + "   sal_sales_order_detail.discountHeaderDetailPercentFull, "
                    + "   sal_sales_order_detail.discountHeaderDetailAmountFull, "
                    + "   sal_sales_order_detail.discountHeaderDetailPercent, "
                    + "   sal_sales_order_detail.discountHeaderDetailAmount, "
                    + "   sal_sales_order_detail.taxBaseHeaderDetailAmount, "
                    + "   sal_sales_order_detail.vatHeaderDetailAmount, "
                    + "   sal_sales_order_detail.pph22HeaderDetailAmount, "
                    + "   sal_sales_order_detail.otherFeeHeaderDetailAmount "
                    + "  FROM sal_sales_order_detail "
                    + "  INNER JOIN sal_sales_order ON sal_sales_order.code = sal_sales_order_detail.headerCode "
                    + "  INNER JOIN mst_item ON mst_item.code = sal_sales_order_detail.itemCode "
                    + "  LEFT JOIN mst_discount_type discount_type ON discount_type.code = sal_sales_order_detail.discountCode "
                    + " WHERE "
                    + "  sal_sales_order_detail.headerCode = '" + obj.get("code") + "' "
                    + " ORDER BY sal_sales_order_detail." + sort[0] + " " + sort[1];
            long countDataDetail = this.countDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countDataDetail, totalPage(countDataDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countTermDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "   COUNT(termDetail.code) "
                    + "  FROM sal_sales_order_payment_term_detail termDetail "
                    + "  INNER JOIN mst_payment_term term ON term.code = termDetail.PaymentTermCode "
                    + " WHERE "
                    + "  termDetail.headerCode = '" + obj.get("headerCode") + "' ";
            long countTermDetail = countResult(qry);
            return countTermDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults termDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "   termDetail.code, "
                    + "   termDetail.headerCode, "
                    + "   termDetail.paymentTermNo, "
                    + "   termDetail.paymentTermCode, "
                    + "   term.name paymentTermName, "
                    + "   termDetail.paymentTermPercent, "
                    + "   termDetail.remark remarkTerm "
                    + "  FROM sal_sales_order_payment_term_detail termDetail "
                    + "  INNER JOIN mst_payment_term term ON term.code = termDetail.PaymentTermCode "
                    + " WHERE "
                    + "  termDetail.headerCode = '" + obj.get("headerCode") + "' "
                    + " ORDER BY termDetail." + sort[0] + " " + sort[1];
            long countTermDetail = this.countTermDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countTermDetail, totalPage(countTermDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countARDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + "     COUNT(qr1.documentCode)  "
                    + " FROM ("
                    + "     SELECT "
                    + "         ciso.Code AS documentCode,"
                    + "         ciso.transactionDate,"
                    + "         ciso.currencyCode,"
                    + "         currency.Name AS currencyName,"
                    + "         ciso.exchangeRate,"
                    + "         ciso.grandTotalAmount,"
                    + "         ciso.paidAmount,"
                    + "         ciso.grandTotalAmount - ciso.paidAmount AS balanceAmount "
                    + " FROM fin_customer_invoice_sales_order ciso "
                    + " INNER JOIN mst_currency currency ON currency.Code = ciso.CurrencyCode "
                    + "     WHERE ciso.customerCode = '" + obj.get("customerCode") + "' "
                    + "     AND ciso.grandTotalAmount > ciso.paidAmount "
                    + " UNION ALL "
                    + " SELECT "
                    + "         cdn.Code AS documentCode,"
                    + "         cdn.transactionDate,"
                    + "         cdn.currencyCode,"
                    + "         currency.Name AS currencyName,"
                    + "         cdn.exchangeRate,"
                    + "         cdn.grandTotalAmount,"
                    + "         cdn.paidAmount,"
                    + "         cdn.grandTotalAmount - cdn.paidAmount AS balanceAmount "
                    + " FROM fin_customer_debit_note cdn "
                    + " INNER JOIN mst_currency currency ON currency.Code = cdn.CurrencyCode "
                    + "     WHERE cdn.customerCode = '" + obj.get("customerCode") + "' "
                    + "     AND cdn.grandTotalAmount > cdn.paidAmount "
                    + " ) AS qr1 ";
            long countDataARDetail = countResult(qry);
            return countDataARDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingARDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "   IFNULL(qr1.documentCode,'') documentCode, "
                    + "   DATE_FORMAT(qr1.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "   IFNULL(qr1.currencyCode,'') currencyCode, "
                    + "   IFNULL(qr1.currencyName,'') currencyName, "
                    + "   DATE_FORMAT(qr1.dueDate, '%d-%m-%Y') dueDate, "
                    + "   IFNULL(qr1.exchangeRate,0) exchangeRate, "
                    + "   IFNULL(qr1.grandTotalAmount,0) grandTotalAmount, "
                    + "   IFNULL(qr1.paidAmount,0) paidAmount, "
                    + "   IFNULL(qr1.balanceAmount,0)  balanceAmount"
                    + " FROM ("
                    + "     SELECT "
                    + "         ciso.Code AS documentCode,"
                    + "         ciso.transactionDate,"
                    + "         ciso.currencyCode,"
                    + "         currency.Name AS currencyName,"
                    + "         ciso.dueDate, "
                    + "         ciso.exchangeRate,"
                    + "         ciso.grandTotalAmount,"
                    + "         ciso.paidAmount,"
                    + "         ciso.grandTotalAmount - ciso.paidAmount AS balanceAmount "
                    + " FROM fin_customer_invoice_sales_order ciso "
                    + " INNER JOIN mst_currency currency ON currency.Code = ciso.CurrencyCode "
                    + "	INNER JOIN mst_payment_term term ON ciso.PaymentTermCode = term.Code "
                    + "     WHERE ciso.customerCode = '" + obj.get("customerCode") + "' "
                    + "     AND ciso.grandTotalAmount > ciso.paidAmount "
                    + " UNION ALL "
                    + " SELECT "
                    + "         cdn.Code AS documentCode,"
                    + "         cdn.transactionDate,"
                    + "         cdn.currencyCode,"
                    + "         currency.Name AS currencyName,"
                    + "         cdn.dueDate, "
                    + "         cdn.exchangeRate,"
                    + "         cdn.grandTotalAmount,"
                    + "         cdn.paidAmount,"
                    + "         cdn.grandTotalAmount - cdn.paidAmount AS balanceAmount "
                    + " FROM fin_customer_debit_note cdn "
                    + " INNER JOIN mst_currency currency ON currency.Code = cdn.CurrencyCode "
                    + "     WHERE cdn.customerCode = '" + obj.get("customerCode") + "' "
                    + "     AND cdn.grandTotalAmount > cdn.paidAmount "
                    + " ) AS qr1 "
                    + " ORDER BY qr1." + sort[0] + " " + sort[1];
            long countDataARDetail = this.countARDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countDataARDetail, totalPage(countDataARDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDataStock(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT "
                    + " COUNT(mst_item_jn_current_stock.code)  "
                    + " FROM "
                    + " mst_item_jn_current_stock "
                    + " INNER JOIN mst_warehouse ON mst_warehouse.Code = mst_item_jn_current_stock.WarehouseCode "
                    + " INNER JOIN mst_item ON mst_item.Code = mst_item_jn_current_stock.ItemCode "
                    + " INNER JOIN mst_rack ON mst_item_jn_current_stock.RackCode =mst_rack.Code "
                    + " WHERE "
                    + " mst_item_jn_current_stock.itemCode = '" + obj.get("code") + "' "
                    + " GROUP BY mst_item_jn_current_stock.warehouseCode, mst_item_jn_current_stock.itemCode, mst_item_jn_current_stock.rackCode ";
            long countDataARDetail = countResult(qry);
            return countDataARDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults dataStock(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + " mst_item_jn_current_stock.code, "
                    + " mst_item_jn_current_stock.warehouseCode, "
                    + " mst_warehouse.Name AS warehouseName, "
                    + " mst_item_jn_current_stock.itemCode, "
                    + " mst_item.UnitOfMeasureCode AS unitOfMeasureCode, "
                    + " mst_item_jn_current_stock.rackCode, "
                    + " mst_rack.Name AS rackName, "
                    + " SUM(mst_item_jn_current_stock.actualStock)  actualStock "
                    + " FROM "
                    + " mst_item_jn_current_stock "
                    + " INNER JOIN mst_warehouse ON mst_warehouse.Code = mst_item_jn_current_stock.WarehouseCode "
                    + " INNER JOIN mst_item ON mst_item.Code = mst_item_jn_current_stock.ItemCode "
                    + " INNER JOIN mst_rack ON mst_item_jn_current_stock.RackCode =mst_rack.Code "
                    + " WHERE "
                    + " mst_item_jn_current_stock.itemCode = '" + obj.get("code") + "' "
                    + " GROUP BY mst_item_jn_current_stock.warehouseCode,  mst_item_jn_current_stock.rackCode, mst_item_jn_current_stock.itemCode  "
                    + " ORDER BY mst_item_jn_current_stock." + sort[0] + " " + sort[1];
            long countDataStock = this.countDataStock(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countDataStock, totalPage(countDataStock, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(so.code) FROM( SELECT";
            String list = "SELECT so.* FROM( SELECT ";
            String select = " "
                    + "    so.code, "
                    + "    so.branchCode, "
                    + "    DATE_FORMAT(so.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    DATE_FORMAT(so.requestDeliveryDate, '%d-%m-%Y') requestDeliveryDate, "
                    + "    DATE_FORMAT(so.expiredDate, '%d-%m-%Y') expiredDate, "
                    + "    so.transactionDate searchDate, "
                    + "    so.currencyCode, "
                    + "    so.projectCode, "
                    + "    prj.Name projectName, "
                    + "    so.unlockBy, "
                    + "    so.discountCode, "
                    + "    dt.Name discountName, "
                    + "    so.customerPurchaseOrderNo, "
                    + "    so.proformaInvoiceNo, "
                    + "    so.salesPersonCode, "
                    + "    mst_sales_person.Name AS salesPersonName, "
                    + "    so.customerCode, "
                    + "    mst_customer.Name AS customerName, "
                    + "    so.billToCode, "
                    + "    so.priceTypeCode, "
                    + "    so.shipToCode, "
                    + "    shipTo.Name AS shipToName, "
                    + "    shipTo.cityCode AS shipToCityCode, "
                    + "    city.Name AS shipToCityName, "
                    + "    shipTo.Address AS shipToAddress, "
                    + "    shipTo.ContactPerson AS shipToContactPerson, "
                    + "    so.refNo, "
                    + "    so.remark, "
                    + "    so.totalTransactionAmount, "
                    + "    so.discountAmount, "
                    + "    so.taxBaseAmount, "
                    + "    so.vatPercent, "
                    + "    so.vatAmount, "
                    + "    so.grandTotalAmount, "
                    + "    so.approvalStatus, "
                    + "    so.approvalReasonCode, "
                    + "    so.approvalBy, "
                    + "    so.approvalDate, "
                    + "    so.closingStatus, "
                    + "    so.closingDate, "
                    + "    so.closingBy ";
            String qry = " "
                    + " FROM sal_sales_order so "
                    + " INNER JOIN mst_customer ON mst_customer.code = so.customerCode "
                    + " INNER JOIN mst_sales_person ON mst_sales_person.code = so.salesPersonCode "
                    + " INNER JOIN mst_customer_jn_address shipTo ON shipTo.code = so.shipToCode "
                    + " INNER JOIN mst_city city ON city.code = shipTo.cityCode "
                    + " LEFT JOIN mst_project prj ON prj.code = so.projectCode "
                    + " LEFT JOIN mst_discount_type dt ON dt.code = so.discountCode "
                    + ")so "
                    + " WHERE "
                    + "  so.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND so.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + "  AND so.customerName LIKE '%" + obj.get("customerName") + "%' "
                    + "  AND IFNULL(so.projectCode,'') LIKE '%" + obj.get("projectCode") + "%' "
                    + "  AND IFNULL(so.projectName,'') LIKE '%" + obj.get("projectName") + "%' "
                    + "  AND so.searchDate BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + " ORDER BY so." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countHistory(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT COUNT(documentSales.code) "
                    + " FROM( "
                    + "     SELECT  "
                    + "         inv.code,  "
                    + "         DATE_FORMAT(inv.transactionDate, '%d-%m-%Y') transactionDate,  "
                    + "         'INV' AS documentType,  "
                    + "         inv.customerCode,  "
                    + "         mst_customer.Name AS customerName,  "
                    + "         inv.currencyCode,  "
                    + "         inv.exchangeRate, "
                    + "         inv.refNo,  "
                    + "         inv.remark "
                    + "     FROM fin_customer_invoice_sales_order inv "
                    + "     INNER JOIN mst_customer ON inv.CustomerCode=mst_customer.Code "
                    + "     WHERE IFNULL(inv.Code,'') LIKE '%" + obj.get("code") + "%'  "
                    + "         AND IFNULL(inv.CustomerCode,'') LIKE '%" + obj.get("customerCode") + "%' "
                    + "         AND IFNULL(mst_customer.Name,'') LIKE '%" + obj.get("customerName") + "%' "
                    + "         AND IFNULL(inv.RefNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "         AND IFNULL(inv.Remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "         AND DATE(inv.TransactionDate) BETWEEN '" + obj.get("transactionStartDate") + "' AND '" + obj.get("transactionEndDate") + "' "
                    + " ) AS documentSales ";
            long countHistory = countResult(qry);
            return countHistory;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingHistory(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry
                    = " SELECT documentSales.* "
                    + " FROM( "
                    + "     SELECT  "
                    + "         inv.code,  "
                    + "         DATE_FORMAT(inv.transactionDate, '%d-%m-%Y') transactionDate,  "
                    + "         'INV' AS documentType,  "
                    + "         inv.customerCode,  "
                    + "         mst_customer.Name AS customerName,  "
                    + "         inv.currencyCode,  "
                    + "         inv.exchangeRate, "
                    + "         inv.refNo,  "
                    + "         inv.remark "
                    + "     FROM fin_customer_invoice_sales_order inv "
                    + "     INNER JOIN mst_customer ON inv.CustomerCode=mst_customer.Code "
                    + "     WHERE IFNULL(inv.Code,'') LIKE '%" + obj.get("code") + "%'  "
                    + "         AND IFNULL(inv.CustomerCode,'') LIKE '%" + obj.get("customerCode") + "%' "
                    + "         AND IFNULL(mst_customer.Name,'') LIKE '%" + obj.get("customerName") + "%' "
                    + "         AND IFNULL(inv.RefNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "         AND IFNULL(inv.Remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "         AND DATE(inv.TransactionDate) BETWEEN '" + obj.get("transactionStartDate") + "' AND '" + obj.get("transactionEndDate") + "' "
                    + " ) AS documentSales "
                    + " ORDER BY documentSales." + sort[0] + " " + sort[1];
            long countHistory = this.countHistory(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countHistory, totalPage(countHistory, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countHistoryDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = "";
            switch (obj.get("type").toString()) {
//                case "CDN":
//                    qry = " "
//                            + "SELECT "
//                            + "	Count(cdnd.code) "
//                            + "FROM fin_customer_debit_note_detail cdnd "
//                            + "WHERE cdnd.HeaderCode = '" + obj.get("code") + "' ";
//                    break;
                case "INV":
                    qry = " "
                            + "  SELECT COUNT(invSoDetail.code) "
                            + "  FROM fin_customer_invoice_sales_order_item_detail invSoDetail "
                            + "  INNER JOIN mst_item item ON item.code = invSoDetail.itemCode "
                            + "  LEFT JOIN mst_discount_type discount_type ON discount_type.code = invSoDetail.discountCode "
                            + "  LEFT JOIN ivt_delivery_note_sales_order_item_detail dlnDetail ON dlnDetail.Code = invSoDetail.DeliveryNoteSalesOrderDetailCode "
                            + "  LEFT JOIN ivt_picking_list_sales_order_item_detail pickDetail ON pickDetail.Code = dlnDetail.PickingListSalesOrderDetailCode "
                            + "  LEFT JOIN sal_sales_order_detail soDetail ON soDetail.Code = pickDetail.SalesOrderDetailCode "
                            + " WHERE invSoDetail.headerCode = '" + obj.get("code") + "' ";
                    break;
                default:
                    break;
            }
            long countHistoryDetail = countResult(qry);
            return countHistoryDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingHistoryDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = "";
            switch (obj.get("type").toString()) {
//                case "CDN":
//                    qry = " "
//                            + "SELECT "
//                            + "	IFNULL(cdnd.code,'') code "
//                            + "	IFNULL(cdnd.headerCode,'') headerCode "
//                            + "	'' itemCode, "
//                            + "	'' itemName, "
//                            + "	'' itemAlias, "
//                            + "	'' catalogNo, "
//                            + " IFNULL(cdnd.remark,'') remark, "
//                            + "	IFNULL(cdnd.quantity,0) quantity "
//                            + "	'' unitOfMeasureCode, "
//                            + "	IFNULL(cdnd.price,0) price "
//                            + "	IFNULL(cdnd.quantity,0) * IFNULL(cdnd.price,0) nettPrice,  "
//                            + " IFNULL(cdnd.totalAmount,0) totalAmount "
//                            + "FROM fin_customer_debit_note_detail cdnd "
//                            + "WHERE cdnd.HeaderCode = '" + obj.get("code") + "' "
//                            + " ORDER BY cdnd.HeaderCode ASC, cdnd." + sort[0] + " " + sort[1];
//                    break;
                case "INV":
                    qry = " "
                            + "  SELECT "
                            + "   invSoDetail.code, "
                            + "   invSoDetail.headerCode, "
                            + "   invSoDetail.itemCode, "
                            + "   item.name AS itemName, "
                            + "   IFNULL(dlnDetail.itemAlias,'') itemAlias, "
                            + "   IFNULL(soDetail.catalogNo,'') catalogNo, "
                            + "   IFNULL(soDetail.remark,'') remark, "
                            + "   invSoDetail.quantity, "
                            + "   item.unitOfMeasureCode, "
                            + "   invSoDetail.price, "
                            + "   invSoDetail.nettPrice, "
                            + "   invSoDetail.totalAmount "
                            + "  FROM fin_customer_invoice_sales_order_item_detail invSoDetail "
                            + "  INNER JOIN mst_item item ON item.code = invSoDetail.itemCode "
                            + "  LEFT JOIN mst_discount_type discount_type ON discount_type.code = invSoDetail.discountCode "
                            + "  LEFT JOIN ivt_delivery_note_sales_order_item_detail dlnDetail ON dlnDetail.Code = invSoDetail.DeliveryNoteSalesOrderDetailCode "
                            + "  LEFT JOIN ivt_picking_list_sales_order_item_detail pickDetail ON pickDetail.Code = dlnDetail.PickingListSalesOrderDetailCode "
                            + "  LEFT JOIN sal_sales_order_detail soDetail ON soDetail.Code = pickDetail.SalesOrderDetailCode "
                            + " WHERE invSoDetail.headerCode = '" + obj.get("code") + "' "
                            + " ORDER BY invSoDetail." + sort[0] + " " + sort[1];
                    break;
                default:
                    break;
            }
            long countHistoryDetail = this.countHistoryDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countHistoryDetail, totalPage(countHistoryDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public List<Map<String, Object>> test(JSONObject obj, long page, long size) {
        try {
            return listPagingResult("select * from sal_sales_order_detail", page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
