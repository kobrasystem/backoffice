package com.system.api.sales.repo;

import com.system.api.sales.model.SalesOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface SalesOrderRepo extends JpaRepository<SalesOrder, String> {

    SalesOrder findByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
