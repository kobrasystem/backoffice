package com.system.api.sales.repo;

import com.system.api.sales.model.SalesOrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface SalesOrderDetailRepo extends JpaRepository<SalesOrderDetail, String> {

    SalesOrderDetail findByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);
    
    @Transactional
    void deleteByHeaderCode(String code);

}
