
package com.system.api.sales.model;

import lombok.Data;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

/**
 *
 * @author Rayis
 */

@Data
@Entity
@Table(name = "fin_customer_invoice_sales_order_jn_delivery_note")
public class CustomerInvoiceSalesOrderDeliveryNote extends BaseModel{
    
    @Id
    @SheetColumn("Code")
    @Column (name="Code")
    private String code = "";
    
    @SheetColumn("HeaderCode")
    @Column (name="HeaderCode")
    private String headerCode = "";
        
    @SheetColumn("OldHeaderCode")
    @Column (name="OldHeaderCode")
    private String oldHheaderCode = "";
    
    @SheetColumn("DeliveryNoteCode")
    @Column (name="DeliveryNoteCode")
    private String deliveryNoteCode = "";
        
    @SheetColumn("OldDeliveryNoteCode")
    @Column (name="OldDeliveryNoteCode")
    private String oldDeliveryNoteCode = "";
    
//    @Column(name = "DocumentDate")
//    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
//    private Date documentDate = DateUtils.newDate(1900, 01, 01);
    
}
