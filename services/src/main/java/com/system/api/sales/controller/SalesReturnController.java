/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.controller;

import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import com.system.api.sales.model.CustomerInvoiceSalesOrder;
import com.system.api.sales.model.SalesReturn;
import com.system.api.sales.model.SalesReturnDetail;
import com.system.api.sales.service.SalesReturnService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.IOExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Rayis
 */
@RestController
@Api(tags = "Sales Return")
@RequestMapping("/sales-return")
public class SalesReturnController extends BaseProgress {

    @Autowired
    private SalesReturnService service;

    @Authorization(value = "Authorization")
    @GetMapping(value = "/search")
    public HttpEntity<Object> list(
            @RequestParam(defaultValue = "", required = false) String code,
            @RequestParam(defaultValue = "", required = false) String customerCode,
            @RequestParam(defaultValue = "", required = false) String customerName,
            @RequestParam(defaultValue = "", required = false) String transactionStartDate,
            @RequestParam(defaultValue = "", required = false) String transactionEndDate,
            @RequestParam(defaultValue = "", required = false) String refNo,
            @RequestParam(defaultValue = "", required = false) String remark,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "code,ASC") String[] sort) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code
                    + "','customerCode':'" + customerCode
                    + "','customerName':'" + customerName
                    + "','transactionStartDate':'" + transactionStartDate
                    + "','transactionEndDate':'" + transactionEndDate
                    + "','refNo':'" + refNo
                    + "','remark':'" + remark
                    + "'}");
              return new HttpEntity<>(service.paging(obj, page, size, sort, SalesReturn.class));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @GetMapping(value = "/detail/search")
    public HttpEntity<Object> listDetail(
            @RequestParam(defaultValue = "", required = false) String headerCode,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "code,ASC") String[] sort) {
        try {
            JSONObject obj = new JSONObject("{'headerCode':'" + headerCode + "'}");
            return new HttpEntity<>(service.pagingDetail(obj, page, size, sort, SalesReturnDetail.class));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @GetMapping(value = "/data")
    public HttpEntity<Object> data(@RequestParam String code) {
        try {
            Object model = service.data(code, SalesReturn.class);
            if (model == null) {
                throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
            }

            return new HttpEntity<>(new ResponsBody(model));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @GetMapping(value = "inv/data")
    public HttpEntity<Object> dataInv(@RequestParam String code) {
        try {
            Object model = service.dataInv(code, CustomerInvoiceSalesOrder.class);
            if (model == null) {
                throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
            }

            return new HttpEntity<>(new ResponsBody(model));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @GetMapping(value = "/inv/search")
    public HttpEntity<Object> listINV(
            @RequestParam(defaultValue = "", required = false) String code,
            @RequestParam(defaultValue = "", required = false) String transactionStartDate,
            @RequestParam(defaultValue = "", required = false) String transactionEndDate,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "code,ASC") String[] sort) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code
                    + "','transactionStartDate':'" + transactionStartDate
                    + "','transactionEndDate':'" + transactionEndDate
                    + "'}");
            return new HttpEntity<>(service.pagingINV(obj, page, size, sort, SalesReturnDetail.class));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @GetMapping(value = "/item-inv/search")
    public HttpEntity<Object> listItemINV(
            @RequestParam(defaultValue = "", required = false) String code,
            @RequestParam(defaultValue = "", required = false) String itemCode,
            @RequestParam(defaultValue = "", required = false) String itemName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "code,ASC") String[] sort) {
        try {
            JSONObject obj = new JSONObject("{'code':'" + code
                    + "','itemCode':'" + itemCode
                    + "','itemName':'" + itemName
                    + "'}");
            return new HttpEntity<>(service.pagingItemINV(obj, page, size, sort, SalesReturnDetail.class));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @PostMapping(value = "/save")
    public HttpEntity<Object> save(@RequestBody SalesReturn model) {
        try {
            long data = service.checkData(model.getCode(), SalesReturn.class);
            if (data > 0) {
                throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
            }
            service.save(model);
            return new HttpEntity<>(new ResponsBody("Save data success"));
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @PostMapping(value = "/imports")
    public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
        try {
            List<SalesReturn> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, SalesReturn.class);
            for (SalesReturn m : model) {
                service.save(m);
            }
            return new HttpEntity<>(new ResponsBody(model));
        } catch (IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
        }
    }

    @Authorization(value = "Authorization")
    @GetMapping(value = "/exports")
    public void exports(
            @RequestParam(defaultValue = "", required = false) String code,
            @RequestParam(defaultValue = "", required = false) String name,
            @RequestParam(defaultValue = "", required = false) String bankCode,
            @RequestParam(defaultValue = "", required = false) String bankName,
            @RequestParam(defaultValue = "", required = false) String title,
            @RequestParam(defaultValue = "All") String activeStatus,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "code,ASC") String[] sort,
            HttpServletResponse response) throws ParseException {

        JSONObject obj = new JSONObject("{'code':'" + code
                + "','name':'" + name
                + "','activeStatus':'" + activeStatus
                + "'}");
        List<Map<String, Object>> list = service.list(obj, page, size, sort, SalesReturn.class);

        XSSFWorkbook wb = new XSSFWorkbook();
        IOExcel.exportUtils(wb, title);
        IOExcel.expCellValues("Sales Return", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
        int rowHeader = 1;
        IOExcel.cols = 0;
        IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
        IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
        IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
        IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
        IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

        int no = 1;
        int row = 2;
        for (int i = 0; i < list.size(); i++) {
            IOExcel.cols = 0;
            IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
            IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
            IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
            IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
            IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
            row++;
        }
        try {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=sales-" + title + ".xlsx");
            response.setStatus(HttpServletResponse.SC_OK);
            response.getOutputStream().write(IOExcel.exportExcel());
            response.getOutputStream().close();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        }
    }

}
