/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.model;

import lombok.Data;
import java.util.Date;
import java.util.List;
import javax.persistence.Id;
import java.math.BigDecimal;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.system.base.GroupModel;
import org.springframework.format.annotation.DateTimeFormat;
import io.github.millij.poi.ss.model.annotations.SheetColumn;


/**
 *
 * @author Rayis
 */
@Data
@Entity
@Table(name = "sal_sales_return_sales_order_by_invoice")
public class SalesReturn extends GroupModel {

    @Id
    @Column(name = "Code")
    @SheetColumn("Code")
    private String code = "";

    @Column(name = "TransactionDate")
    @SheetColumn("TransactionDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date transactionDate = new Date();

    @Column(name = "InvoiceSalesOrderCode")
    @SheetColumn("InvoiceSalesOrderCode")
    private String invoiceSalesOrderCode = "";

    @Column(name = "CustomerCode")
    @SheetColumn("CustomerCode")
    private String customerCode = "";

    @Column(name = "BillToCode")
    @SheetColumn("BillToCode")
    private String billToCode = "";

    @Column(name = "PaymentTermCode")
    @SheetColumn("PaymentTermCode")
    private String paymentTermCode = "";

    @Column(name = "DueDate")
    @SheetColumn("DueDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dueDate = new Date();

    @Column(name = "WarehouseCode")
    @SheetColumn("WarehouseCode")
    private String warehouseCode = "";
    
    @Column(name = "SalesPersonCode")
    @SheetColumn("SalesPersonCode")
    private String salesPersonCode = "";

    @Column(name = "RefNo")
    @SheetColumn("RefNo")
    private String refNo = "";

    @Column(name = "Remark")
    @SheetColumn("Remark")
    private String remark = "";

    @Column(name = "CustomerTaxInvoiceNo")
    @SheetColumn("CustomerTaxInvoiceNo")
    private String customerTaxInvoiceNo = "";

    @Column(name = "CustomerTaxInvoiceDate")
    @SheetColumn("CustomerTaxInvoiceDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date customerTaxInvoiceDate = new Date();

    @Column(name = "CurrencyCode")
    @SheetColumn("CurrencyCode")
    private String currencyCode = "";

    @Column(name = "ExchangeRate")
    @SheetColumn("ExchangeRate")
    private BigDecimal exchangeRate = new BigDecimal(0.00);

    @Column(name = "TotalTransactionAmount")
    @SheetColumn("TotalTransactionAmount")
    private BigDecimal totalTransactionAmount = new BigDecimal(0.00);
    
    @SheetColumn("DiscountCode")
    @Column(name = "DiscountCode")
    private String discountCode = "";

    @Column(name = "DiscountAmount")
    @SheetColumn("DiscountAmount")
    private BigDecimal discountAmount = new BigDecimal(0.00);

    @Column(name = "TaxBaseAmount")
    @SheetColumn("TaxBaseAmount")
    private BigDecimal taxBaseAmount = new BigDecimal(0.00);

    @SheetColumn("VATPercent")
    private BigDecimal vatPercent = new BigDecimal(0.00);

    @Column(name = "VATAmount")
    @SheetColumn("VATAmount")
    private BigDecimal vatAmount = new BigDecimal(0.00);

    @Column(name = "GrandTotalAmount")
    @SheetColumn("GrandTotalAmount")
    private BigDecimal grandTotalAmount = new BigDecimal(0.00);

    @Column(name = "PaidAmount")
    @SheetColumn("PaidAmount")
    private BigDecimal paidAmount = new BigDecimal(0.00);

    @Column(name = "SettlementDate")
    @SheetColumn("SettlementDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date settlementDate = new Date();

    @Column(name = "SettlementDocumentNo")
    @SheetColumn("SettlementDocumentNo")
    private String settlementDocumentNo = "";

    @Transient
    private transient String rackCode = "";
    private transient List<SalesReturnDetail> listSalesReturnDetail;

}
