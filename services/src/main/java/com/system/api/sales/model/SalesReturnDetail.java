/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.model;

import lombok.Data;
import java.math.BigDecimal;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

/**
 *
 * @author Rayis
 */

@Data
@Entity
@Table(name = "sal_sales_return_sales_order_by_invoice_item_detail")
public class SalesReturnDetail extends BaseModel{
    
  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code = "";

  @Column(name = "HeaderCode")
  @SheetColumn("HeaderCode")
  private String headerCode = "";
  
  @Column(name = "ItemCode")
  @SheetColumn("ItemCode")
  private String itemCode = "";
  
  @Column(name = "ItemAlias")
  @SheetColumn("ItemAlias")
  private String itemAlias = "";
  
  @Column(name = "Quantity")
  @SheetColumn("Quantity")
  private BigDecimal quantity = new BigDecimal(0.00);
  
  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";
  
//  @Column(name = "Price")
//  @SheetColumn("Price")
//  private BigDecimal price = new BigDecimal(0.00);
  
  @Column(name = "NettPrice")
  @SheetColumn("NettPrice")
  private BigDecimal nettPrice = new BigDecimal(0.00);
  
  @Column(name = "TotalAmount")
  @SheetColumn("TotalAmount")
  private BigDecimal totalAmount = new BigDecimal(0.00);

}
