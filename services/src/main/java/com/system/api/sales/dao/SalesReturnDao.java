/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * AND open the template in the editor.
 */
package com.system.api.sales.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rayis
 */
@Repository
public class SalesReturnDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM sal_sales_return_sales_order_by_invoice WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long count(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT COUNT(qry.code) FROM ( "
                    + "  SELECT "
                    + "    srt.code, "
                    + "    DATE_FORMAT(srt.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    srt.customerCode, "
                    + "    IFNULL(cus.name,'') AS customerName, "
                    + "    IFNULL(srt.refNo,'') AS refNo, "
                    + "    IFNULL(srt.remark,'') AS remark "
                    + "  FROM sal_sales_return_sales_order_by_invoice srt "
                    + "  INNER JOIN mst_customer cus ON srt.CustomerCode = cus.Code "
                    + "  INNER JOIN mst_branch branch ON srt.BranchCode = branch.Code "
                    + "  LEFT JOIN mst_discount_type dt ON dt.Code = srt.discountCode "
                    + "  ) qry "
                    + "  WHERE "
                    + " qry.code LIKE '%" +obj.get("code") + "%' "
                    + " AND qry.customerCode LIKE '%" +obj.get("customerCode") + "%' "
                    + " AND qry.customerName LIKE '%" +obj.get("customerName") + "%' "
                    + " AND qry.refNo LIKE '%" +obj.get("refNo") + "%' "
                    + " AND qry.remark LIKE '%" +obj.get("remark") + "%' "
                    + " AND qry.transactionDate BETWEEN '" +obj.get("transactionStartDate") + "' AND '" +obj.get("transactionEndDate") + "' "
                    + " ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT * FROM( "
                    + "  SELECT "
                    + "    srt.code, "
                    + "    DATE_FORMAT(srt.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    branch.Name branchName, "
                    + "    IFNULL(srt.invoiceSalesOrderCode,'') AS invoiceSalesOrderCode, "
                    + "    srt.customerCode, "
                    + "    IFNULL(cus.name,'') AS customerName, "
                    + "    IFNULL(srt.refNo,'') AS refNo, "
                    + "    IFNULL(srt.remark,'') AS remark, "
                    + "    srt.totalTransactionAmount, "
                    + "    srt.discountCode discountTypeCode, "
                    + "    dt.Name discountTypeName, "
                    + "    srt.discountAmount, "
                    + "    srt.taxBaseAmount, "
                    + "    srt.vatPercent, "
                    + "    srt.vatAmount, "
                    + "    srt.grandTotalAmount "
                    + "  FROM sal_sales_return_sales_order_by_invoice srt "
                    + "  INNER JOIN mst_customer cus ON srt.CustomerCode = cus.Code "
                    + "  INNER JOIN mst_branch branch ON srt.BranchCode = branch.Code "
                    + "  LEFT JOIN mst_discount_type dt ON dt.Code = srt.discountCode "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.code LIKE '%" +obj.get("code") + "%' "
                    + " AND qry.customerCode LIKE '%" +obj.get("customerCode") + "%' "
                    + " AND qry.customerName LIKE '%" +obj.get("customerName") + "%' "
                    + " AND qry.refNo LIKE '%" +obj.get("refNo") + "%' "
                    + " AND qry.remark LIKE '%" +obj.get("remark") + "%' "
                    + " AND qry.transactionDate BETWEEN '" +obj.get("transactionStartDate") + "' AND '" +obj.get("transactionEndDate") + "' ";
            qry += " ORDER BY qry." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  COUNT(srtDetailQry.code) FROM( "
                    + " SELECT "
                    + "  srtd.code, "
                    + "  srtd.headerCode "
                    + "  FROM sal_sales_return_sales_order_by_invoice_item_detail srtd "
                    + "  INNER JOIN mst_item ON srtd.ItemCode=mst_item.Code  "
                    + "  )srtDetailQry "
                    + "  WHERE srtDetailQry.headerCode = '" +obj.get("headerCode") + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT * FROM( "
                    + "  SELECT "
                    + "  srtd.code, "
                    + "  srtd.headerCode, "
                    + "  srtd.itemCode,  "
                    + "  item.Name AS itemName,  "
                    + "  srtd.itemAlias,  "
                    + "  srtd.catalogNo,  "
                    + "  srtd.quantity,  "
                    + "  item.unitOfMeasureCode, "
//                    + "  srtd.price,  "
                    + "  srtd.nettPrice,  "
                    + "  srtd.totalAmount,  "
                    + "  srtd.remark  "
                    + "  FROM sal_sales_return_sales_order_by_invoice_item_detail srtd "
                    + "  INNER JOIN mst_item item ON srtd.ItemCode=item.Code  "
                    + " )srtDetailQry "
                    + " WHERE "
                    + "  srtDetailQry.headerCode = '" +obj.get("headerCode") + "' "
                    + " ORDER BY srtDetailQry.Code " + sort[1];
            long countData = this.countDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT * FROM( "
                    + " SELECT "
                    + " srt.code, "
                    + " srt.branchCode, "
                    + " branch.Name AS branchName, "
                    + " DATE_FORMAT(srt.transactionDate, '%d/%m/%Y') transactionDate, "
                    + " srt.invoiceSalesOrderCode,"
                    + " srt.customerCode, "
                    + " IFNULL(cus.name,'') AS customerName,"
                    + " IFNULL(cus.address,'') AS customerAddress,"
                    + " srt.salesPersonCode,"
                    + " spe.Name salesPersonName,"
                    + " srt.billToCode,"
                    + " billTo.Name billToName,"
                    + " billTo.Address billToAddress,"
                    + " srt.currencyCode, "
                    + " cur.Name currencyName,"
                    + " srt.exchangeRate, "
                    + " srt.paymentTermCode,"
                    + " term.Name paymentTermName,"
                    + " term.Days paymentTermDays,"
                    + " DATE_FORMAT(srt.dueDate, '%d/%m/%Y') dueDate,"
                    + " srt.customerTaxInvoiceNo,"
                    + " DATE_FORMAT(srt.customerTaxInvoiceDate, '%d/%m/%Y') customerTaxInvoiceDate,"
                    + " srt.refNo, "
                    + " srt.remark,"
                    + " srt.totalTransactionAmount, "
                    + " srt.discountCode discountTypeCode, "
                    + " dt.Name discountTypeName, "
                    + " srt.discountAmount, "
                    + " srt.taxBaseAmount, "
                    + " srt.vatPercent, "
                    + " srt.vatAmount, "
                    + " srt.grandTotalAmount "
                    + " FROM sal_sales_return_sales_order_by_invoice srt "
                    + " INNER JOIN mst_customer_jn_address billTo ON srt.BillToCode = billTo.Code  "
                    + " INNER JOIN mst_payment_term term ON srt.PaymentTermCode = term.Code "
                    + " INNER JOIN mst_sales_person spe ON srt.salesPersonCode = spe.Code "
                    + " LEFT JOIN mst_discount_type dt ON dt.Code = srt.discountCode "
                    + " INNER JOIN mst_branch branch ON srt.branchCode = branch.Code "
                    + " INNER JOIN mst_customer cus ON srt.CustomerCode = cus.Code  "
                    + " INNER JOIN mst_currency cur ON srt.CurrencyCode = cur.Code "
                    + "  ) qry "
                    + " WHERE "
                    + "  qry.code = '" +obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public Object dataInv(JSONObject obj, Class<?> module) {
        try {
            String qry = " SELECT * FROM( "
                    + " SELECT "
                    + "        invSo.code invoiceSalesOrderCode, " 
                    + "        invSo.branchCode, "
                    + "        branch.Name branchName, "
                    + "        DATE_FORMAT(invSo.transactionDate, '%d/%m/%Y') transactionDate, "
                    + "        invSo.salesOrderCode, "
                    + "        invSo.customerCode, "
                    + "        customer.Name customerName, "
                    + "        customer.DefaultContactPersonCode customerContactCode, "
                    + "        contact.Name customerContactName, "
                    + "        customer.Address customerAddress, "
                    + "        customer.Phone1 customerPhone1, "
                    + "        customer.Phone2 customerPhone2, "
                    + "        invSo.billToCode, "
                    + "        billTo.Name billToName, "
                    + "        billTo.Address billToAddress, "
                    + "        billTo.ContactPerson AS billToContactPerson, "
                    + "        billTo.NPWPStatus npwpStatus, "
                    + "        billTo.NPWP npwp, "
                    + "        invSo.currencyCode, "
                    + "        currency.Name currencyName, "
                    + "        invSo.exchangeRate, "
                    + "        invSo.paymentTermCode, "
                    + "        term.Name paymentTermName, "
                    + "        term.Days paymentTermDays, "
                    + "        DATE_FORMAT(invSo.DueDate, '%d/%m/%Y') dueDate, "
                    + "        invSo.discountCode discountTypeCode, "
                    + "        discount_type.Name discountTypeName, "
                    + "        invSo.refNo, "
                    + "        invSo.remark, "
                    + "        invSo.totalTransactionAmount, "
                    + "        invSo.discountAmount, "
                    + "        invSo.taxBaseAmount, "
                    + "        invSo.vatPercent, "
                    + "        invSo.vatAmount, "
                    + "        invSo.grandTotalAmount "
                    + "  FROM fin_customer_invoice_sales_order invSo "
                    + "        INNER JOIN mst_branch branch ON branch.Code = invSo.BranchCode "
                    + "        INNER JOIN mst_currency currency ON currency.Code = invSo.currencyCode "
                    + "        INNER JOIN mst_payment_term term ON term.Code = invSo.PaymentTermCode "
                    + "        INNER JOIN mst_customer customer ON customer.Code = invSo.CustomerCode  "
                    + "        INNER JOIN mst_customer_jn_address billTo ON billTo.Code = invSo.BillToCode "
                    + "        LEFT JOIN mst_discount_type discount_type ON discount_type.Code = invSo.discountCode "
                    + "        INNER JOIN mst_customer_jn_contact contact ON contact.Code = customer.DefaultContactPersonCode  "
                    + "  ) qry "
                    + " WHERE "
                    + "  qry.invoiceSalesOrderCode = '" +obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countINV(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  COUNT(invSo.invoiceSalesOrderCode) FROM( "
                    + "  SELECT  "
                    + "        invSo.code invoiceSalesOrderCode, " 
                    + "        DATE_FORMAT(invSo.transactionDate, '%d/%m/%Y') transactionDate "
                    + "  FROM fin_customer_invoice_sales_order invSo "
                    + "        INNER JOIN mst_branch branch ON branch.Code = invSo.BranchCode "
                    + "        LEFT JOIN mst_sales_person sape ON sape.Code = invSo.SalesPersonCode "
                    + "        INNER JOIN mst_payment_term term ON term.Code = invSo.PaymentTermCode "
                    + "        INNER JOIN mst_currency currency ON currency.Code = invSo.currencyCode "
                    + "        INNER JOIN mst_customer customer ON customer.Code = invSo.CustomerCode  "
                    + "        INNER JOIN mst_customer_jn_address billTo ON billTo.Code = invSo.BillToCode "
                    + "        LEFT JOIN mst_discount_type discount_type ON discount_type.Code = invSo.discountCode "
                    + "        INNER JOIN mst_customer_jn_contact contact ON contact.Code = customer.DefaultContactPersonCode  "
                    + "  )invSo "
                    + "  WHERE invSo.invoiceSalesOrderCode = '" +obj.get("code") + "' "
                    + "  AND invSo.TransactionDate BETWEEN '" +obj.get("transactionStartDate") + "' AND '" +obj.get("transactionEndDate") + "' ";
            long countINV = countResult(qry);
            return countINV;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingINV(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT * FROM( "
                    + " SELECT "
                    + "        invSo.code, " 
                    + "        invSo.code invoiceSalesOrderCode, " 
                    + "        invSo.branchCode, "
                    + "        branch.Name branchName, "
                    + "        DATE_FORMAT(invSo.transactionDate, '%d/%m/%Y') transactionDate, "
                    + "        invSo.salesOrderCode, "
                    + "        invSo.customerCode, "
                    + "        customer.Name customerName, "
                    + "        customer.DefaultContactPersonCode customerContactCode, "
                    + "        contact.Name customerContactName, "
                    + "        customer.Address customerAddress, "
                    + "        customer.Phone1 customerPhone1, "
                    + "        customer.Phone2 customerPhone2, "
                    + "        IFNULL(invSo.salesPersonCode,'') salesPersonCode, "
                    + "        IFNULL(sape.Name,'') salesPersonName, "
                    + "        invSo.billToCode, "
                    + "        billTo.Name billToName, "
                    + "        billTo.Address billToAddress, "
                    + "        billTo.ContactPerson AS billToContactPerson, "
                    + "        billTo.NPWPStatus npwpStatus, "
                    + "        billTo.NPWP npwp, "
                    + "        invSo.currencyCode, "
                    + "        currency.Name currencyName, "
                    + "        invSo.exchangeRate, "
                    + "        invSo.paymentTermCode, "
                    + "        term.Name paymentTermName, "
                    + "        term.Days paymentTermDays, "
                    + "        DATE_FORMAT(invSo.DueDate, '%d/%m/%Y') dueDate, "
                    + "        invSo.discountCode discountTypeCode, "
                    + "        discount_type.Name discountTypeName, "
                    + "        invSo.refNo, "
                    + "        invSo.remark, "
                    + "        invSo.totalTransactionAmount, "
                    + "        invSo.discountAmount, "
                    + "        invSo.taxBaseAmount, "
                    + "        invSo.vatPercent, "
                    + "        invSo.vatAmount, "
                    + "        invSo.grandTotalAmount "
                    + "  FROM fin_customer_invoice_sales_order invSo "
                    + "        INNER JOIN mst_branch branch ON branch.Code = invSo.BranchCode "
                    + "        LEFT JOIN mst_sales_person sape ON sape.Code = invSo.SalesPersonCode "
                    + "        INNER JOIN mst_payment_term term ON term.Code = invSo.PaymentTermCode "
                    + "        INNER JOIN mst_currency currency ON currency.Code = invSo.currencyCode "
                    + "        INNER JOIN mst_customer customer ON customer.Code = invSo.CustomerCode  "
                    + "        INNER JOIN mst_customer_jn_address billTo ON billTo.Code = invSo.BillToCode "
                    + "        LEFT JOIN mst_discount_type discount_type ON discount_type.Code = invSo.discountCode "
                    + "        INNER JOIN mst_customer_jn_contact contact ON contact.Code = customer.DefaultContactPersonCode  "
                    + " )invSo "
                    + " WHERE "
                    + "  invSo.invoiceSalesOrderCode LIKE '%" +obj.get("code") + "%' "
                    + "  AND invSo.TransactionDate BETWEEN '" +obj.get("transactionStartDate") + "' AND '" +obj.get("transactionEndDate") + "' "
                    + " ORDER BY invSo." + sort[0] + " " + sort[1];
            long countINV = this.countINV(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countINV, totalPage(countINV, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countItemINV(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + " SELECT  "
                    + "		COUNT(invd.Code) "
                    + "	FROM fin_customer_invoice_sales_order_item_detail invd "
                    + "	LEFT JOIN( "
                    + "		SELECT "
                    + "			srt.Code, "
                    + "			srt.invoiceSalesOrderCode, "
                    + "			srtd.ItemCode, "
                    + "			SUM(srtd.Quantity)AS Quantity "
                    + "		FROM sal_sales_return_sales_order_by_invoice srt "
                    + "		INNER JOIN sal_sales_return_sales_order_by_invoice_item_detail srtd ON srt.Code=srtd.HeaderCode "
                    + "		WHERE srt.invoiceSalesOrderCode = '" +obj.get("code") + "' "
                    + "		GROUP BY srtd.Code "
                    + "	)AS srt ON invd.Headercode= srt.invoiceSalesOrderCode "
                    + "		AND invd.ItemCode=srt.ItemCode "
                    + "	INNER JOIN mst_item item ON item.Code = invd.ItemCode "
                    + "	LEFT JOIN mst_item_brand ON mst_item_brand.Code = item.ItemBrandCode "
                    + "	LEFT JOIN ivt_delivery_note_sales_order_item_detail dlnD ON dlnD.Code = invd.DeliveryNoteSalesOrderDetailCode "
                    + "	WHERE invd.Headercode = '" +obj.get("code") + "' "
                    + "	AND invd.ItemCode LIKE '%" +obj.get("itemCode") + "%' "
                    + "	AND item.Name LIKE '%" +obj.get("itemName") + "%' ";
            long countItemINV = countResult(qry);
            return countItemINV;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingItemINV(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "		invd.code, "
                    + "		invd.itemCode,  "
                    + "		item.Name itemName, "
                    + "		IFNULL(dlnD.itemAlias,'') itemAlias, "
                    + "		item.inventoryType, "
                    + "		item.unitOfMeasureCode, "
                    + "		item.itemBrandCode, "
                    + "		item_brand.Name itemBrandName, "
                    + "		invd.quantity, "
                    + "		ifnull(srt.Quantity,0) srtQuantity, "
                    + "		invd.Quantity - ifnull(srt.Quantity,0) balancedQuantity, "
                    + "		invd.price, "
                    + "		invd.nettPrice, "
                    + "		invd.totalAmount, "
                    + "		item.COGSIDR AS cogsIdr "
                    + "	FROM fin_customer_invoice_sales_order_item_detail invd "
                    + "	LEFT JOIN( "
                    + "		SELECT "
                    + "			srt.Code, "
                    + "			srt.invoiceSalesOrderCode, "
                    + "			srtd.ItemCode, "
                    + "			SUM(srtd.Quantity)AS Quantity "
                    + "		FROM sal_sales_return_sales_order_by_invoice srt "
                    + "		INNER JOIN sal_sales_return_sales_order_by_invoice_item_detail srtd ON srt.Code=srtd.HeaderCode "
                    + "		WHERE srt.invoiceSalesOrderCode = '" +obj.get("code") + "' "
                    + "		GROUP BY srtd.Code "
                    + "	)AS srt on invd.Headercode = srt.invoiceSalesOrderCode "
                    + "		AND invd.ItemCode=srt.ItemCode "
                    + "	INNER JOIN mst_item item ON item.Code = invd.ItemCode "
                    + "	LEFT JOIN mst_item_brand item_brand ON item_brand.Code = item.ItemBrandCode "
                    + "	LEFT JOIN ivt_delivery_note_sales_order_item_detail dlnD ON dlnD.Code = invd.DeliveryNoteSalesOrderDetailCode "
                    + "	WHERE invd.Headercode = '" +obj.get("code") + "' "
                    + "	AND invd.ItemCode LIKE '%" +obj.get("itemCode") + "%' "
                    + "	AND item.Name LIKE '%" +obj.get("itemName") + "%' "
                    + " ORDER BY invd." + sort[0] + " " + sort[1];
            long countItemINV = this.countItemINV(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countItemINV, totalPage(countItemINV, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT * FROM( "
                    + "  SELECT "
                    + "    sal_sales_return_sales_order_by_invoice.code, "
                    + "    sal_sales_return_sales_order_by_invoice.transactionDate, "
                    + "    sal_sales_return_sales_order_by_invoice.branchCode, "
                    + "    sal_sales_return_sales_order_by_invoice.divisionCode, "
                    + "    IFNULL(mst_division.name,'') AS divisionName, "
                    + "    sal_sales_return_sales_order_by_invoice.requestBy, "
                    + "    sal_sales_return_sales_order_by_invoice.refNo, "
                    + "    sal_sales_return_sales_order_by_invoice.remark, "
                    + "    sal_sales_return_sales_order_by_invoice.approvalReasonCode, "
                    + "    sal_sales_return_sales_order_by_invoice.approvalStatus, "
                    + "    sal_sales_return_sales_order_by_invoice.approvalDate, "
                    + "    sal_sales_return_sales_order_by_invoice.approvalBy, "
                    + "    sal_sales_return_sales_order_by_invoice.approvalRemark, "
                    + "    sal_sales_return_sales_order_by_invoice.closingStatus, "
                    + "    sal_sales_return_sales_order_by_invoice.closingDate, "
                    + "    sal_sales_return_sales_order_by_invoice.closingBy, "
                    + "    sal_sales_return_sales_order_by_invoice.closingRemark "
                    + "  FROM sal_sales_return_sales_order_by_invoice "
                    + "  INNER JOIN mst_division ON sal_sales_return_sales_order_by_invoice.ItemDivisionCode = mst_division.Code "
                    + "  LEFT JOIN mst_approval_reason ON sal_sales_return_sales_order_by_invoice.approvalReasonCode = mst_approval_reason.Code "
                    + "  ) qry "
                    + " WHERE "
                    + " qry.code LIKE '%" +obj.get("code") + "%' "
                    + " AND qry.transactionDate BETWEEN '" +obj.get("transactionStartDate") + "' AND '" +obj.get("transactionEndDate") + "' "
                    + " AND qry.divisionCode LIKE '%" +obj.get("divisionCode") + "%' "
                    + " AND qry.divisionName LIKE '%" +obj.get("divisionName") + "%' "
                    + " AND qry.refNo LIKE '%" +obj.get("refNo") + "%' "
                    + " AND qry.remark LIKE '%" +obj.get("remark") + "%' ";

            qry +=obj.get("approvalStatus") == "" ? "" : " AND qry.closingStatus = '" +obj.get("approvalStatus") + "'";
            qry +=obj.get("closingStatus") == "" ? "" : " AND qry.closingStatus = '" +obj.get("closingStatus") + "'";
            qry += " ORDER BY qry." + sort[0] + " " + sort[1];
            long countData = this.count(obj, module);
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
