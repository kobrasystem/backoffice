package com.system.api.sales.controller;

import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.json.JSONObject;
import com.poiji.bind.Poiji;
import java.text.ParseException;
import io.swagger.annotations.Api;
import com.poiji.exception.PoijiExcelType;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpEntity;
import io.swagger.annotations.Authorization;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.system.api.sales.model.CustomerInvoiceSalesOrder;
import com.system.api.sales.model.CustomerInvoiceSalesOrderItemDetail;
import com.system.api.sales.model.SalesOrder;
import com.system.api.sales.service.CustomerInvoiceSalesOrderService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.IOExcel;

/**
 *
 * @author Rayis
 */
@RestController
@Api(tags = "Customer Invoice Sales Order")
@RequestMapping("/customer-invoice-sales-order")
public class CustomerInvoiceSalesOrderController extends BaseProgress {

  @Autowired
  private CustomerInvoiceSalesOrderService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String salesOrderCode,
          @RequestParam(defaultValue = "", required = false) String customerCode,
          @RequestParam(defaultValue = "", required = false) String customerName,
          @RequestParam(defaultValue = "", required = false) String transactionStartDate,
          @RequestParam(defaultValue = "", required = false) String transactionEndDate,
          @RequestParam(defaultValue = "", required = false) String refNo,
          @RequestParam(defaultValue = "", required = false) String remark,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort) {
    try {

      JSONObject obj = new JSONObject("{'code':'" + code
              + "','salesOrderCode':'" + salesOrderCode
              + "','customerCode':'" + customerCode
              + "','customerName':'" + customerName
              + "','transactionStartDate':'" + transactionStartDate
              + "','transactionEndDate':'" + transactionEndDate
              + "','refNo':'" + refNo
              + "','remark':'" + remark
              + "'}");
      return new HttpEntity<>(service.paging(obj, page, size, sort, CustomerInvoiceSalesOrder.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/detail/search")
  public HttpEntity<Object> listDetail(@RequestParam String qry) {
    try {

      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.detail(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), CustomerInvoiceSalesOrderItemDetail.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody CustomerInvoiceSalesOrder model) {
    try {
      long data = service.checkData(model.getCode(), CustomerInvoiceSalesOrder.class);
      if (data > 0) {
        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
      }
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, CustomerInvoiceSalesOrder.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/imports")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<CustomerInvoiceSalesOrder> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, CustomerInvoiceSalesOrder.class);
      for (CustomerInvoiceSalesOrder m : model) {
        service.save(m);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/exports")
  public void exports(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "", required = false) String bankCode,
          @RequestParam(defaultValue = "", required = false) String bankName,
          @RequestParam(defaultValue = "", required = false) String title,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort,
          HttpServletResponse response) throws ParseException {

    JSONObject obj = new JSONObject("{'code':'" + code
            + "','name':'" + name
            + "','bankCode':'" + bankCode
            + "','bankName':'" + bankName
            + "','activeStatus':'" + activeStatus
            + "'}");
    List<Map<String, Object>> list = service.list(obj, page, size, sort, CustomerInvoiceSalesOrder.class);

    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, title);
    IOExcel.expCellValues("Sales Order", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (int i = 0; i < list.size(); i++) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      row++;
    }
    try {
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition", "attachment;filename=sales-" + title + ".xlsx");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }
}
