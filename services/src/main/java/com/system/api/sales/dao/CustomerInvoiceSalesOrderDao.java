/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.sales.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rayis
 */
@Repository
public class CustomerInvoiceSalesOrderDao extends BaseQuery {

    public long checkExiting(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM fin_customer_invoice_sales_order WHERE code = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(invSo.code) FROM( SELECT";
            String list = "SELECT invSo.* FROM( SELECT ";
            String select = " "
                    + "    invSo.code, "
                    + "    invSo.branchCode, "
                    + "    invSo.salesOrderCode, "
                    + "    DATE_FORMAT(invSo.transactionDate, '%d-%m-%Y') invoiceDate, "
                    + "    invSo.transactionDate, "
                    + "    invSo.currencyCode, "
                    + "    IFNULL(invSo.discountCode,'') discountTypeCode, "
                    + "    IFNULL(discount_type.Name,'') discountTypeName, "
                    + "    invSo.paymentTermCode, "
                    + "    IFNULL(invSo.salesPersonCode,'') salesPersonCode, "
                    + "    IFNULL(sape.Name,'') salesPersonName, "
                    + "    invSo.customerCode, "
                    + "    customer.Name AS customerName, "
                    + "    invSo.billToCode, "
                    + "    invSo.refNo, "
                    + "    invSo.remark, "
                    + "    invSo.totalTransactionAmount, "
                    + "    invSo.discountAmount, "
                    + "    invSo.downPaymentAmount, "
                    + "    invSo.roundedAmount, "
                    + "    invSo.taxBaseAmount, "
                    + "    invSo.vatPercent, "
                    + "    invSo.vatAmount, "
                    + "    invSo.grandTotalAmount ";
            String qry = " "
                    + " FROM fin_customer_invoice_sales_order invSo "
                    + " INNER JOIN mst_customer customer ON customer.code = invSo.customerCode "
                    + " LEFT JOIN mst_sales_person sape ON sape.code = invSo.salesPersonCode "
                    + " LEFT JOIN mst_discount_type discount_type ON discount_type.code = invSo.discountCode "
                    + ")invSo "
                    + " WHERE invSo.code LIKE '%" + obj.get("code") + "%' "
                    + "  AND invSo.salesOrderCode LIKE '%" + obj.get("salesOrderCode") + "%' "
                    + "  AND invSo.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + "  AND invSo.customerName LIKE '%" + obj.get("customerName") + "%' "
                    + "  AND IFNULL(invSo.remark,'') LIKE '%" + obj.get("remark") + "%' "
                    + "  AND IFNULL(invSo.refNo,'') LIKE '%" + obj.get("refNo") + "%' "
                    + "  AND DATE(invSo.transactionDate) BETWEEN DATE('" + obj.get("transactionStartDate") + "') AND DATE('" + obj.get("transactionEndDate") + "') "
                    + "ORDER BY invSo." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults findOverDue(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(over_due_sales.code) FROM( SELECT";
            String list = "SELECT over_due_sales.* FROM( SELECT ";
            String select = " "
                    + "         IFNULL(fin_customer_invoice_sales_order.code,'') AS code, "
                    + "		IFNULL(fin_customer_invoice_sales_order.transactionDate,'') AS transactionDate, "
                    + "		IFNULL(mst_payment_term.Days,'') AS paymentTermDays,"
                    + "		IFNULL(DATE_ADD(fin_customer_invoice_sales_order.TransactionDate,INTERVAL mst_payment_term.Days DAY),'') AS invoiceDueDate, "
                    + "		IFNULL(DATEDIFF(CURRENT_DATE,DATE_ADD(fin_customer_invoice_sales_order.TransactionDate,INTERVAL mst_payment_term.Days DAY)),'')AS invoiceLateDay ";
            String qry = " "
                    + "	FROM fin_customer_invoice_sales_order "
                    + "	INNER JOIN mst_payment_term ON fin_customer_invoice_sales_order.PaymentTermCode=mst_payment_term.Code "
                    + "	INNER JOIN mst_customer ON mst_customer.Code = fin_customer_invoice_sales_order.CustomerCode "
                    + "	WHERE fin_customer_invoice_sales_order.CustomerCode = '" + obj.get("code") + "' "
                    + "	AND fin_customer_invoice_sales_order.GrandTotalAmount > fin_customer_invoice_sales_order.PaidAmount "
                    + ")over_due_sales "
                    + "ORDER BY over_due_sales." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object data(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "        invSo.code, "
                    + "        invSo.branchCode, "
                    + "        branch.Name AS branchName, "
                    + "        DATE_FORMAT(invSo.transactionDate, '%d/%m/%Y') transactionDate, "
                    + "        invSo.salesOrderCode, "
                    + "        invSo.customerCode, "
                    + "        customer.Name AS customerName, "
                    + "        customer.DefaultContactPersonCode AS customerContactCode, "
                    + "        contact.Name AS customerContactName, "
                    + "        customer.Address AS customerAddress, "
                    + "        customer.Phone1 AS customerPhone1, "
                    + "        customer.Phone2 AS customerPhone2, "
                    + "        invSo.billToCode, "
                    + "        billTo.Name AS billToName, "
                    + "        billTo.Address AS billToAddress, "
                    + "        billTo.ContactPerson AS billToContactPerson, "
                    + "        billTo.NPWPStatus AS npwpStatus, "
                    + "        billTo.NPWP AS npwp, "
                    + "        invSo.currencyCode, "
                    + "        currency.Name AS currencyName, "
                    + "        invSo.exchangeRate, "
                    + "        invSo.paymentTermCode, "
                    + "        term.Name AS paymentTermName, "
                    + "        term.Days AS paymentTermDays, "
                    + "        IFNULL(invSo.salesPersonCode,'') salesPersonCode, "
                    + "        IFNULL(sape.Name,'') salesPersonName, "
                    + "        IFNULL(invSo.projectCode,'') projectCode, "
                    + "        IFNULL(project.Name,'') projectName, "
                    + "        DATE_FORMAT(invSo.DueDate, '%d/%m/%Y') dueDate, "
                    + "        invSo.discountCode AS discountCode, "
                    + "        discount_type.Name AS discountName, "
                    + "        invSo.refNo, "
                    + "        invSo.remark, "
                    + "        invSo.totalTransactionAmount, "
                    + "        invSo.discountAmount, "
                    + "        IFNULL(invSo.roundedAmount,0) roundedAmount, "
                    + "        IFNULL(invSo.downPaymentAmount,0)downPaymentAmount,  "
                    + "        invSo.taxBaseAmount, "
                    + "        invSo.vatPercent, "
                    + "        invSo.vatAmount, "
                    + "        invSo.grandTotalAmount "
                    + "  FROM fin_customer_invoice_sales_order invSo "
                    + "        INNER JOIN mst_branch branch ON branch.Code = invSo.BranchCode "
                    + "        INNER JOIN mst_currency currency ON currency.Code = invSo.currencyCode "
                    + "        INNER JOIN mst_payment_term term ON term.Code = invSo.PaymentTermCode "
                    + "        INNER JOIN mst_customer customer ON customer.Code = invSo.CustomerCode  "
                    + "        INNER JOIN mst_customer_jn_address billTo ON billTo.Code = invSo.BillToCode "
                    + "        LEFT JOIN mst_sales_person sape ON sape.Code = invSo.salesPersonCode "
                    + "        LEFT JOIN mst_project project ON project.Code = invSo.projectCode "
                    + "        LEFT JOIN mst_discount_type discount_type ON discount_type.Code = invSo.discountCode "
                    + "        INNER JOIN mst_customer_jn_contact contact ON contact.Code = customer.DefaultContactPersonCode  "
                    + " WHERE "
                    + "  invSo.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDataTerm(String code, Class<?> module) {
        try {
            String qry = "SELECT COUNT(*) FROM sal_sales_order_payment_term_detail soTerm WHERE soTerm.HeaderCode = '" + code + "' ";
            long countData = countResult(qry);
            return countData;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object dataTerm(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "        soTerm.headerCode, "
                    + "        soTerm.paymentTermCode, "
                    + "        term.Name paymentTermName, "
                    + "        term.Days paymentTermDays "
                    + "  FROM sal_sales_order_payment_term_detail soTerm "
                    + "        INNER JOIN mst_payment_term term ON term.Code = soTerm.PaymentTermCode "
                    + " WHERE soTerm.headerCode = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    invSo.code, "
                    + "    invSo.branchCode, "
                    + "    DATE_FORMAT(invSo.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    invSo.currencyCode, "
                    + "    invSo.discountTypeCode, "
                    + "    invSo.paymentTermCode, "
                    + "    invSo.customerCode, "
                    + "    invSo.billToCode, "
                    + "    invSo.priceTypeCode, "
                    + "    invSo.shipToCode, "
                    + "    invSo.refNo, "
                    + "    invSo.remark, "
                    + "    invSo.totalTransactionAmount, "
                    + "    invSo.discountAmount, "
                    + "    invSo.discountPercent, "
                    + "    invSo.taxBaseAmount, "
                    + "    invSo.vatPercent, "
                    + "    invSo.vatAmount, "
                    + "    invSo.grandTotalAmount "
                    + "  FROM fin_customer_invoice_sales_order invSo "
                    + " WHERE "
                    + "  invSo.code LIKE '%" + obj.get("code") + "%' "
                    + " ORDER BY invSo." + sort[0] + " " + sort[1];
            return listPagingResult(qry, page, size);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDlnDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = "";
            switch (obj.get("type").toString()) {
                case "SOD":
                    qry = " "
                            + " SELECT "
                            + "  COUNT(dln.code) "
                            + "FROM ivt_delivery_note_sales_order dln "
                            + "LEFT JOIN fin_customer_invoice_sales_order_jn_delivery_note jn_dln on dln.Code=jn_dln.DeliveryNoteCode "
                            + "WHERE dln.SalesOrderCode = '" + obj.get("code") + "' "
                            + " AND jn_dln.DeliveryNoteCode IS NULL ";
                    break;
                case "INV":
                    qry = " "
                            + " SELECT "
                            + "  COUNT(dln.code) "
                            + "FROM ivt_delivery_note_sales_order dln "
                            + "LEFT JOIN fin_customer_invoice_sales_order_jn_delivery_note jn_dln on dln.Code=jn_dln.DeliveryNoteCode "
                            + "WHERE dln.SalesOrderCode = '" + obj.get("code") + "' ";
                    break;
                default:
                    break;
            }
            long countDlnDetail = countResult(qry);
            return countDlnDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingDlnDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = "";
            switch (obj.get("type").toString()) {
                case "SOD":
                    qry = " "
                            + " SELECT "
                            + "	dln.code, "
                            + "	dln.code deliveryNoteCode, "
                            + " DATE_FORMAT(dln.transactionDate, '%d-%m-%Y') transactionDate "
                            + "FROM ivt_delivery_note_sales_order dln "
                            + "LEFT JOIN fin_customer_invoice_sales_order_jn_delivery_note jn_dln on dln.Code=jn_dln.DeliveryNoteCode "
                            + "WHERE dln.SalesOrderCode = '" + obj.get("code") + "' "
                            + " AND jn_dln.DeliveryNoteCode IS NULL "
                            + " ORDER BY dln." + sort[0] + " " + sort[1];
                    break;
                case "INV":
                    qry = " "
                            + " SELECT "
                            + "	dln.code, "
                            + "	dln.code deliveryNoteCode, "
                            + " DATE_FORMAT(dln.transactionDate, '%d-%m-%Y') transactionDate "
                            + "FROM ivt_delivery_note_sales_order dln "
                            + "LEFT JOIN fin_customer_invoice_sales_order_jn_delivery_note jn_dln on dln.Code=jn_dln.DeliveryNoteCode "
                            + "WHERE jn_dln.HeaderCode = '" + obj.get("code") + "' "
                            + " ORDER BY dln." + sort[0] + " " + sort[1];
                    break;
                default:
                    break;
            }
            long countDlnDetail = this.countDlnDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countDlnDetail, totalPage(countDlnDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countCdpDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = "";
            switch (obj.get("type").toString()) {
                case "SOD":
                    qry = " "
                            + "SELECT "
                            + "COUNT(cdp.code) "
                            + " FROM fin_customer_down_payment cdp "
                            + "INNER JOIN fin_customer_down_payment_used used ON used.Code=cdp.Code "
                            + "INNER JOIN fin_customer_invoice_sales_order_jn_customer_down_payment invCdp ON invCdp.CustomerDownPaymentCode=cdp.Code "
                            + "WHERE cdp.CustomerCode = '" + obj.get("code") + "' ";
                    break;
                case "INV":
                    qry = " "
                            + "SELECT "
                            + "COUNT(invCdp.code) "
                            + " FROM fin_customer_invoice_sales_order_jn_customer_down_payment invCdp "
                            + "LEFT JOIN fin_customer_down_payment_used used ON used.Code = invCdp.CustomerDownPaymentCode "
                            + "LEFT JOIN fin_customer_down_payment cdp ON cdp.Code = invCdp.CustomerDownPaymentCode "
                            + "WHERE invCdp.HeaderCode = '" + obj.get("code") + "' ";
                    break;
                default:
                    break;
            }
            long countDlnDetail = countResult(qry);
            return countDlnDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingCdpDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String qry = "";
            switch (obj.get("type").toString()) {
                case "SOD":
                    qry = " "
                            + "SELECT "
                            + "IFNULL(cdp.code,'') AS code, "
                            + "IFNULL(cdp.code,'') AS customerDownPaymentCode, "
                            + "IFNULL(DATE_FORMAT(cdp.transactionDate, '%d-%m-%Y'),'') AS transactionDate, "
                            + "IFNULL(cdp.currencyCode,'') AS currencyCode, "
                            + "IFNULL(cdp.exchangeRate,0)  AS exchangeRate, "
                            + "IFNULL(used.totalTransactionAmount,0) AS cdpAmount, "
                            + "IFNULL(SUM(invCdp.AppliedAmount),0) AS usedAmount, "
                            + "IFNULL(used.TotalTransactionAmount,0) - IFNULL(invCdp.AppliedAmount,0) AS balanceAmount, "
                            + "IFNULL(invCdp.AppliedAmount,0) AS appliedAmount  "
                            + "FROM fin_customer_down_payment cdp "
                            + "LEFT JOIN fin_customer_down_payment_used used ON used.Code=cdp.Code "
                            + "LEFT JOIN fin_customer_invoice_sales_order_jn_customer_down_payment invCdp ON invCdp.CustomerDownPaymentCode=cdp.Code "
                            + "WHERE cdp.CustomerCode = '" + obj.get("code") + "' "
                            + "AND IFNULL(used.TotalTransactionAmount,0) - IFNULL(invCdp.AppliedAmount,0) > 0 "
                            + " ORDER BY cdp." + sort[0] + " " + sort[1];
                    break;
                case "INV":
                    qry = " "
                            + "SELECT "
                            + "IFNULL(invCdp.code,'') AS code, "
                            + "IFNULL(cdp.code,'') AS customerDownPaymentCode, "
                            + "IFNULL(DATE_FORMAT(cdp.transactionDate, '%d-%m-%Y'),'') AS transactionDate, "
                            + "IFNULL(cdp.currencyCode,'') AS currencyCode, "
                            + "IFNULL(cdp.exchangeRate,0)  AS exchangeRate, "
                            + "IFNULL(used.totalTransactionAmount,0) AS cdpAmount, "
                            + "IFNULL(SUM(invCdp.AppliedAmount),0) AS usedAmount, "
                            + "IFNULL(used.TotalTransactionAmount,0) - IFNULL(invCdp.AppliedAmount,0) AS balanceAmount, "
                            + "IFNULL(invCdp.AppliedAmount,0) AS appliedAmount  "
                            + " FROM fin_customer_invoice_sales_order_jn_customer_down_payment invCdp "
                            + "LEFT JOIN fin_customer_down_payment_used used ON used.Code = invCdp.CustomerDownPaymentCode "
                            + "LEFT JOIN fin_customer_down_payment cdp ON cdp.Code = invCdp.CustomerDownPaymentCode "
                            + "WHERE invCdp.HeaderCode = '" + obj.get("code") + "' "
                            + " ORDER BY cdp." + sort[0] + " " + sort[1];
                    break;
                default:
                    break;
            }
            long countCdpDetail = this.countCdpDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countCdpDetail, totalPage(countCdpDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public long countDetail(JSONObject obj, Class<?> module) {
        try {
            String qry = "";
            String invCode = "";
            String headerCode = "";
            JSONArray jsonArray = obj.getJSONArray("detailCode");
            String strDataList = jsonArray.toString();
            strDataList = strDataList.replaceAll(" ", "");
            strDataList = strDataList.replaceAll("[\\[\\]]", "");

            headerCode = " dln_detail.HeaderCode IN( " + strDataList + " )";
            invCode = "invSoDetail.headerCode = " + strDataList + " ";

            switch (obj.get("type").toString()) {
                case "SOD":
                    qry = " "
                            + "SELECT "
                            + "	Count(dln_detail.code) "
                            + "FROM ivt_delivery_note_sales_order_item_detail dln_detail "
                            + "INNER JOIN ivt_picking_list_sales_order_item_detail pickd ON dln_detail.PickingListSalesOrderDetailCode=pickd.Code "
                            + "INNER JOIN sal_sales_order_detail sod ON pickd.SalesOrderDetailCode=sod.Code "
                            + "INNER JOIN sal_sales_order so ON so.Code = sod.HeaderCode "
                            + "INNER JOIN mst_item item ON dln_detail.ItemCode=item.Code "
                            + "WHERE " + headerCode;
                    break;
                case "INV":
                    qry = " "
                            + " SELECT "
                            + "   COUNT(invSoDetail.code) "
                            + " FROM fin_customer_invoice_sales_order_item_detail invSoDetail"
                            + " WHERE " + invCode;
                    break;
                default:
                    break;
            }
            long countDataDetail = countResult(qry);
            return countDataDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PaginatedResults pagingDetail(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {

            String qry = "";
            String invCode = "";
            String headerCode = "";
            JSONArray jsonArray = obj.getJSONArray("detailCode");
            String strDataList = jsonArray.toString();
            strDataList = strDataList.replaceAll(" ", "");
            strDataList = strDataList.replaceAll("[\\[\\]]", "");

            headerCode = " dln_detail.HeaderCode IN( " + strDataList + " )";
            invCode = "invSoDetail.headerCode = " + strDataList + " ";

            switch (obj.get("type").toString()) {
                case "SOD":
                    qry = " "
                            + "SELECT "
                            + "	dln_detail.code, "
                            + "	dln_detail.code deliveryNoteSalesOrderDetailCode, "
                            + "	dln_detail.headerCode, "
                            + "	dln_detail.itemCode, "
                            + "	item.Name AS itemName, "
                            + "	sod.catalogNo, "
                            + "	dln_detail.itemAlias, "
                            + "	dln_detail.quantity, "
                            + "	item.UnitOfMeasureCode AS unitOfMeasureCode, "
                            + "	sod.price, "
                            + "	sod.discountCode, "
                            + "	dt.Name discountName, "
                            + " fn_gradual_discount_amount( "
                            + " IFNULL(sod.price,0), "
                            + " IFNULL(dt.discountPercent01,0), "
                            + " IFNULL(dt.discountPercent02,0), "
                            + " IFNULL(dt.discountPercent03,0), "
                            + " IFNULL(dt.discountPercent04,0), "
                            + " IFNULL(dt.discountPercent05,0), "
                            + " IFNULL(dt.discountPercent06,0), "
                            + " IFNULL(dt.discountPercent07,0), "
                            + " IFNULL(dt.discountPercent08,0), "
                            + " IFNULL(dt.discountPercent09,0), "
                            + " IFNULL(dt.discountPercent10,0),11 "
                            + " ) discountAmountCalc, "
                            + " sod.price -(select discountAmountCalc) nettPriceCalc, "
                            + " (select discountAmountCalc) discountAmount, "
                            + " (select nettPriceCalc) nettPrice, "
                            + " sod.quantity * (select nettPriceCalc) totalAmount "
                            + "FROM ivt_delivery_note_sales_order_item_detail dln_detail "
                            + "INNER JOIN ivt_picking_list_sales_order_item_detail pickd ON dln_detail.PickingListSalesOrderDetailCode=pickd.Code "
                            + "INNER JOIN sal_sales_order_detail sod ON pickd.SalesOrderDetailCode=sod.Code "
                            + "LEFT JOIN mst_discount_type dt ON dt.code = sod.discountCode "
                            + "INNER JOIN sal_sales_order so ON so.Code = sod.HeaderCode "
                            + "INNER JOIN mst_item item ON dln_detail.ItemCode=item.Code "
                            + "WHERE " + headerCode
                            + " ORDER BY dln_detail.HeaderCode ASC,dln_detail." + sort[0] + " " + sort[1];
                    break;
                case "INV":
                    qry = " "
                            + "  SELECT "
                            + "   invSoDetail.code, "
                            + "   invSoDetail.headerCode, "
                            + "   invSoDetail.headerCode invoiceSalesOrderCode, "
                            + "   invSoDetail.itemCode, "
                            + "   item.name AS itemName, "
                            + "   IFNULL(soDetail.catalogNo,'') catalogNo, "
                            + "   invSoDetail.itemAlias, "
                            + "   IFNULL(soDetail.remark,'') remark, "
                            + "   invSoDetail.quantity, "
                            + "   item.unitOfMeasureCode, "
                            + "   invSoDetail.price, "
                            + "   invSoDetail.discountCode AS discountCode, "
                            + "   discount_type.Name AS discountName, "
                            + "   invSoDetail.discountAmount discountAmount, "
                            + "   invSoDetail.nettPrice nettPrice, "
                            + "   invSoDetail.totalAmount totalAmount "
                            + "  FROM fin_customer_invoice_sales_order_item_detail invSoDetail "
                            + "  INNER JOIN mst_item item ON item.code = invSoDetail.itemCode "
                            + "  LEFT JOIN mst_discount_type discount_type ON discount_type.code = invSoDetail.discountCode "
                            + "  LEFT JOIN ivt_delivery_note_sales_order_item_detail dlnDetail ON dlnDetail.Code = invSoDetail.DeliveryNoteSalesOrderDetailCode "
                            + "  LEFT JOIN ivt_picking_list_sales_order_item_detail pickDetail ON pickDetail.Code = dlnDetail.PickingListSalesOrderDetailCode "
                            + "  LEFT JOIN sal_sales_order_detail soDetail ON soDetail.Code = pickDetail.SalesOrderDetailCode "
                            + " WHERE " + invCode
                            + " ORDER BY invSoDetail." + sort[0] + " " + sort[1];
                    break;
                default:
                    break;
            }

            long countDataDetail = this.countDetail(obj, module);
            return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countDataDetail, totalPage(countDataDetail, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults pagingDetaildetailForReturn(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(invSoDetail.code) FROM( SELECT";
            String list = "SELECT invSoDetail.* FROM( SELECT ";
            String select = " "
                    + "   invSoDetail.code, "
                    + "   invSoDetail.headerCode, "
                    + "   invSoDetail.headerCode invoiceSalesOrderCode, "
                    + "   invSoDetail.itemCode, "
                    + "   item.name AS itemName, "
                    + "   IFNULL(soDetail.catalogNo,'') catalogNo, "
                    + "   dlnDetail.itemAlias, "
                    + "   IFNULL(soDetail.remark,'') remark, "
                    + "   invSoDetail.quantity, "
                    + "   item.unitOfMeasureCode, "
                    + "   invSoDetail.price, "
                    + "   invSoDetail.discountCode AS discountCode, "
                    + "   discount_type.Name AS discountName, "
                    + "   invSoDetail.discountAmount discountAmount, "
                    + "   invSoDetail.nettPrice nettPrice, "
                    + "   invSoDetail.totalAmount totalAmount ";
            String qry = " "
                    + "  FROM fin_customer_invoice_sales_order_item_detail invSoDetail "
                    + "  INNER JOIN mst_item item ON item.code = invSoDetail.itemCode "
                    + "  LEFT JOIN mst_discount_type discount_type ON discount_type.code = invSoDetail.discountCode "
                    + "  LEFT JOIN ivt_delivery_note_sales_order_item_detail dlnDetail ON dlnDetail.Code = invSoDetail.DeliveryNoteSalesOrderDetailCode "
                    + "  LEFT JOIN ivt_picking_list_sales_order_item_detail pickDetail ON pickDetail.Code = dlnDetail.PickingListSalesOrderDetailCode "
                    + "  LEFT JOIN sal_sales_order_detail soDetail ON soDetail.Code = pickDetail.SalesOrderDetailCode "
                    + "	WHERE invSoDetail.HeaderCode = '" + obj.get("code") + "' "
                    + ")invSoDetail "
                    + "ORDER BY invSoDetail.Code " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

// new
    
    public Object dataLookUpSo(JSONObject obj, Class<?> module) {
        try {
            String qry = " "
                    + "  SELECT "
                    + "    so.code, "
                    + "    DATE_FORMAT(so.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    so.transactionDate searchDate, "
                    + "    so.customerCode, "
                    + "    customer.Name AS customerName, "
                    + "    so.salesPersonCode, "
                    + "    sape.Name AS salesPersonName, "
                    + "    so.refNo, "
                    + "    so.remark, "
                    + "    so.approvalStatus, "
                    + "    so.closingStatus, "
                    + "    customer.DefaultContactPersonCode AS customerContactCode,"
                    + "    cuscont.Name AS customerContactName,"
                    + "    customer.Address AS customerAddress,"
                    + "    customer.Phone1 AS customerPhone1,"
                    + "    customer.Phone2 AS customerPhone2,"
                    + "    so.billToCode, "
                    + "    billTo.Name AS billToName, "
                    + "    billTo.Address AS billToAddress, "
                    + "    billTo.ContactPerson AS billToContactPerson, "
                    + "    billTo.npwpStatus AS npwpStatus, "
                    + "    billTo.npwp AS npwp, "
                    + "    so.currencyCode, "
                    + "    cur.Name AS currencyName, "
                    + "    so.discountCode, "
                    + "    dt.Name discountName, "
                    + "    so.projectCode, "
                    + "    prj.Name projectName, "
                    + "    IFNULL(so.roundedAmount,0) roundedAmount,"
                    + "    so.vatPercent, "
                    + "    so.vatAmount, "
                    + "    IFNULL(_dln_inv.dlnCode,'') dlnCode, "
                    + "    _dln_inv.countPickingListDeliveryOrder, "
                    + "    _dln_inv.countDeliveryNoteInvoice "
                    + " FROM sal_sales_order so "
                    + " INNER JOIN mst_currency cur ON cur.code = so.currencyCode "
                    + " INNER JOIN mst_sales_person sape ON sape.code = so.salesPersonCode "
                    + " INNER JOIN mst_customer customer ON customer.code = so.customerCode "
                    + " INNER JOIN mst_customer_jn_address billTo ON billTo.code = so.shipToCode "
                    + " LEFT JOIN mst_project prj ON prj.code = so.projectCode "
                    + " LEFT JOIN mst_discount_type dt ON dt.code = so.discountCode "
                    + " INNER JOIN mst_customer_jn_contact cuscont ON cuscont.code = customer.DefaultContactPersonCode "
                    + " LEFT JOIN( "
                    + "     SELECT dln.SalesOrderCode, "
                    + "     dln.Code dlnCode, "
                    + "     GROUP_CONCAT(DISTINCT dln.Remark ORDER BY dln.Code ASC)AS DeliveryOrderRemark, "
                    + "     COUNT(dlnJnPick.PickingListSalesOrderCode)AS countPickingListDeliveryOrder, "
                    + "     COUNT(_inv_jn_dln.DeliveryNoteCode)AS countDeliveryNoteInvoice "
                    + " FROM ivt_delivery_note_sales_order_jn_picking_list dlnJnPick "
                    + " INNER JOIN ivt_delivery_note_sales_order dln ON dlnJnPick.HeaderCode=dln.Code "
                    + " LEFT JOIN ( "
                    + "     SELECT inv.SalesOrderCode, inv_jn_dln.DeliveryNoteCode "
                    + " FROM fin_customer_invoice_sales_order_jn_delivery_note inv_jn_dln "
                    + " INNER JOIN fin_customer_invoice_sales_order inv ON inv_jn_dln.HeaderCode = inv.Code "
                    + " ) _inv_jn_dln ON dln.Code = _inv_jn_dln.DeliveryNoteCode AND dln.SalesOrderCode = _inv_jn_dln.SalesOrderCode "
                    + " WHERE _inv_jn_dln.DeliveryNoteCode IS NULL GROUP BY dln.SalesOrderCode "
                    + " ) _dln_inv ON _dln_inv.SalesOrderCode = so.Code  "
                    + "WHERE so.code = '" + obj.get("code") + "' ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults lookUp(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(so.code) FROM( SELECT";
            String list = "SELECT so.* FROM( SELECT ";
            String select = " "
                    + "    so.code, "
                    + "    DATE_FORMAT(so.transactionDate, '%d-%m-%Y') transactionDate, "
                    + "    so.transactionDate searchDate, "
                    + "    so.customerCode, "
                    + "    customer.Name AS customerName, "
                    + "    so.salesPersonCode, "
                    + "    sape.Name AS salesPersonName, "
                    + "    so.refNo, "
                    + "    so.remark, "
                    + "    so.approvalStatus, "
                    + "    so.closingStatus, "
                    + "    customer.DefaultContactPersonCode AS customerContactCode,"
                    + "    cuscont.Name AS customerContactName,"
                    + "    customer.Address AS customerAddress,"
                    + "    customer.Phone1 AS customerPhone1,"
                    + "    customer.Phone2 AS customerPhone2,"
                    + "    so.billToCode, "
                    + "    billTo.Name AS billToName, "
                    + "    billTo.Address AS billToAddress, "
                    + "    billTo.ContactPerson AS billToContactPerson, "
                    + "    billTo.npwpStatus AS npwpStatus, "
                    + "    billTo.npwp AS npwp, "
                    + "    so.currencyCode, "
                    + "    cur.Name AS currencyName, "
                    + "    so.discountCode, "
                    + "    dt.Name discountName, "
                    + "    so.projectCode, "
                    + "    prj.Name projectName, "
                    + "    IFNULL(so.roundedAmount,0) roundedAmount,"
                    + "    so.vatPercent, "
                    + "    so.vatAmount, "
                    + "    IFNULL(_dln_inv.dlnCode,'') dlnCode, "
                    + "    _dln_inv.countPickingListDeliveryOrder, "
                    + "    _dln_inv.countDeliveryNoteInvoice ";
            String qry = " "
                    + " FROM sal_sales_order so "
                    + " INNER JOIN mst_currency cur ON cur.code = so.currencyCode "
                    + " INNER JOIN mst_sales_person sape ON sape.code = so.salesPersonCode "
                    + " INNER JOIN mst_customer customer ON customer.code = so.customerCode "
                    + " INNER JOIN mst_customer_jn_address billTo ON billTo.code = so.shipToCode "
                    + " LEFT JOIN mst_project prj ON prj.code = so.projectCode "
                    + " LEFT JOIN mst_discount_type dt ON dt.code = so.discountCode "
                    + " INNER JOIN mst_customer_jn_contact cuscont ON cuscont.code = customer.DefaultContactPersonCode "
                    + " LEFT JOIN( "
                    + "     SELECT dln.SalesOrderCode, "
                    + "     dln.Code dlnCode, "
                    + "     GROUP_CONCAT(DISTINCT dln.Remark ORDER BY dln.Code ASC)AS DeliveryOrderRemark, "
                    + "     COUNT(dlnJnPick.PickingListSalesOrderCode)AS countPickingListDeliveryOrder, "
                    + "     COUNT(_inv_jn_dln.DeliveryNoteCode)AS countDeliveryNoteInvoice "
                    + " FROM ivt_delivery_note_sales_order_jn_picking_list dlnJnPick "
                    + " INNER JOIN ivt_delivery_note_sales_order dln ON dlnJnPick.HeaderCode=dln.Code "
                    + " LEFT JOIN ( "
                    + "     SELECT inv.SalesOrderCode, inv_jn_dln.DeliveryNoteCode "
                    + " FROM fin_customer_invoice_sales_order_jn_delivery_note inv_jn_dln "
                    + " INNER JOIN fin_customer_invoice_sales_order inv ON inv_jn_dln.HeaderCode = inv.Code "
                    + " ) _inv_jn_dln ON dln.Code = _inv_jn_dln.DeliveryNoteCode AND dln.SalesOrderCode = _inv_jn_dln.SalesOrderCode "
                    + " WHERE _inv_jn_dln.DeliveryNoteCode IS NULL GROUP BY dln.SalesOrderCode "
                    + " ) _dln_inv ON _dln_inv.SalesOrderCode = so.Code  "
                    + " ) so "
                    + "WHERE so.code LIKE '%" + obj.get("code") + "%' "
                    + " AND so.approvalStatus = 'APPROVED' "
                    //                    + " AND so.closingStatus='OPEN' "
                    + " AND so.customerCode LIKE '%" + obj.get("customerCode") + "%' "
                    + " AND so.customerName LIKE '%" + obj.get("customerName") + "%' "
                    + " AND IFNULL(so.projectCode,'') LIKE '%" + obj.get("projectCode") + "%' "
                    + " AND IFNULL(so.projectName,'') LIKE '%" + obj.get("projectName") + "%' "
                    + " AND so.countPickingListDeliveryOrder - so.countDeliveryNoteInvoice > 0 "
                    + " ORDER BY so." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PaginatedResults searchTerm(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
        try {
            String count = "SELECT COUNT(soTerm.code) FROM( SELECT";
            String list = "SELECT soTerm.* FROM( SELECT ";
            String select = " "
                    + "    soTerm.headercode, "
                    + "    term.code, "
                    + "    term.name, "
                    + "    term.days ";
            String qry = " "
                    + " FROM sal_sales_order_payment_term_detail soTerm "
                    + " INNER JOIN mst_payment_term term ON term.Code = soTerm.PaymentTermCode "
                    + " ) soTerm "
                    + "WHERE "
                    + "soTerm.headerCode LIKE '%" + obj.get("salesOrderCode") + "%' "
                    + " AND soTerm.code LIKE '%" + obj.get("code") + "%' "
                    + " AND soTerm.name LIKE '%" + obj.get("name") + "%' "
                    + " ORDER BY soTerm." + sort[0] + " " + sort[1];
            long countData = countResult(count + select + qry);
            return new PaginatedResults(listPagingResult(list + select + qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//new
    @Transactional
    public Object findFivesGang(String code) {
        try {
            String qry = " "
                    + " SELECT IFNULL(inv.Code,'') documentNo FROM fin_customer_invoice_sales_order inv  "
                    + "	INNER JOIN ( "
                    + "	SELECT bbk.DocumentCode FROM fin_bank_payment_detail bbk WHERE bbk.DocumentCode ='" + code + "' "
                    + "	UNION ALL "
                    + "	SELECT bbm.DocumentCode FROM fin_bank_received_detail bbm WHERE bbm.DocumentCode ='" + code + "' "
                    + " UNION ALL "
                    + "	SELECT bkk.DocumentCode FROM fin_cash_payment_detail bkk WHERE bkk.DocumentCode ='" + code + "' "
                    + " UNION ALL "
                    + " SELECT bbm.DocumentCode FROM fin_cash_received_detail bbm WHERE bbm.DocumentCode ='" + code + "' "
                    + " UNION ALL "
                    + " SELECT gj.DocumentCode FROM fin_general_journal_detail gj WHERE gj.DocumentCode ='" + code + "' "
                    + "	) qry ON qry.DocumentCode = inv.Code LIMIT 1 ";
            return singleResult(qry);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Transactional
    public void invoiceRecalculating(JSONObject obj) {
        try {
            session = em.unwrap(org.hibernate.Session.class);
            String sql = "CALL usp_recalculating_customer_invoice('" + obj.get("transactionStartDate") + "','" + obj.get("transactionEndDate") + "')";
            session.createSQLQuery(sql).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
