package com.system.api.sales.repo;

import org.springframework.data.repository.query.Param;
import com.system.api.sales.model.CustomerInvoiceSalesOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rayis
 */
public interface CustomerInvoiceSalesOrderRepo extends JpaRepository<CustomerInvoiceSalesOrder, String> {

    CustomerInvoiceSalesOrder findByCode(@Param("code") String code);

    @Transactional
    void deleteByCode(String code);

}
