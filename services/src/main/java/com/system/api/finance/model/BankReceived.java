/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.finance.model;

import lombok.Data;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

/**
 *
 * @author Rayis
 */

@Data
@Entity
@Table(name = "fin_bank_received")
public class BankReceived extends GroupModel{

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  private String code = "";

  @SheetColumn("TransactionDate")
  @Column(name = "TransactionDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();
  
  @SheetColumn("PaymentTermCode")
  @Column(name = "PaymentTermCode")
  private String paymentTermCode = "";
  
  @SheetColumn("DueDate")
  @Column(name = "DueDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date dueDate = new Date();
  
  @SheetColumn("CustomerCode")
  @Column(name = "CustomerCode")
  private String customerCode = "";
  
  @SheetColumn("BillToCode")
  @Column(name = "BillToCode")
  private String billToCode = "";
  
  @SheetColumn("CurrencyCode")
  @Column(name = "CurrencyCode")
  private String currencyCode = "";
  
  @SheetColumn("ExchangeRate")
  @Column(name = "ExchangeRate")
  private BigDecimal exchangeRate = new BigDecimal(0.00);

  @SheetColumn("BankAccountCode")
  @Column(name = "BankAccountCode")
  private String bankAccountCode = "";
  
  @SheetColumn("TransactionType")
  @Column(name = "TransactionType")
  private String transactionType = "REGULAR";

  @SheetColumn("CustomerDepositTypeCode")
  @Column(name = "CustomerDepositTypeCode")
  private String customerDepositTypeCode = "";

  @SheetColumn("ReceivedFrom")
  @Column(name = "ReceivedFrom")
  private String receivedFrom = "";
  
  @SheetColumn("ReceivedType")
  @Column(name = "ReceivedType")
  private String receivedType = "Transfer";

  @SheetColumn("GiroReceivedCode")
  @Column(name = "GiroReceivedCode")
  private String giroReceivedCode = "";
  
  @SheetColumn("TransferReceivedNo")
  @Column(name = "TransferReceivedNo")
  private String transferReceivedNo = "";
  
  @SheetColumn("TransferReceivedDate")
  @Column(name = "TransferReceivedDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transferReceivedDate = new Date();
  
  @SheetColumn("TransferBankName")
  @Column(name = "TransferBankName")
  private String transferBankName = "";
  
  @SheetColumn("TotalTransactionAmount")
  @Column(name = "TotalTransactionAmount")
  private BigDecimal totalTransactionAmount = new BigDecimal(0.00);
  
  @SheetColumn("RefNo")
  @Column(name = "RefNo")
  private String refNo = "";

  @SheetColumn("Remark")
  @Column(name = "Remark")
  private String remark = "";
  
  private transient String bankAccountBBMVoucherNo = "";
  private transient String customerDepositTypeChartOfAccountCode = "";
  private transient BigDecimal forexAmount = new BigDecimal(0.00);
  
  private transient List<BankReceivedDetail> listBankReceivedDetail;

}
