/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.finance.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author lukassh
 */
@Data
@Entity
@Table(name = "fin_bank_payment")
public class BankPayment extends GroupModel {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  @ApiModelProperty(hidden = true)
  private String code = "";

  @SheetColumn("TransactionDate")
  @Column(name = "TransactionDate")
  @ApiModelProperty(hidden = true)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transactionDate = new Date();

  @SheetColumn("TransactionType")
  @Column(name = "TransactionType")
  private String transactionType = "Regular";

  @SheetColumn("VendorDepositTypeCode")
  @Column(name = "VendorDepositTypeCode")
  private String vendorDepositTypeCode = "";

  @SheetColumn("PaymentTermCode")
  @Column(name = "PaymentTermCode")
  private String paymentTermCode = "";

  @SheetColumn("DueDate")
  @Column(name = "DueDate")
  @ApiModelProperty(hidden = true)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date dueDate = new Date();

  @SheetColumn("PaymentTo")
  @Column(name = "PaymentTo")
  private String paymentTo = "";

  @SheetColumn("CurrencyCode")
  @Column(name = "CurrencyCode")
  private String currencyCode = "";

  @SheetColumn("VendorCode")
  @Column(name = "VendorCode")
  private String vendorCode = "";

  @SheetColumn("ExchangeRate")
  @Column(name = "ExchangeRate")
  private BigDecimal exchangeRate = new BigDecimal(0.00);

  @SheetColumn("BankAccountCode")
  @Column(name = "BankAccountCode")
  private String BankAccountCode = "";

  @SheetColumn("EmployeeCode")
  @Column(name = "EmployeeCode")
  private String employeeCode = "";

  @SheetColumn("PaymentType")
  @Column(name = "PaymentType")
  private String paymentType = "Transfer";

  @SheetColumn("GiroPaymentCode")
  @Column(name = "GiroPaymentCode")
  private String giroPaymentCode = "";

  @SheetColumn("TransferPaymentNo")
  @Column(name = "TransferPaymentNo")
  private String transferPaymentNo = "";

  @SheetColumn("TransferPaymentDate")
  @Column(name = "TransferPaymentDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date transferPaymentDate = new Date();

  @SheetColumn("TransferBankName")
  @Column(name = "TransferBankName")
  private String transferBankName = "";

  @SheetColumn("TotalTransactionAmount")
  @Column(name = "TotalTransactionAmount")
  private BigDecimal totalTransactionAmount = new BigDecimal(0.00);

  @SheetColumn("RefNo")
  @Column(name = "RefNo")
  private String refNo = "";

  @SheetColumn("Remark")
  @Column(name = "Remark")
  private String remark = "";

  private transient String bankAccountBBKVoucherNo = "";
  private transient String vendorDepositTypeChartOfAccountCode = "";
  private transient BigDecimal forexAmount = new BigDecimal(0.00);

  private transient List<BankPaymentDetail> listBankPaymentDetail;
}
