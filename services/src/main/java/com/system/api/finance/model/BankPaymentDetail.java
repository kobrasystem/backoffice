/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.finance.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author lukassh
 */
@Data
@Entity
@Table(name = "fin_bank_payment_detail")
public class BankPaymentDetail extends GroupModel {

  @Id
  @Column(name = "Code")
  @SheetColumn("Code")
  @ApiModelProperty(hidden = true)
  private String code = "";

  @Column(name = "HeaderCode")
  @SheetColumn("HeaderCode")
  @ApiModelProperty(hidden = true)
  private String headerCode = "";

  @Column(name = "PaymentRequestDetailCode")
  @SheetColumn("PaymentRequestDetailCode")
  private String paymentRequestDetailCode = "";

  @Column(name = "DocumentBranchCode")
  @SheetColumn("DocumentBranchCode")
  private String documentBranchCode = "";

  @Column(name = "DocumentType")
  @SheetColumn("DocumentType")
  private String documentType = "";

  @Column(name = "DocumentCode")
  @SheetColumn("DocumentCode")
  private String documentCode = "";

  @SheetColumn("DocumentDate")
  @Column(name = "DocumentDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date documentDate = new Date();

  @Column(name = "CurrencyCode")
  @SheetColumn("CurrencyCode")
  private String currencyCode = "";

  @Column(name = "DepartmentCode")
  @SheetColumn("DepartmentCode")
  private String departmentCode = "";

  @Column(name = "ExchangeRate")
  @SheetColumn("ExchangeRate")
  private BigDecimal exchangeRate = new BigDecimal(0.00);

  @Column(name = "TransactionStatus")
  @SheetColumn("TransactionStatus")
  private String transactionStatus = "Transaction";

  @Column(name = "Credit")
  @SheetColumn("Credit")
  private BigDecimal credit = new BigDecimal(0.00);

  @Column(name = "CreditIDR")
  @SheetColumn("CreditIDR")
  private BigDecimal creditIDR = new BigDecimal(0.00);

  @Column(name = "Debit")
  @SheetColumn("Debit")
  private BigDecimal debit = new BigDecimal(0.00);

  @Column(name = "DebitIDR")
  @SheetColumn("DebitIDR")
  private BigDecimal debitIDR = new BigDecimal(0.00);

  @Column(name = "ChartOfAccountCode")
  @SheetColumn("ChartOfAccountCode")
  private String chartOfAccountCode = "";

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

}
