package com.system.api.master.repo;

import com.system.api.master.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CustomerRepo extends JpaRepository<Customer, String> {

  boolean existsByCode(String code);

  Customer findByCode(String code);

  @Transactional
  void deleteByCode(String code);
}
