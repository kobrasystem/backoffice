package com.system.api.master.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_bank_account")
public class BankAccount extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code = "";
  
  @Column(name = "name")
  @SheetColumn("name")
  private String name = "";

  @Column(name = "accNo")
  @SheetColumn("accNo")
  private String acNo = "";

  @Column(name = "accName")
  @SheetColumn("accName")
  private String acName = "";

  @Column(name = "BankCode")
  @SheetColumn("BankCode")
  private String bankCode;

  @Column(name = "bankBranch")
  @SheetColumn("bankBranch")
  private String bankBranch = "";

  @Column(name = "bbmVoucherNo")
  @SheetColumn("bbmVoucherNo")
  private String bbmVoucherNo = "";

  @Column(name = "BBKVoucherNo")
  @SheetColumn("BBKVoucherNo")
  private String bbkVoucherNo = "";

  @Column(name = "chartOfAccountCode")
  @SheetColumn("chartOfAccountCode")
  private String chartOfAccountCode;

  @Column(name = "remark")
  @SheetColumn("remark")
  private String remark = "";

  @SheetColumn("activeStatus")
  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
