package com.system.api.master.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */

@Sheet
@Entity
@Data
@Table(name = "mst_cash_account")
public class CashAccount extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code;
  
  @Column(name = "Name")
  @SheetColumn("Name")
  private String name;
  
  @Column(name = "chartOfAccountCode")
  @SheetColumn("chartOfAccountCode")
  private String chartOfAccountCode;
  
  @Column(name = "bkmVoucherNo")
  @SheetColumn("bkmVoucherNo")
  private String bkmVoucherNo;
  
  @Column(name = "bkkVoucherNo")
  @SheetColumn("bkkVoucherNo")
  private String bkkVoucherNo;
  
  @Column(name = "remark")
  @SheetColumn("remark")
  private String remark;

  @SheetColumn("activeStatus")
  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
