package com.system.api.master.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_vendor")
public class Vendor extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code;

  @SheetColumn("name")
  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "businessEntityCode")
  @SheetColumn("businessEntityCode")
  private String businessEntityCode = "";

  @Column(name = "address")
  @SheetColumn("address")
  private String address = "";

  @Column(name = "cityCode")
  @SheetColumn("cityCode")
  private String cityCode = "";

  @Column(name = "zipCode")
  @SheetColumn("zipCode")
  private String zipCode = "";

  @Column(name = "phone1")
  @SheetColumn("phone1")
  private String phone1 = "";

  @Column(name = "phone2")
  @SheetColumn("phone2")
  private String phone2 = "";

  @Column(name = "fax")
  @SheetColumn("fax")
  private String fax = "";

  @Column(name = "emailAddress")
  @SheetColumn("emailAddress")
  private String emailAddress = "";

  @Column(name = "vendorCategoryCode")
  @SheetColumn("vendorCategoryCode")
  private String vendorCategoryCode = "";

  @Column(name = "apChartOfAccountCode")
  @SheetColumn("apChartOfAccountCode")
  private String apChartOfAccountCode = "";

  @Column(name = "npwp")
  @SheetColumn("npwp")
  private String npwp = "";

  @Column(name = "npwpName")
  @SheetColumn("npwpName")
  private String npwpName = "";

  @Column(name = "npwpAddress")
  @SheetColumn("npwpAddress")
  private String npwpAddress = "";

  @Column(name = "npwpCityCode")
  @SheetColumn("npwpCityCode")
  private String npwpCityCode = "";

  @Column(name = "npwpZipCode")
  @SheetColumn("npwpZipCode")
  private String npwpZipCode = "";

  @Column(name = "paymentTermCode")
  @SheetColumn("paymentTermCode")
  private String paymentTermCode = "";

  @Column(name = "defaultContactPersonCode")
  @SheetColumn("defaultContactPersonCode")
  private String defaultContactPersonCode = "";

  @Column(name = "remark")
  @SheetColumn("remark")
  private String remark = "";

  @SheetColumn("activeStatus")
  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
