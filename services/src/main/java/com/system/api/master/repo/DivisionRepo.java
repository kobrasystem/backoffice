package com.system.api.master.repo;

import com.system.api.master.model.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface DivisionRepo extends JpaRepository<Division, String> {

  boolean existsByCode(String code);

  Division findByCode(String code);

  @Transactional
  void deleteByCode(String id);
}
