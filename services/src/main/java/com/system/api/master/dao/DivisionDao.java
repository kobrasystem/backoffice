/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.master.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.hibernate.query.NativeQuery;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lukassh
 */
@Repository
public class DivisionDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_division "
              + "   INNER JOIN mst_branch ON mst_division.branchCode = mst_branch.code "
              + "  WHERE mst_division.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_division.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND mst_division.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("mst_division.activeStatus", AppUtils.toString(obj, "activeStatus")) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(String code) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_division.*, "
              + "    IFNULL(mst_branch.name,'') as branchName "
              + "  FROM mst_division "
              + "   INNER JOIN mst_branch ON mst_division.branchCode = mst_branch.code "
              + "  WHERE mst_division.code = '" + code + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    mst_division.*, "
              + "    IFNULL(mst_branch.name,'') as branchName "
              + " FROM mst_division "
              + "   INNER JOIN mst_branch ON mst_division.branchCode = mst_branch.code "
              + "  WHERE mst_division.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_division.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND mst_division.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("mst_division.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
//              + getBranchQuery("mst_division.branchCode") + ""
              + "  ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Transactional
  public void deleteById(String code) {
    try {
      String sql = "DELETE FROM mst_division WHERE mst_division.code = '" + code + "'";
      session = em.unwrap(org.hibernate.Session.class);
      session.createSQLQuery(sql).executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
