package com.system.api.master.model;

import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_division")
public class Division extends BaseModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code;

  @SheetColumn("name")
  @Column(name = "name", nullable = false)
  private String name;

  @SheetColumn("branchCode")
  @Column(name = "branchCode", nullable = false)
  private String branchCode;

  @Column(name = "remark")
  @SheetColumn("remark")
  private String remark = "";

  @SheetColumn("activeStatus")
  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
