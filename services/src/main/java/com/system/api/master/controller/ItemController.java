package com.system.api.master.controller;

import com.poiji.exception.PoijiExcelType;
import com.poiji.bind.Poiji;
import com.system.api.master.model.Item;
import com.system.api.master.service.ItemService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.IOExcel;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "Item")
@RequestMapping("${jwt.path-master}/item")
public class ItemController extends BaseProgress {

  @Autowired
  private ItemService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,DESC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), Item.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/qr/search")
  public HttpEntity<Object> list() {
    try {
      return new HttpEntity<>(service.list());
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody Item model) {
    try {
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody Item model) {
    try {
      service.save(model);
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/imports")
  public HttpEntity<Object> imports(@RequestParam MultipartFile file) {
    try {
      List<Item> model = Poiji.fromExcel(file.getInputStream(), PoijiExcelType.XLS, Item.class);
      for (Item m : model) {
        service.save(m);
      }
      return new HttpEntity<>(new ResponsBody(model));
    } catch (IOException e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/exports")
  public void exports(
          @RequestParam(defaultValue = "", required = false) String code,
          @RequestParam(defaultValue = "", required = false) String name,
          @RequestParam(defaultValue = "", required = false) String title,
          @RequestParam(defaultValue = "All") String activeStatus,
          @RequestParam(defaultValue = "0") int page,
          @RequestParam(defaultValue = "10") int size,
          @RequestParam(defaultValue = "code,ASC") String[] sort,
          HttpServletResponse response) throws ParseException {

    JSONObject obj = new JSONObject("{'code':'" + code + "','name':'" + name + "','activeStatus':'" + activeStatus + "'}");
    List<Map<String, Object>> list = service.list(obj, page, size, sort, Item.class);

    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, title);
    IOExcel.expCellValues("Master Item", EnumDataType.ENUM_DataType.STRING, 0, IOExcel.cols + 1, true);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Name", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Remark", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (int i = 0; i < list.size(); i++) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("name").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("remark").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(list.get(i).get("activeStatus").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      row++;
    }
    try {
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition", "attachment;filename=master-" + title + ".xlsx");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception ex) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
    }
  }
}
