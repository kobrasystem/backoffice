package com.system.api.master.model;

import com.system.base.BaseModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_branch")
public class Branch extends BaseModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code;

  @SheetColumn("name")
  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "fax")
  @SheetColumn("fax")
  private String fax = "";

  @Column(name = "address")
  @SheetColumn("address")
  private String address = "";

  @Column(name = "cityCode")
  @SheetColumn("cityCode")
  private String cityCode = "";

  @Column(name = "phone")
  @SheetColumn("phone")
  private String phone = "";

  @Column(name = "zipCode")
  @SheetColumn("zipCode")
  private String zipCode = "";

  @Column(name = "emailAddress")
  @SheetColumn("emailAddress")
  private String emailAddress = "";

  @Column(name = "contactPerson")
  @SheetColumn("contactPerson")
  private String contactPerson = "";

  @Column(name = "journalCode")
  @SheetColumn("journalCode")
  private String journalCode = "";

  @Column(name = "remark")
  @SheetColumn("remark")
  private String remark = "";

  @SheetColumn("activeStatus")
  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
