package com.system.api.master.service;

import com.system.api.master.dao.MasterDao;
import com.system.api.master.model.Courier;
import com.system.api.master.repo.CourierRepo;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import com.system.base.Session;
import com.system.utils.AutoNumber;

@Service
public class CourierService extends Session {

  @Autowired
  private CourierRepo repo;
  @Autowired
  private MasterDao dao;

  public boolean checkData(String code) {
    try {
      return repo.existsByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String id) {
    try {
      return dao.data(id, Courier.class);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Courier save(Courier model) {
    try {
      String where = " branchCode='" + model.getBranchCode() + "' AND divisionCode='" + model.getDivisionCode() + "' ";
      String maxCode = AutoNumber.getMaxCode(em, "code", Courier.class, where);
      String code = AutoNumber.generateCode(model.getDivisionCode().replace(model.getBranchCode(), ""), maxCode, 5);

      model.setCode(code);
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Courier update(Courier model) {
    try {
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      dao.deleteById(code, Courier.class);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
