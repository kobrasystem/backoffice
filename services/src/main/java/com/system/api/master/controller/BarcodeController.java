package com.system.api.master.controller;

import com.system.api.master.model.BarcodeDetail;
import com.system.api.master.model.BarcodeHeader;
import com.system.api.master.model.BarcodeStatus;
import com.system.api.master.model.Item;
import com.system.api.master.service.BarcodeService;
import com.system.api.master.service.ItemService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.enums.EnumDataType;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import com.system.utils.IOExcel;
import com.system.generator.QrCodeGenerator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@RequestMapping("${jwt.path-master}/barcode")
@Api(tags = "Barcode")
public class BarcodeController extends BaseProgress {

  @Autowired
  private AppUtils appUtils;

  @Autowired
  private BarcodeService service;

  @Autowired
  private ItemService itemService;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,DESC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), BarcodeHeader.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/detail/search")
  public HttpEntity<Object> detail(
          @RequestParam(defaultValue = "{\"code\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,DESC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.pagingDetail(AppUtils.toString(obj, "code"), AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), BarcodeHeader.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, BarcodeHeader.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Async
  @ApiOperation(value = "${comments.generade}", authorizations = {
    @Authorization(value = "Authorization")})
  @PostMapping(value = "/save")
  public HttpEntity<Object> generateQrCode(
          @RequestBody BarcodeHeader model, HttpServletRequest request,
          @Autowired final QrCodeGenerator qrCodeGenerator) {
    try {
      String baseUrl = ServletUriComponentsBuilder.fromRequestUri(request)
              .replacePath(null)
              .build()
              .toUriString();
      return new HttpEntity<>(new ResponsBody(service.saveHeader(model, qrCodeGenerator, baseUrl + "/check"), "Barcode created success"));

    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @GetMapping(value = "/check/{product_id}", produces = MediaType.ALL_VALUE)
  public void checkQrCode(
          @PathVariable(required = true) String product_id,
          @Autowired final QrCodeGenerator qrCodeGenerator,
          HttpServletResponse response) {
    try {
      response.setContentType("image/png");
      String decodedString = appUtils.getDecode(product_id);
      BarcodeDetail model = service.dataDetail(decodedString);

      byte[] qrCode = null;
      if (model == null) {
        URL url = new URL("https://shop.unicornstore.in/assets/img/ProductNotFound.png");
        InputStream is = url.openStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
        int n;
        while ((n = is.read(byteChunk)) > 0) {
          baos.write(byteChunk, 0, n);
        }
        qrCode = baos.toByteArray();
      } else {
        qrCode = qrCodeGenerator.generatePotraitByte(model.getPathUrl(), 300, 300, "[secret]");
      }
      OutputStream outputStream = response.getOutputStream();
      outputStream.write(qrCode);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(value = "/status/{product_id}")
  public HttpEntity<Object> statusQrCode(@PathVariable(required = true) String product_id) {
    BarcodeStatus data = new BarcodeStatus();
    try {
      if (product_id.equals("")) {
        return new HttpEntity<>(new ResponsBody("Not Valid"));
      }
      String decodedString = appUtils.getDecode(product_id);
      BarcodeDetail model = service.dataDetail(decodedString);
      if (model == null) {
        data.setId(decodedString);
        data.setStatus("QR NOT VALID");
        data.setScreet("QR NOT VALID");
        data.setProduk("QR NOT VALID");
        data.setQuantity("QR NOT VALID");
        data.setLogo("https://check-produk.herbalputihindonesia.com//assets/images/ProductNotFound.png");
        data.setDescription("Mohon Maaf Produk Anda Tidak Terdaftar.!!");
        return new HttpEntity<>(new ResponsBody(data));
      }

      Item item = itemService.dataModel(model.getItemCode());
      data.setId(decodedString);
      data.setProduk(item.getName());
      data.setQuantity(item.getRemark());
      data.setStatus(model.getBarcodeStatus());
      data.setScreet(Base64.getEncoder().encodeToString(model.getKeyCode().getBytes()));
      data.setDescription("Product Anda Terdaftar, dan Terverifikasi oleh BPOM.");
      data.setLogo(model.getPathLogo());

      if (model.getBarcodeStatus().equals("REGISTERED")) {
        data.setDescription("Selamat, Produk sudah ter Verifikasi.!!");
      }

      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Async
  @ApiOperation(value = "${comments.update}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/registry/{product_id}/{screet_key}")
  public HttpEntity<Object> registQrCode(@PathVariable(required = true) String product_id, @PathVariable(required = true) String screet_key) {
    BarcodeStatus data = new BarcodeStatus();
    try {
      String decodedString = appUtils.getDecode(product_id);
      BarcodeDetail model = service.dataDetail(decodedString);
      if (model == null) {
        data.setId(decodedString);
        data.setStatus("QR NOT VALID");
        data.setScreet("KEY NOT FOUND");
        data.setDescription("Mohon Maaf Produk Anda Tidak Terdaftar.!!");
        return new HttpEntity<>(new ResponsBody(data));
      }
      if (!model.getKeyCode().equals(screet_key)) {
        data.setId(decodedString);
        data.setStatus(model.getBarcodeStatus());
        data.setScreet("KEY NOT VALID");
        data.setDescription("Mohon Maaf Produk Anda Tidak Terdaftar.!!");
        return new HttpEntity<>(new ResponsBody(data));
      }
      model.setBarcodeStatus("REGISTERED");
      service.saveDetail(model);

      data.setId(decodedString);
      data.setStatus(model.getBarcodeStatus());
      data.setScreet(Base64.getEncoder().encodeToString(model.getKeyCode().getBytes()));
      data.setDescription("Selamat, Produk sudah ter Verifikasi.!!");

      return new HttpEntity<>(new ResponsBody(data));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @ApiOperation(value = "${comments.zip}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/export-zip/{header}")
  public void zipQrCode(HttpServletResponse response, @PathVariable(required = true) String header) {

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=DOCQR-" + header + ".zip");
    response.setStatus(HttpServletResponse.SC_OK);

    List<Map<String, Object>> list = service.list(header);

    try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
      for (Map<String, Object> model : list) {
        QrCodeGenerator qrCodeGenerator = new QrCodeGenerator();
//        QrCodeGenerator.LOGO = model.get("PathLogo").toString();
        String pathUrl = model.get("pathUrl").toString();
        String keyCode = model.get("keyCode").toString();
        BufferedImage qrImage = qrCodeGenerator.generatePotrait(pathUrl, 300, 300, keyCode);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(qrImage, "png", baos);
        byte[] bytes = baos.toByteArray();

        ZipEntry zipEntry = new ZipEntry(model.get("code").toString() + ".png");
        zipEntry.setSize(baos.size());
        zipEntry.setTime(System.currentTimeMillis());
        zippedOut.putNextEntry(zipEntry);
        zippedOut.write(bytes);
        zippedOut.closeEntry();
      }
      zippedOut.finish();

    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @ApiOperation(value = "${comments.export-excel}", authorizations = {
    @Authorization(value = "Authorization")})
  @GetMapping(value = "/export-excel/{header}")
  public void exportExcelList(
          HttpServletResponse response,
          @PathVariable(required = false) String header) throws ParseException {

    response.setContentType("application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=DOCQR-" + header + ".xlsx");
    response.setStatus(HttpServletResponse.SC_OK);

    List<Map<String, Object>> list = service.list(header);
    XSSFWorkbook wb = new XSSFWorkbook();
    IOExcel.exportUtils(wb, header);
    int rowHeader = 1;
    IOExcel.cols = 0;
    IOExcel.expCellValues("No", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Company Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Item Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Group Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Product Code", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Screet Key", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("QR Status", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Path Image", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Path Logo", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);
    IOExcel.expCellValues("Path Url", EnumDataType.ENUM_DataType.STRING, rowHeader, IOExcel.cols + 1, true);

    int no = 1;
    int row = 2;
    for (Map<String, Object> model : list) {
      IOExcel.cols = 0;
      IOExcel.expCellValues(Integer.toString(no++), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("branchCode").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("itemCode").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("headerCode").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("code").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("keyCode").toString(), EnumDataType.ENUM_DataType.NUMERIC, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("barcodeStatus").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("pathImage").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("pathLogo").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      IOExcel.expCellValues(model.get("pathUrl").toString(), EnumDataType.ENUM_DataType.STRING, row, IOExcel.cols + 1);
      row++;
    }

    try {
      response.getOutputStream().write(IOExcel.exportExcel());
      response.getOutputStream().close();
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

}
