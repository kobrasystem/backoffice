package com.system.api.master.repo;

import com.system.api.master.model.BarcodeHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BarcodeHeaderRepo extends JpaRepository<BarcodeHeader, String> {

  @Transactional
  void deleteByCode(String code);
}
