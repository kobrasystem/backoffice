package com.system.api.master.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_chart_of_account")
public class ChartOfAccount extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code;

  @Column(name = "Name")
  @SheetColumn("Name")
  private String name = "";

  @Column(name = "accountType")
  @SheetColumn("accountType")
  private String accountType = "H";


  @Column(name = "pyqStatus")
  @SheetColumn("pyqStatus")
  private boolean pyqStatus = false;

  @Column(name = "bbmStatus")
  @SheetColumn("bbmStatus")
  private boolean bbmStatus = false;

  @Column(name = "bkkStatus")
  @SheetColumn("bkkStatus")
  private boolean bkkStatus = false;

  @Column(name = "bkmStatus")
  @SheetColumn("bkmStatus")
  private boolean bkmStatus = false;

  @Column(name = "gjmStatus")
  @SheetColumn("gjmStatus")
  private boolean gjmStatus = false;

  @Column(name = "adjStatus")
  @SheetColumn("adjStatus")
  private boolean adjStatus = false;
  
  @Column(name = "remark")
  @SheetColumn("remark")
  private String remark = "";

  @SheetColumn("activeStatus")
  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;
}
