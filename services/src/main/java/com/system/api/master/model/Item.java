package com.system.api.master.model;

import com.system.base.GroupModel;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "mst_item")
public class Item extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  private String code = "";

  @Column(name = "name")
  private String name = "";

  @Column(name = "companyCode")
  private String companyCode = null;

  @Column(name = "remark")
  private String remark = null;

  @Column(name = "volume")
  private BigDecimal volume = new BigDecimal("0.00");

  @Column(name = "basedPrice")
  private BigDecimal basedPrice = new BigDecimal("0.00");

  @Column(name = "normalPrice")
  private BigDecimal normalPrice = new BigDecimal("0.00");

  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;
}
