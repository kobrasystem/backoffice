package com.system.api.master.controller;

import com.system.api.master.model.Bank;
import com.system.api.master.service.BankService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "Bank")
@RequestMapping("${jwt.path-master}/bank")
public class BankController extends BaseProgress {

  @Autowired
  private BankService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,DESC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), Bank.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code);
      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody Bank model) {
    try {
      return new HttpEntity<>(new ResponsBody(service.save(model), "Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody Bank model) {
    try {
      return new HttpEntity<>(new ResponsBody(service.update(model), "Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }
}
