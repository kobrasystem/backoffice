package com.system.api.master.service;

import com.system.api.master.dao.DivisionDao;
import com.system.api.master.model.Division;
import com.system.api.master.repo.DivisionRepo;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class DivisionService {

  @Autowired
  private DivisionRepo repo;
  @Autowired
  private DivisionDao dao;

  public boolean checkData(String code) {
    try {
      return repo.existsByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String id) {
    try {
      return dao.data(id);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Division save(Division model) {
    try {
      model.setCode(model.getBranchCode() + model.getCode());
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
