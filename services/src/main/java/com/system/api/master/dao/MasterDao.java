/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.master.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class MasterDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM " + getTableName(module) + " "
              + "   INNER JOIN mst_branch ON master.branchCode = mst_branch.code "
              + "   INNER JOIN mst_division ON master.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + "  WHERE master.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND master.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND master.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "  AND master.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("master.activeStatus", AppUtils.toString(obj, "activeStatus")) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    master.*, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.name,'') as divisionName "
              + "  FROM " + getTableName(module) + " master "
              + "   INNER JOIN mst_branch ON master.branchCode = mst_branch.code "
              + "   INNER JOIN mst_division ON master.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + "  WHERE master.code = '" + code + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    master.*, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.name,'') as divisionName "
              + "  FROM " + getTableName(module) + " master "
              + "   INNER JOIN mst_branch ON master.branchCode = mst_branch.code "
              + "   INNER JOIN mst_division ON master.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + "  WHERE master.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND master.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND master.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "  AND master.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("master.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + "  ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Transactional
  public void deleteById(String code, Class<?> module) {
    try {
      String sql = "DELETE FROM " + getTableName(module) + " master WHERE master.code = '" + code + "'";
      session = em.unwrap(org.hibernate.Session.class);
      session.createSQLQuery(sql).executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
