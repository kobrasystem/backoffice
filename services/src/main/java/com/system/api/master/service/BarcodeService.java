package com.system.api.master.service;

import com.system.api.master.dao.BarcodeDao;
import com.system.api.master.model.BarcodeDetail;
import com.system.api.master.model.BarcodeHeader;
import com.system.api.master.repo.BarcodeDetailRepo;
import com.system.api.master.repo.BarcodeHeaderRepo;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import com.system.generator.QrCodeGenerator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BarcodeService {

  @Autowired
  private BarcodeHeaderRepo repoHeader;
  @Autowired
  private BarcodeDetailRepo repoDetail;
  @Autowired
  private BarcodeDao dao;

  public long checkData(String code, Class<?> module) {
    try {
      return dao.checkExiting(code, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public BarcodeDetail dataDetail(String code) {
    try {
      return repoDetail.findByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(String header) {
    try {
      return dao.list(header);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults pagingDetail(String code, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.pagingDetail(code, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public BarcodeHeader saveHeader(BarcodeHeader model, QrCodeGenerator qrCodeGenerator, String base) {
    try {

//      if (model.getUrlLogo() != null || !model.getUrlLogo().equals("")) {
//        QrCodeGenerator.LOGO = model.getUrlLogo();
//      }
      DateFormat df = new SimpleDateFormat("yyMMdd");
      String todayAsString = df.format(model.getBarcodeDate());

      String perfix = model.getBranchCode() + model.getItemCode() + todayAsString;
      model.setCode(model.getBranchCode() + "-" + perfix + String.format("%06d", model.getProductStart()) + String.format("%06d", model.getProductEnd()));
      model.setName(perfix + " " + model.getProductStart() + " to " + model.getProductEnd());

      Random rand = new Random();
      List<BarcodeDetail> modelList = new ArrayList<BarcodeDetail>();
      repoHeader.save(model);

      for (int i = model.getProductStart(); i <= model.getProductEnd(); i++) {
        BarcodeDetail detail = new BarcodeDetail();
        detail.setBarcodeStatus("READY REGISTERED");
        detail.setHeaderCode(model.getCode());
        detail.setCode(perfix + String.format("%06d", i));
        detail.setBranchCode(model.getBranchCode());
        detail.setItemCode(model.getItemCode());
        detail.setKeyCode(String.format("%04d", rand.nextInt(10000)));
        detail.setBarcodeDate(model.getBarcodeDate());

        String encodeCode = Base64.getEncoder().encodeToString(detail.getCode().getBytes());
        detail.setPathUrl(model.getUrlLink() + "/" + encodeCode);
        detail.setEncodeCode(encodeCode);
        detail.setPathImage(base + "/" + encodeCode);
        detail.setPathLogo(model.getUrlLogo());

        repoDetail.save(detail);
        modelList.add(detail);
      }

      return model;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public BarcodeDetail saveDetail(BarcodeDetail model) {
    try {
      return repoDetail.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
