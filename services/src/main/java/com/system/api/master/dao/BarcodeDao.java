/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.master.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lukassh
 */
@Repository
public class BarcodeDao extends BaseQuery {

  public long checkExiting(String code, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM mst_barcode_header WHERE code = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_barcode_header "
              + "  WHERE mst_barcode_header.code LIKE '%" + AppUtils.toString(obj, "code") + "%' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public long countDetail(String code) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_barcode_detail "
              + "  WHERE mst_barcode_detail.headerCode = '" + code + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    * "
              + " FROM mst_barcode_header "
              + " WHERE "
              + "  mst_barcode_header.code = '" + AppUtils.toString(obj, "code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(String header) {
    try {
      String qry = " "
              + " SELECT  "
              + " * "
              + " FROM mst_barcode_detail ";
      return listResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    * "
              + " FROM mst_barcode_header "
              + " WHERE mst_barcode_header.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + " ORDER BY mst_barcode_header." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults pagingDetail(String code, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT  "
              + "    * "
              + " FROM mst_barcode_detail "
              + " WHERE mst_barcode_detail.headerCode = '" + code + "' "
              + " ORDER BY mst_barcode_detail." + sort[0] + " " + sort[1];
      long countData = this.countDetail(code);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
