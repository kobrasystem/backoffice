package com.system.api.master.model;

import com.system.base.GroupModel;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "mst_customer")
public class Customer extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  private String code = "";

  @Column(name = "name")
  private String name = "";

  @Column(name = "email")
  private String email = "";

  @Column(name = "phone")
  private String phone = "";

  @Column(name = "address")
  private String address = "";

  @Column(name = "remark")
  private String remark = "";

  @Column(name = "addressNo")
  private String addressNo = "";
  
  @Column(name = "addressVilage")
  private String addressVilage = "";
  
  @Column(name = "addressRt")
  private String addressRt = "";
  
  @Column(name = "addressRw")
  private String addressRw = "";
  
  @Column(name = "addressBenchmark")
  private String addressBenchmark = "";
  
  @Column(name = "addressDistric")
  private String addressDistric = "";
  
  @Column(name = "addressProvince")
  private String addressProvince = "";
  
  @Column(name = "addressCity")
  private String addressCity = "";
  
  @Column(name = "addressArea")
  private String addressArea = "";
  
  @Column(name = "addressZipCode")
  private String addressZipCode = "";

  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;
}
