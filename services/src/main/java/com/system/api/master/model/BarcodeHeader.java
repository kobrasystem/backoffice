package com.system.api.master.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.system.base.BaseModel;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import lombok.Data;

@Data
@Entity
@Table(name = "mst_barcode_header")
public class BarcodeHeader extends BaseModel implements Serializable {

  @Id
  private String code = null;
  
  @Column(name = "branchCode")
  private String branchCode = null;

  @Column(name = "name")
  private String name = null;

  @Column(name = "itemCode")
  private String itemCode = null;

  @Column(name = "typeBarcode")
  private String typeBarcode = null;

  @Column(name = "urlLogo")
  private String urlLogo = null;

  @Column(name = "urlLink")
  private String urlLink = null;

  @JsonFormat(pattern="yyyy-MM-dd HH:mm")
  @Column(name = "barcodeDate")
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date barcodeDate;

  @Column(name = "productStart")
  private int productStart;
  
  @Column(name = "productEnd")
  private int productEnd;

  @Column(name = "description")
  private String description = null;
}
