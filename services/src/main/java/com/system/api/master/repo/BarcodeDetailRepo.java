package com.system.api.master.repo;

import com.system.api.master.model.BarcodeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BarcodeDetailRepo extends JpaRepository<BarcodeDetail, String> {

  BarcodeDetail findByCode(String code);
  
  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByHeaderCode(String code);
}
