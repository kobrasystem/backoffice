package com.system.api.master.repo;

import com.system.api.master.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BranchRepo extends JpaRepository<Branch, String> {

  boolean existsByCode(String code);

  Branch findByCode(String code);

  @Transactional
  void deleteByCode(String code);
}
