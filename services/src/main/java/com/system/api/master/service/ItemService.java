package com.system.api.master.service;

import com.system.api.master.dao.ItemDao;
import com.system.api.master.model.Item;
import com.system.api.master.repo.ItemRepo;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;

@Service
public class ItemService {

  @Autowired
  private ItemRepo repo;
  @Autowired
  private ItemDao dao;

  public boolean checkData(String code) {
    try {
      return repo.existsByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Object data(String id) {
    try {
      return dao.data(id);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
  
  public Item dataModel(String id) {
    try {
      return dao.dataModel(id);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.list(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public List<Map<String, Object>> list() {
    try {
      return dao.list();
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public Item save(Item model) {
    try {
      model.setCode(model.getBranchCode() + "_" + model.getCode());
      return repo.save(model);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
