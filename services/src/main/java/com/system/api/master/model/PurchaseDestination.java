package com.system.api.master.model;

import com.system.base.GroupModel;
import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import java.io.Serializable;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Imam Solikhin
 */
@Sheet
@Entity
@Data
@Table(name = "mst_purchase_destination")
public class PurchaseDestination extends GroupModel implements Serializable {

  @Id
  @Column(name = "code")
  @SheetColumn("code")
  private String code;

  @SheetColumn("name")
  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "address")
  @SheetColumn("address")
  private String address = "";

  @Column(name = "zipCode")
  @SheetColumn("zipCode")
  private String zipCode = "";

  @Column(name = "cityCode")
  @SheetColumn("cityCode")
  private String cityCode = "";

  @Column(name = "countryCode")
  @SheetColumn("countryCode")
  private String countryCode = "";

  @Column(name = "phone1")
  @SheetColumn("phone1")
  private String phone1 = "";

  @Column(name = "phone2")
  @SheetColumn("phone2")
  private String phone2 = "";

  @Column(name = "fax")
  @SheetColumn("fax")
  private String fax = "";

  @Column(name = "contactPerson")
  @SheetColumn("contactPerson")
  private String contactPerson = "";

  @Column(name = "emailAddress")
  @SheetColumn("emailAddress")
  private String emailAddress = "";

  @Column(name = "shipToStatus")
  @SheetColumn("shipToStatus")
  private boolean shipToStatus = false;

  @Column(name = "billToStatus")
  @SheetColumn("billToStatus")
  private boolean billToStatus = false;

  @Column(name = "Remark")
  @SheetColumn("Remark")
  private String remark = "";

  @SheetColumn("ActiveStatus")
  @Column(name = "ActiveStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = true;

}
