package com.system.api.master.repo;

import com.system.api.master.model.Courier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface CourierRepo extends JpaRepository<Courier, String> {

  boolean existsByCode(String code);

  Courier findByCode(String code);

  @Transactional
  void deleteByCode(String code);
}
