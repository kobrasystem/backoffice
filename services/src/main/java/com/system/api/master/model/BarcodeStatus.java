package com.system.api.master.model;

import lombok.Data;

@Data
public class BarcodeStatus {
  private String id;
  private String produk;
  private String quantity;
  private String status;
  private String description;
  private String screet;
  private String logo;
}
