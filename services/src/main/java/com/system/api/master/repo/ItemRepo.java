package com.system.api.master.repo;

import com.system.api.master.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface ItemRepo extends JpaRepository<Item, String> {

  boolean existsByCode(String code);

  Item findByCode(String code);

  @Transactional
  void deleteByCode(String code);
}
