/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.master.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lukassh
 */
@Repository
public class BranchDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_branch "
              + "  WHERE mst_branch.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_branch.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus("mst_branch.activeStatus", AppUtils.toString(obj, "activeStatus")) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(String code) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + "  FROM mst_branch "
              + "  WHERE mst_branch.code = '" + code + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + " FROM mst_branch "
              + "  WHERE mst_branch.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_branch.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus("mst_branch.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + "  ) model "
              + " ORDER BY model.code." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list() {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + " FROM mst_branch ";
      return listResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    mst_branch.code, "
              + "    mst_branch.name, "
              + "    mst_branch.activeStatus, "
              + "    mst_branch.remark "
              + " FROM mst_branch "
              + "  WHERE mst_branch.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_branch.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus("mst_branch.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + "  ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Transactional
  public void deleteById(String code) {
    try {
      String sql = "DELETE FROM mst_branch WHERE mst_branch.code = '" + code + "'";
      session = em.unwrap(org.hibernate.Session.class);
      session.createSQLQuery(sql).executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
