/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.master.dao;

import com.system.api.master.model.Item;
import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class ItemDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM mst_item "
              + "   LEFT JOIN mst_branch ON mst_item.branchCode = mst_branch.code "
              + "   LEFT JOIN mst_division ON mst_item.divisionCode = mst_division.code "
              + "  WHERE mst_item.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_item.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  " + getStatus("mst_item.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + getBranchQuery("mst_item.branchCode") + ""
              + getDivisionQuery("mst_item.divisionCode") + "";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(String code) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item.code, "
              + "    mst_item.name, "
              + "    IFNULL(mst_branch.code,'') as branchCode, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.code,'') as divisionCode, "
              + "    IFNULL(mst_division.name,'') as divisionName, "
              + "    mst_item.activeStatus, "
              + "    mst_item.remark "
              + "  FROM mst_item "
              + "   LEFT JOIN mst_branch ON mst_item.branchCode = mst_branch.code "
              + "   LEFT JOIN mst_division ON mst_item.divisionCode = mst_division.code "
              + "  WHERE mst_item.code = '" + code + "' "
              + getBranchQuery("mst_item.branchCode") + ""
              + getDivisionQuery("mst_item.divisionCode") + "";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public Item dataModel(String code) {
    try {
      String qry = " "
              + " SELECT  "
              + "    mst_item.code, "
              + "    mst_item.name, "
              + "    mst_item.remark "
              + "  FROM mst_item "
              + "  WHERE mst_item.code = '" + code + "' ";
      return uniqueResult(qry, Item.class);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    mst_item.code, "
              + "    mst_item.name, "
              + "    IFNULL(mst_branch.code,'') as branchCode, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.code,'') as divisionCode, "
              + "    IFNULL(mst_division.name,'') as divisionName, "
              + "    mst_item.activeStatus, "
              + "    mst_item.remark "
              + "  FROM mst_item "
              + "   LEFT JOIN mst_branch ON mst_item.branchCode = mst_branch.code "
              + "   LEFT JOIN mst_division ON mst_item.divisionCode = mst_division.code "
              + "  WHERE mst_item.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_item.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND IFNULL(mst_item.divisionCode,'') LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "  AND IFNULL(mst_item.branchCode,'') LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("mst_item.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + "  ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list() {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    mst_item.code, "
              + "    mst_item.name, "
              + "    IFNULL(mst_branch.code,'') as branchCode, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    mst_item.activeStatus, "
              + "    mst_item.remark "
              + "  FROM mst_item "
              + "   LEFT JOIN mst_branch ON mst_item.branchCode = mst_branch.code "
              + "  WHERE mst_item.branchCode = 'HPI' "
              + "  ) model "
              + " ORDER BY model.code ASC";
      return listResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    mst_item.code, "
              + "    mst_item.name, "
              + "    IFNULL(mst_branch.code,'') as branchCode, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.code,'') as divisionCode, "
              + "    IFNULL(mst_division.name,'') as divisionName, "
              + "    mst_item.activeStatus, "
              + "    mst_item.remark "
              + "  FROM mst_item "
              + "   LEFT JOIN mst_branch ON mst_item.branchCode = mst_branch.code "
              + "   LEFT JOIN mst_division ON mst_item.divisionCode = mst_division.code "
              + "  WHERE mst_item.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND mst_item.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND IFNULL(mst_item.divisionCode,'') LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "  AND IFNULL(mst_item.branchCode,'') LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("mst_item.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + "  ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Transactional
  public void deleteById(String code) {
    try {
      String sql = "DELETE FROM mst_item WHERE mst_item.code = '" + code + "'";
      session = em.unwrap(org.hibernate.Session.class);
      session.createSQLQuery(sql).executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
