package com.system.api.master.repo;

import com.system.api.master.model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface BankRepo extends JpaRepository<Bank, String> {

  boolean existsByCode(String code);

  Bank findByCode(String code);

  @Transactional
  void deleteByCode(String code);
}
