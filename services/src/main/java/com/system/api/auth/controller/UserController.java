package com.system.api.auth.controller;

import com.system.api.auth.model.Users;
import com.system.api.auth.service.UserService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "User")
@RequestMapping("${jwt.path-user}/user")
public class UserController extends BaseProgress {

  private final static Logger log = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"code\":\"\",\"name\":\"\",\"activeStatus\":\"ALL\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), Users.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @GetMapping(value = "/data")
  public HttpEntity<Object> data(@RequestParam String code) {
    try {
      Object model = service.data(code, Users.class);
      if (model == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }

      return new HttpEntity<>(new ResponsBody(model));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/save")
  public HttpEntity<Object> save(@RequestBody Users model) {
    try {
//      long data = service.checkData(model.getCode(), Users.class);
//      if (data > 0) {
//        throw new CustomException("The data is exist", HttpStatus.BAD_REQUEST);
//      }
      service.save(model);
      log.info("save users success #" + AppUtils.modelToJson(model));
      return new HttpEntity<>(new ResponsBody("Save data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@RequestBody Users model) {
    try {
//      long data = service.checkData(model.getCode(), Users.class);
//      if (data < 1) {
//        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
//      }
      service.save(model);
      log.info("update users success #" + AppUtils.modelToJson(model));
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/change-password/update")
  public HttpEntity<Object> password(@RequestBody Users model) {
    try {
      service.change(model);
      log.info("change password users success #" + AppUtils.modelToJson(model));
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/delete")
  public HttpEntity<Object> delete(@RequestParam String code) {
    try {
      Object data = service.data(code, Users.class);
      if (data == null) {
        throw new CustomException("The data doesn't exist", HttpStatus.BAD_REQUEST);
      }
      service.delete(code);
      log.info("delete users success #" + code);
      return new HttpEntity<>(new ResponsBody("Delete data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

}
