package com.system.api.auth.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "scr_user_item")
@NoArgsConstructor
public class UserItem implements Serializable {

  @Id
  @Column(name = "code")
  private String code;

  @Column(name = "userCode")
  private String userCode;

  @Column(name = "itemCode")
  private String itemCode;
}
