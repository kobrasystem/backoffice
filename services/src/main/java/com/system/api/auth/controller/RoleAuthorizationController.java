package com.system.api.auth.controller;

import com.system.api.auth.model.RoleAuthorization;
import com.system.api.auth.model.Users;
import com.system.api.auth.service.RoleAuthorizationService;
import com.system.base.BaseProgress;
import com.system.base.ResponsBody;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Authorization;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Imam Solikhin
 */
@RestController
@Api(tags = "Role Authorization")
@RequestMapping("${jwt.path-user}/role-authorization")
public class RoleAuthorizationController extends BaseProgress {

  @Autowired
  private RoleAuthorizationService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public HttpEntity<Object> list(
          @RequestParam(defaultValue = "{\"roleCode\":\"\",\"page\":\"0\",\"size\":\"10\",\"sort\":\"code,ASC\"}",
                  required = true) String qry) {
    try {
      JSONObject obj = new JSONObject(qry);
      return new HttpEntity<>(service.paging(obj, AppUtils.toInteger(obj, "page"), AppUtils.toInteger(obj, "size"), AppUtils.toString(obj, "sort").split(","), Users.class));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/update")
  public HttpEntity<Object> update(@ApiParam(name = "example", value = "{\"code\":\"SPV001002_SAL_DEALER_INVOICE_BY_SALES_ORDER\",\"authority\":\"AssignAuthority\",\"moduleStatus\":\"false\"}") @RequestBody String model) {
    try {
      JSONObject obj = new JSONObject(model);
      switch (AppUtils.toString(obj, "authority")) {
        case "AssignAuthority":
          service.updateAssignAuthority(AppUtils.toString(obj, "code"), Boolean.parseBoolean(AppUtils.toString(obj, "moduleStatus")));
          break;
        case "SaveAuthority":
          service.updateSaveAuthority(AppUtils.toString(obj, "code"), Boolean.parseBoolean(AppUtils.toString(obj, "moduleStatus")));
          break;
        case "UpdateAuthority":
          service.updateUpdateAuthority(AppUtils.toString(obj, "code"), Boolean.parseBoolean(AppUtils.toString(obj, "moduleStatus")));
          break;
        case "DeleteAuthority":
          service.updateDeleteAuthority(AppUtils.toString(obj, "code"), Boolean.parseBoolean(AppUtils.toString(obj, "moduleStatus")));
          break;
        case "PrintAuthority":
          service.updatePrintAuthority(AppUtils.toString(obj, "code"), Boolean.parseBoolean(AppUtils.toString(obj, "moduleStatus")));
          break;
      }
      return new HttpEntity<>(new ResponsBody("Update data success"));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

}
