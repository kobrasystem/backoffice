package com.system.api.auth.repo;

import com.system.api.auth.model.Users;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author imamsolikhin
 */
@Repository
public interface UserRepo extends JpaRepository<Users, String> {

  boolean existsByUsername(String username);

  List<Users> findAll();

  Users findByCode(String code);

  Users findByUsername(String username);

  @Transactional
  void deleteByCode(String code);
}
