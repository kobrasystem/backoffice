package com.system.api.auth.repo;

import com.system.api.auth.model.UserItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UserItemRepo extends JpaRepository<UserItem, String> {

  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByUserCode(String code);
}
