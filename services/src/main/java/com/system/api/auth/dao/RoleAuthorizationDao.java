/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.auth.dao;

import com.system.api.auth.model.RoleAuthorization;
import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class RoleAuthorizationDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_role_authorization "
              + "  INNER JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  INNER JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN mst_division ON scr_role.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + " WHERE "
              + "  scr_role_authorization.HeaderCode = '" + AppUtils.toString(obj, "code") + "' "
              + "  AND scr_role.divisionCode = '" + AppUtils.toString(obj, "divisionCode") + "' "
              + "  AND scr_role.branchCode = '" + AppUtils.toString(obj, "branchCode") + "' ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.code,"
              + "  IF(scr_role_authorization.assignAuthority=1,'true','false') AS assignAuthority,"
              + "  IF(scr_role_authorization.saveAuthority=1,'true','false') AS saveAuthority,"
              + "  IF(scr_role_authorization.updateAuthority=1,'true','false') AS updateAuthority,"
              + "  IF(scr_role_authorization.deleteAuthority=1,'true','false') AS deleteAuthority,"
              + "  IF(scr_role_authorization.printAuthority=1,'true','false') AS printAuthority,"
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  scr_menu.code as authorizationCode,"
              + "  scr_menu.text as authorizationName, "
              + "  scr_menu.type as menuType "
              + " FROM scr_role_authorization "
              + "  INNER JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  INNER JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN mst_division ON scr_role.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + " WHERE "
              + "  scr_role_authorization.code = '" + obj.get("code") + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public RoleAuthorization data(String id) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.*"
              + " FROM scr_role_authorization "
              + " WHERE "
              + "  scr_role_authorization.code = '" + id + "' ";
      return uniqueResult(qry, RoleAuthorization.class);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public List<Map<String, Object>> list(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.code,"
              + "  IF(scr_role_authorization.assignAuthority=1,'true','false') AS assignAuthority,"
              + "  IF(scr_role_authorization.saveAuthority=1,'true','false') AS saveAuthority,"
              + "  IF(scr_role_authorization.updateAuthority=1,'true','false') AS updateAuthority,"
              + "  IF(scr_role_authorization.deleteAuthority=1,'true','false') AS deleteAuthority,"
              + "  IF(scr_role_authorization.printAuthority=1,'true','false') AS printAuthority,"
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  scr_menu.code as authorizationCode,"
              + "  scr_menu.text as authorizationName, "
              + "  scr_menu.type as menuType "
              + " FROM scr_role_authorization "
              + "  INNER JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  INNER JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN mst_division ON scr_role.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + " WHERE "
              + "  scr_role_authorization.HeaderCode = '" + AppUtils.toString(obj, "code") + "' "
              + "  AND scr_role.divisionCode = '" + AppUtils.toString(obj, "divisionCode") + "' "
              + "  AND scr_role.branchCode = '" + AppUtils.toString(obj, "branchCode") + "' "
              + " ORDER BY scr_menu.SortNo ASC";
      long countData = this.count(obj, module);
      return listPagingResult(qry, page, size);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_role_authorization.code,"
              + "  IF(scr_role_authorization.assignAuthority=1,'true','false') AS assignAuthority,"
              + "  IF(scr_role_authorization.saveAuthority=1,'true','false') AS saveAuthority,"
              + "  IF(scr_role_authorization.updateAuthority=1,'true','false') AS updateAuthority,"
              + "  IF(scr_role_authorization.deleteAuthority=1,'true','false') AS deleteAuthority,"
              + "  IF(scr_role_authorization.printAuthority=1,'true','false') AS printAuthority,"
              + "  scr_role.code as roleCode,"
              + "  scr_role.name as roleName, "
              + "  scr_menu.code as authorizationCode,"
              + "  scr_menu.text as authorizationName, "
              + "  scr_menu.type as menuType "
              + " FROM scr_role_authorization "
              + "  INNER JOIN scr_role ON scr_role.code = scr_role_authorization.HeaderCode "
              + "  INNER JOIN scr_menu ON scr_menu.code = scr_role_authorization.AuthorizationCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN mst_division ON scr_role.divisionCode = mst_division.code AND mst_division.branchCode =  mst_branch.code "
              + " WHERE "
              + "  scr_role_authorization.HeaderCode = '" + AppUtils.toString(obj, "code") + "' "
              + "  AND scr_role.divisionCode = '" + AppUtils.toString(obj, "divisionCode") + "' "
              + "  AND scr_role.branchCode = '" + AppUtils.toString(obj, "branchCode") + "' "
              + " ORDER BY scr_menu.SortNo ASC";
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
