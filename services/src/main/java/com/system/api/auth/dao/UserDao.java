/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.auth.dao;

import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Imam Solikhin
 */
@Repository
public class UserDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = "SELECT COUNT(*) FROM scr_user "
              + "  INNER JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN scr_user_division ON scr_user_division.userCode = scr_user.code "
              + "  INNER JOIN mst_division ON scr_user_division.divisionCode = mst_division.code "
              + "  WHERE scr_user.username LIKE '%" + AppUtils.toString(obj, "username") + "%' "
              + "  AND scr_user.fullName LIKE '%" + AppUtils.toString(obj, "fullName") + "%' "
              + "  AND scr_user_division.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "  AND scr_role.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("scr_user.activeStatus", AppUtils.toString(obj, "activeStatus").toString()) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "  scr_user.*,"
              + "    IFNULL(scr_role.name,'') as roleName, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.name,'') as divisionName "
              + " FROM scr_user "
              + "  INNER JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN mst_division ON scr_user.divisionCode = mst_division.code "
              + " WHERE "
              + "  scr_user.code = '" + AppUtils.toString(obj, "code") + "' ";
      Map<String, Object> data = singleResult(qry);
      data.put("password", "de4ragil");
      return data;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + " SELECT model.* FROM "
              + " (SELECT "
              + "  scr_user.*,"
              + "    IFNULL(scr_role.name,'') as roleName, "
              + "    IFNULL(mst_branch.name,'') as branchName, "
              + "    IFNULL(mst_division.name,'') as divisionName "
              + " FROM scr_user "
              + "  INNER JOIN scr_role ON scr_role.code = scr_user.RoleCode "
              + "  INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  INNER JOIN scr_user_division ON scr_user_division.userCode = scr_user.code "
              + "  INNER JOIN mst_division ON scr_user_division.divisionCode = mst_division.code "
              + "  WHERE scr_user.username LIKE '%" + AppUtils.toString(obj, "username") + "%' "
              + "  AND scr_user.fullName LIKE '%" + AppUtils.toString(obj, "fullName") + "%' "
              + "  AND scr_user_division.divisionCode LIKE '%" + AppUtils.toString(obj, "divisionCode") + "%' "
              + "  AND scr_role.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus(getTableName(module) + ".activeStatus", obj.get("activeStatus").toString()) + " "
              + " ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

}
