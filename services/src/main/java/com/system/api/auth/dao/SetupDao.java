/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.auth.dao;

import com.system.base.BaseQuery;
import com.system.base.Setup;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class SetupDao extends BaseQuery {

  public List<Map<String, Object>> mapDestination(String code, String destination) {
    try {
      String qry = " "
              + " SELECT "
              + "     model." + destination + " code,"
              + "     model." + destination + " name "
              + " FROM mst_destination model";
      if (destination.equals("city")) {
        qry += " WHERE model.province = '" + code + "' ";
      } else if (destination.equals("area")) {
        qry += " WHERE model.city = '" + code + "' ";
      }
      qry += " GROUP BY model." + destination + " ";
      qry += " ORDER BY model." + destination + " ASC ";
      return listResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public Setup data() {
    try {
      String qry = " "
              + " SELECT "
              + "   model.branchCode, "
              + "   mst_branch.code branchName, "
              + "   mst_branch.phone branchPhone, "
              + "   mst_branch.fax branchFax, "
              + "   mst_branch.address branchAddress, "
              + "   model.divisionCode, "
              + "   model.roleCode, "
              + "   scr_role.name roleName, "
              + "   model.fullName, "
              + "   model.username, "
              + "   model.activeStatus, "
              + "   model.SuperUserStatus superUser   "
              + " FROM scr_user model "
              + "   INNER JOIN scr_role ON scr_role.code = model.roleCode"
              + "   INNER JOIN mst_branch ON mst_branch.code = model.branchCode"
              + " WHERE "
              + "  model.branchCode = '" + branchCode() + "' "
              + "  AND model.username = '" + username() + "' ";
      return uniqueResult(qry, Setup.class);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public Setup data(String code, String branch) {
    try {
      String qry = " "
              + " SELECT "
              + "   model.branchCode, "
              + "   mst_branch.code branchName, "
              + "   mst_branch.phone branchPhone, "
              + "   mst_branch.fax branchFax, "
              + "   mst_branch.address branchAddress, "
              + "   model.divisionCode, "
              + "   model.roleCode, "
              + "   scr_role.name roleName, "
              + "   model.fullName, "
              + "   model.username, "
              + "   model.activeStatus, "
              + "   model.SuperUserStatus superUser   "
              + " FROM scr_user model "
              + "   INNER JOIN scr_role ON scr_role.code = model.roleCode"
              + "   INNER JOIN mst_branch ON mst_branch.code = model.branchCode"
              + " WHERE "
              + "  model.branchCode = '" + branch + "' "
              + "  AND model.username = '" + code + "' ";
      return uniqueResult(qry, Setup.class);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
