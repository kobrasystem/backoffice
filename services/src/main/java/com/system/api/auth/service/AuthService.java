package com.system.api.auth.service;

import com.system.api.auth.dao.SetupDao;
import com.system.api.auth.model.Users;
import com.system.api.auth.repo.UserRepo;
import com.system.base.Setup;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 *
 * @author imamsolikhin
 */
@Service
public class AuthService implements UserDetailsService {

  @Autowired
  private UserRepo repo;

  @Autowired
  private SetupDao dao;

  @Override
  public UserDetails loadUserByUsername(String username) {
    Users model = repo.findByUsername(username);
    return new User(model.getUsername(), model.getPassword(),
            new ArrayList<>());
  }

  public Users loadUserDetail(String username) {
    Users sysUser = repo.findByUsername(username);
    return sysUser;
  }

  public Setup loadSetup() {
    return dao.data();
  }

  public boolean register(Users model) {
    if (repo.existsByUsername(model.getUsername())) {
      return false;
    }
    repo.save(model);
    return true;
  }
  
  public boolean check(String username) {
    if (repo.existsByUsername(username)) {
      return false;
    }
    return true;
  }
  
  public List<Map<String, Object>> mapDestination(String code, String destination) {
    return dao.mapDestination(code, destination);
  }
}
