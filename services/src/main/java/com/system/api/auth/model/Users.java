package com.system.api.auth.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "scr_user")
@NoArgsConstructor
public class Users implements Serializable {

  @Id
  @Column(name = "code")
  private String code;

  @Column(name = "branchCode")
  private String branchCode;

  @Column(name = "divisionCode")
  private String divisionCode;

  @Column(name = "roleCode")
  private String roleCode;

  @NotEmpty
  @Column(name = "fullName")
  private String fullName;

  @NotEmpty
  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  @Column(name = "email")
  private String email;

  @Column(name = "phone")
  private String phone;

  @Column(name = "activeStatus", columnDefinition = "TINYINT(1)")
  private boolean activeStatus = false;

  @Column(name = "superUserStatus", columnDefinition = "TINYINT(1)")
  private boolean superUser = false;
  
  public transient List<UserItem> ListItem;
  public transient List<UserDivision> ListDivision;
}
