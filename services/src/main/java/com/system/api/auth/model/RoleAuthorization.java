package com.system.api.auth.model;

import com.system.base.GroupModel;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "scr_role_authorization")
@Data
@NoArgsConstructor
public class RoleAuthorization extends GroupModel implements Serializable {

  @Id
  @NotEmpty
  @Column(name = "code")
  private String code = "";

  @Column(name = "branchCode")
  private String branchCode = "";

  @Column(name = "headerCode")
  private String headerCode = "";

  @Column(name = "authorizationCode")
  private String authorizationCode = "";

  @Column(name = "assignAuthority", columnDefinition = "TINYINT(1)")
  private boolean assignAuthority = false;

  @Column(name = "saveAuthority", columnDefinition = "TINYINT(1)")
  private boolean saveAuthority = false;

  @Column(name = "updateAuthority", columnDefinition = "TINYINT(1)")
  private boolean updateAuthority = false;

  @Column(name = "deleteAuthority", columnDefinition = "TINYINT(1)")
  private boolean deleteAuthority = false;

  @Column(name = "printAuthority", columnDefinition = "TINYINT(1)")
  private boolean printAuthority = false;

}
