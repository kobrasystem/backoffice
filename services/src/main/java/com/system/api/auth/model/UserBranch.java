package com.system.api.auth.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "scr_user_branch")
@NoArgsConstructor
public class UserBranch implements Serializable {

  @Id
  @Column(name = "code")
  private String code;

  @Column(name = "userCode")
  private String userCode;

  @Column(name = "branchCode")
  private String branchCode;
}
