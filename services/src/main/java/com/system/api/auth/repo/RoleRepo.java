package com.system.api.auth.repo;

import com.system.api.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface RoleRepo extends JpaRepository<Role, String> {

  @Transactional
  void deleteByCode(String code);
}
