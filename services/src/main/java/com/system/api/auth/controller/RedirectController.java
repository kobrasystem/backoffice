package com.system.api.auth.controller;

import com.system.base.ResponsBody;
import static com.system.base.Session.setup;
import com.system.exception.CustomException;
import com.system.utils.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author imamsolikhin
 */
@RestController
@Api(tags = "Redirect")
public class RedirectController {

  @Autowired
  private AppUtils appUtils;

  @Value("${config.perfix}")
  private String perfixPath;

  @RequestMapping("/")
  void handleBase(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String baseUrl = ServletUriComponentsBuilder.fromRequestUri(request)
            .replacePath(null)
            .build()
            .toUriString();
    response.sendRedirect(baseUrl + "/swagger-ui.html");
  }

  @RequestMapping("${jwt.path-auth}")
  void handleApi(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String baseUrl = ServletUriComponentsBuilder.fromRequestUri(request)
            .replacePath(null)
            .build()
            .toUriString();
    response.sendRedirect(baseUrl + "/swagger-ui.html");
  }

  @Authorization(value = "Authorization")
  @PostMapping(value = "/{group}/{module}/{print}/request")
  public HttpEntity<Object> check(HttpServletRequest request,
          @ApiParam("purchase") @PathVariable String group,
          @ApiParam("purchase-request") @PathVariable String module,
          @ApiParam("pdf") @PathVariable String print,
          @ApiParam("{\"code\":\"}") @RequestBody String body) {
    try {

      String baseUrl = ServletUriComponentsBuilder.fromRequestUri(request)
              .replacePath(null)
              .build()
              .toUriString();

      JSONObject data = new JSONObject(body);
      data.put("userName", setup().getFullName());
      data.put("branchName", setup().getBranchName());
      data.put("branchTelp", setup().getBranchPhone());
      data.put("branchFax", setup().getBranchFax());
      data.put("branchAddress", setup().getBranchAddress());
      data.put("branchImage", setup().getLogoPath());
      String token = appUtils.getEncode(data.toString()).toString();
      return new HttpEntity<>(new ResponsBody(baseUrl + perfixPath + "/" + group + "/" + module + "/" + print + "?token=" + token));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

}
