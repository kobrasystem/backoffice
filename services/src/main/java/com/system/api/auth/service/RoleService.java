package com.system.api.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.system.api.auth.dao.RoleDao;
import com.system.api.auth.model.Role;
import com.system.api.auth.repo.RoleRepo;
import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;

@Service
public class RoleService extends BaseQuery {

  @Autowired
  private RoleRepo repo;

  @Autowired
  private RoleDao dao;

  public Object data(String code) {
    try {
      return dao.data(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public Role save(Role model) {
    try {
      String code = model.getBranchCode() + model.getCode();
      String sql = ""
              + " INSERT INTO `scr_role_authorization` ( `branchCode`, `Code`, `HeaderCode`, `AuthorizationCode`, `AssignAuthority`, `SaveAuthority`, `UpdateAuthority`, `DeleteAuthority`, `PrintAuthority` ) "
              + " SELECT menu.*  "
              + " FROM ( SELECT '" + model.getBranchCode() + "' branchCode, CONCAT( '" + code + "', '_', scr_menu.CODE ) CODE, '" + code + "' HeaderCode, "
              + "          scr_menu.code AuthorizationCode, 0 AssignAuthority, 0 SaveAuthority, 0 UpdateAuthority, 0 DeleteAuthority, 0 PrintAuthority "
              + "     FROM scr_menu "
              + "     LEFT JOIN scr_role_authorization role ON role.AuthorizationCode = scr_menu.code  "
              + "               AND role.HeaderCode = '" + code + "'  "
              + "               AND role.branchCode = '" + model.getBranchCode() + "'  "
              + "     WHERE role.CODE IS NULL "
              + " ) menu; ";

      model.setCode(code);
      repo.save(model);

      session = em.unwrap(org.hibernate.Session.class);
      session.createNativeQuery(sql).executeUpdate();

      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(model);
      return model;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void delete(String code) {
    try {
      Role role = dao.dataModel(code);
      repo.deleteByCode(role.getCode());

      String sql = " DELETE FROM scr_role_authorization WHERE headerCode = '" + role.getCode() + "' AND '" + role.getBranchCode() + "' = scr_role_authorization.branchCode ";
      session = em.unwrap(org.hibernate.Session.class);
      session.createNativeQuery(sql).executeUpdate();

    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
