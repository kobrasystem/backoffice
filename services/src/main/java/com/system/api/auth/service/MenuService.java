/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.auth.service;

import com.system.api.auth.dao.MenuDao;
import com.system.base.Menu;
import com.system.base.Session;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author imamsolikhin
 */
@Service
public class MenuService extends Session{

  @Autowired
  private MenuDao menuDao;
  
  public List<Menu> loadMenu() {
    return menuDao.menu();
  }
}
