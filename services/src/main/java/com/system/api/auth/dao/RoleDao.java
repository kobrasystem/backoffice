/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.auth.dao;

import com.system.api.auth.model.Role;
import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.utils.AppUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class RoleDao extends BaseQuery {

  public long count(JSONObject obj, Class<?> module) {
    try {
      String qry = " "
              + " SELECT "
              + "   COUNT(*) "
              + "  FROM scr_role "
              + "   INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  WHERE scr_role.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND scr_role.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND scr_role.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("scr_role.activeStatus", AppUtils.toString(obj, "activeStatus")) + " ";
      long countData = countResult(qry);
      return countData;
    } catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

  public Object data(String code) {
    try {
      String qry = " "
              + " SELECT  "
              + "    scr_role.*, "
              + "    IFNULL(mst_branch.name,'') as branchName "
              + "  FROM scr_role "
              + "   INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  WHERE scr_role.code = '" + code + "' ";
      return singleResult(qry);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public Role dataModel(String code) {
    try {
      String qry = " "
              + " SELECT  "
              + "    scr_role.* "
              + "  FROM scr_role "
              + "  WHERE scr_role.code = '" + code + "' ";
      return uniqueResult(qry, Role.class);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      String qry = " "
              + "SELECT model.* FROM (  "
              + " SELECT  "
              + "    scr_role.*, "
              + "    IFNULL(mst_branch.name,'') as branchName "
              + "  FROM scr_role "
              + "   INNER JOIN mst_branch ON scr_role.branchCode = mst_branch.code "
              + "  WHERE scr_role.code LIKE '%" + AppUtils.toString(obj, "code") + "%' "
              + "  AND scr_role.name LIKE '%" + AppUtils.toString(obj, "name") + "%' "
              + "  AND scr_role.branchCode LIKE '%" + AppUtils.toString(obj, "branchCode") + "%' "
              + "  " + getStatus("scr_role.activeStatus", AppUtils.toString(obj, "activeStatus")) + " "
              + "  ) model "
              + " ORDER BY model." + sort[0] + " " + sort[1];
      long countData = this.count(obj, module);
      return new PaginatedResults(listPagingResult(qry, page, size), "Loading data success", page, countData, totalPage(countData, size));
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Transactional
  public void deleteById(String code) {
    try {
      String sql = "DELETE FROM scr_role WHERE scr_role.code = '" + code + "'";
      session = em.unwrap(org.hibernate.Session.class);
      session.createSQLQuery(sql).executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
