package com.system.api.auth.controller;

import io.swagger.annotations.Api;
import com.system.base.Login;
import com.system.api.auth.model.Users;
import com.system.security.JwtTokenUtil;
import com.system.api.auth.service.AuthService;
import com.system.api.auth.service.MenuService;
import com.system.api.master.service.BranchService;
import com.system.base.Register;
import com.system.base.ResponsBody;
import com.system.base.Session;
import com.system.exception.CustomException;
import io.swagger.annotations.Authorization;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author imamsolikhin
 */
@RestController
@RequestMapping("${jwt.path-auth}")
@Api(tags = "Auth")
public class AuthController extends Session {

  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private AuthService service;
  @Autowired
  private MenuService menuService;
  @Autowired
  private BranchService branchService;

  @Authorization(value = "Authorization")
  @PostMapping(value = "/security/validate")
  public HttpEntity<Object> validate(HttpServletRequest req) {
    try {
      return new HttpEntity<>(this.service.loadSetup());
    } catch (Exception e) {
      e.printStackTrace();
      throw new CustomException("Service maintenance", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/security/branch")
  public HttpEntity<Object> list() {
    try {
      return new HttpEntity<>(branchService.list());
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @GetMapping(value = "/security/destination")
  public HttpEntity<Object> mapDestination(@RequestParam(defaultValue = "", required = false) String code, @RequestParam(defaultValue = "", required = true) String dest) {
    try {
      return new HttpEntity<>(service.mapDestination(code, dest));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @GetMapping(value = "/security/check/{username}")
  public HttpEntity<Object> listUser(@PathVariable String username) {
    try {
      return new HttpEntity<>(service.check(username));
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_GATEWAY);
    }
  }

  @PostMapping(value = "/security/login")
  public HttpEntity<Object> login(@RequestBody Login model, HttpServletResponse response) throws IOException, Exception {
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
            model.getUsername(), model.getPassword()));

    Users user = service.loadUserDetail(model.getUsername());
    Map<String, Object> claims = new HashMap<>();
    claims.put("branch", model.getBranch());
    claims.put("username", model.getUsername());
    final String token = jwtTokenUtil.doGenerateToken(claims, model.getUsername());
    return new HttpEntity<>(token);
  }

  @PostMapping(value = "/security/register")
  public HttpEntity<Object> register(@RequestBody Register model, HttpServletResponse response) throws IOException, Exception {
    Users user = new Users();
    user.setCode(model.getUsername().toUpperCase());
    user.setFullName(model.getFullname());
    user.setUsername(model.getUsername());

    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String encodedPassword = passwordEncoder.encode(model.getPassword());
    user.setPassword(encodedPassword);
    user.setActiveStatus(false);
    if (service.register(user)) {
      final UserDetails userDetails = service.loadUserByUsername(model.getUsername());
      final String token = jwtTokenUtil.generateToken(userDetails);
      return new HttpEntity<>(token);
    }
    return new HttpEntity<>("User has been taken");
  }

  @GetMapping("/security/menus")
  public HttpEntity<Object> getAllMenu() {
    return new HttpEntity<>(new ResponsBody(menuService.loadMenu(), "menu is avaliable", HttpStatus.OK));
  }
}
