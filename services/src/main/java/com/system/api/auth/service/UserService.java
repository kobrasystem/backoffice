package com.system.api.auth.service;

import com.system.api.auth.dao.UserDao;
import com.system.api.auth.model.UserDivision;
import com.system.api.auth.model.UserItem;
import com.system.api.auth.model.Users;
import com.system.api.auth.repo.UserDivisionRepo;
import com.system.api.auth.repo.UserItemRepo;
import com.system.api.auth.repo.UserRepo;
import com.system.base.BaseQuery;
import com.system.base.PaginatedResults;
import com.system.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService extends BaseQuery {

  @Autowired
  private UserRepo repo;
  @Autowired
  private UserDivisionRepo divisionRepo;
  @Autowired
  private UserItemRepo itemRepo;

  @Autowired
  private UserDao dao;

  public Object data(String code, Class<?> module) {
    try {
      JSONObject obj = new JSONObject("{'code':'" + code + "'}");
      return dao.data(obj, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  public PaginatedResults paging(JSONObject obj, long page, long size, String[] sort, Class<?> module) {
    try {
      return dao.paging(obj, page, size, sort, module);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public Users change(Users model) {
    try {
      Users users = repo.findByCode(model.getCode());
      BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
      String encodedPassword = passwordEncoder.encode(model.getPassword());
      users.setPassword(encodedPassword);

      return repo.save(users);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public Users save(Users model) {
    try {
      if (model.getPassword().equals("de4ragil")) {
        Users user = repo.findByCode(model.getCode());
        model.setPassword(user.getPassword());
      } else {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(model.getPassword());
        model.setPassword(encodedPassword);
      }
      model.setCode(model.getUsername().toUpperCase());

      itemRepo.deleteByUserCode(model.getUsername().toUpperCase());
      if (model.getListItem().size() > 0) {
        for (UserItem detail : model.getListItem()) {
          detail.setItemCode(detail.getCode());
          detail.setUserCode(model.getUsername().toUpperCase());
          detail.setCode(model.getUsername().toUpperCase() + "_" + detail.getCode());
          itemRepo.save(detail);
        }
      }

      divisionRepo.deleteByUserCode(model.getUsername().toUpperCase());
      if (model.getListDivision().size() > 0) {
        for (UserDivision detail : model.getListDivision()) {
          detail.setDivisionCode(detail.getCode());
          detail.setUserCode(model.getUsername().toUpperCase());
          detail.setCode(model.getUsername().toUpperCase() + "_" + detail.getCode());
          divisionRepo.save(detail);
        }
      }
      repo.save(model);

      return model;
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Transactional
  public void delete(String code) {
    try {
      repo.deleteByCode(code);
    } catch (Exception e) {
      throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
