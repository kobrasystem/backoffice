package com.system.api.auth.model;

import com.system.base.BaseModel;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Entity
@Data
@Table(name = "scr_role")
public class Role extends BaseModel implements Serializable {

  @Id
  @NotEmpty
  
  @Column(name = "code")
  private String code = "";

  @Column(name = "name")
  private String name = "";

  @Column(name = "branchCode")
  private String branchCode = "";

  @Column(name = "activeStatus")
  private boolean activeStatus = false;
  
  @Column(name = "remark")
  private String remark = "";

}
