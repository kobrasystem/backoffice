package com.system.api.auth.repo;

import com.system.api.auth.model.UserDivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Imam Solikhin
 */
public interface UserDivisionRepo extends JpaRepository<UserDivision, String> {

  @Transactional
  void deleteByCode(String code);

  @Transactional
  void deleteByUserCode(String code);
}
