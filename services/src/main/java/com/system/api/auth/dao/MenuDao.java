/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system.api.auth.dao;

import com.system.base.BaseQuery;
import com.system.base.Menu;
import com.system.base.Menus;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author imamsolikhin
 */
@Repository
public class MenuDao extends BaseQuery {

  public List<Menu> menu() {
    try {
      List<Menu> auditMenu = new ArrayList();
      List<Menu> listMenu = new ArrayList();

      List<Menus> Header = listResult(" "
              + " SELECT scr_menu.* "
              + " FROM scr_menu "
              + " INNER JOIN scr_role_authorization ON scr_role_authorization.AuthorizationCode = scr_menu.`code` "
              + " INNER JOIN scr_role ON scr_role.`code` = scr_role_authorization.HeaderCode AND scr_role.branchCode = scr_role_authorization.branchCode "
              + " INNER JOIN scr_user ON scr_user.RoleCode = scr_role.code AND scr_user.`username` = '" + username() + "' "
              + " WHERE scr_menu.ParentCode IS NULL"
              + " ORDER BY scr_menu.SortNo ASC;", Menus.class);
      if (!Header.isEmpty()) {
        for (int i = 0; i < Header.size(); i++) {
          if (Header.get(i).getType().equals("GROUP")) {
            Menu m = new Menu();
            m.setCode(Header.get(i).getCode());
            m.setLabel(Header.get(i).getText());
            m.setIcon(Header.get(i).getIcon());
            listMenu.add(m);
          } else if (Header.get(i).getType().equals("MODULE")) {
            Menu m = new Menu();
            m.setCode(Header.get(i).getCode());
            m.setLabel(Header.get(i).getText());
            m.setIcon(Header.get(i).getIcon());
            m.setTo(Header.get(i).getLink());
            listMenu.add(m);
          }
        }
      }

      List<Menus> menus = listResult(" "
              + " SELECT scr_menu.* "
              + " FROM scr_menu "
              + " INNER JOIN scr_role_authorization ON scr_role_authorization.AuthorizationCode = scr_menu.`code` AND scr_role_authorization.AssignAuthority = 1 "
              + " INNER JOIN scr_role ON scr_role.`code` = scr_role_authorization.HeaderCode AND scr_role.branchCode = scr_role_authorization.branchCode "
              + " INNER JOIN scr_user ON scr_user.RoleCode = scr_role.code AND scr_user.`username` = '" + username() + "' "
              + " WHERE scr_menu.ParentCode IS NOT NULL "
              + " ORDER BY scr_menu.SortNo ASC;", Menus.class);
      if (!menus.isEmpty()) {
        for (int i = 0; i < menus.size(); i++) {
          if (menus.get(i).getType().equals("SUB")) {
            for (int s = 0; s < listMenu.size(); s++) {
              if (listMenu.get(s).getCode().equals(menus.get(i).getParentCode())) {
                Menu m = new Menu();
                m.setCode(menus.get(i).getCode());
                m.setLabel(menus.get(i).getText());
                m.setIcon(menus.get(i).getIcon());
                m.setTo(menus.get(i).getLink());
                listMenu.get(s).getItems().add(m);
              }
            }
          } else if (menus.get(i).getType().equals("MODULE")) {
            for (int s = 0; s < listMenu.size(); s++) {
              if (listMenu.get(s).getCode().equals(menus.get(i).getParentCode())) {
                Menu m = new Menu();
                m.setCode(menus.get(i).getCode());
                m.setLabel(menus.get(i).getText());
                m.setIcon(menus.get(i).getIcon());
                m.setTo(menus.get(i).getLink());
                listMenu.get(s).getItems().add(m);
              }
            }
          }

          if (menus.get(i).getType().equals("MODULE")) {
            for (int s = 0; s < listMenu.size(); s++) {
              if (listMenu.get(s).getItems().size() > 0) {
                for (int n = 0; n < listMenu.get(s).getItems().size(); n++) {
                  if (listMenu.get(s).getItems().get(n).getCode().equals(menus.get(i).getParentCode())) {
                    Menu m = new Menu();
                    m.setCode(menus.get(i).getCode());
                    m.setLabel(menus.get(i).getText());
                    m.setIcon(menus.get(i).getIcon());
                    m.setTo(menus.get(i).getLink());
                    listMenu.get(s).getItems().get(n).getItems().add(m);
                  }
                }
              }
            }
          }
        }
      }

      for (Menu m : listMenu) {
        if (m.getItems().size() > 0) {
          auditMenu.add(m);
        } else if (m.getTo() != null) {
          auditMenu.add(m);
        }
      }
      return auditMenu;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
