/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : imamsolikhin

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 23/01/2023 20:34:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mst_barcode_detail
-- ----------------------------
DROP TABLE IF EXISTS `mst_barcode_detail`;
CREATE TABLE `mst_barcode_detail` (
  `Code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `createdBy` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `barcodeStatus` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `companyCode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `headerCode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `itemCode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keyCode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pathImage` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pathLogo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pathUrl` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `barcodeDate` datetime(6) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mst_barcode_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mst_barcode_header
-- ----------------------------
DROP TABLE IF EXISTS `mst_barcode_header`;
CREATE TABLE `mst_barcode_header` (
  `Code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `createdBy` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `CompanyCode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `barcodeDate` datetime(6) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `itemCode` varchar(255) DEFAULT NULL,
  `productEnd` int(11) DEFAULT NULL,
  `productStart` int(11) DEFAULT NULL,
  `typeBarcode` varchar(255) DEFAULT NULL,
  `urlLink` varchar(255) DEFAULT NULL,
  `urlLogo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mst_barcode_header
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for mst_branch
-- ----------------------------
DROP TABLE IF EXISTS `mst_branch`;
CREATE TABLE `mst_branch` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `activeStatus` tinyint(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cityCode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `contactPerson` varchar(255) DEFAULT NULL,
  `emailAddress` varchar(255) DEFAULT NULL,
  `journalCode` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of mst_branch
-- ----------------------------
BEGIN;
INSERT INTO `mst_branch` VALUES ('GDN', NULL, NULL, NULL, NULL, 1, 'Jakarta', 'JKT', 'GDN', 'Administrator', 'admin@gmail.com', 'GDN', 'Administrator', '081808178118', '081808178118', '-', '099128');
INSERT INTO `mst_branch` VALUES ('HZR', 'admin', '2023-01-22 01:31:57.753000', '', NULL, 1, '', '', 'HZR', '', '', '', 'Hizrah Management', '', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for mst_division
-- ----------------------------
DROP TABLE IF EXISTS `mst_division`;
CREATE TABLE `mst_division` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `activeStatus` tinyint(1) DEFAULT NULL,
  `branchCode` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of mst_division
-- ----------------------------
BEGIN;
INSERT INTO `mst_division` VALUES ('GDN_ADM', 'admin', '2023-01-22 00:48:15.991000', '', NULL, 1, 'GDN', 'ADM', 'Admin', 'admin');
INSERT INTO `mst_division` VALUES ('GDN_DEV', 'admin', '2023-01-22 00:47:11.782000', '', NULL, 1, 'GDN', 'DEV', 'Development', 'dev');
COMMIT;

-- ----------------------------
-- Table structure for mst_item
-- ----------------------------
DROP TABLE IF EXISTS `mst_item`;
CREATE TABLE `mst_item` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `company_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `basedPrice` decimal(10,2) DEFAULT NULL,
  `normalPrice` decimal(10,2) DEFAULT NULL,
  `volume` decimal(10,2) DEFAULT NULL,
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `activeStatus` tinyint(1) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `companyCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `mst_item_id_index` (`id`) USING BTREE,
  KEY `mst_item_company_id_index` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_item
-- ----------------------------
BEGIN;
INSERT INTO `mst_item` VALUES ('ABNTV-MH', 'ABNTV', 'MADU HIJAU', 250000.00, 'ADMIN AMBON TV', 1, 'ABNTV2301-686', NULL, '2023-01-10 07:38:00', '2023-01-10 07:38:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('ALL-MDH01', 'ALL', 'Madu Hijau', 250000.00, 'All Ramadhan M', 1, 'ALL2204-934', NULL, '2022-04-04 16:07:00', '2022-04-04 16:07:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('BTV-MDH', 'BTV', 'MADU HIJAU', 250000.00, 'Userbanten', 1, 'BTV2112-463', 'BTV2112-463', '2021-12-27 17:51:00', '2021-12-29 11:50:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('CR1-MDH', 'CR1', 'MADU HIJAU', 250000.00, 'Admin Cirebon TV', 1, 'CR12021-003', NULL, '2021-05-22 19:58:00', '2021-05-22 19:58:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DEV-GGH', 'DEV', 'GANGGANG HIJAU', 250000.00, 'Admin DEV', 1, 'DEV-GLOBAL-001', 'DEV-GLOBAL-001', '2021-09-29 15:13:00', '2021-10-02 14:42:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DEV-MDH', 'DEV', 'MADU HIJAU', 250000.00, 'Fawwaz Abiyyu bahy', 1, 'DEV2021-002', NULL, '2021-04-29 10:12:00', '2021-04-29 10:12:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DGI-MDH', 'DGI', 'MADU HIJAU', 250000.00, 'Dea Mahdiana', 1, 'DGI2021-002', NULL, '2021-04-28 18:24:00', '2021-04-28 18:24:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DGT-DGT - MDG', 'DGT', 'Madu Gorilla', 199000.00, 'Fawwaz Abiyyu Bahy', 1, 'DGT2021-003', NULL, '2021-06-07 17:07:00', '2021-06-07 17:07:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DGT-MDH', 'DGT', 'Madu Hijau', 250000.00, 'Melinda Nopita Sari', 1, 'DGT2021-002', NULL, '2021-04-29 12:33:00', '2021-04-29 12:33:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DMK-MDH', 'DMK', 'Madu Hijau', 250000.00, 'Siti Marlina', 1, 'DMK2021-002', 'DMK2021-001', '2021-04-26 03:07:00', '2021-04-28 18:24:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DMS-GGH', 'DMS', 'Ganggang Hijau', 250000.00, 'Azza Putri', 1, 'DMS2021-003', 'DMS2021-003', '2021-09-25 18:51:00', '2021-09-25 18:51:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DMS-MDH', 'DMS', 'Madu Hijau', 250000.00, 'Meinda Nopita Sari', 1, 'DMS2021-002', NULL, '2021-04-28 19:40:00', '2021-04-28 19:40:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('DUTATV-MH', 'DUTATV', 'MADU HIJAU', 250000.00, 'ADMIN DUTA TV', 1, 'DUTATV2301-759', NULL, '2023-01-12 08:02:00', '2023-01-12 08:02:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('EL1-MDH', 'EL1', 'MADU HIJAU', 250000.00, 'Admin Elshinta TV', 1, 'EL12021-003', NULL, '2021-05-22 23:31:00', '2021-05-22 23:31:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('ELG1-GGH', 'ELG1', 'GANGGANG HIJAU', 250000.00, 'ELSHINTA GANGGANG', 1, 'ELG12203-757', NULL, '2022-03-08 09:25:00', '2022-03-08 09:25:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('ELGH-GH', 'ELGH', 'GANGGANG HIJAU', 500000.00, 'ELSHINTA GANGGANG HIJAU', 1, 'ELGH2209-074', NULL, '2022-09-21 08:25:00', '2022-09-21 08:25:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GGH1-GGH1', 'GGH1', 'ganggang hijau', 250000.00, 'Seny Rahayu', 1, 'GGH12110-346', NULL, '2021-10-02 15:32:00', '2021-10-02 15:32:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GGH2-GGH', 'GGH2', 'Ganggang Hijau', 250000.00, 'Seli', 1, 'GGH22110-062', NULL, '2021-10-04 08:28:00', '2021-10-04 08:28:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GGTVONE-GGTVONE', 'GGTVONE', 'GANGGANG HIJAU', 500000.00, 'GANGGANG TV ONE', 1, 'GGTVONE2206-800', 'GGTVONE2206-800', '2022-06-15 16:39:00', '2022-06-16 07:55:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHEL1-GH', 'GHEL1', 'GANGGANG HIJAU', 500000.00, 'GANGGANG HIJAU ELSHINTA', 1, 'GHEL12206-060', NULL, '2022-06-25 13:49:00', '2022-06-25 13:49:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHJK1-GH', 'GHJK1', 'GANGGANG HIJAU', 500000.00, 'GANGGANG JAKTV MALAM', 1, 'GHJK12209-321', NULL, '2022-09-22 08:52:00', '2022-09-22 08:52:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHJTV1-GH', 'GHJTV1', 'GANGGANG HIJAU', 500000.00, 'GANGGANG HIJAU JTV1', 1, 'GHJTV12209-291', NULL, '2022-09-21 09:05:00', '2022-09-21 09:05:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHJTV2-GH', 'GHJTV2', 'GANGGANG HIJAU', 500000.00, 'GANGGANG HIJAU JTV2', 1, 'GHJTV22206-756', NULL, '2022-06-19 18:08:00', '2022-06-19 18:08:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHJTV3-GH', 'GHJTV3', 'GANGGANG HIJAU', 500000.00, 'GANGGANG HIJAU JTV3', 1, 'GHJTV32209-866', NULL, '2022-09-21 09:25:00', '2022-09-21 09:25:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHM1-GHM1', 'GHM1', 'GANGGANG HIJAU', 250000.00, 'GANGGANG HIJAU MEDSOS', 1, 'GHM12205-664', NULL, '2022-05-15 09:25:00', '2022-05-15 09:25:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('GHTVBM-GH', 'GHTVBM', 'GANGGANG HIJAU', 500000.00, 'GANGGANG TV BATU MALANG', 1, 'GHTVBM2206-368', NULL, '2022-06-30 14:49:00', '2022-06-30 14:49:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JAK TV 2-MDH', 'JAK TV 2', 'MADU HIJAU', 250000.00, 'Admin Jak TV 2', 1, 'JAK TV 22107-525', NULL, '2021-07-02 23:08:00', '2021-07-02 23:08:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JK1-MDH', 'JK1', 'MADU HIJAU', 250000.00, 'Admin JAK TV 1', 1, 'JK12021-003', NULL, '2021-05-22 11:08:00', '2021-05-22 11:08:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JK2-MDH', 'JK2', 'MADU HIJAU', 250000.00, 'admin jak tv 2', 1, 'JK22107-453', NULL, '2021-07-03 14:31:00', '2021-07-03 14:31:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JKMGG-MGH', 'JKMGG', 'Madu Ganggang Hijau', 0.00, 'JAK TV MALAM GANGGANG', 1, 'JKMGG2203-396', NULL, '2022-03-02 17:49:00', '2022-03-02 17:49:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JPM TV-MDH', 'JPM TV', 'MADU HIJAU', 250000.00, 'Azza putri JPM', 1, 'JPM TV2201-687', NULL, '2022-01-08 11:49:00', '2022-01-08 11:49:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JPMGH1-JPMGHM1', 'JPMGH1', 'GANGGANG HIJAU', 250000.00, 'JPM TV GANGGANG HIJAU', 1, 'JPMGH12211-667', NULL, '2022-11-23 09:17:00', '2022-11-23 09:17:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JPMGHTV-GH', 'JPMGHTV', 'GANGGANG HIJAU', 500000.00, 'JPM GANGGANG MEDIA TV', 1, 'JPMGHTV2212-127', NULL, '2022-12-16 08:35:00', '2022-12-16 08:35:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JV1-MDH', 'JV1', 'MADU HIJAU', 250000.00, 'admin JTV Siang', 1, 'JV12021-003', NULL, '2021-05-24 14:39:00', '2021-05-24 14:39:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JV2-MDH', 'JV2', 'MADU HIJAU', 250000.00, 'Admin JTV Sore', 1, 'JV22021-003', NULL, '2021-05-24 13:28:00', '2021-05-24 13:28:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JV3-MDH', 'JV3', 'Madu Hijau', 250000.00, 'Sandra Regita', 1, 'JV32106-862', NULL, '2021-06-05 20:35:00', '2021-06-05 20:35:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JVG2SBY-GGH', 'JVG2SBY', 'GANGGANG HIJAU', 250000.00, 'JTV SIANG SBY GANGGANG', 1, 'JVG2SBY2203-885', NULL, '2022-03-09 11:34:00', '2022-03-09 11:34:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('JVG3-JVG3', 'JVG3', 'ganggang hijau', 250000.00, 'Azza Putri', 1, 'JVG32203-094', NULL, '2022-03-01 22:41:00', '2022-03-01 22:41:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('KTV-KTV', 'KTV', 'MADU HIJAU', 250000.00, 'KOMPAS TV', 1, 'KTV2203-239', NULL, '2022-04-20 09:44:00', '2022-04-20 09:44:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('MDH', 'HPI', 'Madu Hijau', NULL, NULL, 1, '', '', NULL, NULL, 250000.00, 250000.00, 210.00, '210 gr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('MGH', 'HPI', 'Madu Herbal Ganggang Hijau', NULL, NULL, 1, '', '', NULL, NULL, 250000.00, 250000.00, 350.00, '340 gr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('MGH1-MGH', 'MGH1', 'Ganggang Hijau', 250000.00, 'Madu Ganggang Hijau 1', 1, 'MGH12202-731', NULL, '2022-02-21 17:46:00', '2022-02-21 17:46:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('MHTK1-MDH1', 'MHTK1', 'Madu Hijau', 250000.00, 'Super Admin', 1, 'MHTK12209-013', NULL, '2022-09-12 21:00:00', '2022-09-12 21:00:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('MNCTR-MDH', 'MNCTR', 'Madu Hijau', 250000.00, 'MNC TRIJAYA MDH', 1, 'MNCTR2112-262', 'MNCTR2112-262', '2021-12-03 15:20:00', '2021-12-03 15:21:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('OC1-MDH', 'OC1', 'MADU HIJAU', 250000.00, 'Admin O Channel Pagi', 1, 'OC12021-003', NULL, '2021-05-22 22:01:00', '2021-05-22 22:01:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('OC2-MDH', 'OC2', 'MADU HIJAU', 250000.00, 'Admin O Channel Sore', 1, 'OC22021-003', NULL, '2021-05-22 21:07:00', '2021-05-22 21:07:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('PDNG-PD1-MDH', 'PDNG', 'MADU HIJAU', 250000.00, 'PADANG TV 1', 1, 'PDNG2111-056', NULL, '2021-11-25 12:26:00', '2021-11-25 12:26:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('PJTV-MADU HIJAU', 'PJTV', 'MADU HIJAU', 250000.00, 'PJTV', 1, 'PJTV2206-910', NULL, '2022-06-08 13:33:00', '2022-06-08 13:33:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('PKGMD100', 'HPI', 'Package 100 pcs', NULL, NULL, 1, '', '', NULL, NULL, 0.00, 0.00, 100.00, '100 pcs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('PL1-MDH', 'PL1', 'MADU HIJAU', 250000.00, 'Admin PAL TV', 1, 'PL12021-003', NULL, '2021-05-22 20:36:00', '2021-05-22 20:36:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('RD1-MDH', 'RD1', 'Madu Hijau', 250000.00, 'Yessi Antika', 1, 'RD12106-521', NULL, '2021-06-12 13:41:00', '2021-06-12 13:41:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('RD2-MDH', 'RD2', 'Madu Hijau', 250000.00, 'Admin Erlangga', 1, 'RD22109-743', NULL, '2021-09-21 08:45:00', '2021-09-21 08:45:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('RDG-RDG', 'RDG', 'Ganggang Hijau', 250000.00, 'Yessiganggang', 1, 'RDG2112-906', 'RDG2112-906', '2021-12-12 15:28:00', '2021-12-12 15:28:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('RSMTV-GH', 'RSMTV', 'GANGGANG HIJAU', 500000.00, 'RADIO SONORA MEDIA TV', 1, 'RSMTV2207-511', NULL, '2022-07-03 11:15:00', '2022-07-03 11:15:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('RU1-MDH', 'RU1', 'MADU HIJAU', 250000.00, 'Admin Riau Tv 1', 1, 'RU12021-003', NULL, '2021-05-25 16:17:00', '2021-05-25 16:17:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('TATV-MH', 'TATV', 'MADU HIJAU', 250000.00, 'TATV SOLO MADU HIJAU', 1, 'TATV2212-478', NULL, '2022-12-05 08:22:00', '2022-12-05 08:22:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mst_item` VALUES ('TVONE-TVONE', 'TVONE', 'MADU HIJAU', 250000.00, 'TV ONE', 1, 'TVONE2203-770', NULL, '2022-04-02 17:22:00', '2022-04-02 17:22:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_order
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_order`;
CREATE TABLE `pur_purchase_order` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `approvalBy` varchar(255) DEFAULT NULL,
  `approvalDate` datetime(6) DEFAULT NULL,
  `approvalReasonCode` varchar(255) DEFAULT NULL,
  `approvalRemark` varchar(255) DEFAULT NULL,
  `approvalStatus` varchar(255) DEFAULT NULL,
  `billToCode` varchar(255) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `closingBy` varchar(255) DEFAULT NULL,
  `closingDate` datetime(6) DEFAULT NULL,
  `closingRemark` varchar(255) DEFAULT NULL,
  `closingStatus` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `currencyCode` varchar(255) DEFAULT NULL,
  `deliveryDateEnd` datetime(6) DEFAULT NULL,
  `deliveryDateStart` datetime(6) DEFAULT NULL,
  `discountAmount` decimal(19,2) DEFAULT NULL,
  `discountChartOfAccountCode` varchar(255) DEFAULT NULL,
  `discountDescription` varchar(255) DEFAULT NULL,
  `discountPercent` decimal(19,2) DEFAULT NULL,
  `discountType` varchar(255) DEFAULT NULL,
  `expeditionCode` varchar(255) DEFAULT NULL,
  `grandTotalAmount` decimal(19,2) DEFAULT NULL,
  `otherFeeAmount` decimal(19,2) DEFAULT NULL,
  `otherFeeChartOfAccountCode` varchar(255) DEFAULT NULL,
  `otherFeeDescription` varchar(255) DEFAULT NULL,
  `paymentTermCode` varchar(255) DEFAULT NULL,
  `pphAmount` decimal(19,2) DEFAULT NULL,
  `pphChartOfAccountCode` varchar(255) DEFAULT NULL,
  `pphDescription` varchar(255) DEFAULT NULL,
  `pphPercent` decimal(19,2) DEFAULT NULL,
  `refNo` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `shipToCode` varchar(255) DEFAULT NULL,
  `taxBaseSubTotalAmount` decimal(19,2) DEFAULT NULL,
  `totalTransactionAmount` decimal(19,2) DEFAULT NULL,
  `transactionDate` datetime(6) DEFAULT NULL,
  `vatAmount` decimal(19,2) DEFAULT NULL,
  `vatPercent` decimal(19,2) DEFAULT NULL,
  `vendorCode` varchar(255) DEFAULT NULL,
  `vendorPromoCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_order
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_order_detail`;
CREATE TABLE `pur_purchase_order_detail` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `bonusQuantity` decimal(19,2) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `discount1Amount` decimal(19,2) DEFAULT NULL,
  `discount1Percent` decimal(19,2) DEFAULT NULL,
  `discount2Amount` decimal(19,2) DEFAULT NULL,
  `discount2Percent` decimal(19,2) DEFAULT NULL,
  `discount3Amount` decimal(19,2) DEFAULT NULL,
  `discount3Percent` decimal(19,2) DEFAULT NULL,
  `discountHeaderAmount` decimal(19,2) DEFAULT NULL,
  `discountHeaderPercent` decimal(19,2) DEFAULT NULL,
  `headerCode` varchar(255) DEFAULT NULL,
  `itemAlias` varchar(255) DEFAULT NULL,
  `itemCode` varchar(255) DEFAULT NULL,
  `nettPrice` decimal(19,2) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `purchaseRequestDetailCode` varchar(255) DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `totalAmount` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_order_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_order_jn_purchase_request
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_order_jn_purchase_request`;
CREATE TABLE `pur_purchase_order_jn_purchase_request` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `purchaseOrderCode` varchar(255) DEFAULT NULL,
  `purchaseRequestCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_order_jn_purchase_request
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_request
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_request`;
CREATE TABLE `pur_purchase_request` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `approvalBy` varchar(255) DEFAULT NULL,
  `approvalDate` datetime(6) DEFAULT NULL,
  `approvalReasonCode` varchar(255) DEFAULT NULL,
  `approvalRemark` varchar(255) DEFAULT NULL,
  `approvalStatus` varchar(255) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `closingBy` varchar(255) DEFAULT NULL,
  `closingDate` datetime(6) DEFAULT NULL,
  `closingRemark` varchar(255) DEFAULT NULL,
  `closingStatus` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `refNo` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `requestBy` varchar(255) DEFAULT NULL,
  `transactionDate` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_request
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_request_detail
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_request_detail`;
CREATE TABLE `pur_purchase_request_detail` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `headerCode` varchar(255) DEFAULT NULL,
  `itemAlias` varchar(255) DEFAULT NULL,
  `itemCode` varchar(255) DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_request_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_return
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_return`;
CREATE TABLE `pur_purchase_return` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `currencyCode` varchar(255) DEFAULT NULL,
  `discountAmount` decimal(19,2) DEFAULT NULL,
  `discountChartOfAccountCode` varchar(255) DEFAULT NULL,
  `discountDescription` varchar(255) DEFAULT NULL,
  `discountPercent` decimal(19,2) DEFAULT NULL,
  `dueDate` datetime(6) DEFAULT NULL,
  `exchangeRate` decimal(19,2) DEFAULT NULL,
  `grandTotalAmount` decimal(19,2) DEFAULT NULL,
  `otherFeeAmount` decimal(19,2) DEFAULT NULL,
  `otherFeeChartOfAccountCode` varchar(255) DEFAULT NULL,
  `otherFeeDescription` varchar(255) DEFAULT NULL,
  `paidAmount` decimal(19,2) DEFAULT NULL,
  `paymentTermCode` varchar(255) DEFAULT NULL,
  `refNo` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `settlementDate` datetime(6) DEFAULT NULL,
  `settlementDocumentNo` varchar(255) DEFAULT NULL,
  `taxBaseSubTotalAmount` decimal(19,2) DEFAULT NULL,
  `taxInvoiceNo` varchar(255) DEFAULT NULL,
  `totalTransactionAmount` decimal(19,2) DEFAULT NULL,
  `transactionDate` datetime(6) DEFAULT NULL,
  `vatAmount` decimal(19,2) DEFAULT NULL,
  `vatPercent` decimal(19,2) DEFAULT NULL,
  `vendorCode` varchar(255) DEFAULT NULL,
  `vendorInvoiceCode` varchar(255) DEFAULT NULL,
  `vendorInvoiceNo` varchar(255) DEFAULT NULL,
  `warehouseCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_return
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for pur_purchase_return_item_detail
-- ----------------------------
DROP TABLE IF EXISTS `pur_purchase_return_item_detail`;
CREATE TABLE `pur_purchase_return_item_detail` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `headerCode` varchar(255) DEFAULT NULL,
  `itemCode` varchar(255) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `quantity` decimal(19,2) DEFAULT NULL,
  `rackCode` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `totalAmount` decimal(19,2) DEFAULT NULL,
  `vendorInvoiceItemDetailCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of pur_purchase_return_item_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for scr_menu
-- ----------------------------
DROP TABLE IF EXISTS `scr_menu`;
CREATE TABLE `scr_menu` (
  `code` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `parentCode` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `type` enum('GROUP','SUB','MODULE') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `text` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `link` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `icon` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `sortNo` int(18) DEFAULT NULL,
  `isBackEndStatus` tinyint(1) DEFAULT NULL,
  `isMobileApps` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of scr_menu
-- ----------------------------
BEGIN;
INSERT INTO `scr_menu` VALUES ('000_DASHBOARD', NULL, 'MODULE', 'Dashboard', '/', 'pi pi-fw pi-home', 1000000001, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR', NULL, 'GROUP', 'Purchase', NULL, 'pi pi-fw pi-chart-bar', 1001000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_HISTORY', '001_PUR', 'MODULE', 'Purchase History', '/purchase/purchase-history', 'pi pi-fw pi-sign-in', 1001001008, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_ORDER', '001_PUR', 'MODULE', 'Purchase Order', '/purchase/purchase-order', 'pi pi-fw pi-sign-in', 1001001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_ORDER_APPROVAL', '001_PUR', 'MODULE', 'Purchase Order Approval', '/purchase/purchase-order-approval', 'pi pi-fw pi-sign-in', 1001001005, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_ORDER_CLOSING', '001_PUR', 'MODULE', 'Purchase Order Closing', '/purchase/purchase-order-closing', 'pi pi-fw pi-sign-in', 1001001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_REQUEST', '001_PUR', 'MODULE', 'Purchase Request', '/purchase/purchase-request', 'pi pi-fw pi-sign-in', 1001001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_REQUEST_APPROVAL', '001_PUR', 'MODULE', 'Purchase Request Approval', '/purchase/purchase-request-approval', 'pi pi-fw pi-sign-in', 1001001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_REQUEST_CLOSING', '001_PUR', 'MODULE', 'Purchase Request Closing', '/purchase/purchase-request-closing', 'pi pi-fw pi-sign-in', 1001001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('001_PUR_PURCHASE_RETURN', '001_PUR', 'MODULE', 'Purchase Return', '/purchase/purchase-return', 'pi pi-fw pi-sign-in', 1001001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL', NULL, 'GROUP', 'Sales', NULL, 'pi pi-fw pi-chart-line', 1002000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_CUSTOMER_INVOICE_SALES_ORDER', '002_SAL', 'MODULE', 'Customer Invoice Sales Order', '/sales/customer-invoice-sales-order', 'pi pi-fw pi-sign-in', 1002001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_ORDER_MATERIAL_FORM', '002_SAL', 'MODULE', 'Order Material Form', '/sales/order-material-form', 'pi pi-fw pi-sign-in', 1002001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_ORDER_MATERIAL_FORM_CLOSING', '002_SAL', 'MODULE', 'Order Material Form Closing', '/sales/order-material-form-closing', 'pi pi-fw pi-sign-in', 1002001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_ORDER_MATERIAL_FORM_RETURN', '002_SAL', 'MODULE', 'Order Material Form Return', '/sales/order-material-form-return', 'pi pi-fw pi-sign-in', 1002001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_PICKING_LIST_SALES_ORDER', '002_SAL', 'MODULE', 'Picking List Sales Order', '/sales/picking-list-sales-order', 'pi pi-fw pi-sign-in', 1002001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION', '002_SAL', 'MODULE', 'Picking List Sales Order Confirmation', '/sales/picking-list-sales-order-confirmation', 'pi pi-fw pi-sign-in', 1002001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE', '002_SAL', 'MODULE', 'Print Multiple Customer Invoice', '/sales/print-multiple-customer-invoice', 'pi pi-fw pi-sign-in', 1002001010, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE', '002_SAL', 'MODULE', 'Print Multiple Delivery Note', '/sales/print-multiple-delivery-note', 'pi pi-fw pi-sign-in', 1002001009, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_SALES_HISTORY', '002_SAL', 'MODULE', 'Sales History', '/sales/sales-history', 'pi pi-fw pi-sign-in', 1002001011, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_SALES_ORDER', '002_SAL', 'MODULE', 'Sales Order', '/sales/sales-order', 'pi pi-fw pi-sign-in', 1002001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_SALES_ORDER_APPROVAL', '002_SAL', 'MODULE', 'Sales Order Approval', '/sales/sales-order-approval', 'pi pi-fw pi-sign-in', 1002001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_SALES_ORDER_CLOSING', '002_SAL', 'MODULE', 'Sales Order Closing', '/sales/sales-order-closing', 'pi pi-fw pi-sign-in', 1002001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_SALES_RETURN_BY_INVOICE', '002_SAL', 'MODULE', 'Sales Return By Invoice', '/sales/sales-return-by-invoice', 'pi pi-fw pi-sign-in', 1002001008, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER', '002_SAL', 'MODULE', 'Un Assign Delivery Note Sales Order', '/sales/un-assign-delivery-note-sales-order', 'pi pi-fw pi-sign-in', 1002001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER', '003_WMS', 'MODULE', 'Un Assign Picking List Sales Order', '/warehouse/un-assign-picking-list-sales-order', 'pi pi-fw pi-sign-in', 1003001009, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT', NULL, 'GROUP', 'Inventory Management', NULL, 'pi pi-fw pi-inbox', 1003000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_ADJUSTMENT_IN', '003_IVT', 'MODULE', 'Adjustment In', '/inventory/adjustment-in', 'pi pi-fw pi-sign-in', 1003001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_ADJUSTMENT_IN_APPROVAL', '003_IVT', 'MODULE', 'Adjustment In Approval', '/inventory/adjustment-in-approval', 'pi pi-fw pi-sign-in', 1003001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_ADJUSTMENT_OUT', '003_IVT', 'MODULE', 'Adjustment Out', '/inventory/adjustment-out', 'pi pi-fw pi-sign-in', 1003001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_ADJUSTMENT_OUT_APPROVAL', '003_IVT', 'MODULE', 'Adjustment Out Approval', '/inventory/adjustment-out-approval', 'pi pi-fw pi-sign-in', 1003001005, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_ASSEMBLY_JOB_ORDER', '003_IVT', 'MODULE', 'Assembly Job Order', '/inventory/assembly-job-order', 'pi pi-fw pi-sign-in', 1003001011, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_ASSEMBLY_REALIZATION', '003_IVT', 'MODULE', 'Assembly Realization', '/inventory/assembly-realization', 'pi pi-fw pi-sign-in', 1003001012, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_CURRENT_STOCK_QUANTITY', '003_IVT', 'MODULE', 'Current Stock Quantity', '/inventory/current-stock-quantity', 'pi pi-fw pi-sign-in', 1003001014, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_DELIVERY_NOTE_SALES_ORDER', '003_IVT', 'MODULE', 'Delivery Note Sales Order', '/inventory/delivery-note-sales-order', 'pi pi-fw pi-sign-in', 1003001010, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_DOCK_STOCK', '003_IVT', 'MODULE', 'Dock Stock', '/inventory/dock-stock', 'pi pi-fw pi-sign-in', 1003001015, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_GOODS_RECEIVED_NOTE', '003_IVT', 'MODULE', 'Goods Received Note', '/inventory/goods-received-note', 'pi pi-fw pi-sign-in', 1003001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_STOCK_CLOSING', '003_IVT', 'MODULE', 'Stock Closing', '/inventory/stock-closing', 'pi pi-fw pi-sign-in', 1003001013, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_STOCK_OPNAME_APPROVAL', '003_IVT', 'MODULE', 'Stock Opname Approval', '/inventory/stock-opname-approval', 'pi pi-fw pi-sign-in', 1003001018, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_STOCK_OPNAME_DRAFT', '003_IVT', 'MODULE', 'Stock Opname Draft', '/inventory/stock-opname-draft', 'pi pi-fw pi-sign-in', 1003001016, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_STOCK_OPNAME_POSTING', '003_IVT', 'MODULE', 'Stock Opname Posting', '/inventory/stock-opname-posting', 'pi pi-fw pi-sign-in', 1003001017, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE', '003_IVT', 'MODULE', 'Un Assign Picking List Delivery Note', '/inventory/un-assign-picking-list-delivery-note', 'pi pi-fw pi-sign-in', 1003001010, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_WAREHOUSE_TRANSFER_IN', '003_IVT', 'MODULE', 'Warehouse Transfer In', '/inventory/warehouse-transfer-in', 'pi pi-fw pi-sign-in', 1003001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('003_IVT_WAREHOUSE_TRANSFER_OUT', '003_IVT', 'MODULE', 'Warehouse Transfer Out', '/inventory/warehouse-transfer-out', 'pi pi-fw pi-sign-in', 1003001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN', NULL, 'GROUP', 'Finance', NULL, 'pi pi-fw pi-briefcase', 1004000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_AP', '004_FIN', 'SUB', 'Finance AP', '/finance', 'pi pi-fw pi-list', 1004001000, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_AR', '004_FIN', 'SUB', 'Finance AR', '/finance', 'pi pi-fw pi-list', 1004002000, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_BANK_PAYMENT', '004_FIN_AP', 'MODULE', 'Bank Payment', '/finance/bank-payment', 'pi pi-fw pi-sign-in', 1004001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_BANK_RECEIVED', '004_FIN_AR', 'MODULE', 'Bank Received', '/finance/bank-received', 'pi pi-fw pi-sign-in', 1004002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_CASH_PAYMENT', '004_FIN_AP', 'MODULE', 'Cash Payment', '/finance/cash-payment', 'pi pi-fw pi-sign-in', 1004001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_CASH_RECEIVED', '004_FIN_AR', 'MODULE', 'Cash Received', '/finance/cash-received', 'pi pi-fw pi-sign-in', 1004002002, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_CUSTOMER_CREDIT_NOTE', '004_FIN_AR', 'MODULE', 'Customer Credit Note', '/finance/customer-credit-note', 'pi pi-fw pi-sign-in', 1004002004, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_CUSTOMER_DEBIT_NOTE', '004_FIN_AR', 'MODULE', 'Customer Debit Note', '/finance/customer-debit-note', 'pi pi-fw pi-sign-in', 1004002003, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT', '004_FIN_AR', 'MODULE', 'Customer Deposit Assignment', '/finance/customer-deposit-assignment', 'pi pi-fw pi-sign-in', 1004002010, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_CUSTOMER_DOWN_PAYMENT', '004_FIN_AR', 'MODULE', 'Customer Down Payment', '/finance/customer-down-payment', 'pi pi-fw pi-sign-in', 1004002005, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_DLN_CHECKLIST', '004_FIN_AR', 'MODULE', 'DLN Checklist', '/finance/dln-checklist', 'pi pi-fw pi-sign-in', 1004002011, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_FINANCE_CLOSING', '004_FIN_OTHER', 'MODULE', 'Finance Closing', '/finance/finance-closing', 'pi pi-fw pi-sign-in', 1004003004, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_FINANCE_RECALCULATING', '004_FIN_OTHER', 'MODULE', 'Finance Recalculating', '/finance/finance-recalculating', 'pi pi-fw pi-sign-in', 1004003003, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GENERAL_JOURNAL', '004_FIN_OTHER', 'MODULE', 'General Journal', '/finance/general-journal', 'pi pi-fw pi-sign-in', 1004003001, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_HAND_OVER_TO_FINANCE', '004_FIN_AR', 'MODULE', 'Giro Hand Over to Finance', '/finance/giro-hand-over-to-finance', 'pi pi-fw pi-sign-in', 1004002015, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL', '004_FIN_AR', 'MODULE', 'Giro Hand Over to Finance Approval', '/finance/giro-hand-over-to-finance-approval', 'pi pi-fw pi-sign-in', 1004002016, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_PAYMENT', '004_FIN_AP', 'MODULE', 'Giro Payment', '/finance/giro-payment', 'pi pi-fw pi-sign-in', 1004001009, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_PAYMENT_INQUIRY', '004_FIN_AP', 'MODULE', 'Giro Payment Inquiry', '/finance/giro-payment-inquiry', 'pi pi-fw pi-sign-in', 1004001011, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_PAYMENT_REJECTED', '004_FIN_AP', 'MODULE', 'Giro Payment Rejected', '/finance/giro-payment-rejected', 'pi pi-fw pi-sign-in', 1004001010, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_RECEIVED', '004_FIN_AR', 'MODULE', 'Giro Received', '/finance/giro-received', 'pi pi-fw pi-sign-in', 1004002006, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_RECEIVED_INQUIRY', '004_FIN_AR', 'MODULE', 'Giro Received Inquiry', '/finance/giro-received-inquiry', 'pi pi-fw pi-sign-in', 1004002008, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GIRO_RECEIVED_REJECT', '004_FIN_AR', 'MODULE', 'Giro Received Reject', '/finance/giro-received-rejected', 'pi pi-fw pi-sign-in', 1004002007, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_GOODS_RECEIVED_NOTE_VIEW', '004_FIN_AP', 'MODULE', 'Goods Received Note View', '/finance/goods-received-note-view', 'pi pi-fw pi-sign-in', 1004001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER', '004_FIN_AR', 'MODULE', 'Invoice Hand Over to Customer', '/finance/invoice-hand-over-to-customer', 'pi pi-fw pi-sign-in', 1004002014, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_INVOICE_HAND_OVER_TO_FINANCE', '004_FIN_AR', 'MODULE', 'Invoice Hand Over to Finance', '/finance/inv-hand-over-to-finance', 'pi pi-fw pi-sign-in', 1004002012, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL', '004_FIN_AR', 'MODULE', 'Invoice Hand Over to Finance Approval', '/finance/inv-hand-over-to-finance-approval', 'pi pi-fw pi-sign-in', 1004002013, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_OTHER', '004_FIN', 'SUB', 'Finance Other', '/finance', 'pi pi-fw pi-list', 1004003000, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_PAYMENT_HISTORY', '004_FIN_OTHER', 'MODULE', 'Payment History', '/finance/payment-history', 'pi pi-fw pi-sign-in', 1004003002, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_PAYMENT_REQUEST', '004_FIN_AP', 'MODULE', 'Payment Request', '/finance/payment-request', 'pi pi-fw pi-sign-in', 1004001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_PAYMENT_REQUEST_APPROVAL', '004_FIN_AP', 'MODULE', 'Payment Request Approval', '/finance/payment-request-approval', 'pi pi-fw pi-sign-in', 1004001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION', '004_FIN_AR', 'MODULE', 'Update Customer Invoice Sales Order Information', '/finance/update-customer-invoice-sales-order-information', 'pi pi-fw pi-sign-in', 1004002009, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_VENDOR_CREDIT_NOTE', '004_FIN_AP', 'MODULE', 'Vendor Credit Note', '/finance/vendor-credit-note', 'pi pi-fw pi-sign-in', 1004001005, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_VENDOR_DEBIT_NOTE', '004_FIN_AP', 'MODULE', 'Vendor Debit Note', '/finance/vendor-debit-note', 'pi pi-fw pi-sign-in', 1004001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_VENDOR_DEPOSIT_ASSIGNMENT', '004_FIN_AP', 'MODULE', 'Vendor Deposit Assignment', '/finance/vendor-deposit-assignment', 'pi pi-fw pi-sign-in', 1004001012, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_VENDOR_DOWN_PAYMENT', '004_FIN_AP', 'MODULE', 'Vendor Down Payment', '/finance/vendor-down-payment', 'pi pi-fw pi-sign-in', 1004001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_VENDOR_INVOICE', '004_FIN_AP', 'MODULE', 'Vendor Invoice', '/finance/vendor-invoice', 'pi pi-fw pi-sign-in', 1004001008, 1, 1);
INSERT INTO `scr_menu` VALUES ('004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION', '004_FIN_AP', 'MODULE', 'Vendor Invoice Update Information', '/finance/vendor-invoice-update-information', 'pi pi-fw pi-sign-in', 1004001013, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC', NULL, 'GROUP', 'Accounting', NULL, 'pi pi-fw pi-credit-card', 1005000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_ACCOUNTING_CLOSING', '005_ACC_TRANS', 'MODULE', 'Accounting Closing', '/accounting/accounting-closing', 'pi pi-fw pi-sign-in', 1005001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_CURRENT_STOCK_COGSIDR', '005_ACC_TRANS', 'MODULE', 'Current Stock COGSIDR', '/accounting/current-stock-cogsidr', 'pi pi-fw pi-sign-in', 1005001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_E-FAK', '005_ACC', 'SUB', 'e-Faktur', '/accounting', 'pi pi-fw pi-list', 1005002000, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_JOURNAL_POSTING', '005_ACC_TRANS', 'MODULE', 'Journal Posting', '/accounting/journal-posting', 'pi pi-fw pi-sign-in', 1005001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_JOURNAL_QUERY', '005_ACC_TRANS', 'MODULE', 'Journal Query', '/accounting/journal-query', 'pi pi-fw pi-sign-in', 1005001005, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_KELUARAN', '005_ACC_E-FAK', 'MODULE', 'Keluaran', '/accounting/keluaran', 'pi pi-fw pi-sign-in', 1005002008, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_MASUKAN', '005_ACC_E-FAK', 'MODULE', 'Masukan', '/accounting/masukan', 'pi pi-fw pi-sign-in', 1005002007, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_TAX_INVOICE_ASSIGNMENT', '005_ACC_E-FAK', 'MODULE', 'Tax Invoice Assignment', '/accounting/tax-invoice-assignment', 'pi pi-fw pi-sign-in', 1005002002, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_TAX_INVOICE_REGISTRATION', '005_ACC_E-FAK', 'MODULE', 'Tax Invoice Registration', '/accounting/tax-invoice-registration', 'pi pi-fw pi-sign-in', 1005002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_TRANS', '005_ACC', 'SUB', 'Transaction', '/accounting', 'pi pi-fw pi-list', 1005001000, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_UNBALANCE_COGS', '005_ACC_TRANS', 'MODULE', 'Unbalance COGS', '/accounting/unbalance-cogs', 'pi pi-fw pi-sign-in', 1005001008, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_UN_BALANCE_JOURNAL', '005_ACC_TRANS', 'MODULE', 'Un Balance Journal', '/accounting/un-balance-journal', 'pi pi-fw pi-sign-in', 1005001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_UN_JOURNAL', '005_ACC_TRANS', 'MODULE', 'Un Journal', '/accounting/un-journal', 'pi pi-fw pi-sign-in', 1005001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_UPDATE_TAX_INVOICE', '005_ACC_E-FAK', 'MODULE', 'Update Tax Invoice', '/accounting/update-tax-invoice', 'pi pi-fw pi-sign-in', 1005002003, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_VAT_IN', '005_ACC_E-FAK', 'MODULE', 'Vat In', '/accounting/vat-in', 'pi pi-fw pi-sign-in', 1005002006, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_VAT_OUT', '005_ACC_E-FAK', 'MODULE', 'Vat Out', '/accounting/vat-out', 'pi pi-fw pi-sign-in', 1005002005, 1, 1);
INSERT INTO `scr_menu` VALUES ('005_ACC_VOID_TAX_INVOICE', '005_ACC_E-FAK', 'MODULE', 'Void Tax Invoice', '/accounting/void-tax-invoice', 'pi pi-fw pi-sign-in', 1005002004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST', NULL, 'GROUP', 'Master', NULL, 'pi pi-fw pi-th-large', 1015000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_APPROVAL_REASON', '015_MST', 'MODULE', 'Approval Reason', '/master/approval-reason', 'pi pi-fw pi-sign-in', 1015001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_BANK', '015_MST', 'MODULE', 'Bank', '/master/bank', 'pi pi-fw pi-sign-in', 1015001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_BANK_ACCOUNT', '015_MST', 'MODULE', 'Bank Account', '/master/bank-account', 'pi pi-fw pi-sign-in', 1015001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_BILL_OF_MATERIAL', '015_MST', 'MODULE', 'Bill Of Material', '/master/bill-of-material', 'pi pi-fw pi-sign-in', 1015001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_BRANCH', '015_MST', 'MODULE', 'Branch', '/master/branch', 'pi pi-fw pi-sign-in', 1015001005, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_BUSINESS_ENTITY', '015_MST', 'MODULE', 'Business Entity', '/master/business-entity', 'pi pi-fw pi-sign-in', 1015001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CASH_ACCOUNT', '015_MST', 'MODULE', 'Cash Account', '/master/cash-account', 'pi pi-fw pi-sign-in', 1015001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CHART_OF_ACCOUNT', '015_MST', 'MODULE', 'Chart Of Account', '/master/chart-of-account', 'pi pi-fw pi-sign-in', 1015001009, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CITY', '015_MST', 'MODULE', 'City', '/master/city', 'pi pi-fw pi-sign-in', 1015001011, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_COLOR', '015_MST', 'MODULE', 'Color', '/master/color', 'pi pi-fw pi-sign-in', 1015001012, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_COUNTRY', '015_MST', 'MODULE', 'Country', '/master/country', 'pi pi-fw pi-sign-in', 1015001013, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CURRENCY', '015_MST', 'MODULE', 'Currency', '/master/currency', 'pi pi-fw pi-sign-in', 1015001014, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CURRENT_PRICE_LIST', '015_MST', 'MODULE', 'Current Price List', '/master/current-price-list', 'pi pi-fw pi-sign-in', 1015001014, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CUSTOMER', '015_MST', 'MODULE', 'Customer', '/master/customer', 'pi pi-fw pi-sign-in', 1015001015, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CUSTOMER_CATEGORY', '015_MST', 'MODULE', 'Customer Category', '/master/customer-category', 'pi pi-fw pi-sign-in', 1015001016, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CUSTOMER_DEPOSIT_TYPE', '015_MST', 'MODULE', 'Customer Deposit Type', '/master/customer-deposit-type', 'pi pi-fw pi-sign-in', 1015001017, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CUSTOMER_JN_ADDRESS', '015_MST', 'MODULE', 'Customer Jn Address', '/master/customer-jn-address', 'pi pi-fw pi-sign-in', 1015001019, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_CUSTOMER_JN_CONTACT', '015_MST', 'MODULE', 'Customer Jn Contact', '/master/customer-jn-contact', 'pi pi-fw pi-sign-in', 1015001020, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_DEPARTMENT', '015_MST', 'MODULE', 'Department', '/master/department', 'pi pi-fw pi-sign-in', 1015002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_DISCOUNT_TYPE', '015_MST', 'MODULE', 'Discount Type', '/master/discount-type', 'pi pi-fw pi-sign-in', 1015002002, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_DISTRIBUTION_CHANNEL', '015_MST', 'MODULE', 'Distribution Channel', '/master/distribution-channel', 'pi pi-fw pi-sign-in', 1015002003, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_DIVISION', '015_MST', 'MODULE', 'Division', '/master/division', 'pi pi-fw pi-sign-in', 1015002004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_DOCUMENT_TYPE', '015_MST', 'MODULE', 'Document Type', '/master/document-type', 'pi pi-fw pi-sign-in', 1015002005, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_EDUCATION', '015_MST', 'MODULE', 'Education', '/master/education', 'pi pi-fw pi-sign-in', 1015002006, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_EMPLOYEE', '015_MST', 'MODULE', 'Employee', '/master/employee', 'pi pi-fw pi-sign-in', 1015002007, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_EMPLOYEE_TYPE', '015_MST', 'MODULE', 'Employee Type', '/master/employee-type', 'pi pi-fw pi-sign-in', 1015002008, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_EXCHANGE_RATE', '015_MST', 'MODULE', 'Exchange Rate', '/master/exchange-rate', 'pi pi-fw pi-sign-in', 1015002009, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_INVENTORY_ADJ_REASON', '015_MST', 'MODULE', 'Inventory Adj Reason', '/master/inventory-adj-reason', 'pi pi-fw pi-sign-in', 1015003001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ISLAND', '015_MST', 'MODULE', 'Island', '/master/island', 'pi pi-fw pi-sign-in', 1015003003, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM', '015_MST', 'MODULE', 'Item', '/master/item', 'pi pi-fw pi-sign-in', 1015003004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM_BRAND', '015_MST', 'MODULE', 'Item Brand', '/master/item-brand', 'pi pi-fw pi-sign-in', 1015003005, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM_CATEGORY', '015_MST', 'MODULE', 'Item Category', '/master/item-category', 'pi pi-fw pi-sign-in', 1015003006, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM_CLASSIFICATION', '015_MST', 'MODULE', 'Item Classification', '/master/item-classification', 'pi pi-fw pi-sign-in', 1015003007, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM_DIVISION', '015_MST', 'MODULE', 'Item Division', '/master/item-division', 'pi pi-fw pi-sign-in', 1015003008, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM_JN_COGSIDR', '015_MST', 'MODULE', 'Item Jn Cogsidr', '/master/item-jn-cogsidr', 'pi pi-fw pi-sign-in', 1015003010, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ITEM_SUB_CATEGORY', '015_MST', 'MODULE', 'Item Sub Category', '/master/item-sub-category', 'pi pi-fw pi-sign-in', 1015003013, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_JOB_POSITION', '015_MST', 'MODULE', 'Job Position', '/master/job-position', 'pi pi-fw pi-sign-in', 1015003014, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_JOURNAL', '015_MST', 'MODULE', 'Journal', '/master/journal', 'pi pi-fw pi-sign-in', 1015003015, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_MARITAL_STATUS', '015_MST', 'MODULE', 'Marital Status', '/master/marital-status', 'pi pi-fw pi-sign-in', 1015004001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ORDER_MATERIAL_FORM_STATUS', '015_MST', 'MODULE', 'Order Material Form Status', '/master/order-material-form-status', 'pi pi-fw pi-sign-in', 1015004001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_ORDER_MATERIAL_FORM_TYPE', '015_MST', 'MODULE', 'Order Material Form Type', '/master/order-material-form-type', 'pi pi-fw pi-sign-in', 1015004001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_PAYMENT_TERM', '015_MST', 'MODULE', 'Payment Term', '/master/payment-term', 'pi pi-fw pi-sign-in', 1015004002, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_PRICE_TYPE', '015_MST', 'MODULE', 'Price Type', '/master/price-type', 'pi pi-fw pi-sign-in', 1015004004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_PROJECT', '015_MST', 'MODULE', 'Project', '/master/project', 'pi pi-fw pi-sign-in', 1015004005, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_PROJECT_PARENT', '015_MST', 'MODULE', 'Project Parent', '/master/project-parent', 'pi pi-fw pi-sign-in', 1015004006, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_PROVINCE', '015_MST', 'MODULE', 'Province', '/master/province', 'pi pi-fw pi-sign-in', 1015004007, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_PURCHASE_DESTINATION', '015_MST', 'MODULE', 'Purchase Destination', '/master/purchase-destination', 'pi pi-fw pi-sign-in', 1015004008, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_RACK', '015_MST', 'MODULE', 'Rack', '/master/rack', 'pi pi-fw pi-sign-in', 1015005001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_RELIGION', '015_MST', 'MODULE', 'Religion', '/master/religion', 'pi pi-fw pi-sign-in', 1015005002, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_SALES_PERSON', '015_MST', 'MODULE', 'Sales Person', '/master/sales-person', 'pi pi-fw pi-sign-in', 1015005003, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_SALES_PRICE_LIST', '015_MST', 'MODULE', 'Sales Price List', '/master/sales-price-list', 'pi pi-fw pi-sign-in', 1015005004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_SALES_PRICE_LIST_APPROVAL', '015_MST', 'MODULE', 'Sales Price List Approval', '/master/sales-price-list-approval', 'pi pi-fw pi-sign-in', 1015005005, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_UNIT_OF_MEASURE', '015_MST', 'MODULE', 'Unit Of Measure', '/master/unit-of-measure', 'pi pi-fw pi-sign-in', 1015005006, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_VENDOR', '015_MST', 'MODULE', 'Vendor', '/master/vendor', 'pi pi-fw pi-sign-in', 1015006001, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_VENDOR_CATEGORY', '015_MST', 'MODULE', 'Vendor Category', '/master/vendor-category', 'pi pi-fw pi-sign-in', 1015006002, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_VENDOR_DEPOSIT_TYPE', '015_MST', 'MODULE', 'Vendor Deposit Type', '/master/vendor-deposit-type', 'pi pi-fw pi-sign-in', 1015006003, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_VENDOR_JN_CONTACT', '015_MST', 'MODULE', 'Vendor Jn Contact', '/master/vendor-jn-contact', 'pi pi-fw pi-sign-in', 1015006004, 1, 1);
INSERT INTO `scr_menu` VALUES ('015_MST_WAREHOUSE', '015_MST', 'MODULE', 'Warehouse', '/master/warehouse', 'pi pi-fw pi-sign-in', 1015006005, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR', NULL, 'GROUP', 'Security And Utility', NULL, 'pi pi-fw pi-shield', 1017000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_BARCODE', '017_SCR', 'MODULE', 'Barcode', '/setting/barcode', 'pi pi-fw pi-sign-in', 1017000003, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_BRANCH', '017_SCR', 'MODULE', 'Branch', '/setting/branch', 'pi pi-fw pi-sign-in', 1017000001, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_DATA_PROTECT', '017_SCR', 'MODULE', 'Data Protection', '/setting/data-protect', 'pi pi-fw pi-sign-in', 1017000004, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_DATA_PROTECTION', '017_SCR', 'MODULE', 'Data Protection', '/setting/data-protection', 'pi pi-fw pi-sign-in', 1017001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_DIVISION', '017_SCR', 'MODULE', 'Division', '/setting/division', 'pi pi-fw pi-sign-in', 1017000002, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_ROLE', '017_SCR', 'MODULE', 'Role', '/setting/role', 'pi pi-fw pi-sign-in', 1017000005, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_ROLE_AUTHORIZATION', '017_SCR', 'MODULE', 'Role Authorization', '/setting/role-authorization', 'pi pi-fw pi-sign-in', 1017000006, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_TRANSACTION_LOG', '017_SCR', 'MODULE', 'Transaction Log', '/transaction-log', 'pi pi-fw pi-sign-in', 1017001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('017_SCR_USER', '017_SCR', 'MODULE', 'User', '/setting/user', 'pi pi-fw pi-sign-in', 1017000007, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AP_AGING', '020_RPT_FIN', 'MODULE', 'AP Aging', '/reports/finance/ap-aging', 'pi pi-fw pi-sign-in', 1020004010, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AP_AGING_PER_VENDOR', '020_RPT_FIN', 'MODULE', 'AP Aging per Vendor', '/reports/finance/ap-aging-per-vendor', 'pi pi-fw pi-sign-in', 1020004011, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AP_BALANCE', '020_RPT_FIN', 'MODULE', 'AP Balance', '/reports/finance/ap-balance', 'pi pi-fw pi-sign-in', 1020004012, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AP_BALANCE_PER_VENDOR', '020_RPT_FIN', 'MODULE', 'AP Balance per Vendor', '/reports/finance/ap-balance-per-vendor', 'pi pi-fw pi-sign-in', 1020004013, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AP_PAYMENT', '020_RPT_FIN', 'MODULE', 'AP Payment', '/reports/finance/ap-payment', 'pi pi-fw pi-sign-in', 1020004014, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AP_PAYMENT_PER_VENDOR', '020_RPT_FIN', 'MODULE', 'AP Payment per Vendor', '/reports/finance/ap-payment-per-vendor', 'pi pi-fw pi-sign-in', 1020004015, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_AGING', '020_RPT_FIN', 'MODULE', 'AR Aging', '/reports/finance/ar-aging', 'pi pi-fw pi-sign-in', 1020004001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_AGING_PER_CUSTOMER', '020_RPT_FIN', 'MODULE', 'AR Aging per Customer', '/reports/finance/ar-aging-per-customer', 'pi pi-fw pi-sign-in', 1020004002, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_AGING_PER_SALES_PERSON', '020_RPT_FIN', 'MODULE', 'AR Aging per Sales Person', '/reports/finance/ar-aging-per-sales-person', 'pi pi-fw pi-sign-in', 1020004003, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_BALANCE', '020_RPT_FIN', 'MODULE', 'AR Balance', '/reports/finance/ar-balance', 'pi pi-fw pi-sign-in', 1020004004, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_BALANCE_PER_CUSTOMER', '020_RPT_FIN', 'MODULE', 'AR Balance per Customer', '/reports/finance/ar-balance-per-customer', 'pi pi-fw pi-sign-in', 1020004005, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_BALANCE_PER_SALES_PERSON', '020_RPT_FIN', 'MODULE', 'AR Balance per Sales Person', '/reports/finance/ar-balance-per-sales-person', 'pi pi-fw pi-sign-in', 1020004006, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_RECEIVED', '020_RPT_FIN', 'MODULE', 'AR Received', '/reports/finance/ar-received', 'pi pi-fw pi-sign-in', 1020004007, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_RECEIVED_PER_CUSTOMER', '020_RPT_FIN', 'MODULE', 'AR Received per Customer', '/reports/finance/ar-received-per-customer', 'pi pi-fw pi-sign-in', 1020004008, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_AR_RECEIVED_PER_SALES_PERSON', '020_RPT_FIN', 'MODULE', 'AR Received per Sales Person', '/reports/finance/ar-received-per-sales-person', 'pi pi-fw pi-sign-in', 1020004009, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_ASSEMBLY_JOB_ORDER', '020_RPT_WMS', 'MODULE', 'Assembly Job Order', '/reports/inventory/assembly-job-order', 'pi pi-fw pi-sign-in', 1020003015, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_ASSEMBLY_REALIZATION', '020_RPT_WMS', 'MODULE', 'Assembly Realization', '/reports/inventory/assembly-realization', 'pi pi-fw pi-sign-in', 1020003016, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_ASSEMBLY_REALIZATION_PER_ITEM', '020_RPT_WMS', 'MODULE', 'Assembly Realization Per Item', '/reports/inventory/assembly-realization-per-item', 'pi pi-fw pi-sign-in', 1020003017, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BALANCE_SHEET', '020_RPT_ACC', 'MODULE', 'Balance Sheet', '/reports/accounting/balance-sheet', 'pi pi-fw pi-sign-in', 1020005004, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_MUTATION', '020_RPT_FIN', 'MODULE', 'Bank Mutation', '/reports/finance/bank-mutation', 'pi pi-fw pi-sign-in', 1020004028, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_PAYMENT', '020_RPT_FIN', 'MODULE', 'Bank Payment', '/reports/finance/bank-payment', 'pi pi-fw pi-sign-in', 1020004025, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_PAYMENT_PER_BANK_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Bank Payment per Bank Account', '/reports/finance/bank-payment-per-bank-account', 'pi pi-fw pi-sign-in', 1020004026, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Bank Payment per Chart Of Account', '/reports/finance/bank-payment-per-chart-of-account', 'pi pi-fw pi-sign-in', 1020004027, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE', '020_RPT_FIN', 'MODULE', 'Bank Payment Recapitulation per periode', '/reports/finance/bank-payment-recapitulation-per-periode', 'pi pi-fw pi-sign-in', 1020004044, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE', '020_RPT_FIN', 'MODULE', 'Bank Received Recapitulation per periode', '/reports/finance/bank-received-recapitulation-per-periode', 'pi pi-fw pi-sign-in', 1020004045, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_RECEIVING', '020_RPT_FIN', 'MODULE', 'Bank Receiving', '/reports/finance/bank-receiving', 'pi pi-fw pi-sign-in', 1020004019, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_RECEIVING_PER_BANK_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Bank Receiving per Bank Account', '/reports/finance/bank-receiving-per-bank-account', 'pi pi-fw pi-sign-in', 1020004020, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Bank Receiving per Chart Of Account', '/reports/finance/bank-receiving-per-chart-of-account', 'pi pi-fw pi-sign-in', 1020004021, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_MUTATION', '020_RPT_FIN', 'MODULE', 'Cash Mutation', '/reports/finance/cash-mutation', 'pi pi-fw pi-sign-in', 1020004029, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_PAYMENT', '020_RPT_FIN', 'MODULE', 'Cash Payment', '/reports/finance/cash-payment', 'pi pi-fw pi-sign-in', 1020004022, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_PAYMENT_PER_CASH_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Cash Payment per Cash Account', '/reports/finance/cash-payment-per-cash-account', 'pi pi-fw pi-sign-in', 1020004023, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Cash Payment per Chart Of Account', '/reports/finance/cash-payment-per-chart-of-account', 'pi pi-fw pi-sign-in', 1020004024, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE', '020_RPT_FIN', 'MODULE', 'Cash Payment Recapitulation per periode', '/reports/finance/cash-payment-recapitulation-per-periode', 'pi pi-fw pi-sign-in', 1020004042, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE', '020_RPT_FIN', 'MODULE', 'Cash Received Recapitulation per periode', '/reports/finance/cash-received-recapitulation-per-periode', 'pi pi-fw pi-sign-in', 1020004043, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_RECEIVING', '020_RPT_FIN', 'MODULE', 'Cash Receiving', '/reports/finance/cash-receiving', 'pi pi-fw pi-sign-in', 1020004016, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_RECEIVING_PER_CASH_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Cash Receiving per Cash Account', '/reports/finance/cash-receiving-per-cash-account', 'pi pi-fw pi-sign-in', 1020004017, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT', '020_RPT_FIN', 'MODULE', 'Cash Receiving per Chart Of Account', '/reports/finance/cash-receiving-per-chart-of-account', 'pi pi-fw pi-sign-in', 1020004018, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_CREDIT_NOTE', '020_RPT_SAL', 'MODULE', 'Customer Credit Note', '/reports/sales/customer-credit-note', 'pi pi-fw pi-sign-in', 1020002025, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER', '020_RPT_SAL', 'MODULE', 'Customer Credit Note Per Customer', '/reports/sales/customer-credit-note-per-customer', 'pi pi-fw pi-sign-in', 1020002026, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_DEBIT_NOTE', '020_RPT_SAL', 'MODULE', 'Customer Debit Note', '/reports/sales/customer-debit-note', 'pi pi-fw pi-sign-in', 1020002027, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER', '020_RPT_SAL', 'MODULE', 'Customer Debit Note Per Customer', '/reports/sales/customer-debit-note-per-customer', 'pi pi-fw pi-sign-in', 1020002028, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_DEPOSIT', '020_RPT_FIN', 'MODULE', 'Customer Deposit', '/reports/finance/customer-deposit', 'pi pi-fw pi-sign-in', 1020004050, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_DEPOSIT_OUTSTANDING', '020_RPT_FIN', 'MODULE', 'Customer Deposit Outstanding', '/reports/finance/customer-deposit-outstanding', 'pi pi-fw pi-sign-in', 1020004051, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING', '020_RPT_FIN', 'MODULE', 'Customer Down Payment Outstanding', '/reports/finance/customer-down-payment-outstanding', 'pi pi-fw pi-sign-in', 1020004047, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD', '020_RPT_FIN', 'MODULE', 'Customer Down Payment Per Period', '/reports/finance/customer-down-payment-per-period', 'pi pi-fw pi-sign-in', 1020004046, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_DELIVERY_NOTE', '020_RPT_WMS', 'MODULE', 'Delivery Note', '/reports/inventory/delivery-note', 'pi pi-fw pi-sign-in', 1020003006, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_DELIVERY_NOTE_BY_ITEM', '020_RPT_WMS', 'MODULE', 'Delivery Note by Item', '/reports/inventory/delivery-note-by-item', 'pi pi-fw pi-sign-in', 1020003008, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_DELIVERY_NOTE_PER_CUSTOMER', '020_RPT_WMS', 'MODULE', 'Delivery Note per Customer', '/reports/inventory/delivery-note-per-customer', 'pi pi-fw pi-sign-in', 1020003007, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_DETAIL_STOCK_BY_INVENTORY', '020_RPT_WMS', 'MODULE', 'Detail Stock by Inventory', '/reports/inventory/detail-stock-by-inventory', 'pi pi-fw pi-sign-in', 1020003001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_DETAIL_STOCK_BY_QUANTITY', '020_RPT_WMS', 'MODULE', 'Detail Stock by Quantity', '/reports/inventory/detail-stock-by-quantity', 'pi pi-fw pi-sign-in', 1020003002, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_GENERAL_LEDGER', '020_RPT_ACC', 'MODULE', 'General Ledger', '/reports/accounting/general-ledger', 'pi pi-fw pi-sign-in', 1020005001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_GOODS_RECEIVED_NOTE', '020_RPT_WMS', 'MODULE', 'Goods Received Note', '/reports/inventory/goods-received-note', 'pi pi-fw pi-sign-in', 1020003003, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_GOODS_RECEIVED_NOTE_PER_ITEM', '020_RPT_WMS', 'MODULE', 'Goods Received Note per Item', '/reports/inventory/goods-received-note-per-item', 'pi pi-fw pi-sign-in', 1020003005, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_GOODS_RECEIVED_NOTE_PER_VENDOR', '020_RPT_WMS', 'MODULE', 'Goods Received Note per Vendor', '/reports/inventory/goods-received-note-per-vendor', 'pi pi-fw pi-sign-in', 1020003004, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_INVENTORY_IN', '020_RPT_WMS', 'MODULE', 'Inventory In', '/reports/inventory/inventory-in', 'pi pi-fw pi-sign-in', 1020003009, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_INVENTORY_IN_PER_ITEM', '020_RPT_WMS', 'MODULE', 'Inventory In per Item', '/reports/inventory/inventory-in-per-item', 'pi pi-fw pi-sign-in', 1020003011, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_INVENTORY_IN_PER_REASON', '020_RPT_WMS', 'MODULE', 'Inventory In per Reason', '/reports/inventory/inventory-in-per-reason', 'pi pi-fw pi-sign-in', 1020003010, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_INVENTORY_OUT', '020_RPT_WMS', 'MODULE', 'Inventory Out', '/reports/inventory/inventory-out', 'pi pi-fw pi-sign-in', 1020003012, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_INVENTORY_OUT_PER_ITEM', '020_RPT_WMS', 'MODULE', 'Inventory Out per Item', '/reports/inventory/inventory-out-per-item', 'pi pi-fw pi-sign-in', 1020003014, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_INVENTORY_OUT_PER_REASON', '020_RPT_WMS', 'MODULE', 'Inventory Out per Reason', '/reports/inventory/inventory-out-per-reason', 'pi pi-fw pi-sign-in', 1020003013, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_JOURNAL_RECAPITULATION', '020_RPT_ACC', 'MODULE', 'Journal Recapitulation', '/reports/accounting/journal-recapitulation', 'pi pi-fw pi-sign-in', 1020005005, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_BANK_PAYMENT_DEPOSIT', '020_RPT_FIN', 'MODULE', 'Outstanding Bank Payment Deposit', '/reports/finance/outstanding-bank-payment-deposit', 'pi pi-fw pi-sign-in', 1020004033, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_BANK_RECEIVED_DEPOSIT', '020_RPT_FIN', 'MODULE', 'Outstanding Bank Received Deposit', '/reports/finance/outstanding-bank-received-deposit', 'pi pi-fw pi-sign-in', 1020004032, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_CASH_PAYMENT_DEPOSIT', '020_RPT_FIN', 'MODULE', 'Outstanding Cash Payment Deposit', '/reports/finance/outstanding-cash-payment-deposit', 'pi pi-fw pi-sign-in', 1020004035, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_CASH_RECEIVED_DEPOSIT', '020_RPT_FIN', 'MODULE', 'Outstanding Cash Received Deposit', '/reports/finance/outstanding-cash-received-deposit', 'pi pi-fw pi-sign-in', 1020004034, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY', '020_RPT_PUR', 'MODULE', 'Outstanding Purchase Order By Quantity', '/reports/purchase/outstanding-purchase-order-by-quantity', 'pi pi-fw pi-sign-in', 1020001008, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY', '020_RPT_PUR', 'MODULE', 'Outstanding Purchase Order By Value And Quantity', '/reports/purchase/outstanding-purchase-order-by-value-and-quantity', 'pi pi-fw pi-sign-in', 1020001009, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_SALES_ORDER', '020_RPT_SAL', 'MODULE', 'Outstanding Sales Order', '/reports/sales/outstanding-sales-order', 'pi pi-fw pi-sign-in', 1020002006, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER', '020_RPT_SAL', 'MODULE', 'Outstanding Sales Order Per Customer', '/reports/sales/outstanding-sales-order-per-customer', 'pi pi-fw pi-sign-in', 1020002007, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_SALES_ORDER_PER_ITEM', '020_RPT_SAL', 'MODULE', 'Outstanding Sales Order Per Item', '/reports/sales/outstanding-sales-order-per-item', 'pi pi-fw pi-sign-in', 1020002009, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON', '020_RPT_SAL', 'MODULE', 'Outstanding Sales Order Per Sales Person', '/reports/sales/outstanding-sales-order-per-sales-person', 'pi pi-fw pi-sign-in', 1020002008, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PROFIT_AND_LOSS', '020_RPT_ACC', 'MODULE', 'Profit and Loss', '/reports/accounting/profit-and-loss', 'pi pi-fw pi-sign-in', 1020005003, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_ORDER', '020_RPT_PUR', 'MODULE', 'Purchase Order', '/reports/purchase/purchase-order', 'pi pi-fw pi-sign-in', 1020001003, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_ORDER_PER_ITEM', '020_RPT_PUR', 'MODULE', 'Purchase Order per Item', '/reports/purchase/purchase-order-per-item', 'pi pi-fw pi-sign-in', 1020001005, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_ORDER_PER_VENDOR', '020_RPT_PUR', 'MODULE', 'Purchase Order Per Vendor', '/reports/purchase/purchase-order-per-vendor', 'pi pi-fw pi-sign-in', 1020001004, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY', '020_RPT_PUR', 'MODULE', 'Purchase Order Realization By Quantity', '/reports/purchase/purchase-order-realization-by-quantity', 'pi pi-fw pi-sign-in', 1020001006, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY', '020_RPT_PUR', 'MODULE', 'Purchase Order Realization By Value And Quantity', '/reports/purchase/purchase-order-realization-by-value-and-quantity', 'pi pi-fw pi-sign-in', 1020001007, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_RECAPITULATION_PER_ITEM', '020_RPT_PUR', 'MODULE', 'Purchase Recapitulation Per Item', '/reports/purchase/purchase-recapitulation-per-item', 'pi pi-fw pi-sign-in', 1020001011, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_RECAPITULATION_PER_VENDOR', '020_RPT_PUR', 'MODULE', 'Purchase Recapitulation Per Vendor', '/reports/purchase/purchase-recapitulation-per-vendor', 'pi pi-fw pi-sign-in', 1020001010, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_REQUEST', '020_RPT_PUR', 'MODULE', 'Purchase Request', '/reports/purchase/purchase-request', 'pi pi-fw pi-sign-in', 1020001001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_REQUEST_OUTSTANDING', '020_RPT_PUR', 'MODULE', 'Purchase Request Outstanding', '/reports/purchase/purchase-request-outstanding', 'pi pi-fw pi-sign-in', 1020001002, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_RETURN', '020_RPT_PUR', 'MODULE', 'Purchase Return', '/reports/purchase/purchase-return', 'pi pi-fw pi-sign-in', 1020001021, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_RETURN_PER_ITEM', '020_RPT_PUR', 'MODULE', 'Purchase Return per Item', '/reports/purchase/purchase-return-per-item', 'pi pi-fw pi-sign-in', 1020001023, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_PURCHASE_RETURN_PER_VENDOR', '020_RPT_PUR', 'MODULE', 'Purchase Return per Vendor', '/reports/purchase/purchase-return-per-vendor', 'pi pi-fw pi-sign-in', 1020001022, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_REPORT', NULL, 'GROUP', 'Report', NULL, 'pi pi-fw pi-print', 1020000000, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_RPT_ACC', '020_REPORT', 'SUB', 'Report Accounting', '/reports/accounting', 'pi pi-fw pi-list', 1020005000, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_RPT_FIN', '020_REPORT', 'SUB', 'Report Finance', '/reports/finance', 'pi pi-fw pi-list', 1020004000, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_RPT_PUR', '020_REPORT', 'SUB', 'Report Purchase', NULL, 'pi pi-fw pi-list', 1020001000, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_RPT_SAL', '020_REPORT', 'SUB', 'Report Sales', '/reports/sales', 'pi pi-fw pi-list', 1020002000, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_RPT_WMS', '020_REPORT', 'SUB', 'Report WMS', '/reports/wms', 'pi pi-fw pi-list', 1020003000, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ABC_ANALYSIS', '020_RPT_SAL', 'MODULE', 'Abc Analysis', '/reports/sales/abc-analysis', 'pi pi-fw pi-sign-in', 1020002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ABC_ANALYSIS_PER_CATEGORY', '020_RPT_SAL', 'MODULE', 'Abc Analysis Per Category', '/reports/sales/abc-analysis-per-category', 'pi pi-fw pi-sign-in', 1020002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY', '020_RPT_SAL', 'MODULE', 'Abc Analysis Per Sub Category', '/reports/sales/abc-analysis-per-sub-category', 'pi pi-fw pi-sign-in', 1020002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_BY_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales By Cogs Idr', '/reports/sales/sales-by-cogs-idr', 'pi pi-fw pi-sign-in', 1020002013, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_INVOICE_RECAPITULATION', '020_RPT_SAL', 'MODULE', 'Sales Invoice Recapitulation', '/reports/sales/sales-invoice-recapitulation', 'pi pi-fw pi-sign-in', 1020002021, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_NON_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales Non Cogs Idr', '/reports/sales/sales-non-cogs-idr', 'pi pi-fw pi-sign-in', 1020002010, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ORDER', '020_RPT_SAL', 'MODULE', 'Sales Order', '/reports/sales/sales-order', 'pi pi-fw pi-sign-in', 1020002001, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ORDER_PER_CUSTOMER', '020_RPT_SAL', 'MODULE', 'Sales Order Per Customer', '/reports/sales/sales-order-per-customer', 'pi pi-fw pi-sign-in', 1020002002, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ORDER_PER_ITEM', '020_RPT_SAL', 'MODULE', 'Sales Order Per Item', '/reports/sales/sales-order-per-item', 'pi pi-fw pi-sign-in', 1020002004, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ORDER_PER_SALES_PERSON', '020_RPT_SAL', 'MODULE', 'Sales Order Per Sales Person', '/reports/sales/sales-order-per-sales-person', 'pi pi-fw pi-sign-in', 1020002003, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_ORDER_REALIZATION', '020_RPT_SAL', 'MODULE', 'Sales Order Realization', '/reports/sales/sales-order-realization', 'pi pi-fw pi-sign-in', 1020002005, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_PER_CUSTOMER_BY_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales Per Customer By Cogs Idr', '/reports/sales/sales-per-customer-by-cogs-idr', 'pi pi-fw pi-sign-in', 1020002014, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_PER_CUSTOMER_NON_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales Per Customer Non Cogs Idr', '/reports/sales/sales-per-customer-non-cogs-idr', 'pi pi-fw pi-sign-in', 1020002011, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_PER_ITEM_BY_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales Per Item By Cogs Idr', '/reports/sales/sales-per-item-by-cogs-idr', 'pi pi-fw pi-sign-in', 1020002016, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_PER_SALES_PERSON_BY_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales Per Sales Person By Cogs Idr', '/reports/sales/sales-per-sales-person-by-cogs-idr', 'pi pi-fw pi-sign-in', 1020002015, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_PER_SALES_PERSON_NON_COGS_IDR', '020_RPT_SAL', 'MODULE', 'Sales Per Sales Person Non Cogs Idr', '/reports/sales/sales-per-sales-person-non-cogs-idr', 'pi pi-fw pi-sign-in', 1020002012, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RECAPITULATION_PER_CUSTOMER', '020_RPT_SAL', 'MODULE', 'Sales Recapitulation Per Customer', '/reports/sales/sales-recapitulation-per-customer', 'pi pi-fw pi-sign-in', 1020002022, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RECAPITULATION_PER_ITEM', '020_RPT_SAL', 'MODULE', 'Sales Recapitulation Per Item', '/reports/sales/sales-recapitulation-per-item', 'pi pi-fw pi-sign-in', 1020002024, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RECAPITULATION_PER_SALES_PERSON', '020_RPT_SAL', 'MODULE', 'Sales Recapitulation Per Sales Person', '/reports/sales/sales-recapitulation-per-sales-person', 'pi pi-fw pi-sign-in', 1020002023, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RETURN', '020_RPT_SAL', 'MODULE', 'Sales Return', '/reports/sales/sales-return', 'pi pi-fw pi-sign-in', 1020002017, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RETURN_PER_CUSTOMER', '020_RPT_SAL', 'MODULE', 'Sales Return Per Customer', '/reports/sales/sales-return-per-customer', 'pi pi-fw pi-sign-in', 1020002018, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RETURN_PER_ITEM', '020_RPT_SAL', 'MODULE', 'Sales Return Per Item', '/reports/sales/sales-return-per-item', 'pi pi-fw pi-sign-in', 1020002020, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_SALES_RETURN_PER_SALES_PERSON', '020_RPT_SAL', 'MODULE', 'Sales Return Per Sales Person', '/reports/sales/sales-return-per-sales-person', 'pi pi-fw pi-sign-in', 1020002019, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_STOCK_CARD_COGS_IDR_GLOBAL', '020_RPT_WMS', 'MODULE', 'Stock Card COGS IDR Global', '/reports/inventory/stock-card-cogs-idr-global', 'pi pi-fw pi-sign-in', 1020003022, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_STOCK_CARD_MINUS_QUANTITY', '020_RPT_WMS', 'MODULE', 'Stock Card Minus Quantity', '/reports/inventory/stock-card-minus-quantity', 'pi pi-fw pi-sign-in', 1020003020, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_STOCK_CARD_QUANTITY_PER_WAREHOUSE', '020_RPT_WMS', 'MODULE', 'Stock Card Quantity Per Warehouse', '/reports/inventory/stock-card-quantity-per-warehouse', 'pi pi-fw pi-sign-in', 1020003021, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_STOCK_HISTORY_DETAIL', '020_RPT_WMS', 'MODULE', 'Stock History Detail', '/reports/inventory/stock-history-detail', 'pi pi-fw pi-sign-in', 1020003019, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_STOCK_HISTORY_GLOBAL', '020_RPT_WMS', 'MODULE', 'Stock History Global', '/reports/inventory/stock-history-global', 'pi pi-fw pi-sign-in', 1020003018, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_TRIAL_BALANCE', '020_RPT_ACC', 'MODULE', 'Trial Balance', '/reports/accounting/trial-balance', 'pi pi-fw pi-sign-in', 1020005002, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGNED_BANK_PAYMENT_DEPOSIT', '020_RPT_FIN', 'MODULE', 'UnAssigned Bank Payment Deposit', '/reports/finance/unassigned-bank-payment-deposit', 'pi pi-fw pi-sign-in', 1020004037, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGNED_BANK_RECEIVED_DEPOSIT', '020_RPT_FIN', 'MODULE', 'UnAssigned Bank Received Deposit', '/reports/finance/unassigned-bank-received-deposit', 'pi pi-fw pi-sign-in', 1020004036, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGNED_CASH_PAYMENT_DEPOSIT', '020_RPT_FIN', 'MODULE', 'UnAssigned Cash Payment Deposit', '/reports/finance/unassigned-cash-payment-deposit', 'pi pi-fw pi-sign-in', 1020004039, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGNED_CASH_RECEIVED_DEPOSIT', '020_RPT_FIN', 'MODULE', 'UnAssigned Cash Received Deposit', '/reports/finance/unassigned-cash-received-deposit', 'pi pi-fw pi-sign-in', 1020004038, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE', '020_RPT_FIN', 'MODULE', 'UnAssign Delivery Note By Customer Invoice', '/reports/finance/unassign-delivery-note-by-customer-invoice', 'pi pi-fw pi-sign-in', 1020004031, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE', '020_RPT_FIN', 'MODULE', 'UnAssign Goods Received Note By Vendor Invoice', '/reports/finance/unassign-goods-received-note-by-vendor-invoice', 'pi pi-fw pi-sign-in', 1020004030, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT', '020_RPT_FIN', 'MODULE', 'UnAssign Payment Request By Payment', '/reports/finance/unassign-payment-request-by-payment', 'pi pi-fw pi-sign-in', 1020004041, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE', '020_RPT_PUR', 'MODULE', 'UnAssign Purchase Order By Goods Received Note', '/reports/purchase/unassign-purchase-order-by-goods-received-note', 'pi pi-fw pi-sign-in', 1020001025, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER', '020_RPT_PUR', 'MODULE', 'UnAssign Purchase Request By Purchase Order', '/reports/purchase/unassign-purchase-request-by-purchase-order', 'pi pi-fw pi-sign-in', 1020001024, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST', '020_RPT_FIN', 'MODULE', 'UnAssign Vendor Invoice By Payment Request', '/reports/finance/unassign-vendor-invoice-by-payment-request', 'pi pi-fw pi-sign-in', 1020004040, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VAT_IN_RECAPITULATION', '020_RPT_ACC', 'MODULE', 'VAT In Recapitulation', '/reports/accounting/vat-in-recapitulation', 'pi pi-fw pi-sign-in', 1020005006, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VAT_OUT_RECAPITULATION', '020_RPT_ACC', 'MODULE', 'VAT Out Recapitulation', '/reports/accounting/vat-out-recapitulation', 'pi pi-fw pi-sign-in', 1020005007, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_CREDIT_NOTE', '020_RPT_PUR', 'MODULE', 'Vendor Credit Note', '/reports/purchase/vendor-credit-note', 'pi pi-fw pi-sign-in', 1020001017, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_CREDIT_NOTE_PER_VENDOR', '020_RPT_PUR', 'MODULE', 'Vendor Credit Note per Vendor', '/reports/purchase/vendor-credit-note-per-vendor', 'pi pi-fw pi-sign-in', 1020001018, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_DEBIT_NOTE_', '020_RPT_PUR', 'MODULE', 'Vendor Debit Note ', '/reports/purchase/vendor-debit-note-', 'pi pi-fw pi-sign-in', 1020001019, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_DEBIT_NOTE_PER_VENDOR', '020_RPT_PUR', 'MODULE', 'Vendor Debit Note per Vendor', '/reports/purchase/vendor-debit-note-per-vendor', 'pi pi-fw pi-sign-in', 1020001020, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_DEPOSIT', '020_RPT_FIN', 'MODULE', 'Vendor Deposit', '/reports/finance/vendor-deposit', 'pi pi-fw pi-sign-in', 1020004052, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_DEPOSIT_OUTSTANDING', '020_RPT_FIN', 'MODULE', 'Vendor Deposit Outstanding', '/reports/finance/vendor-deposit-outstanding', 'pi pi-fw pi-sign-in', 1020004053, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_DOWN_PAYMENT_OUTSTANDING', '020_RPT_FIN', 'MODULE', 'Vendor Down Payment Outstanding', '/reports/finance/vendor-down-payment-outstanding', 'pi pi-fw pi-sign-in', 1020004049, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_DOWN_PAYMENT_PER_PERIOD', '020_RPT_FIN', 'MODULE', 'Vendor Down Payment Per Period', '/reports/finance/vendor-down-payment-per-period', 'pi pi-fw pi-sign-in', 1020004048, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_INVOICE', '020_RPT_PUR', 'MODULE', 'Vendor Invoice', '/reports/purchase/vendor-invoice', 'pi pi-fw pi-sign-in', 1020001012, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_INVOICE_PER_ITEM', '020_RPT_PUR', 'MODULE', 'Vendor Invoice Per Item', '/reports/purchase/vendor-invoice-per-item', 'pi pi-fw pi-sign-in', 1020001016, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_VENDOR_INVOICE_PER_VENDOR', '020_RPT_PUR', 'MODULE', 'Vendor Invoice Per Vendor', '/reports/purchase/vendor-invoice-per-vendor', 'pi pi-fw pi-sign-in', 1020001015, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_WAREHOUSE_TRANSFER_IN', '020_RPT_WMS', 'MODULE', 'Warehouse Transfer In', '/reports/inventory/warehouse-transfer-in', 'pi pi-fw pi-sign-in', 1020003024, 1, 1);
INSERT INTO `scr_menu` VALUES ('020_WAREHOUSE_TRANSFER_OUT', '020_RPT_WMS', 'MODULE', 'Warehouse Transfer Out', '/reports/inventory/warehouse-transfer-out', 'pi pi-fw pi-sign-in', 1020003023, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for scr_menu_branch
-- ----------------------------
DROP TABLE IF EXISTS `scr_menu_branch`;
CREATE TABLE `scr_menu_branch` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `activeStatus` bit(1) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `menuCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of scr_menu_branch
-- ----------------------------
BEGIN;
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_000_DASHBOARD', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_000_DASHBOARD', '000_DASHBOARD');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR', '001_PUR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_HISTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_HISTORY', '001_PUR_PURCHASE_HISTORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_ORDER', '001_PUR_PURCHASE_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_ORDER_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_ORDER_APPROVAL', '001_PUR_PURCHASE_ORDER_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_ORDER_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_ORDER_CLOSING', '001_PUR_PURCHASE_ORDER_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_REQUEST', '001_PUR_PURCHASE_REQUEST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_REQUEST_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_REQUEST_APPROVAL', '001_PUR_PURCHASE_REQUEST_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_REQUEST_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_REQUEST_CLOSING', '001_PUR_PURCHASE_REQUEST_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_001_PUR_PURCHASE_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_001_PUR_PURCHASE_RETURN', '001_PUR_PURCHASE_RETURN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL', '002_SAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_CUSTOMER_INVOICE_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_CUSTOMER_INVOICE_SALES_ORDER', '002_SAL_CUSTOMER_INVOICE_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_ORDER_MATERIAL_FORM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_ORDER_MATERIAL_FORM', '002_SAL_ORDER_MATERIAL_FORM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_ORDER_MATERIAL_FORM_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_ORDER_MATERIAL_FORM_CLOSING', '002_SAL_ORDER_MATERIAL_FORM_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_ORDER_MATERIAL_FORM_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_ORDER_MATERIAL_FORM_RETURN', '002_SAL_ORDER_MATERIAL_FORM_RETURN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_PICKING_LIST_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_PICKING_LIST_SALES_ORDER', '002_SAL_PICKING_LIST_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION', '002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE', '002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE', '002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_SALES_HISTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_SALES_HISTORY', '002_SAL_SALES_HISTORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_SALES_ORDER', '002_SAL_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_SALES_ORDER_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_SALES_ORDER_APPROVAL', '002_SAL_SALES_ORDER_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_SALES_ORDER_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_SALES_ORDER_CLOSING', '002_SAL_SALES_ORDER_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_SALES_RETURN_BY_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_SALES_RETURN_BY_INVOICE', '002_SAL_SALES_RETURN_BY_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER', '002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER', '002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT', '003_IVT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_ADJUSTMENT_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_ADJUSTMENT_IN', '003_IVT_ADJUSTMENT_IN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_ADJUSTMENT_IN_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_ADJUSTMENT_IN_APPROVAL', '003_IVT_ADJUSTMENT_IN_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_ADJUSTMENT_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_ADJUSTMENT_OUT', '003_IVT_ADJUSTMENT_OUT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_ADJUSTMENT_OUT_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_ADJUSTMENT_OUT_APPROVAL', '003_IVT_ADJUSTMENT_OUT_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_ASSEMBLY_JOB_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_ASSEMBLY_JOB_ORDER', '003_IVT_ASSEMBLY_JOB_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_ASSEMBLY_REALIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_ASSEMBLY_REALIZATION', '003_IVT_ASSEMBLY_REALIZATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_CURRENT_STOCK_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_CURRENT_STOCK_QUANTITY', '003_IVT_CURRENT_STOCK_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_DELIVERY_NOTE_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_DELIVERY_NOTE_SALES_ORDER', '003_IVT_DELIVERY_NOTE_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_DOCK_STOCK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_DOCK_STOCK', '003_IVT_DOCK_STOCK');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_GOODS_RECEIVED_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_GOODS_RECEIVED_NOTE', '003_IVT_GOODS_RECEIVED_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_STOCK_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_STOCK_CLOSING', '003_IVT_STOCK_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_STOCK_OPNAME_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_STOCK_OPNAME_APPROVAL', '003_IVT_STOCK_OPNAME_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_STOCK_OPNAME_DRAFT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_STOCK_OPNAME_DRAFT', '003_IVT_STOCK_OPNAME_DRAFT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_STOCK_OPNAME_POSTING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_STOCK_OPNAME_POSTING', '003_IVT_STOCK_OPNAME_POSTING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE', '003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_WAREHOUSE_TRANSFER_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_WAREHOUSE_TRANSFER_IN', '003_IVT_WAREHOUSE_TRANSFER_IN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_003_IVT_WAREHOUSE_TRANSFER_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_003_IVT_WAREHOUSE_TRANSFER_OUT', '003_IVT_WAREHOUSE_TRANSFER_OUT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN', '004_FIN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_AP', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_AP', '004_FIN_AP');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_AR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_AR', '004_FIN_AR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_BANK_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_BANK_PAYMENT', '004_FIN_BANK_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_BANK_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_BANK_RECEIVED', '004_FIN_BANK_RECEIVED');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_CASH_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_CASH_PAYMENT', '004_FIN_CASH_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_CASH_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_CASH_RECEIVED', '004_FIN_CASH_RECEIVED');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_CUSTOMER_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_CUSTOMER_CREDIT_NOTE', '004_FIN_CUSTOMER_CREDIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_CUSTOMER_DEBIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_CUSTOMER_DEBIT_NOTE', '004_FIN_CUSTOMER_DEBIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT', '004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_CUSTOMER_DOWN_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_CUSTOMER_DOWN_PAYMENT', '004_FIN_CUSTOMER_DOWN_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_DLN_CHECKLIST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_DLN_CHECKLIST', '004_FIN_DLN_CHECKLIST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_FINANCE_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_FINANCE_CLOSING', '004_FIN_FINANCE_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_FINANCE_RECALCULATING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_FINANCE_RECALCULATING', '004_FIN_FINANCE_RECALCULATING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GENERAL_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GENERAL_JOURNAL', '004_FIN_GENERAL_JOURNAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE', '004_FIN_GIRO_HAND_OVER_TO_FINANCE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL', '004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_PAYMENT', '004_FIN_GIRO_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_PAYMENT_INQUIRY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_PAYMENT_INQUIRY', '004_FIN_GIRO_PAYMENT_INQUIRY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_PAYMENT_REJECTED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_PAYMENT_REJECTED', '004_FIN_GIRO_PAYMENT_REJECTED');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_RECEIVED', '004_FIN_GIRO_RECEIVED');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_RECEIVED_INQUIRY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_RECEIVED_INQUIRY', '004_FIN_GIRO_RECEIVED_INQUIRY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GIRO_RECEIVED_REJECT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GIRO_RECEIVED_REJECT', '004_FIN_GIRO_RECEIVED_REJECT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_GOODS_RECEIVED_NOTE_VIEW', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_GOODS_RECEIVED_NOTE_VIEW', '004_FIN_GOODS_RECEIVED_NOTE_VIEW');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER', '004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE', '004_FIN_INVOICE_HAND_OVER_TO_FINANCE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL', '004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_OTHER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_OTHER', '004_FIN_OTHER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_PAYMENT_HISTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_PAYMENT_HISTORY', '004_FIN_PAYMENT_HISTORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_PAYMENT_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_PAYMENT_REQUEST', '004_FIN_PAYMENT_REQUEST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_PAYMENT_REQUEST_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_PAYMENT_REQUEST_APPROVAL', '004_FIN_PAYMENT_REQUEST_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION', '004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_VENDOR_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_VENDOR_CREDIT_NOTE', '004_FIN_VENDOR_CREDIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_VENDOR_DEBIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_VENDOR_DEBIT_NOTE', '004_FIN_VENDOR_DEBIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_VENDOR_DEPOSIT_ASSIGNMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_VENDOR_DEPOSIT_ASSIGNMENT', '004_FIN_VENDOR_DEPOSIT_ASSIGNMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_VENDOR_DOWN_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_VENDOR_DOWN_PAYMENT', '004_FIN_VENDOR_DOWN_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_VENDOR_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_VENDOR_INVOICE', '004_FIN_VENDOR_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION', '004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC', '005_ACC');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_ACCOUNTING_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_ACCOUNTING_CLOSING', '005_ACC_ACCOUNTING_CLOSING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_CURRENT_STOCK_COGSIDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_CURRENT_STOCK_COGSIDR', '005_ACC_CURRENT_STOCK_COGSIDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_E-FAK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_E-FAK', '005_ACC_E-FAK');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_JOURNAL_POSTING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_JOURNAL_POSTING', '005_ACC_JOURNAL_POSTING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_JOURNAL_QUERY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_JOURNAL_QUERY', '005_ACC_JOURNAL_QUERY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_KELUARAN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_KELUARAN', '005_ACC_KELUARAN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_MASUKAN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_MASUKAN', '005_ACC_MASUKAN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_TAX_INVOICE_ASSIGNMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_TAX_INVOICE_ASSIGNMENT', '005_ACC_TAX_INVOICE_ASSIGNMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_TAX_INVOICE_REGISTRATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_TAX_INVOICE_REGISTRATION', '005_ACC_TAX_INVOICE_REGISTRATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_TRANS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_TRANS', '005_ACC_TRANS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_UN_BALANCE_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_UN_BALANCE_JOURNAL', '005_ACC_UN_BALANCE_JOURNAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_UN_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_UN_JOURNAL', '005_ACC_UN_JOURNAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_UNBALANCE_COGS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_UNBALANCE_COGS', '005_ACC_UNBALANCE_COGS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_UPDATE_TAX_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_UPDATE_TAX_INVOICE', '005_ACC_UPDATE_TAX_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_VAT_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_VAT_IN', '005_ACC_VAT_IN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_VAT_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_VAT_OUT', '005_ACC_VAT_OUT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_005_ACC_VOID_TAX_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_005_ACC_VOID_TAX_INVOICE', '005_ACC_VOID_TAX_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST', '015_MST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_APPROVAL_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_APPROVAL_REASON', '015_MST_APPROVAL_REASON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_BANK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_BANK', '015_MST_BANK');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_BANK_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_BANK_ACCOUNT', '015_MST_BANK_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_BILL_OF_MATERIAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_BILL_OF_MATERIAL', '015_MST_BILL_OF_MATERIAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_BRANCH', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_BRANCH', '015_MST_BRANCH');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_BUSINESS_ENTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_BUSINESS_ENTITY', '015_MST_BUSINESS_ENTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CASH_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CASH_ACCOUNT', '015_MST_CASH_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CHART_OF_ACCOUNT', '015_MST_CHART_OF_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CITY', '015_MST_CITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_COLOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_COLOR', '015_MST_COLOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_COUNTRY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_COUNTRY', '015_MST_COUNTRY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CURRENCY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CURRENCY', '015_MST_CURRENCY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CURRENT_PRICE_LIST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CURRENT_PRICE_LIST', '015_MST_CURRENT_PRICE_LIST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CUSTOMER', '015_MST_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CUSTOMER_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CUSTOMER_CATEGORY', '015_MST_CUSTOMER_CATEGORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CUSTOMER_DEPOSIT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CUSTOMER_DEPOSIT_TYPE', '015_MST_CUSTOMER_DEPOSIT_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CUSTOMER_JN_ADDRESS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CUSTOMER_JN_ADDRESS', '015_MST_CUSTOMER_JN_ADDRESS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_CUSTOMER_JN_CONTACT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_CUSTOMER_JN_CONTACT', '015_MST_CUSTOMER_JN_CONTACT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_DEPARTMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_DEPARTMENT', '015_MST_DEPARTMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_DISCOUNT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_DISCOUNT_TYPE', '015_MST_DISCOUNT_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_DISTRIBUTION_CHANNEL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_DISTRIBUTION_CHANNEL', '015_MST_DISTRIBUTION_CHANNEL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_DIVISION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_DIVISION', '015_MST_DIVISION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_DOCUMENT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_DOCUMENT_TYPE', '015_MST_DOCUMENT_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_EDUCATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_EDUCATION', '015_MST_EDUCATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_EMPLOYEE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_EMPLOYEE', '015_MST_EMPLOYEE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_EMPLOYEE_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_EMPLOYEE_TYPE', '015_MST_EMPLOYEE_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_EXCHANGE_RATE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_EXCHANGE_RATE', '015_MST_EXCHANGE_RATE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_INVENTORY_ADJ_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_INVENTORY_ADJ_REASON', '015_MST_INVENTORY_ADJ_REASON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ISLAND', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ISLAND', '015_MST_ISLAND');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM', '015_MST_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM_BRAND', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM_BRAND', '015_MST_ITEM_BRAND');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM_CATEGORY', '015_MST_ITEM_CATEGORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM_CLASSIFICATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM_CLASSIFICATION', '015_MST_ITEM_CLASSIFICATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM_DIVISION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM_DIVISION', '015_MST_ITEM_DIVISION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM_JN_COGSIDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM_JN_COGSIDR', '015_MST_ITEM_JN_COGSIDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ITEM_SUB_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ITEM_SUB_CATEGORY', '015_MST_ITEM_SUB_CATEGORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_JOB_POSITION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_JOB_POSITION', '015_MST_JOB_POSITION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_JOURNAL', '015_MST_JOURNAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_MARITAL_STATUS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_MARITAL_STATUS', '015_MST_MARITAL_STATUS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ORDER_MATERIAL_FORM_STATUS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ORDER_MATERIAL_FORM_STATUS', '015_MST_ORDER_MATERIAL_FORM_STATUS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_ORDER_MATERIAL_FORM_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_ORDER_MATERIAL_FORM_TYPE', '015_MST_ORDER_MATERIAL_FORM_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_PAYMENT_TERM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_PAYMENT_TERM', '015_MST_PAYMENT_TERM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_PRICE_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_PRICE_TYPE', '015_MST_PRICE_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_PROJECT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_PROJECT', '015_MST_PROJECT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_PROJECT_PARENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_PROJECT_PARENT', '015_MST_PROJECT_PARENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_PROVINCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_PROVINCE', '015_MST_PROVINCE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_PURCHASE_DESTINATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_PURCHASE_DESTINATION', '015_MST_PURCHASE_DESTINATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_RACK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_RACK', '015_MST_RACK');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_RELIGION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_RELIGION', '015_MST_RELIGION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_SALES_PERSON', '015_MST_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_SALES_PRICE_LIST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_SALES_PRICE_LIST', '015_MST_SALES_PRICE_LIST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_SALES_PRICE_LIST_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_SALES_PRICE_LIST_APPROVAL', '015_MST_SALES_PRICE_LIST_APPROVAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_UNIT_OF_MEASURE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_UNIT_OF_MEASURE', '015_MST_UNIT_OF_MEASURE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_VENDOR', '015_MST_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_VENDOR_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_VENDOR_CATEGORY', '015_MST_VENDOR_CATEGORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_VENDOR_DEPOSIT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_VENDOR_DEPOSIT_TYPE', '015_MST_VENDOR_DEPOSIT_TYPE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_VENDOR_JN_CONTACT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_VENDOR_JN_CONTACT', '015_MST_VENDOR_JN_CONTACT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_015_MST_WAREHOUSE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_015_MST_WAREHOUSE', '015_MST_WAREHOUSE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR', '017_SCR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_BARCODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_BARCODE', '017_SCR_BARCODE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_BRANCH', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_BRANCH', '017_SCR_BRANCH');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_DATA_PROTECT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_DATA_PROTECT', '017_SCR_DATA_PROTECT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_DATA_PROTECTION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_DATA_PROTECTION', '017_SCR_DATA_PROTECTION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_DIVISION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_DIVISION', '017_SCR_DIVISION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_ROLE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_ROLE', '017_SCR_ROLE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_ROLE_AUTHORIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_ROLE_AUTHORIZATION', '017_SCR_ROLE_AUTHORIZATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_TRANSACTION_LOG', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_TRANSACTION_LOG', '017_SCR_TRANSACTION_LOG');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_017_SCR_USER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_017_SCR_USER', '017_SCR_USER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AP_AGING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AP_AGING', '020_AP_AGING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AP_AGING_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AP_AGING_PER_VENDOR', '020_AP_AGING_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AP_BALANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AP_BALANCE', '020_AP_BALANCE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AP_BALANCE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AP_BALANCE_PER_VENDOR', '020_AP_BALANCE_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AP_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AP_PAYMENT', '020_AP_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AP_PAYMENT_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AP_PAYMENT_PER_VENDOR', '020_AP_PAYMENT_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_AGING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_AGING', '020_AR_AGING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_AGING_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_AGING_PER_CUSTOMER', '020_AR_AGING_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_AGING_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_AGING_PER_SALES_PERSON', '020_AR_AGING_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_BALANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_BALANCE', '020_AR_BALANCE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_BALANCE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_BALANCE_PER_CUSTOMER', '020_AR_BALANCE_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_BALANCE_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_BALANCE_PER_SALES_PERSON', '020_AR_BALANCE_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_RECEIVED', '020_AR_RECEIVED');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_RECEIVED_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_RECEIVED_PER_CUSTOMER', '020_AR_RECEIVED_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_AR_RECEIVED_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_AR_RECEIVED_PER_SALES_PERSON', '020_AR_RECEIVED_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_ASSEMBLY_JOB_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_ASSEMBLY_JOB_ORDER', '020_ASSEMBLY_JOB_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_ASSEMBLY_REALIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_ASSEMBLY_REALIZATION', '020_ASSEMBLY_REALIZATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_ASSEMBLY_REALIZATION_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_ASSEMBLY_REALIZATION_PER_ITEM', '020_ASSEMBLY_REALIZATION_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BALANCE_SHEET', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BALANCE_SHEET', '020_BALANCE_SHEET');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_MUTATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_MUTATION', '020_BANK_MUTATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_PAYMENT', '020_BANK_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_PAYMENT_PER_BANK_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_PAYMENT_PER_BANK_ACCOUNT', '020_BANK_PAYMENT_PER_BANK_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT', '020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE', '020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE', '020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_RECEIVING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_RECEIVING', '020_BANK_RECEIVING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_RECEIVING_PER_BANK_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_RECEIVING_PER_BANK_ACCOUNT', '020_BANK_RECEIVING_PER_BANK_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT', '020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_MUTATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_MUTATION', '020_CASH_MUTATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_PAYMENT', '020_CASH_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_PAYMENT_PER_CASH_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_PAYMENT_PER_CASH_ACCOUNT', '020_CASH_PAYMENT_PER_CASH_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT', '020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE', '020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE', '020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_RECEIVING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_RECEIVING', '020_CASH_RECEIVING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_RECEIVING_PER_CASH_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_RECEIVING_PER_CASH_ACCOUNT', '020_CASH_RECEIVING_PER_CASH_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT', '020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_CREDIT_NOTE', '020_CUSTOMER_CREDIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER', '020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_DEBIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_DEBIT_NOTE', '020_CUSTOMER_DEBIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER', '020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_DEPOSIT', '020_CUSTOMER_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_DEPOSIT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_DEPOSIT_OUTSTANDING', '020_CUSTOMER_DEPOSIT_OUTSTANDING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING', '020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD', '020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_DELIVERY_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_DELIVERY_NOTE', '020_DELIVERY_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_DELIVERY_NOTE_BY_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_DELIVERY_NOTE_BY_ITEM', '020_DELIVERY_NOTE_BY_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_DELIVERY_NOTE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_DELIVERY_NOTE_PER_CUSTOMER', '020_DELIVERY_NOTE_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_DETAIL_STOCK_BY_INVENTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_DETAIL_STOCK_BY_INVENTORY', '020_DETAIL_STOCK_BY_INVENTORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_DETAIL_STOCK_BY_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_DETAIL_STOCK_BY_QUANTITY', '020_DETAIL_STOCK_BY_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_GENERAL_LEDGER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_GENERAL_LEDGER', '020_GENERAL_LEDGER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_GOODS_RECEIVED_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_GOODS_RECEIVED_NOTE', '020_GOODS_RECEIVED_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_GOODS_RECEIVED_NOTE_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_GOODS_RECEIVED_NOTE_PER_ITEM', '020_GOODS_RECEIVED_NOTE_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_GOODS_RECEIVED_NOTE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_GOODS_RECEIVED_NOTE_PER_VENDOR', '020_GOODS_RECEIVED_NOTE_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_INVENTORY_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_INVENTORY_IN', '020_INVENTORY_IN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_INVENTORY_IN_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_INVENTORY_IN_PER_ITEM', '020_INVENTORY_IN_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_INVENTORY_IN_PER_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_INVENTORY_IN_PER_REASON', '020_INVENTORY_IN_PER_REASON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_INVENTORY_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_INVENTORY_OUT', '020_INVENTORY_OUT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_INVENTORY_OUT_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_INVENTORY_OUT_PER_ITEM', '020_INVENTORY_OUT_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_INVENTORY_OUT_PER_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_INVENTORY_OUT_PER_REASON', '020_INVENTORY_OUT_PER_REASON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_JOURNAL_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_JOURNAL_RECAPITULATION', '020_JOURNAL_RECAPITULATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_BANK_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_BANK_PAYMENT_DEPOSIT', '020_OUTSTANDING_BANK_PAYMENT_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_BANK_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_BANK_RECEIVED_DEPOSIT', '020_OUTSTANDING_BANK_RECEIVED_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_CASH_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_CASH_PAYMENT_DEPOSIT', '020_OUTSTANDING_CASH_PAYMENT_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_CASH_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_CASH_RECEIVED_DEPOSIT', '020_OUTSTANDING_CASH_RECEIVED_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY', '020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY', '020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_SALES_ORDER', '020_OUTSTANDING_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER', '020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_SALES_ORDER_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_SALES_ORDER_PER_ITEM', '020_OUTSTANDING_SALES_ORDER_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON', '020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PROFIT_AND_LOSS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PROFIT_AND_LOSS', '020_PROFIT_AND_LOSS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_ORDER', '020_PURCHASE_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_ORDER_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_ORDER_PER_ITEM', '020_PURCHASE_ORDER_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_ORDER_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_ORDER_PER_VENDOR', '020_PURCHASE_ORDER_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY', '020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY', '020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_RECAPITULATION_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_RECAPITULATION_PER_ITEM', '020_PURCHASE_RECAPITULATION_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_RECAPITULATION_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_RECAPITULATION_PER_VENDOR', '020_PURCHASE_RECAPITULATION_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_REQUEST', '020_PURCHASE_REQUEST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_REQUEST_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_REQUEST_OUTSTANDING', '020_PURCHASE_REQUEST_OUTSTANDING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_RETURN', '020_PURCHASE_RETURN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_RETURN_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_RETURN_PER_ITEM', '020_PURCHASE_RETURN_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_PURCHASE_RETURN_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_PURCHASE_RETURN_PER_VENDOR', '020_PURCHASE_RETURN_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_REPORT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_REPORT', '020_REPORT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_RPT_ACC', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_RPT_ACC', '020_RPT_ACC');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_RPT_FIN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_RPT_FIN', '020_RPT_FIN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_RPT_PUR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_RPT_PUR', '020_RPT_PUR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_RPT_SAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_RPT_SAL', '020_RPT_SAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_RPT_WMS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_RPT_WMS', '020_RPT_WMS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ABC_ANALYSIS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ABC_ANALYSIS', '020_SALES_ABC_ANALYSIS');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ABC_ANALYSIS_PER_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ABC_ANALYSIS_PER_CATEGORY', '020_SALES_ABC_ANALYSIS_PER_CATEGORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY', '020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_BY_COGS_IDR', '020_SALES_BY_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_INVOICE_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_INVOICE_RECAPITULATION', '020_SALES_INVOICE_RECAPITULATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_NON_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_NON_COGS_IDR', '020_SALES_NON_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ORDER', '020_SALES_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ORDER_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ORDER_PER_CUSTOMER', '020_SALES_ORDER_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ORDER_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ORDER_PER_ITEM', '020_SALES_ORDER_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ORDER_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ORDER_PER_SALES_PERSON', '020_SALES_ORDER_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_ORDER_REALIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_ORDER_REALIZATION', '020_SALES_ORDER_REALIZATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_PER_CUSTOMER_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_PER_CUSTOMER_BY_COGS_IDR', '020_SALES_PER_CUSTOMER_BY_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_PER_CUSTOMER_NON_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_PER_CUSTOMER_NON_COGS_IDR', '020_SALES_PER_CUSTOMER_NON_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_PER_ITEM_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_PER_ITEM_BY_COGS_IDR', '020_SALES_PER_ITEM_BY_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_PER_SALES_PERSON_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_PER_SALES_PERSON_BY_COGS_IDR', '020_SALES_PER_SALES_PERSON_BY_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_PER_SALES_PERSON_NON_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_PER_SALES_PERSON_NON_COGS_IDR', '020_SALES_PER_SALES_PERSON_NON_COGS_IDR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RECAPITULATION_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RECAPITULATION_PER_CUSTOMER', '020_SALES_RECAPITULATION_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RECAPITULATION_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RECAPITULATION_PER_ITEM', '020_SALES_RECAPITULATION_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RECAPITULATION_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RECAPITULATION_PER_SALES_PERSON', '020_SALES_RECAPITULATION_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RETURN', '020_SALES_RETURN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RETURN_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RETURN_PER_CUSTOMER', '020_SALES_RETURN_PER_CUSTOMER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RETURN_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RETURN_PER_ITEM', '020_SALES_RETURN_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_SALES_RETURN_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_SALES_RETURN_PER_SALES_PERSON', '020_SALES_RETURN_PER_SALES_PERSON');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_STOCK_CARD_COGS_IDR_GLOBAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_STOCK_CARD_COGS_IDR_GLOBAL', '020_STOCK_CARD_COGS_IDR_GLOBAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_STOCK_CARD_MINUS_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_STOCK_CARD_MINUS_QUANTITY', '020_STOCK_CARD_MINUS_QUANTITY');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_STOCK_CARD_QUANTITY_PER_WAREHOUSE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_STOCK_CARD_QUANTITY_PER_WAREHOUSE', '020_STOCK_CARD_QUANTITY_PER_WAREHOUSE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_STOCK_HISTORY_DETAIL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_STOCK_HISTORY_DETAIL', '020_STOCK_HISTORY_DETAIL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_STOCK_HISTORY_GLOBAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_STOCK_HISTORY_GLOBAL', '020_STOCK_HISTORY_GLOBAL');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_TRIAL_BALANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_TRIAL_BALANCE', '020_TRIAL_BALANCE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE', '020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE', '020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT', '020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE', '020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER', '020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST', '020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGNED_BANK_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGNED_BANK_PAYMENT_DEPOSIT', '020_UNASSIGNED_BANK_PAYMENT_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGNED_BANK_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGNED_BANK_RECEIVED_DEPOSIT', '020_UNASSIGNED_BANK_RECEIVED_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGNED_CASH_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGNED_CASH_PAYMENT_DEPOSIT', '020_UNASSIGNED_CASH_PAYMENT_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_UNASSIGNED_CASH_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_UNASSIGNED_CASH_RECEIVED_DEPOSIT', '020_UNASSIGNED_CASH_RECEIVED_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VAT_IN_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VAT_IN_RECAPITULATION', '020_VAT_IN_RECAPITULATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VAT_OUT_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VAT_OUT_RECAPITULATION', '020_VAT_OUT_RECAPITULATION');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_CREDIT_NOTE', '020_VENDOR_CREDIT_NOTE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_CREDIT_NOTE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_CREDIT_NOTE_PER_VENDOR', '020_VENDOR_CREDIT_NOTE_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_DEBIT_NOTE_', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_DEBIT_NOTE_', '020_VENDOR_DEBIT_NOTE_');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_DEBIT_NOTE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_DEBIT_NOTE_PER_VENDOR', '020_VENDOR_DEBIT_NOTE_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_DEPOSIT', '020_VENDOR_DEPOSIT');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_DEPOSIT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_DEPOSIT_OUTSTANDING', '020_VENDOR_DEPOSIT_OUTSTANDING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_DOWN_PAYMENT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_DOWN_PAYMENT_OUTSTANDING', '020_VENDOR_DOWN_PAYMENT_OUTSTANDING');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_DOWN_PAYMENT_PER_PERIOD', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_DOWN_PAYMENT_PER_PERIOD', '020_VENDOR_DOWN_PAYMENT_PER_PERIOD');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_INVOICE', '020_VENDOR_INVOICE');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_INVOICE_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_INVOICE_PER_ITEM', '020_VENDOR_INVOICE_PER_ITEM');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_VENDOR_INVOICE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_VENDOR_INVOICE_PER_VENDOR', '020_VENDOR_INVOICE_PER_VENDOR');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_WAREHOUSE_TRANSFER_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_WAREHOUSE_TRANSFER_IN', '020_WAREHOUSE_TRANSFER_IN');
INSERT INTO `scr_menu_branch` VALUES ('GDN_ADM_020_WAREHOUSE_TRANSFER_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', NULL, 'ADM_020_WAREHOUSE_TRANSFER_OUT', '020_WAREHOUSE_TRANSFER_OUT');
COMMIT;

-- ----------------------------
-- Table structure for scr_role
-- ----------------------------
DROP TABLE IF EXISTS `scr_role`;
CREATE TABLE `scr_role` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `activeStatus` bit(1) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of scr_role
-- ----------------------------
BEGIN;
INSERT INTO `scr_role` VALUES ('GDN_DEV_ADM', 'admin', '2023-01-20 21:59:18.502000', 'admin', '2023-01-21 01:30:27.355000', 'GDN', 'DEV', b'1', 'ADM', 'Development', '-');
COMMIT;

-- ----------------------------
-- Table structure for scr_role_authorization
-- ----------------------------
DROP TABLE IF EXISTS `scr_role_authorization`;
CREATE TABLE `scr_role_authorization` (
  `id` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime(6) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime(6) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `assignAuthority` tinyint(1) DEFAULT NULL,
  `authorizationCode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `deleteAuthority` tinyint(1) DEFAULT NULL,
  `headerCode` varchar(255) DEFAULT NULL,
  `printAuthority` tinyint(1) DEFAULT NULL,
  `saveAuthority` tinyint(1) DEFAULT NULL,
  `updateAuthority` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of scr_role_authorization
-- ----------------------------
BEGIN;
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_000_DASHBOARD', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '000_DASHBOARD', 'ADM_000_DASHBOARD', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR', 'ADM_001_PUR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_HISTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_HISTORY', 'ADM_001_PUR_PURCHASE_HISTORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_ORDER', 'ADM_001_PUR_PURCHASE_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_ORDER_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_ORDER_APPROVAL', 'ADM_001_PUR_PURCHASE_ORDER_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_ORDER_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_ORDER_CLOSING', 'ADM_001_PUR_PURCHASE_ORDER_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_REQUEST', 'ADM_001_PUR_PURCHASE_REQUEST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_REQUEST_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_REQUEST_APPROVAL', 'ADM_001_PUR_PURCHASE_REQUEST_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_REQUEST_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_REQUEST_CLOSING', 'ADM_001_PUR_PURCHASE_REQUEST_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_001_PUR_PURCHASE_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '001_PUR_PURCHASE_RETURN', 'ADM_001_PUR_PURCHASE_RETURN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL', 'ADM_002_SAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_CUSTOMER_INVOICE_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_CUSTOMER_INVOICE_SALES_ORDER', 'ADM_002_SAL_CUSTOMER_INVOICE_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_ORDER_MATERIAL_FORM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_ORDER_MATERIAL_FORM', 'ADM_002_SAL_ORDER_MATERIAL_FORM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_ORDER_MATERIAL_FORM_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_ORDER_MATERIAL_FORM_CLOSING', 'ADM_002_SAL_ORDER_MATERIAL_FORM_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_ORDER_MATERIAL_FORM_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_ORDER_MATERIAL_FORM_RETURN', 'ADM_002_SAL_ORDER_MATERIAL_FORM_RETURN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_PICKING_LIST_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_PICKING_LIST_SALES_ORDER', 'ADM_002_SAL_PICKING_LIST_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION', 'ADM_002_SAL_PICKING_LIST_SALES_ORDER_CONFIRMATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE', 'ADM_002_SAL_PRINT_MULTIPLE_CUSTOMER_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE', 'ADM_002_SAL_PRINT_MULTIPLE_DELIVERY_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_SALES_HISTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_SALES_HISTORY', 'ADM_002_SAL_SALES_HISTORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_SALES_ORDER', 'ADM_002_SAL_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_SALES_ORDER_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_SALES_ORDER_APPROVAL', 'ADM_002_SAL_SALES_ORDER_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_SALES_ORDER_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_SALES_ORDER_CLOSING', 'ADM_002_SAL_SALES_ORDER_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_SALES_RETURN_BY_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_SALES_RETURN_BY_INVOICE', 'ADM_002_SAL_SALES_RETURN_BY_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER', 'ADM_002_SAL_UN_ASSIGN_DELIVERY_NOTE_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER', 'ADM_002_SAL_UN_ASSIGN_PICKING_LIST_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT', 'ADM_003_IVT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_ADJUSTMENT_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_ADJUSTMENT_IN', 'ADM_003_IVT_ADJUSTMENT_IN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_ADJUSTMENT_IN_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_ADJUSTMENT_IN_APPROVAL', 'ADM_003_IVT_ADJUSTMENT_IN_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_ADJUSTMENT_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_ADJUSTMENT_OUT', 'ADM_003_IVT_ADJUSTMENT_OUT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_ADJUSTMENT_OUT_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_ADJUSTMENT_OUT_APPROVAL', 'ADM_003_IVT_ADJUSTMENT_OUT_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_ASSEMBLY_JOB_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_ASSEMBLY_JOB_ORDER', 'ADM_003_IVT_ASSEMBLY_JOB_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_ASSEMBLY_REALIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_ASSEMBLY_REALIZATION', 'ADM_003_IVT_ASSEMBLY_REALIZATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_CURRENT_STOCK_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_CURRENT_STOCK_QUANTITY', 'ADM_003_IVT_CURRENT_STOCK_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_DELIVERY_NOTE_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_DELIVERY_NOTE_SALES_ORDER', 'ADM_003_IVT_DELIVERY_NOTE_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_DOCK_STOCK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_DOCK_STOCK', 'ADM_003_IVT_DOCK_STOCK', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_GOODS_RECEIVED_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_GOODS_RECEIVED_NOTE', 'ADM_003_IVT_GOODS_RECEIVED_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_STOCK_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_STOCK_CLOSING', 'ADM_003_IVT_STOCK_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_STOCK_OPNAME_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_STOCK_OPNAME_APPROVAL', 'ADM_003_IVT_STOCK_OPNAME_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_STOCK_OPNAME_DRAFT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_STOCK_OPNAME_DRAFT', 'ADM_003_IVT_STOCK_OPNAME_DRAFT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_STOCK_OPNAME_POSTING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_STOCK_OPNAME_POSTING', 'ADM_003_IVT_STOCK_OPNAME_POSTING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE', 'ADM_003_IVT_UN_ASSIGN_PICKING_LIST_DELIVERY_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_WAREHOUSE_TRANSFER_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_WAREHOUSE_TRANSFER_IN', 'ADM_003_IVT_WAREHOUSE_TRANSFER_IN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_003_IVT_WAREHOUSE_TRANSFER_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '003_IVT_WAREHOUSE_TRANSFER_OUT', 'ADM_003_IVT_WAREHOUSE_TRANSFER_OUT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN', 'ADM_004_FIN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_AP', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_AP', 'ADM_004_FIN_AP', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_AR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_AR', 'ADM_004_FIN_AR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_BANK_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_BANK_PAYMENT', 'ADM_004_FIN_BANK_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_BANK_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_BANK_RECEIVED', 'ADM_004_FIN_BANK_RECEIVED', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_CASH_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_CASH_PAYMENT', 'ADM_004_FIN_CASH_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_CASH_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_CASH_RECEIVED', 'ADM_004_FIN_CASH_RECEIVED', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_CUSTOMER_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_CUSTOMER_CREDIT_NOTE', 'ADM_004_FIN_CUSTOMER_CREDIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_CUSTOMER_DEBIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_CUSTOMER_DEBIT_NOTE', 'ADM_004_FIN_CUSTOMER_DEBIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT', 'ADM_004_FIN_CUSTOMER_DEPOSIT_ASSIGNMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_CUSTOMER_DOWN_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_CUSTOMER_DOWN_PAYMENT', 'ADM_004_FIN_CUSTOMER_DOWN_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_DLN_CHECKLIST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_DLN_CHECKLIST', 'ADM_004_FIN_DLN_CHECKLIST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_FINANCE_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_FINANCE_CLOSING', 'ADM_004_FIN_FINANCE_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_FINANCE_RECALCULATING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_FINANCE_RECALCULATING', 'ADM_004_FIN_FINANCE_RECALCULATING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GENERAL_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GENERAL_JOURNAL', 'ADM_004_FIN_GENERAL_JOURNAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_HAND_OVER_TO_FINANCE', 'ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL', 'ADM_004_FIN_GIRO_HAND_OVER_TO_FINANCE_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_PAYMENT', 'ADM_004_FIN_GIRO_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_PAYMENT_INQUIRY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_PAYMENT_INQUIRY', 'ADM_004_FIN_GIRO_PAYMENT_INQUIRY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_PAYMENT_REJECTED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_PAYMENT_REJECTED', 'ADM_004_FIN_GIRO_PAYMENT_REJECTED', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_RECEIVED', 'ADM_004_FIN_GIRO_RECEIVED', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_RECEIVED_INQUIRY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_RECEIVED_INQUIRY', 'ADM_004_FIN_GIRO_RECEIVED_INQUIRY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GIRO_RECEIVED_REJECT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GIRO_RECEIVED_REJECT', 'ADM_004_FIN_GIRO_RECEIVED_REJECT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_GOODS_RECEIVED_NOTE_VIEW', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_GOODS_RECEIVED_NOTE_VIEW', 'ADM_004_FIN_GOODS_RECEIVED_NOTE_VIEW', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER', 'ADM_004_FIN_INVOICE_HAND_OVER_TO_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_INVOICE_HAND_OVER_TO_FINANCE', 'ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL', 'ADM_004_FIN_INVOICE_HAND_OVER_TO_FINANCE_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_OTHER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_OTHER', 'ADM_004_FIN_OTHER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_PAYMENT_HISTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_PAYMENT_HISTORY', 'ADM_004_FIN_PAYMENT_HISTORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_PAYMENT_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_PAYMENT_REQUEST', 'ADM_004_FIN_PAYMENT_REQUEST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_PAYMENT_REQUEST_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_PAYMENT_REQUEST_APPROVAL', 'ADM_004_FIN_PAYMENT_REQUEST_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION', 'ADM_004_FIN_UPDATE_CUSTOMER_INVOICE_SALES_ORDER_INFORMATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_VENDOR_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_VENDOR_CREDIT_NOTE', 'ADM_004_FIN_VENDOR_CREDIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_VENDOR_DEBIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_VENDOR_DEBIT_NOTE', 'ADM_004_FIN_VENDOR_DEBIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_VENDOR_DEPOSIT_ASSIGNMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_VENDOR_DEPOSIT_ASSIGNMENT', 'ADM_004_FIN_VENDOR_DEPOSIT_ASSIGNMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_VENDOR_DOWN_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_VENDOR_DOWN_PAYMENT', 'ADM_004_FIN_VENDOR_DOWN_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_VENDOR_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_VENDOR_INVOICE', 'ADM_004_FIN_VENDOR_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION', 'ADM_004_FIN_VENDOR_INVOICE_UPDATE_INFORMATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC', 'ADM_005_ACC', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_ACCOUNTING_CLOSING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_ACCOUNTING_CLOSING', 'ADM_005_ACC_ACCOUNTING_CLOSING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_CURRENT_STOCK_COGSIDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_CURRENT_STOCK_COGSIDR', 'ADM_005_ACC_CURRENT_STOCK_COGSIDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_E-FAK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_E-FAK', 'ADM_005_ACC_E-FAK', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_JOURNAL_POSTING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_JOURNAL_POSTING', 'ADM_005_ACC_JOURNAL_POSTING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_JOURNAL_QUERY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_JOURNAL_QUERY', 'ADM_005_ACC_JOURNAL_QUERY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_KELUARAN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_KELUARAN', 'ADM_005_ACC_KELUARAN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_MASUKAN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_MASUKAN', 'ADM_005_ACC_MASUKAN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_TAX_INVOICE_ASSIGNMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_TAX_INVOICE_ASSIGNMENT', 'ADM_005_ACC_TAX_INVOICE_ASSIGNMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_TAX_INVOICE_REGISTRATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_TAX_INVOICE_REGISTRATION', 'ADM_005_ACC_TAX_INVOICE_REGISTRATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_TRANS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_TRANS', 'ADM_005_ACC_TRANS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_UN_BALANCE_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_UN_BALANCE_JOURNAL', 'ADM_005_ACC_UN_BALANCE_JOURNAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_UN_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_UN_JOURNAL', 'ADM_005_ACC_UN_JOURNAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_UNBALANCE_COGS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_UNBALANCE_COGS', 'ADM_005_ACC_UNBALANCE_COGS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_UPDATE_TAX_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_UPDATE_TAX_INVOICE', 'ADM_005_ACC_UPDATE_TAX_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_VAT_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_VAT_IN', 'ADM_005_ACC_VAT_IN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_VAT_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_VAT_OUT', 'ADM_005_ACC_VAT_OUT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_005_ACC_VOID_TAX_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '005_ACC_VOID_TAX_INVOICE', 'ADM_005_ACC_VOID_TAX_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST', 'ADM_015_MST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_APPROVAL_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_APPROVAL_REASON', 'ADM_015_MST_APPROVAL_REASON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_BANK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_BANK', 'ADM_015_MST_BANK', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_BANK_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_BANK_ACCOUNT', 'ADM_015_MST_BANK_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_BILL_OF_MATERIAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_BILL_OF_MATERIAL', 'ADM_015_MST_BILL_OF_MATERIAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_BRANCH', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_BRANCH', 'ADM_015_MST_BRANCH', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_BUSINESS_ENTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_BUSINESS_ENTITY', 'ADM_015_MST_BUSINESS_ENTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CASH_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CASH_ACCOUNT', 'ADM_015_MST_CASH_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CHART_OF_ACCOUNT', 'ADM_015_MST_CHART_OF_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CITY', 'ADM_015_MST_CITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_COLOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_COLOR', 'ADM_015_MST_COLOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_COUNTRY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_COUNTRY', 'ADM_015_MST_COUNTRY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CURRENCY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CURRENCY', 'ADM_015_MST_CURRENCY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CURRENT_PRICE_LIST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CURRENT_PRICE_LIST', 'ADM_015_MST_CURRENT_PRICE_LIST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CUSTOMER', 'ADM_015_MST_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CUSTOMER_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CUSTOMER_CATEGORY', 'ADM_015_MST_CUSTOMER_CATEGORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CUSTOMER_DEPOSIT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CUSTOMER_DEPOSIT_TYPE', 'ADM_015_MST_CUSTOMER_DEPOSIT_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CUSTOMER_JN_ADDRESS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CUSTOMER_JN_ADDRESS', 'ADM_015_MST_CUSTOMER_JN_ADDRESS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_CUSTOMER_JN_CONTACT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_CUSTOMER_JN_CONTACT', 'ADM_015_MST_CUSTOMER_JN_CONTACT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_DEPARTMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_DEPARTMENT', 'ADM_015_MST_DEPARTMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_DISCOUNT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_DISCOUNT_TYPE', 'ADM_015_MST_DISCOUNT_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_DISTRIBUTION_CHANNEL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_DISTRIBUTION_CHANNEL', 'ADM_015_MST_DISTRIBUTION_CHANNEL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_DIVISION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_DIVISION', 'ADM_015_MST_DIVISION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_DOCUMENT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_DOCUMENT_TYPE', 'ADM_015_MST_DOCUMENT_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_EDUCATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_EDUCATION', 'ADM_015_MST_EDUCATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_EMPLOYEE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_EMPLOYEE', 'ADM_015_MST_EMPLOYEE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_EMPLOYEE_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_EMPLOYEE_TYPE', 'ADM_015_MST_EMPLOYEE_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_EXCHANGE_RATE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_EXCHANGE_RATE', 'ADM_015_MST_EXCHANGE_RATE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_INVENTORY_ADJ_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_INVENTORY_ADJ_REASON', 'ADM_015_MST_INVENTORY_ADJ_REASON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ISLAND', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ISLAND', 'ADM_015_MST_ISLAND', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM', 'ADM_015_MST_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM_BRAND', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM_BRAND', 'ADM_015_MST_ITEM_BRAND', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM_CATEGORY', 'ADM_015_MST_ITEM_CATEGORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM_CLASSIFICATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM_CLASSIFICATION', 'ADM_015_MST_ITEM_CLASSIFICATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM_DIVISION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM_DIVISION', 'ADM_015_MST_ITEM_DIVISION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM_JN_COGSIDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM_JN_COGSIDR', 'ADM_015_MST_ITEM_JN_COGSIDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ITEM_SUB_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ITEM_SUB_CATEGORY', 'ADM_015_MST_ITEM_SUB_CATEGORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_JOB_POSITION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_JOB_POSITION', 'ADM_015_MST_JOB_POSITION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_JOURNAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_JOURNAL', 'ADM_015_MST_JOURNAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_MARITAL_STATUS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_MARITAL_STATUS', 'ADM_015_MST_MARITAL_STATUS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ORDER_MATERIAL_FORM_STATUS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ORDER_MATERIAL_FORM_STATUS', 'ADM_015_MST_ORDER_MATERIAL_FORM_STATUS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_ORDER_MATERIAL_FORM_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_ORDER_MATERIAL_FORM_TYPE', 'ADM_015_MST_ORDER_MATERIAL_FORM_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_PAYMENT_TERM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_PAYMENT_TERM', 'ADM_015_MST_PAYMENT_TERM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_PRICE_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_PRICE_TYPE', 'ADM_015_MST_PRICE_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_PROJECT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_PROJECT', 'ADM_015_MST_PROJECT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_PROJECT_PARENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_PROJECT_PARENT', 'ADM_015_MST_PROJECT_PARENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_PROVINCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_PROVINCE', 'ADM_015_MST_PROVINCE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_PURCHASE_DESTINATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_PURCHASE_DESTINATION', 'ADM_015_MST_PURCHASE_DESTINATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_RACK', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_RACK', 'ADM_015_MST_RACK', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_RELIGION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_RELIGION', 'ADM_015_MST_RELIGION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_SALES_PERSON', 'ADM_015_MST_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_SALES_PRICE_LIST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_SALES_PRICE_LIST', 'ADM_015_MST_SALES_PRICE_LIST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_SALES_PRICE_LIST_APPROVAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_SALES_PRICE_LIST_APPROVAL', 'ADM_015_MST_SALES_PRICE_LIST_APPROVAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_UNIT_OF_MEASURE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_UNIT_OF_MEASURE', 'ADM_015_MST_UNIT_OF_MEASURE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_VENDOR', 'ADM_015_MST_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_VENDOR_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_VENDOR_CATEGORY', 'ADM_015_MST_VENDOR_CATEGORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_VENDOR_DEPOSIT_TYPE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_VENDOR_DEPOSIT_TYPE', 'ADM_015_MST_VENDOR_DEPOSIT_TYPE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_VENDOR_JN_CONTACT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_VENDOR_JN_CONTACT', 'ADM_015_MST_VENDOR_JN_CONTACT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_015_MST_WAREHOUSE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '015_MST_WAREHOUSE', 'ADM_015_MST_WAREHOUSE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR', 'ADM_017_SCR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_BARCODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_BARCODE', 'ADM_017_SCR_BARCODE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_BRANCH', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_BRANCH', 'ADM_017_SCR_BRANCH', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_DATA_PROTECT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_DATA_PROTECT', 'ADM_017_SCR_DATA_PROTECT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_DATA_PROTECTION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_DATA_PROTECTION', 'ADM_017_SCR_DATA_PROTECTION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_DIVISION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_DIVISION', 'ADM_017_SCR_DIVISION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_ROLE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_ROLE', 'ADM_017_SCR_ROLE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_ROLE_AUTHORIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_ROLE_AUTHORIZATION', 'ADM_017_SCR_ROLE_AUTHORIZATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_TRANSACTION_LOG', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_TRANSACTION_LOG', 'ADM_017_SCR_TRANSACTION_LOG', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_017_SCR_USER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '017_SCR_USER', 'ADM_017_SCR_USER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AP_AGING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AP_AGING', 'ADM_020_AP_AGING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AP_AGING_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AP_AGING_PER_VENDOR', 'ADM_020_AP_AGING_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AP_BALANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AP_BALANCE', 'ADM_020_AP_BALANCE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AP_BALANCE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AP_BALANCE_PER_VENDOR', 'ADM_020_AP_BALANCE_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AP_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AP_PAYMENT', 'ADM_020_AP_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AP_PAYMENT_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AP_PAYMENT_PER_VENDOR', 'ADM_020_AP_PAYMENT_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_AGING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_AGING', 'ADM_020_AR_AGING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_AGING_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_AGING_PER_CUSTOMER', 'ADM_020_AR_AGING_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_AGING_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_AGING_PER_SALES_PERSON', 'ADM_020_AR_AGING_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_BALANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_BALANCE', 'ADM_020_AR_BALANCE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_BALANCE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_BALANCE_PER_CUSTOMER', 'ADM_020_AR_BALANCE_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_BALANCE_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_BALANCE_PER_SALES_PERSON', 'ADM_020_AR_BALANCE_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_RECEIVED', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_RECEIVED', 'ADM_020_AR_RECEIVED', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_RECEIVED_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_RECEIVED_PER_CUSTOMER', 'ADM_020_AR_RECEIVED_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_AR_RECEIVED_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_AR_RECEIVED_PER_SALES_PERSON', 'ADM_020_AR_RECEIVED_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_ASSEMBLY_JOB_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_ASSEMBLY_JOB_ORDER', 'ADM_020_ASSEMBLY_JOB_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_ASSEMBLY_REALIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_ASSEMBLY_REALIZATION', 'ADM_020_ASSEMBLY_REALIZATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_ASSEMBLY_REALIZATION_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_ASSEMBLY_REALIZATION_PER_ITEM', 'ADM_020_ASSEMBLY_REALIZATION_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BALANCE_SHEET', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BALANCE_SHEET', 'ADM_020_BALANCE_SHEET', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_MUTATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_MUTATION', 'ADM_020_BANK_MUTATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_PAYMENT', 'ADM_020_BANK_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_PAYMENT_PER_BANK_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_PAYMENT_PER_BANK_ACCOUNT', 'ADM_020_BANK_PAYMENT_PER_BANK_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT', 'ADM_020_BANK_PAYMENT_PER_CHART_OF_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE', 'ADM_020_BANK_PAYMENT_RECAPITULATION_PER_PERIODE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE', 'ADM_020_BANK_RECEIVED_RECAPITULATION_PER_PERIODE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_RECEIVING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_RECEIVING', 'ADM_020_BANK_RECEIVING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_RECEIVING_PER_BANK_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_RECEIVING_PER_BANK_ACCOUNT', 'ADM_020_BANK_RECEIVING_PER_BANK_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT', 'ADM_020_BANK_RECEIVING_PER_CHART_OF_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_MUTATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_MUTATION', 'ADM_020_CASH_MUTATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_PAYMENT', 'ADM_020_CASH_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_PAYMENT_PER_CASH_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_PAYMENT_PER_CASH_ACCOUNT', 'ADM_020_CASH_PAYMENT_PER_CASH_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT', 'ADM_020_CASH_PAYMENT_PER_CHART_OF_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE', 'ADM_020_CASH_PAYMENT_RECAPITULATION_PER_PERIODE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE', 'ADM_020_CASH_RECEIVED_RECAPITULATION_PER_PERIODE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_RECEIVING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_RECEIVING', 'ADM_020_CASH_RECEIVING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_RECEIVING_PER_CASH_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_RECEIVING_PER_CASH_ACCOUNT', 'ADM_020_CASH_RECEIVING_PER_CASH_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT', 'ADM_020_CASH_RECEIVING_PER_CHART_OF_ACCOUNT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_CREDIT_NOTE', 'ADM_020_CUSTOMER_CREDIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER', 'ADM_020_CUSTOMER_CREDIT_NOTE_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_DEBIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_DEBIT_NOTE', 'ADM_020_CUSTOMER_DEBIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER', 'ADM_020_CUSTOMER_DEBIT_NOTE_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_DEPOSIT', 'ADM_020_CUSTOMER_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_DEPOSIT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_DEPOSIT_OUTSTANDING', 'ADM_020_CUSTOMER_DEPOSIT_OUTSTANDING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING', 'ADM_020_CUSTOMER_DOWN_PAYMENT_OUTSTANDING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD', 'ADM_020_CUSTOMER_DOWN_PAYMENT_PER_PERIOD', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_DELIVERY_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_DELIVERY_NOTE', 'ADM_020_DELIVERY_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_DELIVERY_NOTE_BY_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_DELIVERY_NOTE_BY_ITEM', 'ADM_020_DELIVERY_NOTE_BY_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_DELIVERY_NOTE_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_DELIVERY_NOTE_PER_CUSTOMER', 'ADM_020_DELIVERY_NOTE_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_DETAIL_STOCK_BY_INVENTORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_DETAIL_STOCK_BY_INVENTORY', 'ADM_020_DETAIL_STOCK_BY_INVENTORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_DETAIL_STOCK_BY_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_DETAIL_STOCK_BY_QUANTITY', 'ADM_020_DETAIL_STOCK_BY_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_GENERAL_LEDGER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_GENERAL_LEDGER', 'ADM_020_GENERAL_LEDGER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_GOODS_RECEIVED_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_GOODS_RECEIVED_NOTE', 'ADM_020_GOODS_RECEIVED_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_GOODS_RECEIVED_NOTE_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_GOODS_RECEIVED_NOTE_PER_ITEM', 'ADM_020_GOODS_RECEIVED_NOTE_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_GOODS_RECEIVED_NOTE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_GOODS_RECEIVED_NOTE_PER_VENDOR', 'ADM_020_GOODS_RECEIVED_NOTE_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_INVENTORY_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_INVENTORY_IN', 'ADM_020_INVENTORY_IN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_INVENTORY_IN_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_INVENTORY_IN_PER_ITEM', 'ADM_020_INVENTORY_IN_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_INVENTORY_IN_PER_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_INVENTORY_IN_PER_REASON', 'ADM_020_INVENTORY_IN_PER_REASON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_INVENTORY_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_INVENTORY_OUT', 'ADM_020_INVENTORY_OUT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_INVENTORY_OUT_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_INVENTORY_OUT_PER_ITEM', 'ADM_020_INVENTORY_OUT_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_INVENTORY_OUT_PER_REASON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_INVENTORY_OUT_PER_REASON', 'ADM_020_INVENTORY_OUT_PER_REASON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_JOURNAL_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_JOURNAL_RECAPITULATION', 'ADM_020_JOURNAL_RECAPITULATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_BANK_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_BANK_PAYMENT_DEPOSIT', 'ADM_020_OUTSTANDING_BANK_PAYMENT_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_BANK_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_BANK_RECEIVED_DEPOSIT', 'ADM_020_OUTSTANDING_BANK_RECEIVED_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_CASH_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_CASH_PAYMENT_DEPOSIT', 'ADM_020_OUTSTANDING_CASH_PAYMENT_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_CASH_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_CASH_RECEIVED_DEPOSIT', 'ADM_020_OUTSTANDING_CASH_RECEIVED_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY', 'ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY', 'ADM_020_OUTSTANDING_PURCHASE_ORDER_BY_VALUE_AND_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_SALES_ORDER', 'ADM_020_OUTSTANDING_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER', 'ADM_020_OUTSTANDING_SALES_ORDER_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_SALES_ORDER_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_SALES_ORDER_PER_ITEM', 'ADM_020_OUTSTANDING_SALES_ORDER_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON', 'ADM_020_OUTSTANDING_SALES_ORDER_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PROFIT_AND_LOSS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PROFIT_AND_LOSS', 'ADM_020_PROFIT_AND_LOSS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_ORDER', 'ADM_020_PURCHASE_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_ORDER_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_ORDER_PER_ITEM', 'ADM_020_PURCHASE_ORDER_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_ORDER_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_ORDER_PER_VENDOR', 'ADM_020_PURCHASE_ORDER_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY', 'ADM_020_PURCHASE_ORDER_REALIZATION_BY_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY', 'ADM_020_PURCHASE_ORDER_REALIZATION_BY_VALUE_AND_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_RECAPITULATION_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_RECAPITULATION_PER_ITEM', 'ADM_020_PURCHASE_RECAPITULATION_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_RECAPITULATION_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_RECAPITULATION_PER_VENDOR', 'ADM_020_PURCHASE_RECAPITULATION_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_REQUEST', 'ADM_020_PURCHASE_REQUEST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_REQUEST_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_REQUEST_OUTSTANDING', 'ADM_020_PURCHASE_REQUEST_OUTSTANDING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_RETURN', 'ADM_020_PURCHASE_RETURN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_RETURN_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_RETURN_PER_ITEM', 'ADM_020_PURCHASE_RETURN_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_PURCHASE_RETURN_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_PURCHASE_RETURN_PER_VENDOR', 'ADM_020_PURCHASE_RETURN_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_REPORT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_REPORT', 'ADM_020_REPORT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_RPT_ACC', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_RPT_ACC', 'ADM_020_RPT_ACC', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_RPT_FIN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_RPT_FIN', 'ADM_020_RPT_FIN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_RPT_PUR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_RPT_PUR', 'ADM_020_RPT_PUR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_RPT_SAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_RPT_SAL', 'ADM_020_RPT_SAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_RPT_WMS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_RPT_WMS', 'ADM_020_RPT_WMS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ABC_ANALYSIS', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ABC_ANALYSIS', 'ADM_020_SALES_ABC_ANALYSIS', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ABC_ANALYSIS_PER_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ABC_ANALYSIS_PER_CATEGORY', 'ADM_020_SALES_ABC_ANALYSIS_PER_CATEGORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY', 'ADM_020_SALES_ABC_ANALYSIS_PER_SUB_CATEGORY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_BY_COGS_IDR', 'ADM_020_SALES_BY_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_INVOICE_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_INVOICE_RECAPITULATION', 'ADM_020_SALES_INVOICE_RECAPITULATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_NON_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_NON_COGS_IDR', 'ADM_020_SALES_NON_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ORDER', 'ADM_020_SALES_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ORDER_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ORDER_PER_CUSTOMER', 'ADM_020_SALES_ORDER_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ORDER_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ORDER_PER_ITEM', 'ADM_020_SALES_ORDER_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ORDER_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ORDER_PER_SALES_PERSON', 'ADM_020_SALES_ORDER_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_ORDER_REALIZATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_ORDER_REALIZATION', 'ADM_020_SALES_ORDER_REALIZATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_PER_CUSTOMER_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_PER_CUSTOMER_BY_COGS_IDR', 'ADM_020_SALES_PER_CUSTOMER_BY_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_PER_CUSTOMER_NON_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_PER_CUSTOMER_NON_COGS_IDR', 'ADM_020_SALES_PER_CUSTOMER_NON_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_PER_ITEM_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_PER_ITEM_BY_COGS_IDR', 'ADM_020_SALES_PER_ITEM_BY_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_PER_SALES_PERSON_BY_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_PER_SALES_PERSON_BY_COGS_IDR', 'ADM_020_SALES_PER_SALES_PERSON_BY_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_PER_SALES_PERSON_NON_COGS_IDR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_PER_SALES_PERSON_NON_COGS_IDR', 'ADM_020_SALES_PER_SALES_PERSON_NON_COGS_IDR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RECAPITULATION_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RECAPITULATION_PER_CUSTOMER', 'ADM_020_SALES_RECAPITULATION_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RECAPITULATION_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RECAPITULATION_PER_ITEM', 'ADM_020_SALES_RECAPITULATION_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RECAPITULATION_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RECAPITULATION_PER_SALES_PERSON', 'ADM_020_SALES_RECAPITULATION_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RETURN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RETURN', 'ADM_020_SALES_RETURN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RETURN_PER_CUSTOMER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RETURN_PER_CUSTOMER', 'ADM_020_SALES_RETURN_PER_CUSTOMER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RETURN_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RETURN_PER_ITEM', 'ADM_020_SALES_RETURN_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_SALES_RETURN_PER_SALES_PERSON', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_SALES_RETURN_PER_SALES_PERSON', 'ADM_020_SALES_RETURN_PER_SALES_PERSON', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_STOCK_CARD_COGS_IDR_GLOBAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_STOCK_CARD_COGS_IDR_GLOBAL', 'ADM_020_STOCK_CARD_COGS_IDR_GLOBAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_STOCK_CARD_MINUS_QUANTITY', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_STOCK_CARD_MINUS_QUANTITY', 'ADM_020_STOCK_CARD_MINUS_QUANTITY', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_STOCK_CARD_QUANTITY_PER_WAREHOUSE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_STOCK_CARD_QUANTITY_PER_WAREHOUSE', 'ADM_020_STOCK_CARD_QUANTITY_PER_WAREHOUSE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_STOCK_HISTORY_DETAIL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_STOCK_HISTORY_DETAIL', 'ADM_020_STOCK_HISTORY_DETAIL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_STOCK_HISTORY_GLOBAL', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_STOCK_HISTORY_GLOBAL', 'ADM_020_STOCK_HISTORY_GLOBAL', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_TRIAL_BALANCE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_TRIAL_BALANCE', 'ADM_020_TRIAL_BALANCE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE', 'ADM_020_UNASSIGN_DELIVERY_NOTE_BY_CUSTOMER_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE', 'ADM_020_UNASSIGN_GOODS_RECEIVED_NOTE_BY_VENDOR_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT', 'ADM_020_UNASSIGN_PAYMENT_REQUEST_BY_PAYMENT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE', 'ADM_020_UNASSIGN_PURCHASE_ORDER_BY_GOODS_RECEIVED_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER', 'ADM_020_UNASSIGN_PURCHASE_REQUEST_BY_PURCHASE_ORDER', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST', 'ADM_020_UNASSIGN_VENDOR_INVOICE_BY_PAYMENT_REQUEST', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGNED_BANK_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGNED_BANK_PAYMENT_DEPOSIT', 'ADM_020_UNASSIGNED_BANK_PAYMENT_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGNED_BANK_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGNED_BANK_RECEIVED_DEPOSIT', 'ADM_020_UNASSIGNED_BANK_RECEIVED_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGNED_CASH_PAYMENT_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGNED_CASH_PAYMENT_DEPOSIT', 'ADM_020_UNASSIGNED_CASH_PAYMENT_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_UNASSIGNED_CASH_RECEIVED_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_UNASSIGNED_CASH_RECEIVED_DEPOSIT', 'ADM_020_UNASSIGNED_CASH_RECEIVED_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VAT_IN_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VAT_IN_RECAPITULATION', 'ADM_020_VAT_IN_RECAPITULATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VAT_OUT_RECAPITULATION', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VAT_OUT_RECAPITULATION', 'ADM_020_VAT_OUT_RECAPITULATION', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_CREDIT_NOTE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_CREDIT_NOTE', 'ADM_020_VENDOR_CREDIT_NOTE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_CREDIT_NOTE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_CREDIT_NOTE_PER_VENDOR', 'ADM_020_VENDOR_CREDIT_NOTE_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_DEBIT_NOTE_', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_DEBIT_NOTE_', 'ADM_020_VENDOR_DEBIT_NOTE_', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_DEBIT_NOTE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_DEBIT_NOTE_PER_VENDOR', 'ADM_020_VENDOR_DEBIT_NOTE_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_DEPOSIT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_DEPOSIT', 'ADM_020_VENDOR_DEPOSIT', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_DEPOSIT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_DEPOSIT_OUTSTANDING', 'ADM_020_VENDOR_DEPOSIT_OUTSTANDING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_DOWN_PAYMENT_OUTSTANDING', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_DOWN_PAYMENT_OUTSTANDING', 'ADM_020_VENDOR_DOWN_PAYMENT_OUTSTANDING', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_DOWN_PAYMENT_PER_PERIOD', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_DOWN_PAYMENT_PER_PERIOD', 'ADM_020_VENDOR_DOWN_PAYMENT_PER_PERIOD', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_INVOICE', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_INVOICE', 'ADM_020_VENDOR_INVOICE', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_INVOICE_PER_ITEM', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_INVOICE_PER_ITEM', 'ADM_020_VENDOR_INVOICE_PER_ITEM', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_VENDOR_INVOICE_PER_VENDOR', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_VENDOR_INVOICE_PER_VENDOR', 'ADM_020_VENDOR_INVOICE_PER_VENDOR', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_WAREHOUSE_TRANSFER_IN', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_WAREHOUSE_TRANSFER_IN', 'ADM_020_WAREHOUSE_TRANSFER_IN', 0, 'ADM', 0, 0, 0);
INSERT INTO `scr_role_authorization` VALUES ('GDN_DEV_ADM_020_WAREHOUSE_TRANSFER_OUT', NULL, NULL, NULL, NULL, 'GDN', 'ADM', 1, '020_WAREHOUSE_TRANSFER_OUT', 'ADM_020_WAREHOUSE_TRANSFER_OUT', 0, 'ADM', 0, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for scr_user
-- ----------------------------
DROP TABLE IF EXISTS `scr_user`;
CREATE TABLE `scr_user` (
  `id` varchar(255) NOT NULL,
  `activeStatus` tinyint(1) DEFAULT NULL,
  `branchCode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `divisionCode` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roleCode` varchar(255) DEFAULT NULL,
  `superUserStatus` tinyint(1) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of scr_user
-- ----------------------------
BEGIN;
INSERT INTO `scr_user` VALUES ('ADMIN', 1, 'GDN', 'ADMIN', 'ADM-001', NULL, 'Administrator', '$2a$10$AII//6/Dqd5wTMjkyp0x9uUWqWrqT60R.19RTmbHrMfqTzyErU9ea', 'ADM', 0, 'admin');
COMMIT;

-- ----------------------------
-- Table structure for sys_setup
-- ----------------------------
DROP TABLE IF EXISTS `sys_setup`;
CREATE TABLE `sys_setup` (
  `Code` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `CompanyName` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CompanyAcronym` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `LogoPath` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `WebTitle` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CurrencyCode` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `VATPercent` decimal(18,4) DEFAULT NULL,
  `VATDivision` decimal(18,4) DEFAULT NULL,
  `LogoName` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ArAging1` decimal(3,0) DEFAULT NULL,
  `ArAging2` decimal(3,0) DEFAULT NULL,
  `ArAging3` decimal(3,0) DEFAULT NULL,
  `ArAging4` decimal(3,0) DEFAULT NULL,
  `ApAging1` decimal(3,0) DEFAULT NULL,
  `ApAging2` decimal(3,0) DEFAULT NULL,
  `ApAging3` decimal(3,0) DEFAULT NULL,
  `ApAging4` decimal(3,0) DEFAULT NULL,
  `DefaultPriceTypeCode` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `DefaultDepositPaymentTermCode` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CreatedBy` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_setup
-- ----------------------------
BEGIN;
INSERT INTO `sys_setup` VALUES ('GDN', 'Gudang Dunia', 'GDN', NULL, NULL, 'IDR', 11.0000, 1.1100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Procedure structure for sp_branch
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_branch`;
delimiter ;;
CREATE PROCEDURE `sp_branch`()
BEGIN
  SELECT * FROM mst_branch;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
