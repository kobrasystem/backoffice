const brand = {
  marketing: {
    name: 'Herbal Putih Indonesia',
    desc: 'Herbal Putih Indonesia',
    prefix: 'awrora',
    footerText: 'Herbal Putih Indonesia',
    logoText: 'Herbal Putih Indonesia',
    projectName: 'Herbal Putih Indonesia',
    url: 'herbalputihindonesia.com',
    img: '/static/images/hpi.jpg',
    notifMsg:
      'Donec sit amet nulla sed arcu pulvinar ultricies commodo id ligula.',
  },
}

export default brand
