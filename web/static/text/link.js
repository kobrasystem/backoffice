const link = {
  marketing: {
    home: 'http://kobra.appsitemap.com:90/web/',
    about: 'http://kobra.appsitemap.com:90/web/about',
    team: 'http://kobra.appsitemap.com:90/web/about/team',
    blog: 'http://kobra.appsitemap.com:90/web/blog',
    blogDetail: 'http://kobra.appsitemap.com:90/web/blog/detail-blog',
    login: 'http://kobra.appsitemap.com:90/web/login',
    register: 'http://kobra.appsitemap.com:90/web/register',
    check: 'http://kobra.appsitemap.com:90/web/check',
    contact: 'http://kobra.appsitemap.com:90/web/contact',
    contactMap: 'http://kobra.appsitemap.com:90/web/contact/with-map',
    card: 'http://kobra.appsitemap.com:90/web/collection',
    product: 'http://kobra.appsitemap.com:90/web/collection/products',
    productDetail: 'http://kobra.appsitemap.com:90/web/collection/detail-product',
    pricing: 'http://kobra.appsitemap.com:90/web/utils/pricing',
    faq: 'http://kobra.appsitemap.com:90/web/utils/faq',
    maintenance: 'http://kobra.appsitemap.com:90/web/utils/maintenance',
    comingSoon: 'http://kobra.appsitemap.com:90/web/utils/coming-soon'
  }
}

export default link
