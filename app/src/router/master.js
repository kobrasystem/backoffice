const master = [
  //GROUP MASTER
  {
    path: '/master/customer',
    name: 'master customer',
    component: () => import('@/views/master/customer'),
  },
  {
    path: '/master/customer/form',
    name: 'master approval reason form',
    component: () => import('@/views/master/customer/form'),
  },
  {
    path: '/master/bank',
    name: 'master bank',
    component: () => import('@/views/master/bank'),
  },
  {
    path: '/master/bank/form',
    name: 'master bank form',
    component: () => import('@/views/master/bank/form'),
  },
  {
    path: '/master/courier',
    name: 'master courier',
    component: () => import('@/views/master/courier'),
  },
  {
    path: '/master/courier/form',
    name: 'master courier form',
    component: () => import('@/views/master/courier/form'),
  },
];

export default master;
