export const menus = [
  {
    label: "Menus", icon: "pi pi-fw pi-home",
    items: [
      {label: "Dashboard", icon: "pi pi-fw pi-home", to: "/"},
      {
        label: 'Sample', icon: 'pi pi-fw pi-prime',
        items: [
          {label: "Header Detail", icon: "pi pi-fw pi-sign-in", to: "/sample/header-detail"},
        ]
      },
      {
        label: 'Master', icon: 'pi pi-fw pi-microsoft',
        items: [
          {
            label: 'Master A-D', icon: 'pi pi-fw pi-list',
            items: [
              {label: 'Approval Reaso', icon: 'pi pi-fw pi-sign-in', to: '/master/approval-reason'},
              {label: 'Bank', icon: 'pi pi-fw pi-sign-in', to: '/master/bank'},
              {label: 'Bank Account', icon: 'pi pi-fw pi-sign-in', to: '/master/bank-account'},
              {label: 'Bill Of Material', icon: 'pi pi-fw pi-sign-in', to: '/master/bill-of-material'},
              {label: 'Branch', icon: 'pi pi-fw pi-sign-in', to: '/master/branch'},
              {label: 'Business Entity', icon: 'pi pi-fw pi-sign-in', to: '/master/business-entity'},
              {label: 'Cash Account', icon: 'pi pi-fw pi-sign-in', to: '/master/cash-account'},
              {label: 'Chart Of Account', icon: 'pi pi-fw pi-sign-in', to: '/master/chart-of-account'},
              {label: 'City', icon: 'pi pi-fw pi-sign-in', to: '/master/city'},
              {label: 'Color', icon: 'pi pi-fw pi-sign-in', to: '/master/color'},
              {label: 'Country', icon: 'pi pi-fw pi-sign-in', to: '/master/country'},
              {label: 'Currency', icon: 'pi pi-fw pi-sign-in', to: '/master/currency'},
              {label: 'Customer', icon: 'pi pi-fw pi-sign-in', to: '/master/customer'},
              {label: 'Customer Category', icon: 'pi pi-fw pi-sign-in', to: '/master/customer-category'},
              {label: 'Customer Deposit Type', icon: 'pi pi-fw pi-sign-in', to: '/master/customer-deposit-type'},
              {label: 'Customer Deposit Type Jn Chart Of Account', icon: 'pi pi-fw pi-sign-in', to: '/master/customer-deposit-type-jn-chart-of-account'},
              {label: 'Customer Jn Address', icon: 'pi pi-fw pi-sign-in', to: '/master/customer-jn-address'},
              {label: 'Customer Jn Contact', icon: 'pi pi-fw pi-sign-in', to: '/master/customer-jn-contact'},
              {label: 'Department', icon: 'pi pi-fw pi-sign-in', to: '/master/department'},
              {label: 'Discount Type', icon: 'pi pi-fw pi-sign-in', to: '/master/discount-type'},
              {label: 'Distribution Channel', icon: 'pi pi-fw pi-sign-in', to: '/master/distribution-channel'},
              {label: 'Division', icon: 'pi pi-fw pi-sign-in', to: '/master/division'},
              {label: 'Document Type', icon: 'pi pi-fw pi-sign-in', to: '/master/document-type'},
              {label: 'Document Type Chart Of Account', icon: 'pi pi-fw pi-sign-in', to: '/master/document-type-chart-of-account'},

            ]
          },
          {
            label: 'Master E-H', icon: 'pi pi-fw pi-list',
            items: [
              {label: 'Education', icon: 'pi pi-fw pi-sign-in', to: '/master/education'},
              {label: 'Employee', icon: 'pi pi-fw pi-sign-in', to: '/master/employee'},
              {label: 'Employee Type', icon: 'pi pi-fw pi-sign-in', to: '/master/employee-type'},
              {label: 'Exchange Rate', icon: 'pi pi-fw pi-sign-in', to: '/master/exchange-rate'},

            ]
          },
          {
            label: 'Master I-L', icon: 'pi pi-fw pi-list',
            items: [
              {label: 'Inventory Adj Reason', icon: 'pi pi-fw pi-sign-in', to: '/master/inventory-adj-reason'},
              {label: 'Inventory Adj Reason Jn Branch', icon: 'pi pi-fw pi-sign-in', to: '/master/inventory-adj-reason-jn-branch'},
              {label: 'Island', icon: 'pi pi-fw pi-sign-in', to: '/master/island'},
              {label: 'Item', icon: 'pi pi-fw pi-sign-in', to: '/master/item'},
              {label: 'Item Brand', icon: 'pi pi-fw pi-sign-in', to: '/master/item-brand'},
              {label: 'Item Category', icon: 'pi pi-fw pi-sign-in', to: '/master/item-category'},
              {label: 'Item Classification', icon: 'pi pi-fw pi-sign-in', to: '/master/item-classification'},
              {label: 'Item Division', icon: 'pi pi-fw pi-sign-in', to: '/master/item-division'},
              {label: 'Item Jn Catalog Number', icon: 'pi pi-fw pi-sign-in', to: '/master/item-jn-catalog-number'},
              {label: 'Item Jn Cogsidr', icon: 'pi pi-fw pi-sign-in', to: '/master/item-jn-cogsidr'},
              {label: 'Item Jn Current Stock', icon: 'pi pi-fw pi-sign-in', to: '/master/item-jn-current-stock'},
              {label: 'Item Jn Current Stock Daily', icon: 'pi pi-fw pi-sign-in', to: '/master/item-jn-current-stock-daily'},
              {label: 'Item Sub Category', icon: 'pi pi-fw pi-sign-in', to: '/master/item-sub-category'},
              {label: 'Job Position', icon: 'pi pi-fw pi-sign-in', to: '/master/job-position'},
              {label: 'Journal', icon: 'pi pi-fw pi-sign-in', to: '/master/journal'},
              {label: 'Journal Chart Of Account', icon: 'pi pi-fw pi-sign-in', to: '/master/journal-chart-of-account'},
              {label: 'Journal Posting Type', icon: 'pi pi-fw pi-sign-in', to: '/master/journal-posting-type'},
              {label: 'Journal Type', icon: 'pi pi-fw pi-sign-in', to: '/master/journal-type'},
            ]
          },
          {
            label: 'Master M-Q', icon: 'pi pi-fw pi-list',
            items: [
              {label: 'Minus Stock', icon: 'pi pi-fw pi-sign-in', to: '/master/minus-stock'},
              {label: 'Payment Term', icon: 'pi pi-fw pi-sign-in', to: '/master/payment-term'},
              {label: 'Price List', icon: 'pi pi-fw pi-sign-in', to: '/master/price-list'},
              {label: 'Price List Item', icon: 'pi pi-fw pi-sign-in', to: '/master/price-list-item'},
              {label: 'Price Type', icon: 'pi pi-fw pi-sign-in', to: '/master/price-type'},
              {label: 'Project', icon: 'pi pi-fw pi-sign-in', to: '/master/project'},
              {label: 'Province', icon: 'pi pi-fw pi-sign-in', to: '/master/province'},
              {label: 'Purchase Destination', icon: 'pi pi-fw pi-sign-in', to: '/master/purchase-destination'},
            ]
          },
          {
            label: 'Master R-U', icon: 'pi pi-fw pi-list',
            items: [
              {label: 'Rack', icon: 'pi pi-fw pi-sign-in', to: '/master/rack'},
              {label: 'Religion', icon: 'pi pi-fw pi-sign-in', to: '/master/religion'},
              {label: 'Sales Person', icon: 'pi pi-fw pi-sign-in', to: '/master/sales-person'},
              {label: 'Sales Person Jn Distribution Channel', icon: 'pi pi-fw pi-sign-in', to: '/master/sales-person-jn-distribution-channel'},
              {label: 'Search Document', icon: 'pi pi-fw pi-sign-in', to: '/master/search-document'},
              {label: 'Supervisory', icon: 'pi pi-fw pi-sign-in', to: '/master/supervisory'},
              {label: 'Unit Of Measure', icon: 'pi pi-fw pi-sign-in', to: '/master/unit-of-measure'},

            ]
          },
          {
            label: 'Master V-Z', icon: 'pi pi-fw pi-list',
            items: [
              {label: 'Vendor', icon: 'pi pi-fw pi-sign-in', to: '/master/vendor'},
              {label: 'Vendor Category', icon: 'pi pi-fw pi-sign-in', to: '/master/vendor-category'},
              {label: 'Vendor Deposit Type', icon: 'pi pi-fw pi-sign-in', to: '/master/vendor-deposit-type'},
              {label: 'Vendor Deposit Type Jn Chart Of Account', icon: 'pi pi-fw pi-sign-in', to: '/master/vendor-deposit-type-jn-chart-of-account'},
              {label: 'Vendor Jn Contact', icon: 'pi pi-fw pi-sign-in', to: '/master/vendor-jn-contact'},
              {label: 'Warehouse', icon: 'pi pi-fw pi-sign-in', to: '/master/warehouse'},
            ]
          },
        ]
      },
      {
        label: 'Purchase', icon: 'pi pi-fw pi-chart-bar',
        items: [
          {label: 'Purchase Request', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Purchase Order', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Purchase Return', icon: 'pi pi-fw pi-sign-in'},
        ]
      },
      {
        label: 'Sales', icon: 'pi pi-fw pi-chart-line',
        items: [
          {label: 'Booking Order', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Sales Order', icon: 'pi pi-fw pi-sign-in', to: '/sales/sales-order'},
          {label: 'Sales Return', icon: 'pi pi-fw pi-sign-in'},
        ]
      },
      {
        label: 'Inventory', icon: 'pi pi-fw pi-inbox',
        items: [
          {label: 'Adjustment In', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Adjustment Out', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Delivery Note', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Delivery Return', icon: 'pi pi-fw pi-sign-in'},
        ]
      },
      {
        label: 'Finance', icon: 'pi pi-fw pi-briefcase',
        items: [
          {label: 'Bank Payment', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Bank Received', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Cash Payment', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Cash Received', icon: 'pi pi-fw pi-sign-in'},
          {label: 'General Jurnal', icon: 'pi pi-fw pi-sign-in'},
        ]
      },
      {
        label: 'Accounting', icon: 'pi pi-fw pi-credit-card',
        items: [
          {label: 'Sales Period', icon: 'pi pi-fw pi-sign-in'},
          {label: 'Purchase Period', icon: 'pi pi-fw pi-sign-in'},
        ]
      },
    ]
  },
]
