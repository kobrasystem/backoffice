import axios from 'axios';
import { searchInventoryDefault, searchInventoryObjectDefault, lookupInventoryDefault, saveInventoryDefault, dataInventoryDefault, updateInventoryDefault, deleteInventoryDefault, authInventoryPrint } from '@/api/endpoint/inventory/InventoryEndpoint';
import { BaseInventoryUrl, BasePrintInventoryUrl } from '@/api/endpoint/base';
import { store } from '@/store/index.js';

export const searchInventoryObjectRequest = (module, params, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseInventoryUrl()}${searchInventoryObjectDefault(module, params)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const searchInventoryDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseInventoryUrl()}${searchInventoryDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const lookupInventoryDefaultRequest = (module, path, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseInventoryUrl()}${lookupInventoryDefault(module, path, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const dataInventoryDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseInventoryUrl()}${dataInventoryDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const saveInventoryDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseInventoryUrl()}${saveInventoryDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const updateInventoryDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseInventoryUrl()}${updateInventoryDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const deleteInventoryDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BaseInventoryUrl()}${deleteInventoryDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const authInventoryPrintRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BasePrintInventoryUrl()}${authInventoryPrint(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
    data: data,
  }).then((response) => {
    return response;
  });
  return resp;
};
