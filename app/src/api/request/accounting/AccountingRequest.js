import axios from 'axios';
import {
  searchAccountingDefault,
  lookupAccountingDefault,
  processAccountingDefault,
  saveAccountingDefault,
  dataAccountingDefault,
  updateAccountingDefault,
  deleteAccountingDefault,
  authAccountingPrint,
  authAccountingExport,
} from '@/api/endpoint/accounting/AccountingEndpoint';
import { BaseAccountingUrl, BasePrintAccountingUrl } from '@/api/endpoint/base';
import { store } from '@/store/index.js';

export const searchAccountingObjectRequest = (module, params, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseAccountingUrl()}${searchAccountingDefault(module, params)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const searchAccountingDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseAccountingUrl()}${searchAccountingDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const lookupAccountingDefaultRequest = (module, path, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseAccountingUrl()}${lookupAccountingDefault(module, path, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const dataAccountingDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseAccountingUrl()}${dataAccountingDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const processAccountingDefaultRequest = (module, params) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    url: `${BaseAccountingUrl()}${processAccountingDefault(module, params)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const saveAccountingDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseAccountingUrl()}${saveAccountingDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const updateAccountingDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    data: data,
    url: `${BaseAccountingUrl()}${updateAccountingDefault(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const deleteAccountingDefaultRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BaseAccountingUrl()}${deleteAccountingDefault(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};

export const authAccountingPrintRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'POST',
    url: `${BasePrintAccountingUrl()}${authAccountingPrint(module)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
    data: data,
  }).then((response) => {
    return response;
  });
  return resp;
};
export const authAccountingExportRequest = (module, data) => {
  let resp = null;
  resp = axios({
    method: 'GET',
    responseType: 'blob',
    contentType: 'application/octet-stream',
    url: `${BaseAccountingUrl()}${authAccountingExport(module, data)}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`,
    },
  }).then((response) => {
    return response;
  });
  return resp;
};
