export const searchPurchaseDefault = (module, data) => {
  return `/${module}/search?qry=`+JSON.stringify(data).replaceAll("{","%7B").replaceAll("}","%7D").replaceAll("[","%5B").replaceAll("]","%5D")
}
 
export const lookupPurchaseDefault = (module, path, data) => {
  return `/${module}/`+path+'?'+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const dataPurchaseDefault = (module, data) => {
  return `/${module}/data?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}
 
export const savePurchaseDefault = (module) => {
  return `/${module}/save`
}

export const updatePurchaseDefault = (module) => {
  return `/${module}/update`
}

export const deletePurchaseDefault = (module,data) => {
  return `/${module}/delete?`+Object.keys(data).map(key => key + '=' + encodeURIComponent(data[key])).join('&')
}

export const authPurchasePrint = (module) => {
  return `/${module}/request`
}