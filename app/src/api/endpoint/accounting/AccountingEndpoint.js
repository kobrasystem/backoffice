export const searchAccountingDefault = (module, data) => {
  return `/${module}/search?qry=` + JSON.stringify(data).replaceAll('{', '%7B').replaceAll('}', '%7D').replaceAll('[', '%5B').replaceAll(']', '%5D');
};

export const lookupAccountingDefault = (module, path, data) => {
  return (
    `/${module}/` +
    path +
    '?' +
    Object.keys(data)
      .map((key) => key + '=' + encodeURIComponent(data[key]))
      .join('&')
  );
};

export const dataAccountingDefault = (module, data) => {
  return (
    `/${module}/data?` +
    Object.keys(data)
      .map((key) => key + '=' + encodeURIComponent(data[key]))
      .join('&')
  );
};

export const processAccountingDefault = (module, params) => {
  return `/${module}/process?qry=` + JSON.stringify(params).replaceAll('{', '%7B').replaceAll('}', '%7D').replaceAll('[', '%5B').replaceAll(']', '%5D');
};

export const saveAccountingDefault = (module) => {
  return `/${module}/save`;
};

export const updateAccountingDefault = (module) => {
  return `/${module}/update`;
};

export const deleteAccountingDefault = (module, data) => {
  return (
    `/${module}/delete?` +
    Object.keys(data)
      .map((key) => key + '=' + encodeURIComponent(data[key]))
      .join('&')
  );
};

export const authAccountingPrint = (module) => {
  return `/${module}/request`;
};

export const authAccountingExport = (module, data) => {
  return (
    `/${module}/exports` +
    '?' +
    Object.keys(data)
      .map((key) => key + '=' + encodeURIComponent(data[key]))
      .join('&')
  );
};
