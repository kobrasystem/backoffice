import {
  searchSalesDefaultRequest,
  searchDataArrayRequest,
  searchSalesObjectRequest,
  lookupSalesDefaultRequest,
  dataSalesDefaultRequest,
  saveSalesDefaultRequest,
  updateSalesDefaultRequest,
  deleteSalesDefaultRequest,
  authSalesPrintRequest,
} from '@/api/request/sales/SalesRequest';
import { checkErrorResponse } from '@/store';

export class SalesControllers {
  searchSalesObject(module, params, data) {
    let resp = null;
    resp = searchSalesObjectRequest(module, params, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  searchSales(module, data) {
    let resp = null;
    resp = searchSalesDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  searchDataArraySales(module, data) {
    let resp = null;
    resp = searchDataArrayRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  lookupSales(module, data) {
    let resp = null;
    resp = lookupSalesDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  dataSales(module, data) {
    let resp = null;
    resp = dataSalesDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  saveSales(module, data) {
    let resp = null;
    resp = saveSalesDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  updateSales(module, data) {
    let resp = null;
    resp = updateSalesDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  deleteSales(module, data) {
    let resp = null;
    resp = deleteSalesDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  printSales(module, data) {
    let resp = null;
    resp = authSalesPrintRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }
}
