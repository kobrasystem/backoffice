import {
  searchPurchaseDefaultRequest,
  lookupPurchaseDefaultRequest,
  dataPurchaseDefaultRequest,
  savePurchaseDefaultRequest,
  updatePurchaseDefaultRequest,
  deletePurchaseDefaultRequest,
  authPurchasePrintRequest,
} from '@/api/request/purchase/PurchaseRequest';
import { checkErrorResponse } from '@/store';

export class PurchaseControllers {
  searchPurchase(module, data) {
    let resp = null;
    resp = searchPurchaseDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  lookupPurchase(module, data) {
    let resp = null;
    resp = lookupPurchaseDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  dataPurchase(module, data) {
    let resp = null;
    resp = dataPurchaseDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  savePurchase(module, data) {
    let resp = null;
    resp = savePurchaseDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  updatePurchase(module, data) {
    let resp = null;
    resp = updatePurchaseDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  deletePurchase(module, data) {
    let resp = null;
    resp = deletePurchaseDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  printPurchase(module, data) {
    let resp = null;
    resp = authPurchasePrintRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }
}
