import {
  searchAccountingDefaultRequest,
  searchAccountingObjectRequest,
  lookupAccountingDefaultRequest,
  dataAccountingDefaultRequest,
  processAccountingDefaultRequest,
  saveAccountingDefaultRequest,
  updateAccountingDefaultRequest,
  deleteAccountingDefaultRequest,
  authAccountingPrintRequest,
  authAccountingExportRequest,
} from '@/api/request/accounting/AccountingRequest';
import { checkErrorResponse } from '@/store';

export class AccountingControllers {
  searchAccountingObject(module, params, data) {
    let resp = null;
    resp = searchAccountingObjectRequest(module, params, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  searchAccounting(module, data) {
    let resp = null;
    resp = searchAccountingDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  lookupAccounting(module, data) {
    let resp = null;
    resp = lookupAccountingDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  dataAccounting(module, data) {
    let resp = null;
    resp = dataAccountingDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  processAccounting(module, params) {
    let resp = null;
    resp = processAccountingDefaultRequest(module, params)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  saveAccounting(module, data) {
    let resp = null;
    resp = saveAccountingDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  updateAccounting(module, data) {
    let resp = null;
    resp = updateAccountingDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  deleteAccounting(module, data) {
    let resp = null;
    resp = deleteAccountingDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  printAccounting(module, data) {
    let resp = null;
    resp = authAccountingPrintRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  exportAccounting(module, data) {
    let resp = null;
    resp = authAccountingExportRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }
}
