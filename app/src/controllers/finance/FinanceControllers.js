import {
  searchFinanceDefaultRequest,
  searchFinanceObjectRequest,
  lookupFinanceDefaultRequest,
  dataFinanceDefaultRequest,
  processFinanceDefaultRequest,
  saveFinanceDefaultRequest,
  updateFinanceDefaultRequest,
  deleteFinanceDefaultRequest,
  authFinancePrintRequest,
  authFinanceExportRequest,
} from '@/api/request/finance/FinanceRequest';
import { checkErrorResponse } from '@/store';

export class FinanceControllers {
  searchFinanceObject(module, params, data) {
    let resp = null;
    resp = searchFinanceObjectRequest(module, params, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  searchFinance(module, data) {
    let resp = null;
    resp = searchFinanceDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  lookupFinance(module, data) {
    let resp = null;
    resp = lookupFinanceDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  dataFinance(module, data) {
    let resp = null;
    resp = dataFinanceDefaultRequest(module, data)
      .then((response) => {
        return response.data;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  processFinance(module, params) {
    let resp = null;
    resp = processFinanceDefaultRequest(module, params)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  saveFinance(module, data) {
    let resp = null;
    resp = saveFinanceDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  updateFinance(module, data) {
    let resp = null;
    resp = updateFinanceDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  deleteFinance(module, data) {
    let resp = null;
    resp = deleteFinanceDefaultRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  exportFinance(module, data) {
    let resp = null;
    resp = authFinanceExportRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }

  printFinance(module, data) {
    let resp = null;
    resp = authFinancePrintRequest(module, data)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        checkErrorResponse(err);
      })
      .finally(() => {});
    return resp;
  }
}
