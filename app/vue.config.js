const Dotenv = require('dotenv-webpack');

module.exports = {
	publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
	devServer: {
		proxy: 'http://localhost',
		// proxy: 'http://103.56.148.126',
	},
	configureWebpack: {
		plugins: [
			new Dotenv()
		]
	}
}
